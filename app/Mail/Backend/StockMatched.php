<?php

namespace App\Mail\Backend;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendContact.
 */
class StockMatched extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Request
     */
    public $request;

    /**
     * SendContact constructor.
     *
     * @param Request $request
     */
    public function __construct($matches)
    {
        $this->matches = $matches;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->to(config('mail.from.address'), config('mail.from.name'))
            ->view('backend.mail.stock_matched')->with(['matches'=>$this->matches])
            // ->text('frontend.mail.contact-text')
            ->subject('Stocked Matched');
            // ->from($this->request->email, $this->request->name)
            // ->replyTo($this->request->email, $this->request->name);
    }
}
