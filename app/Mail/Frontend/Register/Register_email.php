<?php

namespace App\Mail\Frontend\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Register_email extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $buyer_seller, $email_template)
    {
        $this->request = $data;
        $this->buyer_seller = $buyer_seller;
        $this->email_template = $email_template;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

       $name = $this->request->first_name.' '.$this->request->last_name;
        // $this->to(config('mail.from.address'), config('mail.from.name'))
        //     ->view('frontend.mail.registration_mail',['user'=>$this->request])
        //     ->subject(__('strings.emails.auth.new_user_registered'))
        //     ->from($this->request->email, $name )
        //     ->replyTo($this->request->email, $name );
        // if($this->request->hasRole('seller')){
        //   $email_template = get_email_template('SELLER REGISTER');
        // }elseif($this->request->hasRole('buyer')){
        //  echo ' hasRole ';
        //   if($this->buyer_seller->from_buyerlead==1){
        //     echo ' Buyerlead ';
        //     $email_template = get_email_template('BUYER SHORT REGISTER');    
        //   }else{
        //     $email_template = get_email_template('BUYER REGISTER');
        //   }
        // }  
        $email_template = $this->email_template;
            // echo '+ '.$email_template->title;
       
        if(!empty($email_template)){
           $email_content = '';
          $email_verification_link = '<a href="'.route('frontend.auth.account.confirm',$this->request->confirmation_code).'">'.\Lang::get('email.confirm_email').'</a>';
           $locale = \App::getLocale();
          if($locale == 'pl'){
            $email_content = $email_template->email_content_pl;
          }else{
            $email_content = $email_template->email_content;
          }
          $prefered_method = array();
          if($this->buyer_seller->contact_sms==1){
            $prefered_method[] = "SMS";
          }
          if($this->buyer_seller->contact_email==1){
            $prefered_method[] = "Email";
          }
          if($this->buyer_seller->contact_whatsapp==1){
            $prefered_method[] = "WhatsApp";
          }
         $product_name_column = (($locale == 'pl') ? 'name_pl' : 'name');
         $product_name = \App\Product::where('id',$this->buyer_seller->product_id)->first()[$product_name_column];
         $email_content = str_replace("[product_name]", $product_name, $email_content);
         $email_content = str_replace("[product_sub_type]", $this->buyer_seller->product_sub_type, $email_content);
         $email_content = str_replace("[name]", ucfirst($this->request->name), $email_content);
         $email_content = str_replace("[first_name]", $this->request->first_name, $email_content);
         $email_content = str_replace("[email]", $this->request->email, $email_content);
         $email_content = str_replace("[phone]", (substr(trim($this->request->phone),0,1) != '+' ? '+' : '').$this->request->phone, $email_content);
         $email_content = str_replace("[contact_preferred_method]", implode(', ',$prefered_method), $email_content);
         $email_content = str_replace("[notes]", $this->buyer_seller->note, $email_content);
         $email_content = str_replace("[verification_link]", $email_verification_link, $email_content);
         $email_content = str_replace("[english_phone_number]", \Lang::get('inner-content.frontend.contactsec.phone-2'), $email_content);
        }
        return $this->to($this->request->email, $name )
            ->view('frontend.mail.general',['user'=>$this->request, 'email_content' => @$email_content])
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->replyTo(config('mail.from.address'), config('mail.from.name'));

    }
}
