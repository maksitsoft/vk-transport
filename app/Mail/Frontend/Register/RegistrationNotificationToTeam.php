<?php

namespace App\Mail\Frontend\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationNotificationToTeam extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$team,$buyer_seller, $email_template)
    {
        $this->user = $user;
        $this->team = $team;
        $this->buyer_seller = $buyer_seller;
        $this->email_template = $email_template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $locale = \App::getLocale();
        // $email_content = '';
        // if($this->user->hasRole('seller')){
        //   $email_template = get_email_template('SELLER REGISTER TO TEAM');
        // }elseif($this->user->hasRole('buyer')){
        //   if($this->buyer_seller->from_buyerlead==1){
        //     $email_template = get_email_template('BUYER SHORT REGISTER TO TEAM');
        //   }else{
        //     $email_template = get_email_template('BUYER REGISTER TO TEAM');
        //   }
        // }
        $email_template = $this->email_template;

        if($email_template){
          if($locale == 'pl'){
            $email_content = $email_template->email_content_pl;
          }else{
            $email_content = $email_template->email_content;
          }
          $email_content = str_replace("[team_member_name]", $this->team->name, $email_content);
          if($this->user->hasRole('buyer')){
            $product_name_column = (($locale == 'pl') ? 'name_pl' : 'name');
            $product_name = \App\Product::where('id',$this->buyer_seller->product_id)->first()[$product_name_column];
            $email_content = str_replace("[product_name]", $product_name, $email_content);
            $email_content = str_replace("[product_sub_type]", $this->buyer_seller->product_sub_type, $email_content);
          }
          $email_content = str_replace("[name]", $this->user->name, $email_content);
          $email_content = str_replace("[email]", $this->user->email, $email_content);
          $email_content = str_replace("[phone]", (substr(trim($this->user->phone),0,1) != '+' ? '+' : '').$this->user->phone, $email_content);
          if($this->user->hasRole('seller')){
            $email_content = str_replace("[view_seller_link]", route('admin.sellers.show',$this->buyer_seller->seller_id), $email_content);
          }elseif($this->user->hasRole('buyer')){
            $email_content = str_replace("[view_buyer_link]", route('admin.buyers.show',$this->buyer_seller->buyer_id), $email_content); 
          }   
        }
        return $this->view('frontend.mail.general',['email_content' => $email_content])
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->replyTo(config('mail.from.address'), config('mail.from.name'));
    }
}
