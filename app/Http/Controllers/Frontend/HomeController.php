<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\Auth\User;
use App\Seller;
use App\Sale;
use App\SaleTruck;
use App\Loadstatus;
use App\PurchaseOrder;
use App\Invoice;
use App\Offer;
use App\Buyer;
use App\Product;
use Carbon\Carbon;
use App\Referrer;
use DateTime;
use Mail;
use App\Subscriber;
use App\UserIps;
use Auth;
use Cookie;
use GuzzleHttp\Client;
use App\CurrencyRate;
/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(){
      if(request()->has('r')){

        $ref_val = request()->r;
        $user_id = decrypt($ref_val);

      }else{
        $user_id='';
      }
        $data = [
            'phone' => '919125368261', // Receivers phone
            'body' => 'WhatsApp API on chat-api.com works good', // Message
        ];
        //wa_send($data);

        $products = Product::where('type', 'product')->where('status', '1')->get();
        //echo "<pre/>"; print_r($products); die;
        return view('frontend.index', compact('products','user_id'));
    }
    public function referrer(Request $request){
      // code for check userips
      Cookie::queue('IP', $request->ip);
      
      $location_detail = \Location::get($request->ip);

      $check_Userips = UserIps::where('ip',$request->ip)->orderBy('id', 'DESC')->first();
      
      $current_date =  \Carbon\Carbon::now()->toDateTimeString();
      if($check_Userips != ''){
      $after_2_hrs = $check_Userips->created_at->addHours(2)->toDateTimeString();
      if($after_2_hrs <= $current_date){
          $Userips = new UserIps;
          $Userips->userid = ($check_Userips->userid != '') ? $check_Userips->userid : null;
          $Userips->ip = $request->ip;
          $Userips->city = $location_detail->cityName;
          $Userips->country = $location_detail->countryName;
          $Userips->didlogin = (Auth::check() == false) ? 'No' : 'Yes';
          $Userips->date = date('Y-m-d', strtotime($current_date));
          $Userips->time = date('h:i:s', strtotime($current_date));
          $Userips->save();
      }
     }else{
          $Userips = new UserIps;
          $Userips->userid =  (Cookie::get('UserId') != '') ? Cookie::get('UserId') : null;
          $Userips->ip = $request->ip;
          $Userips->city = $location_detail->cityName;
          $Userips->country = $location_detail->countryName;
          $Userips->didlogin = (Auth::check() == false) ? 'No' : 'Yes';
          $Userips->date = date('Y-m-d', strtotime($current_date));
          $Userips->time = date('h:i:s', strtotime($current_date));
          $Userips->save();
     }

      // code for check referrer
      if($request->user_id != ''){
        $check_referrer = Referrer::where('user_id',$request->user_id)->where('ip',$request->ip)->first();
       if($check_referrer == ''){
       $referrer = new Referrer;
       $referrer->user_id=$request->user_id;
       $referrer->ip=$request->ip;
       $referrer->browser_name=$request->browser_name;
       $referrer->os_name=$request->os_name;
       $referrer->os_version=$request->os_version;
       $referrer->save();
      }else{

      }
      }else{

      }
     }

    public function authorizeSeller(Request $request, $user_id=NULL){
      $user = User::where('uuid', $user_id)->first();
      if($user){
        auth()->login($user);
        if(auth()->user()){
          $roles = auth()->user()->roles()->pluck('name')->toArray();
          if(in_array('seller', $roles)){
            $seler_id = Seller::where('user_id', $user->id)->pluck('id');
      			Seller::where('id', $seler_id)->update([
      			'invite_sent' => '1',
      			'verified'=>'1',
      			]);
            return redirect()->route('seller.stock.index')->with('success','Seller created successfully.');
          }
        }
      }



    }

    public function tracker(Request $request){
		 $ip = $request->ip();
       $location_detail = \Location::get($ip);
      $user = \Auth::user();
      $data = $request->all();
      $data['user_id']= (!empty($user))?$user->id:0;
      $data['date_time']= \Carbon\Carbon::now()->toDateTimeString();
      $data['ip']= $ip;
	  if($ip != '127.0.0.1'){
      $data['city']= $location_detail->cityName;
      $data['country']= $location_detail->countryName;
	  }
      $user->userTracking()->create($data);
      return response()->json(["status"=>true]);
    }
	
	public function privacypolicy(Request $request){

		return view('frontend.privacypolicy');

	 }

	 public function termsconditions(Request $request){

		return view('frontend.termsconditions');

	 }

	  public function set_site_cookie(Request $request){

		session()->push('Agreecookie', 'yes');
		//return $response;
		return 'success';
		die();
   }
   public function orderConfirmation(Request $request, $purchaseorder_id=NULL){
    $user = auth()->user();
    $user = User::where('uuid', @$user->uuid)->first();
    if($user){
      auth()->login($user);
      if(auth()->user()){
        $roles = auth()->user()->roles()->pluck('name')->toArray();
        if(in_array('seller', $roles)){
            $PurchaseOrderData = PurchaseOrder::where('id', decrypt(@$purchaseorder_id))->where('status','ordered')->with('stock','seller','buyer')->first();
            if(isset($PurchaseOrderData)){
                  PurchaseOrder::where('id', @$PurchaseOrderData->id)->update(['status' => 'confirmed']);
                  Sale::where('id', @$PurchaseOrderData->sale_id)->update(['status' => 'confirmed']);
                  //Invoice Create
                  Invoice::create([
                      'date' => $PurchaseOrderData['delivery_date'],
                      'amount' => $PurchaseOrderData['price'],
                      'status' => 'UNPAID',
                      'buyer_id' => $PurchaseOrderData['buyer_id'],
                      'seller_id' => $PurchaseOrderData['seller_id'],
                      'product_id' => $PurchaseOrderData['stock']['product_id'],
                      'quantity' => $PurchaseOrderData['stock']['quantity'],
                  ]);
              }
             ;
          return redirect()->route('seller.purchaseorder.index')->with('success','Seller created successfully.');
        }else{
          return abort(404);
        }
      }
    }
  }
  
  public function orderEdit(Request $request, $purchaseorder_id=NULL){
    $user = auth()->user();
    $user = User::where('uuid', @$user->uuid)->first();
    if($user){
      auth()->login($user);
      if(auth()->user()){
        $roles = auth()->user()->roles()->pluck('name')->toArray();
        if(in_array('seller', $roles)){
          $purchaseorder = PurchaseOrder::where('id', decrypt(@$purchaseorder_id))->where('status','confirmed')->with('stock','seller','buyer')->first();
          // dd($purchaseorder);
          if(isset($purchaseorder)){
            $stockid = Offer::select('id')->get();
            $buyers = Buyer::where('status', '1')->select('id', 'username', 'name')->get();
            $sellers = Seller::where('status', '1')->select('id', 'username', 'name')->get();
            $saleTrucks = SaleTruck::where('sale_id', @$purchaseorder->sale_id)->get();
            $loads_status = Loadstatus::get();
            return redirect()->route('seller.purchaseorder.edit',compact('purchaseorder','buyers', 'stockid','sellers','saleTrucks','loads_status'));
          }else{
            return redirect()->route('seller.purchaseorder.index')->with('error','Please confirm order before click on edit!');
          }
          // return abort(404);
        }else{
          return abort(404);
        }
      }
    }
  }

  public function subscribe(Request $request){
      $subscribe = Subscriber::updateOrCreate(['email' => $request->email], ['email' => $request->email]);
      if ($subscribe->wasRecentlyCreated) {
         return response()->json(["status"=>'success']);
      }else{
         return response()->json(["status"=>'updated']);
      }
  }
  public function currencyRate(){
    $client = new Client();
    $data = $client->get('http://data.fixer.io/api/latest?access_key=ca4a1e69fec7ec8330a88f2351469415');
    $response = json_decode($data->getBody(), true);
    if(isset($response['rates'])){
      foreach($response['rates'] as $key => $rate){
        $currency_rate = CurrencyRate::where('to',$key)->orWhere('rate',$rate)->first();
        if(isset($currency_rate)){
          $currency_rate->update(['from'=>$response['base'],'to'=>$key,'rate'=>$rate]);
        }else{
          CurrencyRate::create(['from'=>$response['base'],'to'=>$key,'rate'=>$rate]);
        }
      }
      return response()->json(["status"=>'success']);
    }else{
      return response()->json(["status"=>'error']);
    }
  }
}
