<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Offer;
use App\Sale;
use App\Seller;
use App\Buyer;
use App\Match;
use App\Product;
use App\ProductSpecification;
use DataTables;
use DB;
use DateTime;
use App\AppHead;
use App\Models\Auth\User;
use App\OfferSent;
use Mail;
use App\Exports\MatchesExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Events\Backend\StockUpdated;
use App\Events\Backend\BuyerUpdated;

class MatchsController extends Controller{

    function __construct(){
        $this->middleware('permission:view matches', ['only' => ['index']]);
    }

  public function index(Request $request)
  {
    $stock_list = Offer::with('seller', 'product')->where('status', 'listed')->get();
    $buyers_list = Buyer::select('id', 'username')->get();
    $products1 = \App\Product::select('id', 'name')->get();
    $products = array();
    $product_specifications = array();
    foreach ($products1 as $key => $value) {
       $products[$value->id] = $value->name;
    }
    $product_specifications1 = \App\ProductSpecification::select('id', 'display_name')->get();
    foreach ($product_specifications1 as $key => $value) {
       $product_specifications[$value->id] = $value->display_name;
    }
      if(isset($request->product_id) && !empty($request->product_id)){
         $product_id = $request->product_id;
         $ProdSpecArr = ProductSpecification::where('product_id',$request->product_id)->where('parent_id',NULL)->orderBy('order')->limit(3)->pluck('display_name','id')->toArray();
      } else {
         $product = Product::all()->where('status', '1')->first();
         $product_id = @$product->id;
        // echo "<pre/>"; print_r($product); die;
         $ProdSpecArr = ProductSpecification::where('product_id',@$product->id)->where('parent_id',NULL)->orderBy('order')->limit(3)->pluck('display_name','id')->toArray();
      }
      $ProdSpecArrKeys = array_keys($ProdSpecArr);
      $field1 = current($ProdSpecArrKeys);
      if(count($ProdSpecArrKeys) > 1){
         $field2 = next($ProdSpecArrKeys);
      } else {
         $field2 = '';
      }

      if(count($ProdSpecArrKeys) >2){
         $field3 = end($ProdSpecArrKeys);
      } else {
         $field3 = '';
      }
      $ProdSpecArrNames = array();
      $ProdSpecArrNames['field1'] = $field1;
      $ProdSpecArrNames['field2'] = $field2;
      $ProdSpecArrNames['field3'] = $field3;
    // echo '<pre>';print_r($stock_list->toArray());exit;
    //return $data = Match::with('stock', 'buyer', 'stock.seller')->where('status', 'match')->get();;
    if ($request->ajax()) {
       $product_specification_values1 = \App\ProductSpecificationValue::select('id', 'value')->get();
       foreach ($product_specification_values1 as $key => $value) {
          $product_specification_values[$value->id] = $value->value;
       }
      DB::enableQueryLog();
      $data = Match::with('matchItem','stock', 'buyerPref.buyer.prefs.productPrefs.productSpecValue', 'stock.seller', 'stock.product','stock.offerProperty');

      if(isset($request->stock) && !empty($request->stock)){
        $data->where('stock_id', $request->stock);
      }
      if(isset($request->buyer) && !empty($request->buyer)){
        $data->where('buyer_id', $request->buyer);
      }
      if(isset($request->product_id) && !empty($request->product_id)){
        $data->where('product_id', $request->product_id);
      }
    
      if(isset($request->match_type) && !empty($request->match_type)){
         if(@$request->show_matched == 'no'){
            $match_type_array = explode(',',$request->match_type);
            foreach($match_type_array as $match_type){
               $data->whereDoesntHave('matchItem', function($q) use($match_type) {
                      $q->where('product_specification_id', $match_type);
               });
            }
         }
      }
      if(!isset($request->show_matched) || (isset($request->show_matched) && $request->show_matched=='yes')){
         // echo 'dfd';exit;
        $data->where('numofmismatches', 0);
      }else{
        $data->where('numofmismatches','>', 0);
      }
      $data = $data->orderBy('numofmismatches', 'asc')->orderBy('total_profit', 'desc')->get();
      // dd(DB::getQueryLog());
       
      return Datatables::of($data)
      ->addIndexColumn()
      ->addColumn('checkbox', function($row){
         $btn = ' <div class="btn-group btn-group-sm">
         <input type="checkbox" name="sendmail[]" data-id="'.$row->id.'" />
        </div>';
         return $btn;
       })
       ->addColumn('buyer_show_url', function($row){
         //  echo "<pre/>"; print_r($row); die;
        $all_orders = Sale::select('app_heads.name','offers.quantity','offers.street','offers.country','offers.price','offers.postalcode','offers.size_from','offers.size_to','offersent.time_sent','offersent.created_at')->join('offers', 'offers.id', '=', 'sales.stock_id')->join('app_heads', 'app_heads.id', '=', 'offers.product_id')->join('offersent', 'offersent.stock_id', '=', 'offers.id')->where('sales.stock_id',$row->stock_id)->where('offersent.match_id',$row->id)->limit(3)->orderBy('sales.id')->get();
      
        $data = '<div class="btn-group btn-group-sm"><button class=" btn btn-success expand_row" id_v="'.$row->id.'" style="box-sizing: border-box;"><i class="fa fa-plus" aria-hidden="true"></i></button></div>&nbsp;&nbsp;<a href="'.route('admin.buyers.show', @$row->buyerPref->buyer->id).'" target="_blank">'.@$row->buyerPref->buyer->username.'</a>
       <br>
        <div class="tab_show"  id="tab_show'.$row->id.'" style="display: none;">
          <ul class="nav nav-tabs col-md-12 tabs"  id="tabs'.$row->id.'">
              <li class="nav-item"><a id="tab1'.$row->id.'" class="nav-link active" href="javascript:void(0)"  role="tab">Orders</a></li>
              <li class="nav-item"><a id="tab2'.$row->id.'" class="nav-link inactive" href="javascript:void(0)"  role="tab">Offer Sent</a></li>
              <li class="nav-item"><a id="tab3'.$row->id.'" class="nav-link inactive" href="javascript:void(0)"  role="tab">Both</a></li>
         </ul>
          <div class="containerss" id="tab1'.$row->id.'C" style="display: block;">';
          foreach ($all_orders as $key => $val) {

            $data .= 'Product Name: '.$val->name.'<br>Quantity: '.$val->quantity.'<br>Size_from: '.$val->size_from.'<br>Size_to: '.$val->size_to.'<br>Country: '.$val->country.'<br>Street: '.$val->street.'<br>PostalCode: '.$val->postalcode.'<br><hr>';
          }
         $data .= '</div>
          <div class="containerss" id="tab2'.$row->id.'C" style="display: none;">';
          foreach ($all_orders as $key => $val) {

            $data .= 'Product Name: '.$val->name.'<br>Time Sent: '.$val->time_sent.'<br>created_at: '.$val->created_at.'<br><hr>';
          }


           $data .= '</div>
          <div class="containerss" id="tab3'.$row->id.'C" style="display: none;">';
           foreach ($all_orders as $key => $val) {

             $data .= 'Product Name: '.$val->name.'<br>Quantity: '.$val->quantity.'<br>Size_from: '.$val->size_from.'<br>Size_to: '.$val->size_to.'<br>Country: '.$val->country.'<br>Street: '.$val->street.'<br>PostalCode: '.$val->postalcode.'<br>Time Sent: '.$val->time_sent.'<br><hr>';
          }


           $data .= '</div>
          </div>

      ';
        return $data;
      })
      ->addColumn('seller_show_url', function($row){
        $data = '<a href="'.route('admin.sellers.show', isset($row->stock->seller)?@$row->stock->seller->id:'').'" target="_blank">'.isset($row->stock->seller)?@$row->stock->seller->username:''.'</a>';
        return $data;
      })
       ->addColumn('stock_show_url', function($row){
        $data = '<a href="'.route('admin.stock.show', $row->stock_id).'" target="_blank">'.$row->stock_id.'</a>';
        return $data;
      })
      ->addColumn('field1', function($row) use ($field1) {
          $arrFields  = array();
          foreach($row->stock->offerProperty as $prop){
              $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
          }
          if(is_array($arrFields)&& !empty($arrFields[@$field1])){
              return implode(', ',@$arrFields[@$field1]);
          } else {
              return '-';
          }
      })
      ->addColumn('field2', function($row) use ($field2){
          $arrFields  = array();
          foreach($row->stock->offerProperty as $prop){
              $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
          }

          if(is_array($arrFields)&& !empty($arrFields[@$field2])){
              return implode(', ',@$arrFields[@$field2]);
          } else {
              return '-';
          }
      })
      ->addColumn('field3', function($row) use ($field3){
          if($field3 == ''){
              return '';
          } else {
              $arrFields  = array();
              foreach($row->stock->offerProperty as $prop){
                  $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
              }
              if(is_array($arrFields)&& !empty($arrFields[@$field3])){
                  return implode(', ',@$arrFields[@$field3]);
              } else {
                  return '-';
              };
          }
      })
      ->addColumn('profit_per_ton', function($row){
         $pTonCalculation = json_decode($row->pton_calculation);
         $pTonCalculation = $this->pTonCalculation($row->stock, $row->buyerPref, $pTonCalculation->global_premium_factor_name, $pTonCalculation->global_premium_factor, $pTonCalculation->bpfs_count, $pTonCalculation->ec, $pTonCalculation->ecbf);
         $data = '<a href="javascript:;" class="pton">'.$pTonCalculation['profitPerTon'].'</a>';
         $data .= '<div class="pton_calculation d-none">'.$pTonCalculation['pton_calculation'].'</div>';
         // $bpfs_count = Buyer::join('\App\BuyerPref',)->join('\App\BuyerProductPref',)->where('buyer_id', $row->id)->sum('val');
         // $bpfs_count = 0;
         // $etpt = \App\PostalCode::select('price')->where(DB::raw('substr(postal_code,1,2)'),substr($row->buyerPref->buyer->postalcode,0,2))->orWhere('name',$row->buyerPref->buyer->city)->first();
         // $price = $row->stock->price;
         // $transport_base = @$row->stock->product->base_price;
         // $transport_factor = @$transport_base * $row->stock->product->premium / 100;
         // $total_premium = $price * $bpfs_count / 100;
         // $profit_per_ton = number_format($transport_factor + @$etpt->price + $total_premium, 2,'.','');
         // $a = '';
         // $a .= 'Transport factor('.$transport_factor.') = stock.product.base_price('.$transport_base.') * stock.product.premium('.$row->stock->product->premium.')/100 ';
         // // $transport_factor = $transport_base * $stock->product->premium / 100;
         // $a .= '<br/> Profit Per Ton('.$profit_per_ton.') =  Transport Factor('.$transport_factor.') + etpt price('.@$etpt->price.') + Total premium('.$total_premium.')';
         // // $a .= '<br/>'.$transport_factor.'(Transport Factor) + '.@$etpt->price.' (etpt price) +'.$total_premium.' (Total premium)';
         // $data = '<a href="javascript:;" data-toggle="tooltip" data-calculation="'.$a.'" data-original-title="View P/Ton Calculation" class="pton">'.$row->profit_per_ton.'</a>';
         return $data;
        // return @$row->profit_per_ton;
      })
      ->addColumn('mismatches', function($row) use($product_specifications){
         if($row->numofmismatches > 0){
            foreach(@$row->matchItem as $val){
               // if(!empty($val->value)){
                  $tmp[] = [@$product_specifications[$val->product_specification_id],$val->value];
               // }
            }
            $data = str_replace('],[','] , [',stripslashes(json_encode(@$tmp)));
         }
        // $data = (is_array($row->match_items) ? json_encode($row->match_items) : $row->match_items);
        return @$data;
      })
      ->addColumn('trust_level_buyer', function($row){
         $trust_level = trustlevel_list();
         // echo $row->trust_level;exit;
         foreach(@$trust_level as $val){
            if($row->buyerPref->buyer->trust_level == $val->id){
               $data = $val->name;
               break;
            }
         }
         // $data = (is_array($row->match_items) ? json_encode($row->match_items) : $row->match_items);
        return @$data;
      })
      ->addColumn('trust_level_seller', function($row){
         $trust_level = trustlevel_list();
         foreach(@$trust_level as $val){
            if(@$row->stock->seller->trust_level == $val->id){
               $data = $val->name;
               break;
            }
         }
        return @$data;
      })
      ->addColumn('trust_level_combined', function($row){
         $trust_level = trustlevel_list();
         foreach(@$trust_level as $val){
            if(@$row->buyerPref->buyer->trust_level == $val->id){
               $trust_level_buyer = $val->name;
               break;
            }
         }
         foreach(@$trust_level as $val){
            if(@$row->stock->seller->trust_level == $val->id){
               $trust_level_seller = $val->name;
               break;
            }
         }
         $data = @$trust_level_buyer + @$trust_level_seller;
        return @$data;
      })
      ->addColumn('added', function($row){
               $data = $row->created_at->diffForHumans();
             
        return @$data;
      })
      // ->addColumn('flesh_color2', function($row){
      //   return @$row->stock->flesh_color_detail->name;
      // })
      // ->addColumn('purposes2', function($row){
      //    $tmp = json_decode(@$row->stock->purposes,true);
      //    $data = (is_array($tmp) ? implode(',',@$tmp) : '');
      //   return @$data;
      // })
      // ->addColumn('variety_name', function($row) use($product_specifications, $product_specification_values){
      //    // echo '<pre>';print_r($row->stock->offerProperty->toArray());exit;
      //    foreach ($row->stock->offerProperty as $key => $value) {
      //       if($value->product_spec_id == 6){
      //          $data = $product_specification_values[$value->product_spec_val_id];
      //       }
      //    }
      //    // echo $data;exit;
      //    // $data = $product_specifications[$row->stock->offer_property->];
      //   return @$data;
      // })
      // <button data-toggle="tooltip" data-stock_id="'.$row->stock_id.'" data-order_id="'.$row->id.'" data-original-title="Delete" type="button" class="btn btn-success notifyItem"><i class="fas fa-paper-plane"></i></button>
      ->addColumn('action', function($row){
        $stock_detail = Offer::find($row->stock_id);
        $btn = ' <div class="btn-group btn-group-sm">
        <button type="button" class="btn btn-success viewItem" title="Make Sale" data-url="'.route('admin.sales.create', ['buyer' => $row->buyerPref->buyer->id, 'stock' => $row->stock_id, 'match' => $row->id]).'" >Make Sale</button>
        <button type="button" class="btn btn-edit editItem" title="Edit Match" data-url="'.route('admin.matches.edit', $row->id).'" ><i class="fas fa-edit"></i></button>
        <button type="button" class="btn btn-warning sendInvoice" title="Send PDF" data-url="'.route('admin.matches.InvoiceSend', $row->id).'" data-viewurl="'.route('admin.matches.InvoiceView', $row->id).'"><i class="fas fa-file-invoice"></i></button>
        </div>';
        return $btn;
      })
      ->rawColumns(['variety_name','profit_per_ton','pton_calculation','stock_show_url','buyer_show_url','seller_show_url','action','checkbox'])
      ->make(true);
    }
    // print_r($ProdSpecArr);exit;
    return view('backend.matches.index', compact('stock_list', 'buyers_list','products','ProdSpecArr','ProdSpecArrNames','product_specifications'));

  }

    public function createoffer(Match $Match){

    }
    public function edit($id){
      $match = Match::where(['id' => $id])->first();
      if($match){
         $match = Match::with('stock', 'buyerPref.buyer', 'stock.seller', 'stock.product', 'stock.offerProperty','offerproperty.productspec','offerproperty.productspecvalue')->where('id', $match->id)->first();
         $stock  = $match->stock;
          $pTonCalculation = json_decode(@$match->pton_calculation);
          $pTonCalculation = $this->pTonCalculation($match->stock, $match->buyerPref, @$pTonCalculation->global_premium_factor_name, @$pTonCalculation->global_premium_factor, @$pTonCalculation->bpfs_count, @$pTonCalculation->ec, @$pTonCalculation->ecbf);
 
         $seller  = $match->stock->seller;
         $buyer  = $match->buyerPref->buyer;
         $stock_image  = current(json_decode(@$stock->image)??array());
         $available_from_date = date('Y-m-d',strtotime($stock->available_from_date.'+3 days'));
         //echo "<pre/>"; print_r($match); die;
         return view('backend.matches.editoffer',compact('match','stock','buyer','seller','stock_image','available_from_date','pTonCalculation'));
       }else{
         $stock_list = Offer::with('seller', 'product')->where('status', 'listed')->get();
         $buyers_list = Buyer::select('id', 'username')->get();
         $products1 = \App\Product::select('id', 'name')->get();
         $products = array();
         $product_specifications = array();
         foreach ($products1 as $key => $value) {
            $products[$value->id] = $value->name;
         }
         $product_specifications1 = \App\ProductSpecification::select('id', 'display_name')->get();
         foreach ($product_specifications1 as $key => $value) {
            $product_specifications[$value->id] = $value->display_name;
         }
           if(isset($request->product_id) && !empty($request->product_id)){
              $product_id = $request->product_id;
              $ProdSpecArr = ProductSpecification::where('product_id',$request->product_id)->where('parent_id',NULL)->orderBy('order')->limit(3)->pluck('display_name','id')->toArray();
           } else {
              $product = Product::all()->where('status', '1')->first();
              $product_id = @$product->id;
             // echo "<pre/>"; print_r($product); die;
              $ProdSpecArr = ProductSpecification::where('product_id',@$product->id)->where('parent_id',NULL)->orderBy('order')->limit(3)->pluck('display_name','id')->toArray();
           }
           $ProdSpecArrKeys = array_keys($ProdSpecArr);
           $field1 = current($ProdSpecArrKeys);
           if(count($ProdSpecArrKeys) > 1){
              $field2 = next($ProdSpecArrKeys);
           } else {
              $field2 = '';
           }
     
           if(count($ProdSpecArrKeys) >2){
              $field3 = end($ProdSpecArrKeys);
           } else {
              $field3 = '';
           }
           $ProdSpecArrNames = array();
           $ProdSpecArrNames['field1'] = $field1;
           $ProdSpecArrNames['field2'] = $field2;
           $ProdSpecArrNames['field3'] = $field3;
        $msg="Unfortunately this Matches is not exist!";
        return view('backend.matches.index', compact('msg','stock_list', 'buyers_list','products','ProdSpecArr','ProdSpecArrNames','product_specifications'));
       } 
    }

  public function notifyMatched($event)
  {
   // echo 'notifyMatched';exit;
   // echo '<pre>';print_r($event);exit;
      $query = Match::with('matchItem','stock', 'buyerPref.buyer.prefs.productPrefs.productSpecValue', 'stock.seller', 'stock.product','stock.offerProperty')->where('numofmismatches',0);
      if(class_basename($event) == 'StockUpdated'){
         $data = $query->where('stock_id',@$event->stock->id)->get();
      }elseif(class_basename($event) == 'BuyerUpdated'){
         // $data = $query->where(function($query) use ($event){
         //    $query->where('buyer_pref_id',@$event->buyerpref->id)->get();
         // }) 
      }

      // echo '<pre>';print_r($data->toArray());exit;
    if(isset($data) && @$data->count() > 0){
      $message_data = array();
      foreach ($data as $match) {
           // $order_location = '';
           // foreach (json_decode($match->location_range, true) as $key => $value) {
           //   if($key > 0){
           //     $order_location .= ', ';
           //   }
           //   $order_location = $value['from'].' - '.$value['to'];
           // }
         // echo '<pre>';print_r($traders->toArray());exit;
         $pTonCalculation = json_decode($match->pton_calculation);
         $pTonCalculation = $this->pTonCalculation($match->stock, $match->buyerPref, $pTonCalculation->global_premium_factor_name, $pTonCalculation->global_premium_factor, $pTonCalculation->bpfs_count, $pTonCalculation->ec, $pTonCalculation->ecbf);
         $tmp['pTonCalculation'] = $pTonCalculation;
         $tmp['match'] = $match;
         // $tmp['order_location'] = $order_location;
         $message_data[$pTonCalculation['profitPerTon']] = $tmp;
      }
      krsort($message_data);
      $traders = User::whereHas('roles', function($q){$q->where('name', 'trader');})->get();
      $transporters = User::whereHas('roles', function($q){$q->where('name', 'trans');})->get();
      foreach ($traders as $trader) {
         // echo '<pre>';print_r($message_data);exit;
         $i = 0;
         $matches = array();
         foreach(@$message_data as $value){
            $i++;
            if($i > 3){
               break;
            }
            $value['role'] = 'trader';
            $match = $value['match'];
            $pTonCalculation = $value['pTonCalculation'];
          $trader_message = "Dear ".ucfirst($trader->first_name).", \n *Stock: #".$match->stock->id.'* ('.$match->stock->seller->username.') matched to *Buyer: #'.$match->buyerPref->buyer->id.'* ('.$match->buyerPref->buyer->username.') of '.$match->stock->product->name.", \n *Stock Price*: ".$match->stock->price.", \n *Buyer Premiums*: ".$match->buyerPref->buyer->total_prefs."%, \n *P/Ton*: ".$pTonCalculation['profitPerTon'].", \n *View all matches*: ".route('admin.matches.index');
          // SendSMS($trader->phone, $trader_message);
          SendWhatsapp(['phone' => $trader->phone, 'body' => $trader_message,'is_PDF'=>false]);
          // echo $trader->phone. ' '.$trader_message;exit;
          // \Log::info("Stock Matched \n".$trader_message);
           $matches[] = $value;
         }
         \Mail::to($trader->email)->send(new \App\Mail\Backend\StockMatched(@$matches));
      }
     foreach (@$transporters as $trans) {
       // $trans_message = 'Dear '.$trans->first_name.',
       // Stock: #'.$stock->id.' ('.$stock->seller->username.') matched to Order: #'.$match->id.' ('.$match->buyer->username.'), QTY: '.$match->quantity.', location: '.$order_location;
         $i = 0;
         $matches = array();
         foreach(@$message_data as $value){
            $i++;
            if($i > 3){
               break;
            }
            $value['role'] = 'trans';
            $match = $value['match'];
            $pTonCalculation = $value['pTonCalculation'];
          $trans_message = "Dear ".ucfirst($trans->first_name).", \n *Stock: #".$match->stock->id.'* ('.$match->stock->seller->username.') matched to *Buyer: #'.$match->buyerPref->buyer->id.'* ('.$match->buyerPref->buyer->username.') of '.$match->stock->product->name.", \n *Stock Price*: ".$match->stock->price.", \n *Buyer Premiums*: ".$match->buyerPref->buyer->total_prefs."%, \n *P/Ton*: ".$pTonCalculation['profitPerTon'].", \n *Quantity*: ".$match->stock->quantity.", \n *Location*: N/A, \n *View all matches*: ".route('admin.matches.index');
          // SendSMS($trader->phone, $trader_message);
          SendWhatsapp(['phone' => $trans->phone, 'body' => $trans_message,'is_PDF'=>false]);
          // echo $trader->phone. ' '.$trader_message;exit;
          // \Log::info("Stock Matched \n".$trader_message);
           $matches[] = $value;
         }
         \Mail::to($trans->email)->send(new \App\Mail\Backend\StockMatched(@$matches));
     }

    }
  }

  public function CheckMatchesForBuyerPrefId(Buyer $buyer)
  {
      // echo 'dfd';exit;
      // dd($buyer->toArray());
      // DB::enableQueryLog();
      $matched = $mismatched = array();
      $stocks = Offer::all();
      // echo '<pre>';
      foreach($buyer->prefs as $pref){
         foreach($stocks as $stock){
            // echo ' * ';
            $matches = $this->checkMatch($pref, $stock);
            $this->updateMatchedItems($matches);
         }
      }
         // echo '<pre>';print_r($matches);exit;
      // event(new BuyerUpdated($buyer));
      if(isset(request()->url) && request()->url=='link'){
         return redirect()->back();
      }
  }
  public function updateMatchedItems($matches){
      // echo ' updateMatchedItems ';
      // if($matches['ismatched'] == 1){
      //    // $row = $matches['matched'];
      //    $numofmismatches = 0;
      //    $matched = 1;
      // }elseif($matches['ismatched'] == 0){
      //    // $row = $matches['mismatched'];
      //    $numofmismatches = count($matches['items']);
      //    $matched = 0;
      // }else{
      //    echo 'empty';exit;
      // }
      $row = $matches;
      // echo '<pre>';print_r($matches);
         // echo ' buyer: '.$row['buyer_id'];
         // echo ' stock: '.$row['stock_id'];
      if(!isset($row['ismatched'])){
         $match = Match::where('stock_id',$row['stock_id'])->where('buyer_pref_id',$row['buyer_pref_id'])->delete();
         return;
      }
         $match = \App\Match::updateOrCreate(['stock_id' => $row['stock_id'], 'buyer_pref_id' => $row['buyer_pref_id'], 'product_id' => $row['product_id']], [
             'stock_id' => $row['stock_id'],
             'buyer_pref_id' => $row['buyer_pref_id'],
             'product_id' => $row['product_id'],
             'profit_per_truck' => $row['profit_per_truck'],
             'profit_per_ton' => $row['profit_per_ton'],
             'pton_calculation' => $row['pton_calculation'],
             'total_profit' => $row['total_profit'],
             'numofmismatches' => $row['numofmismatches']
         ]);
         // echo ' + '.$match->id;
         \App\MatchItem::where('match_id',$match->id)->delete();
         foreach($row['items'] as $key => $val){
            if(is_array($row['ismatched'])){
               print_r($row['ismatched']);exit;
            }
            // echo ' # '.$key;
            $matchItem = \App\MatchItem::Create([
                'match_id' => $match->id,
                'matched' => $row['ismatched'],
                'product_specification_id' => $key,
                // 'name' => $key,
                'value' => (is_array($val) ? json_encode(array_unique($val)) : $val),
            ]);
         }
         // exit;
  }

  public function CheckMatchesForStockId(Offer $stock)
  {
      $buyers = Buyer::with('prefs.productPrefs.productSpecValue')->get();
      $buyers = buyer::all();
      foreach($buyers as $buyer){
         foreach($buyer->prefs as $pref){
            $matches = $this->checkMatch($pref, $stock);
            $this->updateMatchedItems($matches);
         }
         // echo '<pre>';print_r($matches);exit;
      }
      // echo 'aaaa';exit;
      event(new StockUpdated($stock));
      if(isset(request()->url) && request()->url=='link'){
         return redirect()->back();
      }
  }
  public function checkMatch($pref, $stock){
      ob_start();
      $product_specifications = \App\ProductSpecification::with('options')->where('product_id',$stock->product_id)->get();
      // dd($product_specifications->toArray());
      // echo '<pre>';print_r($product_specifications->toArray());
      // echo '<pre>';print_r($pref->toArray());
      // dd($buyer->toArray());
      // dd($stock->toArray());
      // print_r($stock->offerProperty->toArray());
      $matched  = $mismatched = $matched2= array();
      $mismatched_prodspec = $global_premium_factor_name = $global_premium_factor = array();
      $ec = $ecbf = 0;
      if($stock->product_id != $pref->product_id){
         ob_end_clean();
         return ['buyer_pref_id'=>$pref->id, 'stock_id'=>$stock->id];
         // $stock_pref_product_id_mismatched = true;
         // $mismatched_prodspec[$stock->product_id] = '';
      }
      foreach(@$product_specifications as $product_specification){
         // echo ' ^ ';
         $mismatched_prodspec[$product_specification->id] = '';
         // $stock_mismatched = array();
         foreach(@$stock->offerProperty as $offer_property){
            // print_r($mismatched_prodspec);exit;
            // $mismatched_prodspec[$product_specification->id][] = $offer_property->productSpecValue->value;
            // echo ' % ';
            if(@$product_specification->id == @$offer_property->product_spec_id){
               // echo ' - '.$product_specification->id;
               // $stock_mismatched[] = $offer_property->product_spec_value;
            // echo '&';print_r($mismatched);
               // foreach($buyer->prefs as $pref){
                  // if($pref->product_id == $stock->product_id){
                     // $matched['product'] = true;
                     // if(!is_array($pref->productPrefs->toArray())){
                     //    echo 'pref';print_r($pref->productPrefs);echo '</pre>';
                     //    echo 'aaa';exit;
                     // }
                     $bpfs_count = array();
                     foreach(@$pref->productPrefs as $product_pref){
                        $bpfs_count[] = $product_pref->premium;
                        if($product_pref->value == 'all'){
                           // echo '!!!!!!!!!!!!!!! '.$product_specification->id;exit;
                           unset($mismatched_prodspec[$product_pref->key]);   // Consider matched if accept_all is On
                           continue;
                        }
                        // echo '-';print_r($product_pref->productSpecValue->toArray());
                        // print_r($product_pref->productSpecValue->product_specification_id);exit;
                        // print_r($product_pref);exit;
                        // $mismatched[$offer_property->product_spec_id] = array();
                        // print_r($product_spec_value);exit;
                        if(@$product_pref->productSpecValue->product_specification_id == @$offer_property->product_spec_id){
                     // echo ' * '.$product_pref->productSpecValue->value;
                        // $matched[$offer_property->product_spec_id] =
                           if(@$product_pref->productSpecValue->value == @$offer_property->productSpecValue->value){
                        // echo ' $ '.$offer_property->productSpecValue->value.' ! '.$product_specification->id;
                              foreach(@$product_specification->options as $option){
                                 if($option->value == $offer_property->productSpecValue->value){
                                    // echo ' ~ '.$product_pref->premium.'/'.$option->premium;
                                    $global_premium_factor_name[] = $option->value;
                                    // $global_premium_factor[] = 100 + ($option->premium!='' ? $option->premium : 0)/100;
                                    $tmp = $option->premium + $product_pref->premium;
                                    $global_premium_factor[] = ($tmp!='' ? $tmp : '0');
                                 }
                                 if(!empty(@$option->ec)){
                                    // echo ' EC '.@$offer_property->ecs;
                                    $ec = (!empty(@$offer_property->ecs) ? @$offer_property->ecs : @$option->ec);
                                    // $ec = $option->ec;
                                    $ecbf = $option->ecbf;
                                 }
                              }
                              // $matched[$product_specification->id] = $offer_property->productSpecValue->value;
                              // if (($tmpkey = array_search($product_specification->id, $mismatched_prodspec)) !== false) {
                              //    unset($mismatched_prodspec[$tmpkey]);
                              // }
                              if (array_key_exists($product_specification->id, $mismatched_prodspec)) {
                                 unset($mismatched_prodspec[$product_specification->id]);
                              }
                              // $mismatched_prodspec[$product_specification->id] = 'no';
                              // if(array_key_exists($product_specification->id, $mismatched)){
                              //    unset($mismatched[$product_specification->id]);
                              //    echo ' M ';
                              //    print_r($mismatched);
                              // }
                           }else{
                              // echo ' # '.$offer_property->productSpecValue->value;
                              // $mismatched_prodspec[$product_specification->id][] = $offer_property->productSpecValue->value;
                              // if(!array_key_exists($product_specification->id, $mismatched)){
                              //    echo ' MM ';
                              //    $mismatched[$product_specification->id][] = $offer_property->productSpecValue->value;
                              // }
                           }
                        }
                     }
                  // }
               // }
               // if(in_array($offer_property->product_spec_id , $mismatched_prodspec)){
               //    $mismatched[$product_specification->id] = $mismatched;
               // }
            }else{

               // $mismatched[$product_specification->id][] = $offer_property->productSpecValue->value;
            }
         }
         // if(!isset($matched[$product_specification->id]) && !isset($mismatched[$product_specification->id])){
         //    $mismatched[$product_specification->id] =true;
         // }
      }
      $mismatched_prodspec_with_value = array();
      // if($stock->id==4 && $pref->id == 2){
         // echo '<br/>Mismatched Prodspec ';print_r($mismatched_prodspec);
      foreach ($mismatched_prodspec as $key => $value) {
         // echo '<br/>';
         foreach ($stock->offerProperty as $offerProperty) {
            // echo ' dfd '.$offerProperty->productSpecValue->value.'*'.$key;
            if($offerProperty->product_spec_id == $key){
               $mismatched_prodspec_with_value[$key] = @$offerProperty->productSpecValue->value;
            }
         }
      }
      // echo 'ec = '.$ec. ' ecbf = '.$ecbf;
      // }
      if($stock->id==1 && $pref->id == 2){
         // echo '<br/>Mismatched Prodspec with value ';print_r($mismatched_prodspec_with_value);exit;
         // echo 'dfd';exit;
      }
      // echo '<pre>Matched: ';print_r($matched);echo 'Mismatched: ';print_r($mismatched);echo '</pre>';
      // print_r($bpfs_count);
      // $product_specifications = \App\ProductSpecificationValue::where('product_id',$stock->product_id)->get();
      // print_r($global_premium_factor);
      // exit;
      // $pTonCalculation = $this->pTonCalculation($stock, $pref, $global_premium_factor_name, $global_premium_factor, $bpfs_count, $ec, $ecbf);
      $pTonCalculation = json_encode(['global_premium_factor_name'=>$global_premium_factor_name,'global_premium_factor'=>$global_premium_factor,'bpfs_count'=>@$bpfs_count, 'ec'=>@$ec, 'ecbf'=>@$ecbf]);
      // echo 'aaa';print_r($pTonCalculation);exit;
      // echo $bpfs_count;exit;
      // $etpt = \App\PostalCode::select('price')->where(DB::raw('substr(postal_code,1,2)'),substr($buyer->postalcode,0,2))->orWhere('name',$buyer->city)->first();
      // $price = $stock_price;
      // $transport_base = $stock->product->base_price;
      // $transport_factor = $transport_base * $stock->product->premium / 100;
      // $total_premium = $price * $bpfs_count / 100;
      // $profit_per_ton = number_format($transport_factor + @$etpt->price + $total_premium, 2,'.','');
      // $profit_per_truck = number_format($profit_per_ton * 24, 2,'.','');
      // $total_profit = $profit_per_ton * $stock->quantity * 24;
      // $profit = number_format($total_profit + $total_premium , 2,'.','');
      // if($stock->id==2 && $buyer->id == 10){
      //    echo '*'.$stock->product->premium.' * '.$transport_factor.'* '.@$etpt->price.' * '.$total_premium;exit;
      // }
      $mismatched = $mismatched_prodspec_with_value;
      if(!empty($mismatched)){
         $ismatched = 0;
         $array_name = 'mismatched';
         $numofmismatches = count($mismatched);
      // }elseif(@$stock_pref_product_id_mismatched){
      //    $ismatched = 0;
      //    $array_name = 'mismatched';
      //    $numofmismatches = count($product_specifications);
      }else{
         $ismatched = 1;
         $array_name = 'matched';
         $numofmismatches = 0;
      }
      // if (($key = array_search(1, $mismatched)) !== false) {
      //    unset($mismatched[$key]);
      // }
      $matched2 = [
            'ismatched' => $ismatched,
            'buyer_pref_id'=>$pref->id,
            'stock_id'=>$stock->id,
            'product_id'=>$stock->product_id,
            'profit_per_truck' => '',   // @$pTonCalculation['profitPerTruck'],
            'profit_per_ton' => '',  // @$pTonCalculation['profitPerTon'],
            'pton_calculation' => @$pTonCalculation,
            'total_profit' => '', // @$pTonCalculation['profit'],
            'numofmismatches' => $numofmismatches,
            'items'=>$$array_name
         ];
         // print_r($matched2);exit;
      $tmp = ob_get_contents();
      ob_end_clean();
      return $matched2;
  }
   public function pTonCalculation($stock, $pref, $global_premium_factor_name, $global_premium_factor, $bpfs_count, $ec, $ecbf){
      $str = '';
      $gbpToPln = getCurrencyRate('GBP','PLN');
      $EurToPln = getCurrencyRate('EURO','PLN');
      $stock_price = $stock->price * $gbpToPln;
      $avgSalePrice  = '900';
      $baseTransportCostPerTon  = '159.66';  // in EUR
      $etpt = \App\PostalCode::select('price')->where(DB::raw('substr(postal_code,1,2)'),substr($pref->buyer->postalcode,0,2))->orWhere('name',$pref->buyer->city)->first();
      $extraTransportPerTon = @$etpt->price * $gbpToPln ;
      $str .= '<br/> avgSalePrice = '.$avgSalePrice .'<br/> stock->price = '.$stock_price.' = '.$stock->price.' * '.$gbpToPln.' <br/> baseTransportCostPerTon  = 900/22 = 37.5 Euro = '.$baseTransportCostPerTon .' PLN <br/> extraTransportPerTon = '.$extraTransportPerTon.' = '.@$etpt->price.' * '.$gbpToPln;
      // $bpfs_count = \App\BuyerPref::where('buyer_id', $buyer->id)->sum('val');
      // $salePrice = $salePrice;
      $premiumFlatCost = array_sum((is_array(@$bpfs_count) ? $bpfs_count : array()));
      $totalPremiumsPct  = array_sum((is_array(@$global_premium_factor) ? $global_premium_factor : array()));
      // $totalPremiumsFactor = ((is_array(@$global_premium_factor) ? $global_premium_factor : array()));
      // $salePrice = $avgSalePrice * $global_premium_factor;
      $totalPremiumsFactor = (100 + $totalPremiumsPct)/100;
      $salePriceWithoutExtraCost = $avgSalePrice * $totalPremiumsFactor;
      $totalExtraCosts = $ec * $ecbf;
      $salePrice = $salePriceWithoutExtraCost + $totalExtraCosts;
      // $salePrice = $avgSalePrice + ($avgSalePrice * $totalPremiumsFactor / 100);
      // $salePrice += $premiumFlatCost;
      $transportCost = ($baseTransportCostPerTon  + $extraTransportPerTon);
      $profitPerTon = $salePrice - $stock_price - $transportCost;
      $profitPerTon = number_format($profitPerTon, 2, '.','');
      $profitPerTruck = number_format($profitPerTon * 24, 2,'.','');
      $totalProfit = $profitPerTon * $stock->quantity * 24;
      $profit = number_format($totalProfit + $salePrice , 2,'.','');
      $str .= '<br/> transportCost = '.$transportCost.' = '.$baseTransportCostPerTon.' + '.$extraTransportPerTon;
      // $str .= '<br/> '.$transportCost.' = '.$baseTransportCostPerTon .' + '.$extraTransportPerTon;
      $str .= '<br/> profitPerTon = salePrice - stock->price - transportCost';
      $str .= '<br/> profitPerTon = '.$profitPerTon.' = '.$salePrice.' - '.$stock_price.' - '.$transportCost;
      $str .= '<br/> salePrice = avgSalePrice * totalPremiumsFactor';
      // $str .= '<br/> salePrice = avgSalePrice + (avgSalePrice * totalPremiumsFactor / 100)';
      $global_premium_factor_name_str = implode(' + ',$global_premium_factor_name);
      $global_premium_factor_str = implode('% + ',$global_premium_factor);
      $str .= '<br/> totalPremiumsPct = '.$global_premium_factor_name_str;
      $str .= '<br/> totalPremiumsPct = '.$totalPremiumsPct.'% ('.$global_premium_factor_str.'%)';
      $str .= '<br/> totalPremiumsFactor = '.$totalPremiumsFactor;
      $str .= '<br/> salePrice = '.$avgSalePrice.' * '.$totalPremiumsFactor;
      $str .= '<br/> totalExtraCosts = Nets';   // .implode(' * ',$totalExtraCosts);
      $str .= '<br/> totalExtraCosts = '.$ec.' * '.$ecbf.' = '.$totalExtraCosts;
      $str .= '<br/> salePrice = '.$salePriceWithoutExtraCost.' + '.$totalExtraCosts.' = '.$salePrice;
      // $str .= '<br/> totalExtraCosts = 20 * 1.6 = 32';
      $str .= '<br/><br/> PLN per GBP: '.$gbpToPln.' <br/> PLN per EURO: '.$EurToPln;


      // $str .= '<br/> salePrice = '.$avgSalePrice.' + ('.$avgSalePrice.' * '.$totalPremiumsFactor.' / 100)';

      // $str .= '<br/> profitPerTruck = profitPerTon * 24 ';
      // $str .= '<br/> '.$profitPerTruck.' = '.$profitPerTon.' * 24';
      // $str .= '<br/> totalProfit = profitPerTon * stock->quantity * 24 ';
      // $str .= '<br/> '.$totalProfit.' = '.$profitPerTon.' * '.$stock->quantity.' * 24';
      // $str .= '<br/> profit = totalProfit + salePrice ';
      // $str .= '<br/> '.$profit.' = '.$totalProfit.' + '.$salePrice;
      return ['profitPerTon'=>$profitPerTon, 'profitPerTruck'=>$profitPerTruck, 'profit'=>$profit, 'pton_calculation'=>$str];
   }
  public function sendNotification(Order $order, Offer $stock)
  {
    return $stock->seller->username;
  }

  public function makeSale(Order $order, Offer $stock)
  {
    return $stock->seller->username;
  }

  public function InvoiceSend(Request $request,Match $match){
      $mpdf = new \Mpdf\Mpdf();
      $user = auth()->user();
      $matches = Match::with('stock', 'stock.seller','buyerPref.buyer', 'stock.product','offerproperty.productspec','offerproperty.productspecvalue')->where('id', $match->id)->first();
      $tmp = json_decode(@$matches->stock->packaging,true);
      $packaging = (is_array($tmp) ? implode(',',@$tmp) : '');
      $purpose_tmp = json_decode(@$matches->stock->purposes,true);
      $purpose = (is_array($purpose_tmp) ? implode(',',@$purpose_tmp) : '');
      $template = utf8_encode(view('backend.matches.invoice',compact('matches','user','packaging','purpose')));
      $mpdf->WriteHTML($template);
      //Buyer send mail
      if($matches->buyerPref->buyer->contact_email)
      {
         if(!empty($user) && !empty($matches))
         {
            $dt = new DateTime();
            DB::table('offersent')->insert(['match_id' => $matches->id,'stock_id' => $matches->stock_id,'buyer_id'=>$matches->buyerPref->buyer->id,'time_sent' => $dt->format('H:i:s'),'created_at'=> $dt->format('Y-m-d H:i:s')]);
            Mail::send('backend.mail.default', ['name' => 'Invoice', 'body' => 'Invoice Message'], function ($message) use ($user, $matches,$mpdf) {
               $message->subject('Invoice of Veg King!');
               if($matches->buyerPref->buyer->email != ''){ $message->to($matches->buyerPref->buyer->email); }
                $message->attachData($mpdf->output("Invoice.pdf",'S'), "Invoice.pdf");
            });
         }
      }
      if($matches->buyerPref->buyer->contact_sms){
         //SendSMS($matches->buyerPref->buyer->phone, base_path("invoice/invoice.pdf"));
      }
      if($matches->buyerPref->buyer->contact_whatsapp){
         // Implement whats app API to send message
         $content = $mpdf->output("Invoice.pdf",'S');
         $content = chunk_split(base64_encode($content));
         SendWhatsapp(['phone' => $matches->buyerPref->buyer->phone, 'body' => "data:application/pdf;base64,".$content,'filename'=>'Invoice.pdf','caption'=>'Invoice','is_PDF'=>true]);
      }

      //Seller send mail
      if($matches->stock->seller->contact_email){
         if(!empty($user) && !empty($matches))
         {
            $dt = new DateTime();
            DB::table('offersent')->insert(['match_id' => $matches->id,'stock_id' => $matches->stock_id,'buyer_id'=>$matches->buyerPref->buyer_id,'time_sent' => $dt->format('H:i:s'),'created_at'=> $dt->format('Y-m-d H:i:s')]);
            Mail::send('backend.mail.default', ['name' => 'Invoice', 'body' => 'Invoice Message'], function ($message) use ($user,$matches,$mpdf) {
                  $message->subject('Invoice of Veg King!');
                  if($matches->stock->seller->email != ''){ $message->to($matches->stock->seller->email); }
                  $message->cc($user->email);
                  $message->attachData($mpdf->output("Invoice.pdf",'S'), "Invoice.pdf");
            });
         }else{
            return redirect()->route('admin.matches.index')->with('error','Invalid Request');
         }
      }
      if($matches->stock->seller->contact_sms){
         //SendSMS($matches->buyerPref->buyer->phone, base_path("invoice/invoice.pdf"));
      }
      if($matches->stock->seller->contact_whatsapp){
         // Implement whats app API to send message
         $content = $mpdf->output("Invoice.pdf",'S');
         $content = chunk_split(base64_encode($content));
         SendWhatsapp(['phone' => $matches->stock->seller->phone, 'body' => "data:application/pdf;base64,".$content,'filename'=>'Invoice.pdf','caption'=>'Invoice','is_PDF'=>true]);
      }
      return redirect()->route('admin.matches.index')->with('success','Invoice has been send Successfully.');
    }


     /**
   * View Invoice to particular buyer
   */
  public function InvoiceView(Request $request,Match $match){
      $user = auth()->user();
      $url = url('/') .'/img/'. Settings()->site_logo;
      $matches = Match::with('stock', 'stock.seller', 'stock.product','offerproperty.productspec','offerproperty.productspecvalue')->where('id', $match->id)->first();
      $tmp = json_decode(@$matches->stock->packaging,true);
      $packaging = (is_array($tmp) ? implode(',',@$tmp) : '');
      $purpose_tmp = json_decode(@$matches->stock->purposes,true);
      $purpose = (is_array($purpose_tmp) ? implode(',',@$purpose_tmp) : '');
      if(!empty($user) && !empty($matches))
      {
          $mpdf = new \Mpdf\Mpdf();
          $template = view('backend.matches.invoice',compact('matches','user','packaging','purpose'));
          $mpdf->WriteHTML($template->render());
          $mpdf->Output();
      }else{
          return redirect()->route('admin.matches.index')->with('error','Invalid Request');
      }
  }

   public function store(Request $request)
   {
        $data = $request->all();
        $matches = Match::with('stock', 'buyerPref.buyer', 'stock.seller', 'stock.product')->where('id', $data['match_id'])->first();
        $available_from_date = strtotime($matches->stock->available_from_date.'+3 days');
        if ($available_from_date > strtotime($data['available_from_date'])) {
            return redirect()->back()->withInput($data)->withErrors(['available_from_date'=>"Available from date should not be earlier than current value."]);
        }
        $OfferSent = new OfferSent();
        $OfferSent->match_id = $matches->id;
        $OfferSent->buyer_id = $matches->buyerPref->buyer->id;
        $OfferSent->stock_id = $matches->stock_id;
        $OfferSent->time_sent = date('Y-m-d H:i:s');
        $OfferSent->save();		

		Offer::find($matches->stock_id)->update(['price' => $request->price, 'available_per_day' => $request->available_per_day, 'note' => $request->note]);
         $user = auth()->user();
         $mpdf = new \Mpdf\Mpdf();
         $template = utf8_encode(view('backend.matches.invoice',compact('matches','user')));
         $mpdf->WriteHTML($template);
         Mail::send('backend.mail.default', ['name' => 'Invoice', 'body' => 'Invoice Message'], function ($message) use ($user,$mpdf) {
            $message->subject('Invoice of Veg King!');
            $message->to($user->email);
            $message->cc('himanshunagpal25061992@gmail.com');
            $message->attachData($mpdf->output("Invoice.pdf",'S'), "Invoice.pdf");
         });
        return redirect()->route('admin.matches.index')->with('success','Invoice has been send Successfully.');
   }

   public function updateAll()
   {
      // echo 'dfd';exit;
      // DB::enableQueryLog();
      ini_set('max_execution_time',120);
      $matched = $mismatched = array();
      $buyers = Buyer::with('prefs.productPrefs.productSpecValue')->get();
      $stocks = Offer::with('offerProperty.productSpecValue')->get();
      // $stocks = Offer::with('offerProperty.productSpecValue')->where('id',1)->get();
      // dd($stocks->toArray());
      // echo '<pre>';
      Match::truncate();
      \App\MatchItem::truncate();
      foreach($buyers as $buyer){
         foreach($buyer->prefs as $pref){
            foreach($stocks as $stock){
               // echo ' * ';
               $matches = $this->checkMatch($pref, $stock);
               $this->updateMatchedItems($matches);
            }
         }
         // echo '<pre>';print_r($matches);exit;
      }

      // exit;
      // print_r($matched);echo '*';print_r($mismatched);exit;
      return redirect(route('admin.matches.index'));
   }

  public function InvoiceSendtoAll(Request $request){
     $all = $request->all();
     
     foreach($all['array_main'] as $key => $value){
      
         $user = auth()->user();
         $data = Match::with('stock', 'stock.seller', 'stock.product','buyerPref','offerproperty.productspec','offerproperty.productspecvalue')->where('id',@$value['match_id'])->get();
         foreach($data as $matches)
         {
            $mpdf = new \Mpdf\Mpdf();
            $template = utf8_encode(view('backend.matches.invoice',compact('matches','user')));
            $mpdf->WriteHTML($template);
            //Buyer Send
            if($matches->buyerPref->buyer->contact_email)
            {
               if(!empty($user) && !empty($matches))
               {
                  $dt = new DateTime();
                  DB::table('offersent')->insert(['match_id' => $matches->id,'stock_id' => $matches->stock_id,'buyer_id'=>$matches->buyerPref->buyer->id,'time_sent' => $dt->format('H:i:s'),'created_at'=> $dt->format('Y-m-d H:i:s')]);
                  Mail::send('backend.mail.default', ['name' => 'Invoice', 'body' => 'Invoice Message'], function ($message) use ($user, $matches,$value,$mpdf) {
                     $message->subject('Invoice of Veg King!');
                     if($matches->buyerPref->buyer->email != ''){ $message->to($matches->buyerPref->buyer->email); }
                     $message->attachData($mpdf->output("Invoice.pdf",'S'), "Invoice.pdf");
                  });
               }
            }
            if($matches->buyerPref->buyer->contact_sms){
               //SendSMS($matches->buyerPref->buyer->phone, base_path("invoice/invoice.pdf"));
            }

            if($matches->buyerPref->buyer->contact_whatsapp){
               // Implement whats app API to send message
               $content = $mpdf->output("Invoice.pdf",'S');
               $content = chunk_split(base64_encode($content));
               SendWhatsapp(['phone' => $matches->buyerPref->buyer->phone, 'body' => "data:application/pdf;base64,".$content,'filename'=>'Invoice.pdf','caption'=>'Invoice','is_PDF'=>true]);
            }
            //Seller Send
            if($matches->stock->seller->contact_email)
            {
               if(!empty($user) && !empty($matches))
               {
                  $dt = new DateTime();
                  DB::table('offersent')->insert(['match_id' => $matches->id,'stock_id' => $matches->stock_id,'buyer_id'=>$matches->stock->seller->id,'time_sent' => $dt->format('H:i:s'),'created_at'=> $dt->format('Y-m-d H:i:s')]);
                  Mail::send('backend.mail.default', ['name' => 'Invoice', 'body' => 'Invoice Message'], function ($message) use ($user, $matches,$value,$mpdf) {
                     $message->subject('Invoice of Veg King!');
                     if($matches->stock->seller->email != ''){ $message->to($matches->stock->seller->email); }
                     $message->attachData($mpdf->output("Invoice.pdf",'S'), "Invoice.pdf");
                  });
               }
            }
            if($matches->stock->seller->contact_sms){
               //SendSMS($matches->buyerPref->buyer->phone, base_path("invoice/invoice.pdf"));
            }

            if($matches->stock->seller->contact_whatsapp){
               // Implement whats app API to send message
               $content = $mpdf->output("Invoice.pdf",'S');
               $content = chunk_split(base64_encode($content));
               SendWhatsapp(['phone' => $matches->stock->seller->phone, 'body' => "data:application/pdf;base64,".$content,'filename'=>'Invoice.pdf','caption'=>'Invoice','is_PDF'=>true]);
            }
         }
      }
    

    return redirect()->route('admin.matches.index')->with('success','Invoice has been send Successfully.');
  }

  public function matchesexports(Request $request)
  {
     $data = Match::with('matchItem','stock', 'buyerPref.buyer', 'stock.seller', 'stock.product','stock.offerProperty');
      if(isset($request->stock) && !empty($request->stock)){
        $data->where('stock_id', $request->stock);
      }
      if(isset($request->buyer) && !empty($request->buyer)){
        $data->where('buyer_id', $request->buyer);
      }
      if(isset($request->match_type) && !empty($request->match_type)){
         if(@$request->show_matched == 'no'){
            $match_type_array = explode(',',$request->match_type);
            foreach($match_type_array as $match_type){
               $data->whereDoesntHave('matchItem', function($q) use($match_type) {
                      $q->where('name', $match_type);
               });
            }
         }
      }
      if(!isset($request->show_matched) || (isset($request->show_matched) && $request->show_matched=='yes')){
        $data->where('numofmismatches', 0);
      }else{
        $data->where('numofmismatches','>', 0);
      }
      $data = $data->orderBy('numofmismatches', 'asc')->orderBy('total_profit', 'desc')->get();

      return Excel::download(new MatchesExport($data), 'matches.xlsx');
    // return $exporter->download('matches.xlsx');
  }
}
