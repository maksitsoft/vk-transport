<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Offer;
use App\Seller;
use App\Buyer;
use App\Match;
use App\Sale;
use App\PurchaseOrder;
use App\OfferSent;
use DataTables;
use DB;
use DateTime;
use App\AppHead;
use App\Product;
use App\ProductSpecification;
use App\ProductSpecificationValue;
use Mail;
use App\Models\Auth\User;
use App\Exports\OfferSentExport;
use Maatwebsite\Excel\Facades\Excel;

class OffersentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(){
        $this->middleware('permission:view offer sent', ['only' => ['index']]);
    }

    public function index(Request $request)
    {
        
        $stock_list = Offer::with('seller', 'product', 'offerProperty')->where('status', 'listed')->get();
        $buyers_list = Buyer::select('id', 'username')->get();
        $sellers_list = Seller::select('id', 'username')->get();
        $products = Product::all()->where('status', '1')->pluck('name', 'id');
        if(isset($request->product_id) && !empty($request->product_id)){
            $product_id = $request->product_id;
            $ProdSpecArr = ProductSpecification::where('product_id',$request->product_id)->where('parent_id',NULL)->orderBy('order')->limit(3)->pluck('display_name','id')->toArray();
        } else {
            $product = Product::all()->where('status', '1')->first();
            $product_id = @$product->id;
            $ProdSpecArr = ProductSpecification::where('product_id',@$product->id)->where('parent_id',NULL)->orderBy('order')->limit(3)->pluck('display_name','id')->toArray();
        }
        $ProdSpecArrKeys = array_keys($ProdSpecArr);
        $field1 = current($ProdSpecArrKeys);
        if(count($ProdSpecArrKeys) > 1){
            $field2 = next($ProdSpecArrKeys);
        } else {
            $field2 = '';
        }
        if(count($ProdSpecArrKeys) >2){
            $field3 = end($ProdSpecArrKeys);
        } else {
            $field3 = '';
        }
        $ProdSpecArrNames = array();
        $ProdSpecArrNames['field1'] = $field1;
        $ProdSpecArrNames['field2'] = $field2;
        $ProdSpecArrNames['field3'] = $field3;
        $product_spec_values_arr = array();
        foreach($ProdSpecArrKeys as $id){
            $ProductSpecificationValues = ProductSpecificationValue::where('product_specification_id',@$id)->where('parent_id',NULL)->pluck('value','id')->toArray();
            $product_spec_values_arr[$id] = $ProductSpecificationValues; 
        }
        if ($request->ajax()) {
            $data = OfferSent::with('stock','match', 'buyer', 'stock.seller', 'stock.product', 'stock.offerProperty')
            ->join('offers', function($join)
            {
               $join->on('offers.id', '=', 'offersent.stock_id');
            })
            ->where(function ($query) use ($request,$ProdSpecArrNames) {
                if(isset($request->stock) && !empty($request->stock)){
                    $query->where('stock_id', $request->stock);
                }
                if(isset($request->buyer) && !empty($request->buyer)){
                    $query->where('buyer_id', $request->buyer);
                }
                if(isset($request->seller) && !empty($request->seller)){
                    $query->where('seller_id', $request->seller_id);
                }
                if(isset($request->product_id) && !empty($request->product_id)){
                    $query->where('offers.product_id', $request->product_id);
                }
                if(isset($request->field1) && !empty($request->field1)){
                    $query->whereHas('stock.offerProperty',  function($query2) use ($request,$ProdSpecArrNames)  {
                        $query2->where('product_spec_id', '=', @$ProdSpecArrNames['field1'])->where('product_spec_val_id', '=', $request->field1);
                    });
                }
                if(isset($request->field2) && !empty($request->field2)){
                    $query->whereHas('stock.offerProperty',  function($query2) use ($request,$ProdSpecArrNames)  {
                        $query2->where('product_spec_id', '=', @$ProdSpecArrNames['field2'])->where('product_spec_val_id', '=', $request->field2);
                    });
                }
                if(isset($request->field3) && !empty($request->field3)){
                    $query->whereHas('stock.offerProperty',  function($query2) use ($request,$ProdSpecArrNames)  {
                        $query2->where('product_spec_id', '=', @$ProdSpecArrNames['field3'])->where('product_spec_val_id', '=', $request->field3);
                    });
                }
                if(isset($request->offer_sent_date) && !empty($request->offer_sent_date)){
                    $query->whereDate('offersent.created_at',date("Y-m-d H:i:s",strtotime($request->offer_sent_date)));
                }
              })->orderBy('offers.id', 'DESC')
            ->select('offersent.id as offerID','offersent.match_id','offersent.stock_id','offersent.buyer_id','offersent.time_sent','offersent.created_at','offersent.updated_at','offers.*')
            ->get();
            //echo "<pre/>"; print_r($data); die;
            return Datatables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function($row){
                     $btn = ' <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-success sendInvoice" title="Send PDF" data-url-send="'.route('admin.matchestemp.send_Invoice', $row->offerID).'" data-viewurl="'.route('admin.matchestemp.view_Invoice', $row->offerID).'"><i class="fas fa-file-invoice"></i></button>
                              </div>';
                      return $btn;
              })
              ->addColumn('id', function($row){
                return (@$row->offerID?@$row->offerID:'-');
            })
              ->addColumn('buyer_name', function($row){
                    return (@$row->buyer->username?@$row->buyer->username:'-');
                })
                ->addColumn('buyer_company', function($row){
                    return (@$row->buyer->company?@$row->buyer->company:'-');
                })
                ->addColumn('product_name', function($row){
                    return (@$row->stock->product->name?@$row->stock->product->name:'-');
                })
                ->addColumn('field1', function($row) use ($field1) {
                    $arrFields  = array();
                    foreach($row->stock->offerProperty as $prop){
                        $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
                    }
                    if(is_array($arrFields)&& !empty($arrFields[@$field1])){
                        return implode(', ',@$arrFields[@$field1]);
                    } else {
                        return '-';
                    }
                })
                ->addColumn('field2', function($row) use ($field2){
                    $arrFields  = array();
                    foreach($row->stock->offerProperty as $prop){
                        $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
                    }
                    if(is_array($arrFields)&& !empty($arrFields[@$field2])){
                        return implode(', ',@$arrFields[@$field2]);
                    } else {
                        return '-';
                    }
                })
                ->addColumn('field3', function($row) use ($field3){
                    if($field3 == ''){
                        return '';
                    } else {
                        $arrFields  = array();
                        foreach($row->stock->offerProperty as $prop){
                            $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
                        }
                        if(is_array($arrFields)&& !empty($arrFields[@$field3])){
                            return implode(', ',@$arrFields[@$field3]);
                        } else {
                            return '-';
                        };
                    }
                })
                ->addColumn('size_from', function($row){
                    return (@$row->stock->size_from?@$row->stock->size_from:'-');
                })
                ->addColumn('size_to', function($row){
                    return (@$row->stock->size_to?@$row->stock->size_to:'-');
                })
                ->addColumn('quantity', function($row){
                    return (@$row->stock->quantity?@$row->stock->quantity:'-');
                })
                ->addColumn('price', function($row){
                    return (@$row->stock->price?@$row->stock->price:'-');
                })
          ->rawColumns(['action'])
          ->make(true);
          }

        return view('backend.offersent.index', compact('stock_list', 'buyers_list','sellers_list','ProdSpecArr','ProdSpecArrNames','ProdSpecArrKeys','product_spec_values_arr','products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Send Invoice to partoicular buyer
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send_Invoice(Request $request,$id){ 
        $OfferSent = OfferSent::where('id', @$id)->with('buyer')->first();
        $user = auth()->user();
        $url = url('/') .'/img/'. Settings()->site_logo;
        $matches = Match::with('buyerPref.buyer','stock', 'stock.seller', 'stock.product','offerproperty.productspec','offerproperty.productspecvalue')->where('id', $OfferSent->match_id)->first();
        $tmp = json_decode(@$matches->stock->packaging,true);
        $packaging = (is_array($tmp) ? implode(',',@$tmp) : '');
        $purpose_tmp = json_decode(@$matches->stock->purposes,true);
        $purpose = (is_array($purpose_tmp) ? implode(',',@$purpose_tmp) : '');
        if(!empty($user) && !empty($matches))
        {
            $mpdf = new \Mpdf\Mpdf();
            $template = utf8_encode(view('backend.matches.invoice',compact('matches','user','packaging','purpose')));
            $mpdf->WriteHTML($template);
            if($OfferSent->buyer->contact_email){

                Mail::send('backend.mail.default', ['name' => 'Invoice', 'body' => 'Invoice Message'], function ($message) use ($matches,$user,$OfferSent,$mpdf) {
                    $message->subject('Invoice of Veg King!');
                    
                    if($OfferSent->buyer->email != ''){ $message->to($OfferSent->buyer->email); }
                    $message->cc($user->email);
                    $message->attachData($mpdf->output("Invoice.pdf",'S'), "Invoice.pdf");
                });
            }
        }
        return redirect()->route('admin.offersent.index')->with('success','Invoice send Successfully.');
    }

     /**
     * View Invoice to partoicular buyer
     */
    public function view_Invoice(Request $request, $id){
        $OfferSent = OfferSent::where('id', @$id)->first();
        $user = auth()->user();
        $url = url('/') .'/img/'. Settings()->site_logo;
        $matches = Match::with('stock', 'stock.seller', 'stock.product','offerproperty.productspec','offerproperty.productspecvalue')->where('id', $OfferSent->match_id)->first();
        $tmp = json_decode(@$matches->stock->packaging,true);
        $packaging = (is_array($tmp) ? implode(',',@$tmp) : '');
        $purpose_tmp = json_decode(@$matches->stock->purposes,true);
        $purpose = (is_array($purpose_tmp) ? implode(',',@$purpose_tmp) : '');
        if(!empty($user) && !empty($matches))
        {
            $mpdf = new \Mpdf\Mpdf();
            $template = view('backend.matches.invoice',compact('matches','user','packaging','purpose'));
            $mpdf->WriteHTML($template->render());
            $mpdf->Output();
        }else{
            return redirect()->route('admin.matches.index')->with('error','Invalid Request');
        }
    }

      /** Use for export Excel data */
  public function offersentexports() 
  {
      return Excel::download(new OfferSentExport, 'offersent.xlsx');
  }
}
