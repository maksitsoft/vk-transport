<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sale;
use App\SaleTruck;
use App\Buyer;
use App\Match;
use App\Offer;
use App\Saledelivery;
use App\PurchaseOrder;
use App\Loadstatus;
use App\Product;
use App\TransLoad;
use App\Transportlist;
use DataTables;
use App\BuyerPaymentDetails;
use App\Exports\SalesExport;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use Mail;

class SaleController extends Controller{
	
	public $transportwhatsapp;
	public $transportmail;
    public function __construct(){
        $this->transportwhatsapp = '+919429484296';
        $this->transportmail = 'dev.kretoss@gmail.com';
        $this->middleware('permission:view sales', ['only' => ['index']]);
        $this->middleware('permission:add sales', ['only' => ['create','store']]);
        $this->middleware('permission:edit sales', ['only' => ['edit','update']]);
        $this->middleware('permission:delete sales', ['only' => ['destroy']]);
        $this->middleware('permission:view sales', ['only' => ['show']]);
        $this->middleware('permission:export sales', ['only' => ['show']]);
    }

    public function index(Request $request){
      if ($request->ajax()) {
        $data = Sale::with('buyer', 'stock', 'paymentType', 'paymentTerms', 'currencyId')->get();
		 return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('action', function($row){
                    $checkstatus = Transportlist::where('salesid',$row->id)->where('salestatus','rejected')->first();
                    if(!empty($checkstatus)){
                        $btn = ' <div class="btn-group btn-group-sm">';
                            if(auth()->user()->can('edit sales')){
                                $btn .= '<button type="button" class="btn btn-edit editItem" data-url="'.route('admin.sales.edit', $row->id).'"><i class="fas fa-edit"></i></button>';
                            }
                            if(auth()->user()->can('view sales')){
                                $btn .= '<button type="button" class="btn btn-primary viewItem" data-url="'.route('admin.sales.show', $row->id).'"><i class="fas fa-eye"></i></button>';
                            }
                            if(auth()->user()->can('delete sales')){
                                $btn .= '<button data-toggle="tooltip" data-id="'.$row->id.'" title="Delete" data-placement="top" data-original-title="Delete" type="button" class="btn btn-danger deleteItem"><i class="fas fa-trash-alt"></i></button>';
                            }
                            if(auth()->user()->can('sales - view PDF')){
                                $btn .= '<button type="button" class="btn btn-warning viewInvoice" title="View PDF" data-viewurl="'.route('admin.sales.SaleInvoiceView', $row->id).'"><i class="fas fa-file-invoice"></i></button>';
                            }
                            $btn .= '</div>';
                        } else {
                            $btn = ' <div class="btn-group btn-group-sm">
                                    <button type="button" class="btn btn-primary viewItem" data-url="'.route('admin.sales.show', $row->id).'"><i class="fas fa-eye"></i></button>
                                    <button type="button" class="btn btn-warning viewInvoice" title="View PDF" data-viewurl="'.route('admin.sales.SaleInvoiceView', $row->id).'"><i class="fas fa-file-invoice"></i></button>
                                </div>';
                        }
                  return $btn;
          })
			->addColumn('price', function($row){
				return  currency(@$row->price);
			})
			->addColumn('buyer_name', function($row){
				return (@$row->buyer->username?@$row->buyer->username:'-');
			})
			->addColumn('payment_terms_name', function($row){
				return (@$row->paymentTerms->name?@$row->paymentTerms->name:'-');
			})
			->addColumn('payment_type_name', function($row){
				return (@$row->paymentType->name?@$row->paymentType->name:'-');
			})
			->addColumn('currency_name', function($row){
				return (@$row->currencyId->name?@$row->currencyId->name:'-');
			})
          ->rawColumns(['action'])
          ->make(true);
      }

      return view('backend.sales.index');
    }

    public function create(){
		$stockid = Offer::pluck('id','id');
        $buyers = Buyer::where('status', '1')->pluck('username','id');
		$loads_status = Loadstatus::get();
        $payment_types = \App\AppHead::where('type', 'payment_type')->pluck('name','id');
		$payment_terms = \App\AppHead::where('type', 'payment_terms')->pluck('name','id');
		$currencyIds = \App\AppHead::where('type', 'currency')->pluck('name','id');
		return view('backend.sales.create',compact('buyers','loads_status', 'stockid', 'payment_types', 'payment_terms', 'currencyIds'));
	}

    public function store(Request $request){
		$arr = array();
foreach($request->truck as $date){
	$arr[] = $date['delivery_date'];
}
//dd($arr);

		$all = $request->all();
		if(isset($request->truck)){
	
			foreach($request->truck as $keyMain => $truck){
				if (is_array($truck)){

					$valid_array = array();
					foreach($truck as $key => $val){
						if(is_array($val))
						{
							$valid_Val['price'] = 'truck.'.$keyMain.'.'.$key.'.price';
							$valid_Val['sale_date'] = 'truck.'.$keyMain.'.'.$key.'.sale_date';
							$valid_Val['delivery_location'] = 'truck.'.$keyMain.'.'.$key.'.delivery_location';
							$valid_Val['delivery_date'] = 'truck.'.$keyMain.'.'.$key.'.delivery_date';
							$valid_Val['truck_loads'] = 'truck.'.$keyMain.'.'.$key.'.truck_loads';
							array_push($valid_array,$valid_Val);
						}
						
					}
				}
			}
		}
		if(isset($valid_array[0]) && $valid_array[0]!=''){
			foreach($valid_array as $key => $value){
				if(isset($value)){
					$request->validate([
						'buyer_id' => 'required',
						'match_id' => 'required',
						'stock_id' => 'required',
						'quantity' => 'required',
						'payment_term' => 'required',
						'payment_type' => 'required',
						'payment_currency' => 'required',
						'defect_percentage'=> 'required',
						'truck.*.delivery_date'=>'required',
						'truck.*.number_of_loads'=>'required',
						$value['price']=>'required',
						$value['sale_date']=>'required',
						$value['delivery_location']=>'required',
						$value['delivery_date']=>'required',
						$value['truck_loads']=>'required',
					  ],[
						'truck.*.delivery_date.required' => 'The delivery date field is required.',
						'truck.*.number_of_loads.required' => 'The number of loads field is required.',
						$value['price'].'.required' => 'The price field is required.',
						$value['sale_date'].'.required' => 'The sale date field is required.',
						$value['delivery_location'].'.required' => 'The delivery location field is required.',
						$value['delivery_date'].'.required' => 'The delivery date field is required.',
						$value['truck_loads'].'.required' => 'The truck loads field is required.',
					  ]);
				}
			}
		}else{
			$request->validate([
				'buyer_id' => 'required',
				'match_id' => 'required',
				'stock_id' => 'required',
				'quantity' => 'required',
				'payment_term' => 'required',
				'payment_type' => 'required',
				'payment_currency' => 'required',
				'defect_percentage'=> 'required',
				'truck.*.delivery_date'=>'required',
				'truck.*.number_of_loads'=>'required',
			],[
				'truck.*.delivery_date.required' => 'The delivery date field is required.',
				'truck.*.number_of_loads.required' => 'The number of loads field is required.',
			]);
		}
			$total = 0;
		$main_total = 0;
		//echo "ads";exit;
		
		foreach($all['truck'] as $truck){
			if (is_array($truck)){
				//dd($truck);
				foreach($truck as $key => $val){
					if(is_array($val))
					{
						$total += $val['price'];
						$main_total += $val['price'];
					}
					
				}
			}
		}
		$sale = Sale::create(['buyer_id' => $request->buyer_id, 'match_id' => $request->match_id, 'stock_id' => $request->stock_id, 'quantity' => $request->quantity, 'price' => $total,'payment_term' => $request->payment_term, 'payment_type' => $request->payment_type, 'payment_currency' => $request->payment_currency, 'status' => $request->status,'defect_percentage'=>$request->defect_percentage]);
		
		$index = 1;
		$delay = array();
		//dd($all['truck']);
		foreach($all['truck'] as $ky => $delivery){
			if (is_array($delivery)){
				//dd($truck);
				 
				$deliveryid = Saledelivery::create(['salesid' => $sale->id, 'deliverymain' => date('Y-m-d',strtotime($delivery['delivery_date']))]);
				foreach($delivery as $key => $truck){
					if(is_array($truck))
					{
						$total += $val['price'];
						$SaleTruck = SaleTruck::create(['sale_id' => $sale->id, 'sale_date' => date('Y-m-d',strtotime($truck['sale_date'])), 'delivery_date' => date('Y-m-d',strtotime($truck['delivery_date'])), 'price' => isset($truck['price'])?$truck['price']:'0', 'delivery_location'=>$truck['delivery_location'], 'truck_loads'=> $truck['truck_loads'], 'load_status'=>$truck['load_status'], 'number_delivery'=>$index, 'deliveryid' => $deliveryid->id]);
						$index++;
					}
				}
			}
		}
		//dd($delay);
		$checkloadno = Saledelivery::where('salesid',$sale->id)->get();
		foreach($checkloadno as $deleid){
			$countlod = SaleTruck::where('deliveryid',$deleid->id)->count();	
			Saledelivery::where('id',$deleid->id)->update(
         array(
                 'loadcount' => $countlod
              )
         );
		}
		/* $user = auth()->user();
		$url = url('/') .'/img/'. Settings()->site_logo;
		$sales = Sale::with('stock', 'stock.seller', 'stock.product','buyer','trucksone')->where('id', @$sale->id)->first();
	 
		Mail::send('backend.mail.default', ['name' => 'Sales created', 'body' => '<div stype="color:#000">New Sale created.</div>'], function ($message) use ($user,$sales) {
					$message->subject('Sales created of Veg King!');
					  $message->to($sales->stock->seller->email); 
		}); */
		
		$SaleTruck = SaleTruck::where('sale_id',$sale->id)->groupBy('number_delivery')->get();
			foreach($SaleTruck as $saletruckvalue){
				$Transportlist = Transportlist::create(['salesid' => $sale->id, 'carrier'=>"0",'trailer_type'=>"0",'temperature'=>"0",'plate_numbers'=>"0",'drivers_name'=>"0",'drivers_phone_number'=>"0",'salestatus'=>"unplanned"])->id;
				
				$SaleTruck_load = SaleTruck::where('sale_id',$sale->id)->where('number_delivery',$saletruckvalue->number_delivery)->get();
				foreach($SaleTruck_load as $saletruckloadvalue){
					 $offer = Offer::with('product', 'seller','offerProperty','offerProperty.productSpec','offerProperty.productSpecValue')->where('id', $sale->stock_id)->first();
					if($offer!=NULL)
					{
						$product_name = @$offer->product->id;
						$size_from = @$offer->size_from;
						$size_to = @$offer->size_to;
						
						$offerPropertiesArr = array();
						foreach($offer->offerProperty as $productPref){
							if(isset($productPref->productspec)){
								$offerPropertiesArr[$productPref->productspec->display_name][] = (($productPref->productspec->field_type == 'dropdown_switchboxes')?@$productPref->productspecvalue->value:$productPref->value);
							}
						}
						foreach($offerPropertiesArr as $display_name=>$arr){
							$offerProperties[$display_name] = implode(', ',$arr??array());
						}
						
						$TransLoad = TransLoad::create(['salesid' => isset($sale->id)?$sale->id:'', 'goods'=>isset($product_name)?$product_name:'','variety'=>'0','size_from'=>isset($size_from)?$size_from:'','size_to'=>isset($size_to)?$size_to:'','loaded_weight'=>'0','unloaded_weight'=>'0','difference'=>'0','packaging_type'=>'','number_of_packing_units'=>isset($saletruckloadvalue->truck_loads)?$saletruckloadvalue->truck_loads:'','requirements'=>'','freight_cost'=>isset($saletruckloadvalue->price)?$saletruckloadvalue->price:'','payment_term'=>isset($sale->payment_term)?$sale->payment_term:'','payment_type'=>isset($sale->payment_type)?$sale->payment_type:'','transport_invoice_no'=>'','transport_invoice_due_date'=>'','payment_status'=>isset($sale->payment_status)?$sale->payment_status:'','transport_id'=>isset($Transportlist)?$Transportlist:'']);
					}
				}
			}

		$user = auth()->user();
		$url = url('/') .'/img/'. Settings()->site_logo;
		$sales = Sale::with('stock', 'stock.seller', 'stock.product','buyer','trucksone')->where('id', @$sale->id)->first();
		$PurchaseOrder = PurchaseOrder::create(['buyer_id' => $request->buyer_id,'delivery_date'=>@$request->delivery_date,'seller_id' => @$sales->stock->seller->id,'stock_id' => $request->stock_id,'price' => $main_total,'sale_id' => @$sale->id]);
		$mpdf = new \Mpdf\Mpdf();
		$template = utf8_encode(view('backend.sales.invoice',compact('sales','user','PurchaseOrder')));
      	$mpdf->WriteHTML($template);
        $seller_confirm_url = url('confirm-order/'.encrypt(@$PurchaseOrder->id));
		$seller_edit_loads = url('edit-order/'.encrypt(@$PurchaseOrder->id));
		$seller_message = "<b>Hello</b>! ".$sales->stock->seller->username.",
			Click here to Confirm Purchase order:<a href='".$seller_confirm_url."' class='btn btn-success'>Confirm Order</a>
			Click here to Edit Loads Details:<a href='".$seller_edit_loads."' class='btn btn-success'>Edit Order</a>
			Regaurds,
			VegKing\n
			<small>if you're having troble clicking the 'Confirm Order' button,copy and paste the URL below into your web browser: $seller_confirm_url</small>
			";
		 
		if($sales->stock->seller->contact_email){
			if(!empty($user) && !empty($sales))
			{
				$dt = new DateTime();
				Mail::send('backend.mail.default', ['name' => 'Invoice', 'body' => '<div stype="color:#000">'.$seller_message.'</div>'], function ($message) use ($user,$sales,$mpdf) {
					$message->subject('Invoice of Veg King!');
					if($sales->stock->seller->email != ''){ $message->to($sales->stock->seller->email); }
					// $message->cc($user->email);
					$message->attachData($mpdf->output("Invoice.pdf",'S'), "Invoice.pdf");
				});
			}else{
				return redirect()->route('admin.sales.index')->with('error','Invalid Request');
			}
		}
		if($sales->stock->seller->contact_whatsapp){
		//Whatsapp
			$content = $mpdf->output("Invoice.pdf",'S');
         	$content = chunk_split(base64_encode($content));
         	SendWhatsapp(['phone' => $sales->stock->seller->phone, 'body' => "data:application/pdf;base64,".$content,'filename'=>'Invoice.pdf','caption'=>'Invoice','is_PDF'=>true]);
		}
		if($sales->stock->seller->contact_sms){
		//Text
		//SendSMS($sellers->phone, $seller_url);
		}
		//$TransLoad = TransLoad::create(['salesid' => isset($sale->id)?$sale->id:'', 'goods'=>isset($product_name)?$product_name:'','variety'=>'0','size_from'=>isset($size_from)?$size_from:'','size_to'=>isset($size_to)?$size_to:'','loaded_weight'=>'0','unloaded_weight'=>'0','difference'=>'0','packaging_type'=>'','number_of_packing_units'=>isset($saletruckloadvalue->truck_loads)?$saletruckloadvalue->truck_loads:'','requirements'=>'','freight_cost'=>isset($saletruckloadvalue->price)?$saletruckloadvalue->price:'','payment_term'=>isset($sale->payment_term)?$sale->payment_term:'','payment_type'=>isset($sale->payment_type)?$sale->payment_type:'','transport_invoice_no'=>'','transport_invoice_due_date'=>'','payment_status'=>isset($sale->payment_status)?$sale->payment_status:'','transport_id'=>isset($Transportlist)?$Transportlist:'']);
		
		//SendWhatsapp(['phone' => "+91",'body' => "New Sale create. Transload id ".$TransLoad->id]);
		 $TransLoadget = TransLoad::where('salesid',$sale->id)->get();
		 
		 foreach($TransLoadget as $transget){
			SendWhatsapp(['phone' => $this->transportwhatsapp,'body' => "New Sale create. Transload id ".$transget->id]);
			SendWhatsapp(['phone' => $sales->stock->seller->phone,'body' => "New Sale create. Transload id ".$transget->id]); 
		
 	Mail::send('backend.mail.default', ['name' => 'Sales created', 'body' => '<div stype="color:#000">New Sale created. TransLoad Id '.$transget->id.'</div>'], function ($message) use ($user,$sales) {
					$message->subject('Sales created of Veg King!');
					  $message->to($sales->stock->seller->email); 
		});
		Mail::send('backend.mail.default', ['name' => 'Sales created', 'body' => '<div stype="color:#000">New Sale created. TransLoad Id '.$transget->id.'</div>'], function ($message) use ($user,$sales) {
					$message->subject('Sales created to transport Veg King!');
					  $message->to($this->transportmail); 
		});  		

		}
		
		
		
	/*	Mail::send('backend.mail.default', ['name' => 'Sales created', 'body' => '<div stype="color:#000">New Sale created. TransLoad Id '.$TransLoad->id.'</div>'], function ($message) use ($user,$sales) {
					$message->subject('Sales created of Veg King!');
					  $message->to($sales->stock->seller->email); 
		});
		Mail::send('backend.mail.default', ['name' => 'Sales created', 'body' => '<div stype="color:#000">New Sale created. TransLoad Id '.$TransLoad->id.'</div>'], function ($message) use ($user,$sales) {
					$message->subject('Sales created to transport Veg King!');
					  $message->to($this->transportmail); 
		}); */
		
		/*
		if($sale->wasRecentlyCreated){
			$offer = Offer::find($request->offer_id);
			$offer_request = OfferRequest::find($request->request_id);
			$seller_message = 'Dear '.$offer->seller->first_name.',
			Your offer matched for '.$offer_request->product->name.' ('.$offer_request->variety.'), Flesh Color: '.$offer_request->flesh_color.', Size: '.$offer_request->size_from.' - '.$offer_request->size_to.', QTY: '.$offer_request->quantity.', Price: '.$offer_request->price_from.' - '.$offer_request->price_to.', location: '.$offer_request->location_from.' - '.$offer_request->location_to;
			$buyer_message = 'Dear '.$offer_request->buyer->first_name.',
			Your request matched for '.$offer->product->name.' ('.$offer->variety.'), Flesh Color: '.$offer->flesh_color.', Size: '.$offer->size.', QTY: '.$offer->quantity.', Price: '.$offer->price.', location: '.$offer->location_from.' - '.$offer->location_to;

          //SendSMS($offer->seller->phone, $seller_message);
          //SendSMS($offer_request->buyer->phone, $buyer_message);
          //SendWhatsapp(['phone' => $offer->seller->phone, 'body' => $seller_message]);
          //SendWhatsapp(['phone' => $offer_request->buyer->phone, 'body' => $buyer_message]);
        }
		*/
		//return redirect()->route('admin.sales.index')->with('success','Sale created successfully.');
		return response()->json(['status' => 'success', 'message' => 'Sale created successfully!']);
	}
	public function show($id){
		$sale = Sale::where(['id' => $id])->first();
		if($sale){
			$sale = Sale::with('buyer', 'stock', 'paymentType', 'paymentTerms', 'currencyId')->where('id', $sale->id)->first();
			$offer = Offer::with('product', 'seller')->where('id', @$sale->stock_id)->first();
			$saleTrucks = SaleTruck::where('sale_id', $sale->id)->get();
			return view('backend.sales.show',compact('sale','offer','saleTrucks'));
		 }else{
		   $msg="Unfortunately this sales is not exist!";
		  return view('backend.sales.index', compact('msg'));
		 } 
	  }
	  public function edit($id){
		$sale = Sale::where(['id' => $id])->first();
		if($sale){
			$stockid = Offer::pluck('id','id');
		$buyers = Buyer::where('status', '1')->pluck('username','id');
        $matches = Match::pluck('id','id');
		$payment_types = \App\AppHead::where('type', 'payment_type')->pluck('name','id');
		$payment_terms = \App\AppHead::where('type', 'payment_terms')->pluck('name','id');
		$currencyIds = \App\AppHead::where('type', 'currency')->pluck('name','id');
		$saleTrucks = SaleTruck::where('sale_id', $sale->id)->get();
		$saledelivery = Saledelivery::where('salesid', $sale->id)->get();
		$loads_status = Loadstatus::get();
		$currentsaleTrucks = current($saleTrucks);
		
 //echo "<pre/>"; print_r($sale->id); die;
		return view('backend.sales.edit',compact( 'buyers','loads_status' ,'matches', 'payment_types', 'payment_terms', 'currencyIds', 'sale', 'saleTrucks','saledelivery','stockid'));
		 }else{
		  $msg="Unfortunately this sales is not exist!";
		  return view('backend.sales.index',compact('msg'));
		 } 
	  }

    public function update(Request $request, Sale $sale){
		$all = $request->all();
		if(isset($request->truck)){
			$valid_array = array();
			foreach($request->truck as $keyMain => $truck){
				if (is_array($truck)){
					
					foreach($truck as $key => $val){
						if(is_array($val))
						{
							
							$valid_Val['price'] = 'truck.'.$keyMain.'.'.$key.'.price';
							$valid_Val['sale_date'] = 'truck.'.$keyMain.'.'.$key.'.sale_date';
							$valid_Val['delivery_location'] = 'truck.'.$keyMain.'.'.$key.'.delivery_location';
							$valid_Val['delivery_date'] = 'truck.'.$keyMain.'.'.$key.'.delivery_date';
							$valid_Val['truck_loads'] = 'truck.'.$keyMain.'.'.$key.'.truck_loads';
							array_push($valid_array,$valid_Val);
						}
						
					}
				}
			}
		}
		if(isset($valid_array[0]) && $valid_array[0]!=''){
			
			foreach($valid_array as $key => $value){
				if(isset($value)){
					
					$request->validate([
						'buyer_id' => 'required',
						'match_id' => 'required',
						'stock_id' => 'required',
						'quantity' => 'required',
						'payment_term' => 'required',
						'payment_type' => 'required',
						'payment_currency' => 'required',
						'defect_percentage'=> 'required',
						'truck.*.delivery_date'=>'required',
						'truck.*.number_of_loads'=>'required',
						$value['price']=>'required',
						$value['sale_date']=>'required',
						$value['delivery_location']=>'required',
						$value['delivery_date']=>'required',
						$value['truck_loads']=>'required',
					  ],[
						'truck.*.delivery_date.required' => 'The delivery date field is required.',
						'truck.*.number_of_loads.required' => 'The number of loads field is required.',
						$value['price'].'.required' => 'The price field is required.',
						$value['sale_date'].'.required' => 'The sale date field is required.',
						$value['delivery_location'].'.required' => 'The delivery location field is required.',
						$value['delivery_date'].'.required' => 'The delivery date field is required.',
						$value['truck_loads'].'.required' => 'The truck loads field is required.',
					  ]);
				}
			}
		}else{
			
			$request->validate([
				'buyer_id' => 'required',
				'match_id' => 'required',
				'stock_id' => 'required',
				'quantity' => 'required',
				'payment_term' => 'required',
				'payment_type' => 'required',
				'payment_currency' => 'required',
				'defect_percentage'=> 'required',
				'truck.*.delivery_date'=>'required',
				'truck.*.number_of_loads'=>'required',
			],[
				'truck.*.delivery_date.required' => 'The delivery date field is required.',
				'truck.*.number_of_loads.required' => 'The number of loadssss field is required.',
			]);
		}
		
		$total = 0;

		if($request->has('truck')){
		foreach($all['truck'] as $truck){
			$total += (isset($truck['price'])) ? $truck['price'] : 0;
		}
        }
		$tableArray = ['buyer_id' => $request->buyer_id, 'match_id' => $request->match_id, 'stock_id' => $request->stock_id, 'quantity' => $request->quantity, 'price' => $total,'payment_term' => $request->payment_term, 'payment_type' => $request->payment_type, 'payment_currency' => $request->payment_currency, 'status' => $request->status,'defect_percentage'=>$request->defect_percentage];

		$sale->update($tableArray);
		$saleTrucks = SaleTruck::where('sale_id', $sale->id)->get();
		foreach($saleTrucks as $saleTruck){
			$saleTruck->delete();
		}
		$Saledelivery = Saledelivery::where('salesid', $sale->id)->get();
		foreach($Saledelivery as $Saledelivery){
			$Saledelivery->delete();
		}
		if($request->has('truck')){
		$index = 1;
		foreach($all['truck'] as $ky => $delivery){
			if (is_array($delivery)){
				$deliveryid = Saledelivery::create(['salesid' => $sale->id, 'deliverymain' => date('Y-m-d',strtotime($delivery['delivery_date'])),'loadcount' => $delivery['number_of_loads']]);
				foreach($delivery as $key => $truck){
					if(is_array($truck))
					{
						$total += $val['price'];
						$SaleTruck = SaleTruck::create(['sale_id' => $sale->id, 'sale_date' => date('Y-m-d',strtotime($truck['sale_date'])), 'delivery_date' => date('Y-m-d',strtotime($truck['delivery_date'])), 'price' => isset($truck['price'])?$truck['price']:'0', 'delivery_location'=>$truck['delivery_location'], 'truck_loads'=> $truck['truck_loads'], 'load_status'=>$truck['load_status'], 'number_delivery'=>$index, 'deliveryid' => $deliveryid->id]);
						$index++;
					}
				}
			}
		}
		}
		$sale->update($request->all());
		return response()->json(['status' => 'success', 'message' => 'Sale updated successfully!']);
		//return redirect()->route('admin.sales.index')->with('success','Sale undated successfully.');
    }

    public function destroy(Sale $sale){
        $sale->delete();
        return response()->json(['success'=>'Sale deleted successfully.']);
    }

    public function getbuyerpaymentprefAjax(Request $request){
        $buyers = BuyerPaymentDetails::with('paymentType','paymentTerms','currencyId')->where('buyer_id', $request->buyer_id)->get();
		$payment_options = '<option value="">Select</option>';
		foreach($buyers as $buyer){
			$option = '<option value="'.@$buyer->payment_type.'_'.@$buyer->payment_terms.'_'.@$buyer->currency.'">'.ucfirst(@$buyer->paymentType->name).", ".@$buyer->paymentTerms->name." days, ".@$buyer->currencyId->name.'</option>';
			$payment_options .= $option;
		}

        return response()->json(['payment_options'=>$payment_options]);
    }

	public function getmatchAjax(Request $request){
        $match = Match::where('id', $request->match_id)->first();
		$offer = Offer::with('product', 'seller', 'variety_detail', 'packing_detail', 'flesh_color_detail')->where('id', @$match->stock_id)->first();

		$seller_username = @$offer->seller->username;
		$product_name = @$offer->product->name;
		$variety_detail_name = @$offer->variety_detail->name;
		$packing_detail_name = @$offer->packing_detail->name;
		$size_from = @$offer->size_from;
		$size_to = @$offer->size_to;
		$flesh_color_detail_name = @$offer->flesh_color_detail->name;
		//echo "<pre/>"; print_r($offer); die;
		return response()->json(['stock_id'=>@$match->stock_id,'seller_username'=>$seller_username,'product_name'=>$product_name,'variety_detail_name'=>$variety_detail_name,'packing_detail_name'=>$packing_detail_name,'size_from'=>$size_from,'size_to'=>$size_to,'flesh_color_detail_name'=>$flesh_color_detail_name,'location'=>$offer->country,'postalcode'=>$offer->postalcode]);
    }

    public function getStockAjax(Request $request){

        $offer = Offer::with('product', 'seller','offerProperty','offerProperty.productSpec','offerProperty.productSpecValue')->where('id', @$request->stock_id)->first();
		if($offer!=NULL)
		{
			$seller_username = @$offer->seller->username;
			$product_name = @$offer->product->name;
			$size_from = @$offer->size_from;
			$size_to = @$offer->size_to;
			$offerProperties = ['Seller'=>$seller_username,'Product'=>$product_name,'Size From'=>$size_from,'Size To'=>$size_to,'Location'=>$offer->country,'Postal Code'=>$offer->postalcode];

			$offerPropertiesArr = array();
			foreach($offer->offerProperty as $productPref){
				if(isset($productPref->productspec)){
					$offerPropertiesArr[$productPref->productspec->display_name][] = (($productPref->productspec->field_type == 'dropdown_switchboxes')?@$productPref->productspecvalue->value:$productPref->value);
				}
			}
			foreach($offerPropertiesArr as $display_name=>$arr){
				$offerProperties[$display_name] = implode(', ',$arr??array());
			}
			return response()->json($offerProperties);
		}
		else
			return "";
    }

	public function saleexports()
    {
        return Excel::download(new SalesExport, 'sales.xlsx');
    }

	public function saletotran()
    {
		$TransLoadq = TransLoad::get();
		$trans_arr = array();

		foreach($TransLoadq as $TransLoadv){
			$trans_arr []= $TransLoadv->salesid;
		}

		$sales = Sale::whereNotIn('id',$trans_arr)->get();
		
		foreach($sales as $sale){
			$SaleTruck = SaleTruck::where('sale_id',$sale->id)->groupBy('number_delivery')->get();
			foreach($SaleTruck as $saletruckvalue){
				$Transportlist = Transportlist::create(['salesid' => $sale->id, 'carrier'=>"0",'trailer_type'=>"0",'temperature'=>"0",'plate_numbers'=>"0",'drivers_name'=>"0",'drivers_phone_number'=>"0",'salestatus'=>"unplanned"])->id;
				
				$SaleTruck_load = SaleTruck::where('sale_id',$sale->id)->where('number_delivery',$saletruckvalue->number_delivery)->get();
				foreach($SaleTruck_load as $saletruckloadvalue){
					 $offer = Offer::with('product', 'seller','offerProperty','offerProperty.productSpec','offerProperty.productSpecValue')->where('id', $sale->stock_id)->first();
					if($offer!=NULL)
					{
						$product_name = @$offer->product->id;
						$size_from = @$offer->size_from;
						$size_to = @$offer->size_to;
						
						$offerPropertiesArr = array();
						foreach($offer->offerProperty as $productPref){
							if(isset($productPref->productspec)){
								$offerPropertiesArr[$productPref->productspec->display_name][] = (($productPref->productspec->field_type == 'dropdown_switchboxes')?@$productPref->productspecvalue->value:$productPref->value);
							}
						}
						foreach($offerPropertiesArr as $display_name=>$arr){
							$offerProperties[$display_name] = implode(', ',$arr??array());
						}
						
						$TransLoad = TransLoad::create(['salesid' => isset($sale->id)?$sale->id:'', 'goods'=>isset($product_name)?$product_name:'','variety'=>'0','size_from'=>isset($size_from)?$size_from:'','size_to'=>isset($size_to)?$size_to:'','loaded_weight'=>'0','unloaded_weight'=>'0','difference'=>'0','packaging_type'=>'','number_of_packing_units'=>isset($saletruckloadvalue->truck_loads)?$saletruckloadvalue->truck_loads:'','requirements'=>'','freight_cost'=>isset($saletruckloadvalue->price)?$saletruckloadvalue->price:'','payment_term'=>isset($sale->payment_term)?$sale->payment_term:'','payment_type'=>isset($sale->payment_type)?$sale->payment_type:'','transport_invoice_no'=>'','transport_invoice_due_date'=>'','payment_status'=>isset($sale->payment_status)?$sale->payment_status:'','transport_id'=>isset($Transportlist)?$Transportlist:'']);
						
						$salesdata = Sale::with('stock', 'stock.seller', 'stock.product','buyer','trucksone')->where('id', @$sale->id)->first();
						
						SendWhatsapp(['phone' => $this->transportwhatsapp,'body' => "New Sale create. Transload id ".$TransLoad->id]);
						SendWhatsapp(['phone' => $salesdata->stock->seller->phone,'body' => "New Sale create. Transload id ".$TransLoad->id]);
						
						Mail::send('backend.mail.default', ['name' => 'Sales created', 'body' => '<div stype="color:#000">New Sale created. TransLoad Id '.$TransLoad->id.'</div>'], function ($message) use ($user,$sales) {
									$message->subject('Sales created of Veg King!');
									  $message->to($salesdata->stock->seller->email); 
						});
						Mail::send('backend.mail.default', ['name' => 'Sales created', 'body' => '<div stype="color:#000">New Sale created. TransLoad Id '.$TransLoad->id.'</div>'], function ($message) use ($user,$sales) {
									$message->subject('Sales created to transport Veg King!');
									  $message->to($this->transportmail); 
						});
					
					}
				}
			}
			
			//print_r($sale->stock->product_id);
		}
		return response()->json(['status' => 'success', 'message' => 'Send to transport successfully.']);
		exit;
	//	return redirect()->route('admin.sales.index');
	}
	public function InvoiceView(Request $request,Sale $sale){
		$user = auth()->user();
		$url = url('/') .'/img/'. Settings()->site_logo;
		$sales = Sale::with('stock', 'stock.seller', 'stock.product','buyer','trucksone')->where('id', @$sale->id)->first();
		$PurchaseOrder = PurchaseOrder::where('sale_id', @$sale->id)->select('id')->first();
		if(!empty($user) && !empty($sales))
		{
			$mpdf = new \Mpdf\Mpdf();
			$template = view('backend.sales.invoice',compact('sales','user','PurchaseOrder'));
			$mpdf->WriteHTML($template->render());
			$mpdf->Output();
		}else{
			return redirect()->route('admin.sales.index')->with('error','Invalid Request');
		}
	}
}
