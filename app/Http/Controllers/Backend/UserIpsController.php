<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\UserIps;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Models\Auth\UserTracking;
use DataTables;
use Carbon\Carbon;
class UserIpsController extends Controller
{

public function index(Request $request){
		
	  if ($request->ajax()) {
        $data =  UserIps::select('userips.*', 'users.id as u_id','users.first_name','users.last_name')
                     ->leftjoin('users', 'users.id', '=', 'userips.userid')
                     ->get();
        return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('name', function($row){
          	if($row->first_name != ''){
                if(strtoupper($row->didLogin) == 'YES'){
                  $name = '<a href="auth/user/'.$row->u_id.'" target="_blank">'.$row->first_name.' '.$row->last_name.'</a>';
                }else{
                    $name = '<a href="auth/user/'.$row->u_id.'" target="_blank">'.$row->first_name.' '.$row->last_name.'</a>';
                }
          	}else{
          		 $name = 'Guest User';
          	}
                return $name;
            })
          ->addColumn('City', function($row){
                return $row->city;
            })
          ->addColumn('Country', function($row){
                return $row->country;
            })
          ->addColumn('didlogin', function($row){
                if($row->first_name != ''){
                  return ucfirst($row->didlogin);  
                }else{
                  return 'No';
                }
                
            })
			->addColumn('date', function($row){
               $time = Carbon::create($row); 
                return $time;
            })
          ->addColumn('time', function($row){
                $time = Carbon::create($row); 
                return $time;
            })
          
          ->rawColumns(['name','City', 'Country','Time','Date'])   
          ->make(true);
        }
    
      return view('backend.UserIps.index');
    }
	
	public function usertracking(Request $request){
	 
	  if ($request->ajax()) {
        $data =  UserTracking::with('userdata')->orderBy('id','desc')->get();
		
		return Datatables::of($data)
			->addIndexColumn()
			->editColumn('user_id', function($row){
          	  		   
                return @$row->userdata->first_name.' '.@$row->userdata->last_name;
			  
            })
			->editColumn('date_time', function($row){
               //$time = Carbon::create($row); 
			   return date("h:i A",strtotime($row->date_time));
                
            })
          ->addColumn('date', function($row){
                //$time = Carbon::create($row); 
                return date("M-j-Y",strtotime($row->date_time));
            })  
          ->make(true);
		  
        /*return Datatables::of($data)
          ->addIndexColumn()
          ->make(true);*/
        }
    
      return view('backend.UserIps.usertracking');
    }

}    