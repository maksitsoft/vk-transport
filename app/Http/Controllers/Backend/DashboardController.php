<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Buyer;
use App\BuyerPref;
use App\BuyerProductPref;
use App\ProductSpecificationValue;
use App\ProductSpecification;
use App\PostalCode;
use App\Product;
use DB;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
    	$roles = auth_roles();
    	if(in_array("administrator", $roles) || in_array("seller", $roles)){
    		return view('backend.dashboard');
    	}elseif(in_array("buyer", $roles)){
	    	$buyer = \App\Buyer::where('user_id', auth()->user()->id)->first();
	        $buyer = Buyer::where('buyers.id',$buyer->id)->with('buyer_prefs','payment_details')->first();

	        $price = PostalCode::select('price')->where(DB::raw('substr(postal_code,1,2)'),substr($buyer->postalcode,0,2))->orWhere('name',$buyer->city)->first();
	        $buyerprefs = BuyerPref::where('buyer_prefs.buyer_id',$buyer->id)->get();
	        $productPrefRel = array();
	        $productProdRel = array();
	       
	        foreach($buyerprefs as $buyerpref){
	            $productProdRel[$buyerpref->id]['product_id'] = $buyerpref->product_id;
	            $productProdRel[$buyerpref->id]['delivery_city'] = $buyerpref->city;
	            $productProdRel[$buyerpref->id]['delivery_street'] = $buyerpref->street;
	            $productProdRel[$buyerpref->id]['delivery_postalcode'] = $buyerpref->postalcode;
	            $productProdRel[$buyerpref->id]['delivery_country'] = $buyerpref->country;
	            $buyerProductPref = $buyerpref->productPrefs()->select('buyer_pref_id','key','value','premium')->get()->toArray();
	            $productPrefsMapping = array();
	            $productPrefsMappingPremiums = array();
	            foreach($buyerProductPref as $productPref){
	              $productPrefsMapping[$productPref['key']][] = $productPref['value'];    
	              $productPrefsMappingPremiums[$productPref['key']][$productPref['value']] = $productPref['premium'];
	            }
	            
	            $product_list = Product::all()->where('status',1)->pluck('name','id');
	            $productspecification_list = ProductSpecification::with('options')
	            ->where('product_id',$buyerpref->product_id)->where('parent_id',null)->orderBy('importance','desc')->get();	            
	            //echo "<pre/>"; print_r($productspecification_list);exit;
	            foreach($productspecification_list as $spec){
	                $productPrefRel[$buyerpref->id][$spec->id]['name'] = $spec->display_name;
	                $productPrefRel[$buyerpref->id][$spec->id]['hasmany'] = $spec->buyer_hasmany;
	                $productPrefRel[$buyerpref->id][$spec->id]['buyer_pref_anylogic'] = $spec->buyer_pref_anylogic;
	                $productPrefRel[$buyerpref->id][$spec->id]['field_type'] = $spec->field_type;
	                if($spec->buyer_hasmany == 'Yes'){
	                    $productPrefRel[$buyerpref->id][$spec->id]['default'] = @$productPrefsMapping[$spec->id];
	                } else {
	                    $productPrefRel[$buyerpref->id][$spec->id]['default'] = current($productPrefsMapping[$spec->id]??array());
	                }
	                $productPrefRel[$buyerpref->id][$spec->id]['options'] = array();
	                foreach($spec->options as $option){
	                  if($spec->buyer_hasmany == 'Yes'){
	                      $productPrefRel[$buyerpref->id][$spec->id]['options'][$option->id]['name'] = $option->value;
	                      $productPrefRel[$buyerpref->id][$spec->id]['options'][$option->id]['premium'] = @$productPrefsMappingPremiums[$spec->id][$option->id];
	                  } else {
	                      $productPrefRel[$buyerpref->id][$spec->id]['options'][$option->id] = $option->value;
	                  }
	                }
	            }
	        }
	        //echo "<pre/>"; print_r($productProdRel); die;
	        $products = Product::all()->where('status', '1')->pluck('name', 'id');        
	        return view('backend.dashboard',compact('buyer','price','products','productPrefRel','productProdRel'));
    	}
    }

    public function getQuotes()
    {
    	$buyer = \App\Buyer::where('user_id', auth()->user()->id)->first();
    	$buyers_data = array(
    		'name' => auth()->user()->name,
    		'buyer_id' => $buyer->id,
    		'phone' => $buyer->phone,
    	);    	
    	if(\Spatie\Permission\Models\Role::where('name','trader')->exists()){
            $roles[] = 'trader';
        }
        if(\Spatie\Permission\Models\Role::where('name','trader admin')->exists()){
            $roles[] = 'trader admin';
        }
    	$users = \App\Models\Auth\User::role(@$roles)->get();
    	//print_r($users->toArray());exit;
    	foreach(@$users as $user){
         \Mail::to($user->email, $user->first_name.' '.$user->last_name)->send(new \App\Mail\Buyer\GetQuotes($user,$buyers_data));
         $locale = \App::getLocale();
  			$email_content = '';
      	$email_template = get_email_template('GET QUOTES');
         if($email_template){
             if($locale == 'pl'){
             	$email_content = $email_template->sms_content_pl;
             }else{
             	$email_content = $email_template->sms_content;
             }
				$email_content = str_replace("[team_member_name]", $user->name, $email_content);
				$email_content = str_replace("[name]", auth()->user()->name, $email_content);
				$email_content = str_replace("[phone]", (substr(trim($buyer->phone),0,1) != '+' ? '+' : '').$buyer->phone, $email_content);
				$email_content = str_replace("[view_buyer_link]", route('admin.buyers.show',$buyer->id), $email_content); 
            SendWhatsapp(['phone' => (!empty(@$user->whatsapp_number)?$user->whatsapp_number:@$user->phone), 'body' => $email_content,'is_PDF'=>false]);
            SendSMS(@$user->sms_number,$email_content);
			}
     }
     return redirect()->back()->with('success', 'Thank you, Our Sales team will reach you shortly.');
    }
}
