<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Offer;
use App\OfferProperty;
use App\Product;
use App\Seller;
use App\ProductSpecification;
use App\ProductSpecificationValue;
use App\Models\Auth\User;
use DataTables;
use App\Events\Backend\CheckMatchesForStock;
use App\Exports\StocksExport;
use App\Imports\SellerImport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class StockController extends Controller{

    protected $roles;

    function __construct(){
        $this->middleware('permission:view stock', ['only' => ['index']]);
        $this->middleware('permission:add stock', ['only' => ['create','store']]);
        $this->middleware('permission:edit stock', ['only' => ['edit','update']]);
        $this->middleware('permission:delete stock', ['only' => ['destroy']]);
        $this->middleware('permission:view stock detail', ['only' => ['show']]);
        $this->middleware('permission:export stocks', ['only' => ['stocksexports']]);
    }

    public function index(Request $request){
        $products = Product::all()->where('status', '1')->pluck('name', 'id');
        if(isset($request->product_id) && !empty($request->product_id)){
            $product_id = $request->product_id;
            $ProdSpecArr = ProductSpecification::where('product_id',$request->product_id)->where('parent_id',NULL)->whereIn('type_name',['Variety','Packing','Quality'])->select('display_name','id','type_name')->get()->toArray();
        } else {
            $product = Product::all()->where('status', '1')->first();
            $product_id = @$product->id;
            $ProdSpecArr = ProductSpecification::where('product_id',$product_id)->where('parent_id',NULL)->whereIn('type_name',['Variety','Packing','Quality'])->select('display_name','id','type_name')->get()->toArray();
        }
        
        $ProdSpecArrKeys = array();
        foreach($ProdSpecArr as $type_data){
            $ProdSpecArrKeys[$type_data['type_name']] = $type_data['id'];
        }
       
        $field1 = @$ProdSpecArrKeys['Variety'];
        $field2 = @$ProdSpecArrKeys['Packing'];
        $field3 = @$ProdSpecArrKeys['Quality'];
        //echo $field1; die;
        $ProdSpecArrNames = array();
        $ProdSpecArrNames['field1'] = $field1;
        $ProdSpecArrNames['field2'] = $field2;
        $ProdSpecArrNames['field3'] = $field3;
        //echo "<pre/>"; print_r($ProdSpecArrNames); die;
        if($request->ajax()) {
            $data = Offer::with('product', 'seller','offerProperty','offerProperty.productSpec','offerProperty.productSpecValue')->where('product_id',$product_id)->orderBy('id','desc')->get();
            if(auth()->user()->hasRole('seller')){
                $seller_id =  auth()->user()->id;
                $data = Offer::with('product', 'seller','offerProperty','offerProperty.productSpec','offerProperty.productSpecValue')->where('seller_id', $seller_id)->get();
            }
            //echo "<pre/>"; print_r($data); die;
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if(in_array('seller', auth_roles())){
                    $route_pre = 'seller';
                }else{
                    $route_pre = 'admin';
                }
                $btn = ' <div class="btn-group btn-group-sm">';
                if(auth()->user()->can('edit stock')){
                    //$btn .= '<button type="button" class="btn btn-edit editItem" data-url="'.route($route_pre.'.stock.edit', $row->id).'"><i class="fas fa-edit"></i></button>'; 
                }
                if(auth()->user()->can('view stock detail')){
                    //$btn .= '<button type="button" class="btn btn-primary viewItem" data-url="'.route($route_pre.'.stock.show', $row->id).'"><i class="fas fa-eye"></i></button>'; 
                } 
                if(auth()->user()->can('delete stock')){
                    $btn .= '<button data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="Delete" type="button" class="btn btn-danger deleteItem"><i class="fas fa-trash-alt"></i></button>'; 
                }
                $btn .= '</div>';
                return $btn;
            })
            ->addColumn('price', function($row){
                return  currency($row->price);
            }) 
            ->addColumn('raw_price', function($row){
                return  @$row->price;
            })
            ->addColumn('raw_stock_status', function($row){
                return  @$row->stock_status;
            })
            ->addColumn('stock_status', function($row){
                return ucfirst(@$row->stock_status);
            })
            ->addColumn('seller_username', function($row){
                return (@$row->seller->username?@$row->seller->username:'-');
            })
            ->addColumn('field1', function($row) use ($field1) {
                $arrFields  = array();
                 
                foreach($row->offerProperty as $prop){
                    if(@$prop->productSpec->field_type == 'dropdown_switchboxes'){
                        $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
                    } else {
                        $arrFields[$prop->product_spec_id][] = @$prop->value;
                    }
                }
               
                //echo "<pre>"; print_r($arrFields); die;
                if(is_array($arrFields)&& !empty($arrFields[@$field1])){
                    return implode(', ',@$arrFields[@$field1]);
                } else {
                    return '-';
                }
            })
            ->addColumn('field2', function($row) use ($field2){
                $arrFields  = array();
                foreach($row->offerProperty as $prop){
                    if(@$prop->productSpec->field_type == 'dropdown_switchboxes'){
                        $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
                    } else {
                        $arrFields[$prop->product_spec_id][] = @$prop->value;
                    }
                }
                if(is_array($arrFields)&& !empty($arrFields[@$field2])){
                    return implode(', ',@$arrFields[@$field2]);
                } else {
                    return '-';
                }
            })
            ->addColumn('field3', function($row) use ($field3){
                if($field3 == ''){
                    return '';        
                } else {
                    $arrFields  = array();
                    foreach($row->offerProperty as $prop){
                        if(@$prop->productSpec->field_type == 'dropdown_switchboxes'){
                            $arrFields[$prop->product_spec_id][] = @$prop->productSpecValue->value;
                        } else {
                            $arrFields[$prop->product_spec_id][] = @$prop->value;
                        }
                    }
                    if(is_array($arrFields)&& !empty($arrFields[@$field3])){
                        return implode(', ',@$arrFields[@$field3]);
                    } else {
                        return '-';
                    };
                }
            })
            ->addColumn('product_name', function($row){
                return (@$row->product->name?@$row->product->name:'-');
            })
            ->addColumn('variety_detail_name', function($row){
                return (@$row->variety_detail->name?@$row->variety_detail->name:'-');
            })
           ->addColumn('size', function($row){
                return (@$row->size_from.'-'.@$row->size_to);
            })
            ->addColumn('pallets', function($row){
                if($row->pallets_available == 1) 
                {
                    $pallet = 'Yes';
                }
                else
                {
                    $pallet = 'No';
                }
                return (@$pallet);
            })
           
            ->rawColumns(['action','price'])
            ->make(true);
        }
        $products = Product::all()->where('status', '1')->pluck('name', 'id', 'image');
       // echo "<pre/>"; print_r($products); die;
        return view('backend.stockv2.index',compact('ProdSpecArr','ProdSpecArrNames','products'));
    }

  public function matchingRequests(Request $request,$offer = null){
  
    $data = Offer::with('product', 'seller','offerProperty','offerProperty.productSpec','offerProperty.productSpecValue')->where('seller_id',$offer)->get();
    //echo "<pre/>"; print_r($data);  die;    
    return Datatables::of($data)
    ->addIndexColumn()
    ->addColumn('action', function($row){
      if(in_array('seller', auth_roles())){
        $route_pre = 'seller';
      }else{
        $route_pre = 'admin';
      }
      $btn = ' <div class="btn-group btn-group-sm">
      <button type="button" class="btn btn-primary editItem" data-url="'.route($route_pre.'.stock.edit', $row->id).'"><i class="fas fa-edit"></i></button>
      <button type="button" class="btn btn-success viewItem" data-url="'.route($route_pre.'.stock.show', $row->id).'"><i class="fas fa-eye"></i></button>
      <button data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="Delete" type="button" class="btn btn-danger deleteItem"><i class="fas fa-trash-alt"></i></button>
      </div>';
      return $btn;
    })
    ->addColumn('price', function($row){
      return  currency($row->price);
    })
    ->addColumn('seller_username', function($row){
      return (@$row->seller->username?@$row->seller->username:'-');
    })
    ->addColumn('product_name', function($row){
      return (@$row->product->name?@$row->product->name:'-');
    })
    
    ->rawColumns(['action','price'])
    ->make(true);
  }

  public function applyMatchingRequest(Request $request){
    $offer = Offer::find($request->offer_id);
    $offer_request = OfferRequest::find($request->request_id);

    $sale = Sale::updateOrCreate(
      ['offer_id' => $request->offer_id, 'request_id' => $request->request_id],
      ['offer_id' => $request->offer_id, 'request_id' => $request->request_id, 'price' => $offer->price]
    );

    if($sale->wasRecentlyCreated){
      $seller_message = 'Dear '.$offer->seller->first_name.',
      Your offer matched for '.$offer_request->product->name.' ('.$offer_request->variety.'), Flesh Color: '.$offer_request->flesh_color.', Size: '.$offer_request->size_from.' - '.$offer_request->size_to.', QTY: '.$offer_request->quantity.', Price: '.$offer_request->price_from.' - '.$offer_request->price_to.', location: '.$offer_request->location_from.' - '.$offer_request->location_to;
      $buyer_message = 'Dear '.$offer_request->buyer->first_name.',
      Your request matched for '.$offer->product->name.' ('.$offer->variety.'), Flesh Color: '.$offer->flesh_color.', Size: '.$offer->size.', QTY: '.$offer->quantity.', Price: '.$offer->price.', location: '.$offer->location_from.' - '.$offer->location_to;

      SendSMS($offer->seller->phone, $seller_message);
      SendSMS($offer_request->buyer->phone, $buyer_message);
      SendWhatsapp(['phone' => $offer->seller->phone, 'body' => $seller_message,'is_PDF'=>false]);
      SendWhatsapp(['phone' => $offer_request->buyer->phone, 'body' => $buyer_message,'is_PDF'=>false]);
    }

  }

    public function create(){
        if(auth()->user()->confirmed != 1){
            return redirect()->route(home_route());         
        }
        $products = Product::all()->where('status', '1')->pluck('name', 'id', 'image');
        $product_image = Product::where('status', '1')->get();
        $sellers = Seller::where('status', '1')->select('id', 'username', 'name', 'city', 'postalcode', 'address', 'country')->get();
        //echo "<pre/>"; print_r($sellers); die;
        $product = \App\AppHead::where('name','Like','Potato')->where('type', 'product')->select('id', 'image')->first();
        $stock_image = @$product->image;
        
        return view('backend.stockv2.create',compact('products', 'sellers', 'stock_image', 'product', 'product_image'));
    }

    public function store(Request $request)
    {
        if(auth()->user()->confirmed != 1){
            return redirect()->route(home_route());         
        }
      
            request()->validate([
                'product' => 'required',
                'price' => 'required',
                'sizeto' => 'required',
                'sizefrom' => 'required'
            ],[
              'product.required' => 'Product is required.',
            ]);
        
       
        // if(empty($request->seller_id)){
            // $validate_array = [];
        // } else {
            // $validate_array = ['seller_id' => 'required|not_in:0'];
        // }
            
        //$all = $request->all();
        //$stocks = $all['stock'];
        // foreach($_POST as $stock){
            // $validate_array['stock.'.$key.'.product_id'] = 'required';
            // $validate_array['stock.'.$key.'.city'] = 'required';
            // $validate_array['stock.'.$key.'.postalcode'] = 'required';
            // $validate_array['stock.'.$key.'.price'] = 'required';
        // }
        //echo "<pre/>"; print_r($validate_array);
        //$this->validate($request, $validate_array);
        //echo "<pre/>"; print_r($all); die;
      
        //foreach($stocks as $key=>$stock){
            //foreach($_POST as $_POST){
            
            $tableArray = array();
            $tableArray['product_id'] = $_POST['product'];
            $tableArray['size_from'] = $_POST['sizefrom'];
            $tableArray['size_to'] = $_POST['sizeto'];
            $tableArray['city'] = '';
            $tableArray['postalcode'] = '';
            $tableArray['street'] = '';
            $tableArray['country'] = '';
            $tableArray['price'] = $_POST['price'];
            $tableArray['stock_status'] = $_POST['stock_status'];
            $tableArray['image'] = '';
           
            if(empty($request->seller_id)){
                $tableArray['seller_id']= auth()->user()->id;
            } else {
               $tableArray['seller_id']=$request->seller_id;
            }
            $offer = Offer::create($tableArray);
         // print_r($offer->id); die;
            //echo "<pre/>"; print_r($offer); 
            //echo "data add in offer table";
            //die;
            if(!empty($_POST['field1val'])){
                $data = [
                    'offer_id'=>$offer->id,
                    'product_spec_id' => $_POST['field1id'],
                    'product_spec_val_id' => $_POST['field1val']
                    ];
                OfferProperty::create($data);
            }
            if(!empty($_POST['field2val'])){
                $data = [
                    'offer_id'=>$offer->id,
                    'product_spec_id' => $_POST['field2id'],
                    'product_spec_val_id' => $_POST['field2val']
                    ];
                OfferProperty::create($data);
            } 
            if(!empty($_POST['field3val'])){    
                                $data = [
                                'offer_id'=>$offer->id,
                                'product_spec_id' => $_POST['field3id'],
                                'product_spec_val_id' => $_POST['field3val']
                                ];
                OfferProperty::create($data);
            }                    
                                
                              
                                
           
            
            // $productspecification_list = ProductSpecification::with('options')->where('product_id',$_POST['product_id'])->where('parent_id',null)->pluck('field_type','id')->toArray();
          // echo "<pre/>"; print_r($productspecification_list); die;
            // $offer_id = $offer->id;
            // $specification = @$stock['specification'];
            // if(is_array($specification) && !empty($specification)){
                // foreach(@$specification as $product_spec_id=>$specValue)
                // {
                    // if($productspecification_list[$product_spec_id] == 'dropdown_switchboxes'){
                        // if(is_array($specValue))
                        // {
                            // foreach($specValue as $specValueKey=>$specValueValue){
                                // $data = [
                                    // 'offer_id'=>$offer_id,
                                    // 'product_spec_id' => $product_spec_id,
                                    // 'product_spec_val_id' => $specValueValue
                                    // ];
                                // OfferProperty::create($data);
                            // }
                        // }  else {
                            // $data = [
                                // 'offer_id'=>$offer_id,
                                // 'product_spec_id' => $product_spec_id,
                                // 'product_spec_val_id' => $specValue,
                                // ];
                            // OfferProperty::create($data);        
                        // }
                    // } else if($productspecification_list[$product_spec_id] == 'inputfield'){
                        // $data = [
                                // 'offer_id'=>$offer_id,
                                // 'product_spec_id' => $product_spec_id,
                                // 'value' => $specValue
                                // ];
                        // OfferProperty::create($data);       
                    // } else if($productspecification_list[$product_spec_id] == 'optionrange'){
                        // $data = [
                                // 'offer_id'=>$offer_id,
                                // 'product_spec_id' => $product_spec_id,
                                // 'value' => $specValue['size_from'].'-'.$specValue['size_to']
                                // ];
                        // OfferProperty::create($data);
                    // }           
                // }
            // }
        
        // }
        
        
        //$stock = Offer::with('offerProperty.productSpecValue')->where('id',$offer_id)->first();
        //event(new CheckMatchesForStock($stock));
        
        

        return response()->json(['status' => 'success', 'message' => 'Stock added successfully!']);
           
    }

    public function show(Offer $stock){
        $offerProperty = $stock->offerProperty()->with('productSpec','productSpecValue')->get();
        $offerPropertyArr = array();
        foreach($offerProperty as $productPref){
            if(isset($productPref->productspec)){
                $offerPropertyArr[$productPref->productspec->display_name][] =  (($productPref->productspec->field_type == 'dropdown_switchboxes')?@$productPref->productspecvalue->value:@$productPref->value);
            }
        }
        //$stock['image'] = trim($stock['image'], '["*"]');
        //echo "<pre/>"; print_r($offerPropertyArr); die;
        return view('backend.stock.show',compact('stock','offerProperty','offerPropertyArr'));
    }

    public function edit(Offer $stock){
        //echo "<pre/>"; print_r($stock->product_id);    
        $offerProperty = $stock->offerProperty()->select('offer_id','product_spec_id','product_spec_val_id','ecs')->get()->toArray();
        $productPrefsMapping = array();
        $ecs = array();
        foreach($offerProperty as $productPref){
            $ecs[$productPref['product_spec_id']][$productPref['product_spec_val_id']] = @$productPref['ecs'];
            $productPrefsMapping[$productPref['product_spec_id']][] = $productPref['product_spec_val_id'];    
        }
        // echo "<pre/>"; print_r($productPrefsMapping);print_r($ecs);
        $product_list = Product::all()->where('status',1)->pluck('name','id');
        $productspecification_list = ProductSpecification::with('options')->where('product_id',$stock->product_id)->where('parent_id',null)->get();
		$productSpecRel = array();
        foreach($productspecification_list as $spec){
            $productSpecRel[$spec->id]['name'] = $spec->display_name;
            $productSpecRel[$spec->id]['hasmany'] = $spec->stock_hasmany;
            $productSpecRel[$spec->id]['can_edit'] = $spec->can_edit;
            $productSpecRel[$spec->id]['field_type'] = $spec->field_type;
            if($spec->stock_hasmany == 'Yes'){
                $productSpecRel[$spec->id]['default'] = @$productPrefsMapping[$spec->id];
            } else {
                $productSpecRel[$spec->id]['default'] = current($productPrefsMapping[$spec->id]??array());
            }
			$productSpecRel[$spec->id]['options'] = array();
            foreach($spec->options as $option){
               if($spec->stock_hasmany == 'Yes'){
                    $productSpecRel[$spec->id]['options'][$option->id]['name'] = $option->value;
                    $productSpecRel[$spec->id]['options'][$option->id]['premium'] = @$productPrefsMappingPremiums[$spec->id][$option->id];
                    $productSpecRel[$spec->id]['options'][$option->id]['ec'] = $option->ec;
                    $productSpecRel[$spec->id]['options'][$option->id]['ecbf'] = $option->ecbf;
                } else {
                    $productSpecRel[$spec->id]['options'][$option->id] = $option->value;
                }
            }
        }
        $productspecificationval_list = ProductSpecificationValue::with('product_specification')->where('product_id',$stock->product_id)->whereNotNull('parent_id')->get();
        $productSpecChildRelation = array();
        foreach($productspecificationval_list as $childItem){
            if($childItem->product_specification->reference_id != ''){
               $productSpecChildRelation[$childItem->parent_id]['reference_id'] =  $childItem->product_specification->reference_id;
               $productSpecChildRelation[$childItem->parent_id]['value'] =  $childItem->value;
            }
        }
        
        //echo "<pre/>"; print_r($productSpecChildRelation); die;
        $products = Product::all()->where('status', '1')->pluck('name', 'id');
        $sellers = Seller::where('status', '1')->select('id', 'username', 'name')->get();
        $product = \App\AppHead::where('name','Like','Potato')->where('type', 'product')->select('id', 'image')->first();
        $stock_image = @$product->image;
        return view('backend.stock.create',compact('products','stock', 'sellers', 'stock_image','product','productSpecRel','ecs','productSpecChildRelation'));
    }

    public function update(Request $request, Offer $stockv2){
       // echo "<pre/>"; print_r($stockv2); die;
        //echo "<pre/>"; print_r($request->all());
        request()->validate([
                'product_id' => 'required',
                'price' => 'required',
                'size_to' => 'required',
                'size_from' => 'required'
            ],[
              'product.required' => 'Product is required.',
            ]);
        foreach($request->all() as $key=>$val){
            $tableArray[$key]=$val;
        }
        if($request->image){
            if(count((array)$request->image) > 6){
                return response()->json(['status' => 'error', 'message' => 'Max 6 images can be uploaded!']);
            }
            $imageName2 = array();
            $i = 0;
            foreach($request->image as $file){
                $i++;
                $tmp = time().'_'.$i.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('images/stock'), $tmp);
                $imageName2[] = $tmp;
            }

            if($request->exp_image){
                $imageName = time().'.'.request()->exp_image->getClientOriginalExtension();
                request()->exp_image->move(public_path('images/stock'), $imageName);
            }
            $json_fields = array('exp_image');
            
            $tableArray['image'] = json_encode(@$imageName2);
            foreach($request->all() as $key=>$val){
                if (in_array($key, $json_fields)) {
                    $tableArray[$key] = @$imageName;
                }
            }
        }

       
        $tableArray['stock_status'] = $request->stock_status;
        $stockv2->update($tableArray);
        
        $all = $request->all();
        $offer_id = $stockv2->id;
        $specification = @$all['specification'];
        $offerPropertExists = $stockv2->offerProperty()->pluck('id','id')->toArray();
        //echo "<pre/>"; print_r($offerPropertExists); die;
        if(is_array($specification) && !empty($specification)){
        foreach($specification as $product_spec_id=>$specValue)
        {
            if(is_array($specValue))
            {
                foreach($specValue as $specValueKey=>$specValueValue){
                    $buyerPP = OfferProperty::updateOrCreate(
                        ['offer_id' => $offer_id,'product_spec_id' => $product_spec_id,'product_spec_val_id' => $specValueValue],
                        ['offer_id' => $offer_id,'product_spec_id' => $product_spec_id,'product_spec_val_id' => $specValueValue,'ecs' => @$all['ecs'][$product_spec_id][$specValueKey]]
                    );
                    unset($offerPropertExists[$buyerPP->id]);  
                      
                }
            }  else {
                $buyerPP = OfferProperty::firstOrCreate(
                    ['offer_id' => $offer_id,'product_spec_id' => $product_spec_id,'product_spec_val_id' => $specValue],
                    ['offer_id' => $offer_id,'product_spec_id' => $product_spec_id,'product_spec_val_id' => $specValue]
                );
                unset($offerPropertExists[$buyerPP->id]);  
            }        
        }
        }
        foreach($offerPropertExists as $propId){
            OfferProperty::destroy($propId);
        }
        
        $stock = Offer::with('offerProperty.productSpecValue')->where('id',$stockv2->id)->first();
        // echo '<pre>';print_r($stock->toArray());exit;
        event(new CheckMatchesForStock($stockv2));
        return response()->json(['status' => 'success', 'message' => 'Stock updated successfully!']);
    }

    public function destroy(Offer $stock){
        $stock->delete();
        return response()->json(['success'=>'Stock deleted successfully.']);
    }

    public function getvcolorAjax(Request $request){
        $vcolor = \App\AppHead::where('id', $request->pid)->where('type', 'potato_variety')->select('id', 'color_id')->first();
        return response()->json(['color_id'=>@$vcolor->color_id]);
    }

    public function getproductsampleAjax(Request $request){
        $product = Product::where('id', $request->pid)->first();
        if(!empty($product)){
            $html = '<a href="'.asset('images/products/').'/'.$product->image.'" data-fancybox data-caption="'.$product->name.'"><img src="'.asset('images/products/').'/'.$product->image.'" class="mb-2 img-thumbnail"></a>';
            print_r($html);
            exit;
        }
    }

    public function stocksexports() 
    {
        return Excel::download(new StocksExport, 'stocks.xlsx');
    }

    public function stockimport(Request $request){
        Excel::import(new SellerImport,request()->file('stock_file'));           
        return redirect()->back();
    }
    

}
