<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\EmailTemplate;
use Illuminate\Http\Request;
use DataTables;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
      $data = EmailTemplate::get();
      return Datatables::of($data)
           ->addIndexColumn()
           ->addColumn('action', function($row){
              $btn = '<div class="btn-group btn-group-sm">
                         <button type="button" class="btn btn-edit editItem" data-url="'.route('admin.email-templates.edit', $row->id).'"><i class="fas fa-edit"></i></button>
                         <button data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="Delete" type="button" class="btn btn-danger deleteItem"><i class="fas fa-trash-alt"></i></button>
                       </div>';
               return $btn;
           })
           ->addColumn('status', function($row){
            if($row->status== '1')
             return 'Active';
             else{ return 'Inactive'; }
          })
           ->rawColumns(['action'])
           ->make(true);
      }
     return view('backend.email-templates.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.email-templates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request){
         $request->validate([
             'title' => 'required',
             'email_content' => 'required',
             'email_content_de' => 'required',
             'email_content_pl' => 'required',
         ]);
         if($request->id){
           $emailTemplate = EmailTemplate::find($request->id);
           $emailTemplate->update([
              'title' => $request->title,
              'subject' => $request->subject,
              'email_content' => $request->email_content,
              'email_content_de' => $request->email_content_de,
              'email_content_pl' => $request->email_content_pl,
              'sms_content' => $request->sms_content,
              'sms_content_de' => $request->sms_content_de,
              'sms_content_pl' => $request->sms_content_pl,
              'whatsapp_content' => '',
              'status' => $request->status
           ]);
           return redirect()->route('admin.email-templates.index')->with('success','Template updated successfully.');
           //return response()->json(['status' => 'success', 'message' => 'Template updated successfully.']);
         }else{
           EmailTemplate::create([
              'title' => $request->title,
              'subject' => $request->subject,
              'email_content' => $request->email_content,
              'email_content_de' => $request->email_content_de,
              'email_content_pl' => $request->email_content_pl,
              'sms_content' => $request->sms_content,
              'sms_content_de' => $request->sms_content_de,
              'sms_content_pl' => $request->sms_content_pl,
              'whatsapp_content' => '',
              'status' => $request->status
           ]);
           return response()->json(['status' => 'success', 'message' => 'Template created successfully.']);
         }

     }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplate $emailTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
   
    public function edit($id){
      $emailTemplate = EmailTemplate::where(['id' => $id])->first();
      if($emailTemplate){
        $data = $emailTemplate;
        return view('backend.email-templates.create',compact('data'));
       }else{
        $msg="Unfortunately this EmailTemplate is not exist!";
        return view('backend.email-templates.index',compact('msg'));
       } 
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailTemplate $emailTemplate)
    {
      $request->validate([
        'title' => 'required',
        'email_content' => 'required',
        'email_content_de' => 'required',
        'email_content_pl' => 'required',
      ]);

      $emailTemplate->update([
        'title' => $request->title,
        'subject' => $request->subject,
        'email_content' => $request->email_content,
        'email_content_de' => $request->email_content_de,
        'email_content_pl' => $request->email_content_pl,
        'sms_content' => $request->sms_content,
        'sms_content_de' => $request->sms_content_de,
        'sms_content_pl' => $request->sms_content_pl,
        'whatsapp_content' => '',
        'status' => $request->status
      ]);
      return response()->json(['status' => 'success', 'message' => 'Template updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTemplate $emailTemplate)
    {
      $emailTemplate->delete();
      return response()->json(['success'=>'Template deleted successfully.']);
    }
}
