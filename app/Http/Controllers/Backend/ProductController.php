<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use DataTables;
use App\AppHead;
use App\Offer;
use View;
use App\ProductSpecification;
use App\ProductSpecificationValue;
use App\Exports\ProductExport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller{

    public function __construct()
    {
        $this->middleware('permission:view products', ['only' => ['index']]);
        $this->middleware('permission:add products', ['only' => ['create','store']]);
        $this->middleware('permission:edit products', ['only' => ['edit','update']]);
        $this->middleware('permission:delete products', ['only' => ['destroy']]);
        $this->middleware('permission:export products', ['only' => ['productsexports']]);
    }

    public function index(Request $request){
      //return $data = Product::with('variety_detail', 'packing_detail')->where('status', '1')->get();
        if ($request->ajax()) {
        $data = Product::get();
        return Datatables::of($data)
             ->addIndexColumn()
             ->addColumn('image', function($row){
                $image = '<a href="'.asset('images/products/').'/'.$row->image.'" data-fancybox data-caption="'.$row->name.'"><img src="'.asset('images/products/').'/'.$row->image.'" style="width:50%;" class="mb-2 img-thumbnail" /></a>';
                return $image;
             })
             ->addColumn('homepage_image', function($row){
                $homepage_image = '<a href="'.asset('images/products/').'/'.$row->homepage_image.'" data-fancybox data-caption="'.$row->name.'"><img src="'.asset('images/products/').'/'.$row->homepage_image.'" style="width:50%;" class="mb-2 img-thumbnail" /></a>';
                return $homepage_image;
             })
             ->addColumn('status', function($row){
               if($row->status== '1')
                return 'Active';
                else{ return 'Inactive'; }
             })
            ->addColumn('action', function($row){
                $btn = ' <div class="btn-group btn-group-sm">';
                if(auth()->user()->can('edit products')){
                    $btn .= '<button type="button" class="btn btn-edit editItem" data-url="'.route('admin.products.edit', $row->id).'"><i class="fas fa-edit"></i></button>';
                }
                if(auth()->user()->can('delete products')){
                    $btn .= '<button data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="Delete" type="button" class="btn btn-danger deleteItem"><i class="fas fa-trash-alt"></i></button>';
                }
                $btn .= '</div>';
                return $btn;
             })
             ->rawColumns(['image', 'action','homepage_image'])
             ->make(true);
        }
       return view('backend.products.index');
    }

    public function create(){
        return view('backend.products.create');
    }

    public function store(Request $request){
        //echo "<pre/>"; print_r($request->all()); die;
        $request->validate([
            'name' => 'required',
            'name_pl' => 'required',
            'name_de' => 'required',
            'image' => 'required',
            'homepage_image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            'homepage_image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);

        $image = $request->file('image');
        $name = time().'_1.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('images/products');
        $image->move($destinationPath, $name);

        if($request->required_type == '1')
        {
            $type="Product";
        }else{
            $type = "Service";
        }
        $homepage_image = $request->file('homepage_image');
        $homepage_image_name = time().'_home.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('images/products');
        $homepage_image->move($destinationPath, $homepage_image_name);

        Product::create([
           'name' => $request->name,
           'name_pl' => $request->name_pl,
           'name_de' => $request->name_de,
           'type' => $type,
           'image' => $name,
           'homepage_image' => $homepage_image_name,
           'status' => $request->required
        ]);

        return response()->json(['status' => 'success', 'message' => 'Product created successfully.']);
    }

    public function show(Product $product)
    {
        //
    }
    public function edit($id){
        $product = Product::where(['id' => $id])->first();
        if($product){
            return view('backend.products.edit',compact('product'));
         }else{
          $msg="Unfortunately this Product is not exist!";
          return view('backend.products.index',compact('msg'));
         } 
         
      }

    public function update(Request $request, Product $product){
       // echo "<pre/>"; print_r($request->all()); die;
        if($request->image){
            $request->validate([
                'name' => 'required',
                'name_pl' => 'required',
                'name_de' => 'required',
                'image' => 'required',
                'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
        }else{
            $request->validate([
                'name' => 'required',
                'name_pl' => 'required',
                'name_de' => 'required'
            ]);
        }

        if($request->image){
            $image = $request->file('image');
            $name = time().'_1.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('images/products');
            $image->move($destinationPath, $name);
            $product_data['image'] = $name;
            if($request->old_image && file_exists( public_path('images/products/'.$request->old_image))){
                unlink(public_path('images/products/'.$request->old_image));
            }
        }

        if($request->homepage_image){
            $homepage_image = $request->file('homepage_image');
            $homepage_imagename = time().'_home.'.$homepage_image->getClientOriginalExtension();
            $destinationPath = public_path('images/products');
            $homepage_image->move($destinationPath, $homepage_imagename);
            $product_data['homepage_image'] = $homepage_imagename;
            if($request->old_home_image && file_exists( public_path('images/products/'.$request->old_home_image))){
                unlink(public_path('images/products/'.$request->old_home_image));
            }
        }
        if($request->required_type == '1')
        {
            $type="Product";
        }else{
            $type = "Service";
        }

        $product_data['name'] = $request->name;
        $product_data['name_pl'] = $request->name_pl;
        $product_data['name_de'] = $request->name_de;
        $product_data['type'] = $type;
        $product_data['status'] = $request->required;

        $product->update($product_data);
        return response()->json(['status' => 'success', 'message' => 'Product updated successfully.']);
    }

    public function destroy(Product $product){
        $product->delete();
        return response()->json(['success'=>'Product deleted successfully.']);
    }


	public function getProductAjax(Request $request, Product $product)
    {
        if($request->has('stock_id')){
            $offer = Offer::select('id','product_id')->where('id',$request->get('stock_id'))->first();
            $product_id = $offer->product_id;    

            $offerProperty = $offer->offerProperty()->select('offer_id','product_spec_id','product_spec_val_id','ecs')->get()->toArray();
            $productPrefsMapping = array();
            $ecs = array();
            foreach($offerProperty as $productPref){
                $ecs[$productPref['product_spec_id']][$productPref['product_spec_val_id']] = @$productPref['ecs'];
                $productPrefsMapping[$productPref['product_spec_id']][] = $productPref['product_spec_val_id'];   
               
            }

        } else {
            $product_id = $request->pid;
        }            
        
        $product = Product::find($product_id);
        $productspecification_list = ProductSpecification::with('options')->where('product_id',$product_id)->where('parent_id',null)->orderBy('order')->get();
        $productspecificationval_list = ProductSpecificationValue::with('product_specification')->where('product_id',$product_id)->whereNotNull('parent_id')->get();
        $productSpecChildRelation = array();
        foreach($productspecificationval_list as $childItem){
            if($childItem->product_specification->reference_id != ''){
               $productSpecChildRelation[$childItem->parent_id]['reference_id'] =  $childItem->product_specification->reference_id;
               $productSpecChildRelation[$childItem->parent_id]['value'] =  $childItem->value;
            }
        }
        //echo "<pre/>"; print_r($productSpecChildRelation); die;
        $productSpecRel = array();
        foreach($productspecification_list as $spec){
            $productSpecRel[$spec->id]['name'] = $spec->display_name;
            $productSpecRel[$spec->id]['hasmany'] = $spec->stock_hasmany;
            $productSpecRel[$spec->id]['can_edit'] = $spec->can_edit;
            $productSpecRel[$spec->id]['required'] = $spec->required;
            $productSpecRel[$spec->id]['field_type'] = $spec->field_type;
            if($spec->stock_hasmany == 'Yes'){
                
                 if($request->has('stock_id')){
                $productSpecRel[$spec->id]['default'] = @$productPrefsMapping[$spec->id];
                 } else {
                     $productSpecRel[$spec->id]['default'] = array();
                 }
            } else {if($request->has('stock_id')){
                $productSpecRel[$spec->id]['default'] = current($productPrefsMapping[$spec->id]??array());
                 } else {
                     $productSpecRel[$spec->id]['default'] = array();
                 }
                
                
            }
            $productSpecRel[$spec->id]['options'] = array();
            foreach($spec->options as $option){
                if($option->default == 1){
                    $productSpecRel[$spec->id]['default'][] = $option->id;
                }
                if($spec->stock_hasmany == 'Yes'){
                    $productSpecRel[$spec->id]['options'][$option->id]['name'] = $option->value;
                    $productSpecRel[$spec->id]['options'][$option->id]['premium'] = 0;
                    $productSpecRel[$spec->id]['options'][$option->id]['ec'] = @$option->ec;
                } else {
                    $productSpecRel[$spec->id]['options'][$option->id] = $option->value;
                }
            }
        }
        $data = array('product_id'=> $product_id,'productSpecRel' => $productSpecRel,'productSpecChildRelation' => $productSpecChildRelation);
        //echo "<pre/>"; print_r($productSpecChildRelation); die;
        return response()->view('backend.products.stock-product-pref', $data, 200);
    }
    
    public function getProductStockAjax(Request $request, Product $product)
    {

        $productspecification_list = ProductSpecification::with('options')->where('product_id',$_POST['productid'])->where('parent_id',null)->whereIn('type_name',['Variety','Packing','Quality'])->get();
        $spec_array = array();
        $spec_ids = array();

        foreach($productspecification_list as $productspec){
            $spec_array[$productspec->type_name] = array();
            $spec_ids[$productspec->type_name] = $productspec->id;
            foreach($productspec->options as $productspecval){
               $spec_array[$productspec->type_name][$productspecval->id] =  $productspecval->value;
            }
        }
        return response()->json([
            'Variety' => @$spec_array['Variety'],
            'Packing' => @$spec_array['Packing'],
            'Quality' => @$spec_array['Quality'],
            'Variety_id' => @$spec_ids['Variety'],
            'Packing_id' => @$spec_ids['Packing'],
            'Quality_id' => @$spec_ids['Quality']
        ]);
        $productspecificationval_list = ProductSpecificationValue::with('product_specification')->where('product_id',$_POST['productid'])->where('product_specification_id',$productspecid )->get();
        $product = Product::find($request->pid);
        $pref_id = $request->pref_id;
        $show_more = $request->show_more;
        $show_more_btn = false;
        if($show_more == 'true'){
            $productspecification_list = ProductSpecification::with('options')->where('product_id',$request->pid)->where('parent_id',null)->orderBy('importance','desc')->offset(3)->limit(10)->get();
        } else {
            $productspecification_list = ProductSpecification::with('options')->where('product_id',$request->pid)->where('importance','>=',50)->where('parent_id',null)->orderBy('importance','desc')->limit(3)->get();
            $productspecification_list_others = ProductSpecification::select('id')->where('product_id',$request->pid)->where('parent_id',null)->orderBy('importance','desc')->offset(3)->limit(10)->get();
            if(count($productspecification_list_others) > 0){
                $show_more_btn = true;
            }
        }

        
        $productspecificationval_list = ProductSpecificationValue::with('product_specification')->where('product_id',$request->pid)->whereNotNull('parent_id')->get();
        $productSpecChildRelation = array();
        foreach($productspecificationval_list as $childItem){
            if($childItem->product_specification->reference_id != ''){
               $productSpecChildRelation[$childItem->parent_id]['reference_id'] =  $childItem->product_specification->reference_id;
               $productSpecChildRelation[$childItem->parent_id]['value'] =  $childItem->value;
            }
        }
        //echo "<pre/>"; print_r($productspecification_list); die;
        $productSpecRel = array();
        foreach($productspecification_list as $spec){
            $productSpecRel[$spec->id]['name'] = $spec->display_name;
            $productSpecRel[$spec->id]['hasmany'] = $spec->stock_hasmany;
            $productSpecRel[$spec->id]['can_edit'] = $spec->can_edit;
            $productSpecRel[$spec->id]['required'] = $spec->required;
            $productSpecRel[$spec->id]['field_type'] = $spec->field_type;
            if($spec->stock_hasmany == 'Yes'){
                $productSpecRel[$spec->id]['default'] = array();
            } else {
                $productSpecRel[$spec->id]['default'] = array();
            }
            $productSpecRel[$spec->id]['options'] = array();
            foreach($spec->options as $option){
                if($option->default == 1){
                    $productSpecRel[$spec->id]['default'][] = $option->id;
                }
                if($spec->stock_hasmany == 'Yes'){
                    $productSpecRel[$spec->id]['options'][$option->id]['name'] = $option->value;
                    $productSpecRel[$spec->id]['options'][$option->id]['premium'] = 0;
                    $productSpecRel[$spec->id]['options'][$option->id]['ec'] = @$option->ec;
                } else {
                    $productSpecRel[$spec->id]['options'][$option->id] = $option->value;
                }
            }
        }
        
        $data = array('product_id'=> $request->pid,'productSpecRel' => $productSpecRel,'productSpecChildRelation' => $productSpecChildRelation,'pref_id' => $pref_id);
        
        $view = View::make('backend.products.product-stock', $data);
        $html = $view->render();
        return response()->json([
            'html' => $html,
            'show_more' => $show_more_btn
        ]);      
        //echo "<pre/>"; print_r($productSpecChildRelation); die;
        //return response()->view('backend.products.product-stock', $data, 200);
    }

    public function getProductForBuyerAjax(Request $request, Product $product){
        $product = Product::find($request->pid);

        $productspecification_list = ProductSpecification::with('options')->where('parent_id',null)->orderBy('order')->get();
        $productSpecRel = array();
        foreach($productspecification_list as $spec){
            $productSpecRel[$spec->product_id][$spec->id]['name'] = $spec->display_name;
            $productSpecRel[$spec->product_id][$spec->id]['id'] = $spec->id;
            $productSpecRel[$spec->product_id][$spec->id]['hasmany'] = $spec->buyer_hasmany;
            $productSpecRel[$spec->product_id][$spec->id]['required'] = $spec->required;

            if($spec->buyer_hasmany == 'Yes'){
                $productSpecRel[$spec->product_id][$spec->id]['default'] = array();
            } else {
                $productSpecRel[$spec->product_id][$spec->id]['default'] = array();
            }

            $productSpecRel[$spec->product_id][$spec->id]['options'] = array();
            foreach($spec->options as $option){
                if($option->default == 1){
                    $productSpecRel[$spec->product_id][$spec->id]['default'][] = $option->id;
                }
                if($spec->buyer_hasmany == 'Yes'){
                    $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['name'] = $option->value;
                    $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['premium'] = 0;
                    $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['id'] = $spec->id;
                } else {
                    $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id] = $option->value;
                }
               //     $productSpecRel[$spec->product_id][$spec->id]['id'][$option->id] =$spec->id;
            }
        }

     //   echo "<pre/>"; print_r($productSpecRel); die;
        $data = array('product_id'=> $request->pid,'productSpecRel' => @$productSpecRel[$request->pid]);

        return response()->view('backend.products.product-pref', $data, 200);
    }

    public function getProductAjaxMultiple(Request $request, Product $product)
    {
        $productSpecRel =array();
        $product = Product::find($request->pid);
        $pref_id = $request->pref_id;
        $productspecification_list = ProductSpecification::with('options')->where('product_id',$request->pid)->where('parent_id',null)->get();
        $isfilter_type = ProductSpecification::with('options')->where('product_id',$request->pid)->where('parent_id',null)->where('field_type','optionrange')->first();
        foreach($productspecification_list as $spec){
            $productSpecRel[$spec->id]['name'] = $spec->display_name;
            $productSpecRel[$spec->id]['hasmany'] = $spec->buyer_hasmany;
            $productSpecRel[$spec->id]['required'] = $spec->required;
            $productSpecRel[$spec->id]['buyer_pref_anylogic'] = $spec->buyer_pref_anylogic;
            $productSpecRel[$spec->id]['field_type'] = $spec->field_type;
            $productSpecRel[$spec->id]['isfilter_type'] = isset($isfilter_type) ? $isfilter_type->field_type :'';
            if($spec->buyer_hasmany == 'Yes'){
                $productSpecRel[$spec->id]['default'] = array();
            } else {
                $productSpecRel[$spec->id]['default'] = array();
            }
            $productSpecRel[$spec->id]['options'] = array();
            foreach($spec->options as $option){
                if($option->default == 1){
                    $productSpecRel[$spec->id]['default'][] = $option->id;
                }
                if($spec->buyer_hasmany == 'Yes'){
                    $productSpecRel[$spec->id]['options'][$option->id]['name'] = $option->value;
                    $productSpecRel[$spec->id]['options'][$option->id]['premium'] = 0;
                } else {
                    $productSpecRel[$spec->id]['options'][$option->id] = $option->value;
                }
            }
        }
        //echo "<pre/>"; print_r($productSpecRel); die;
        $data = array('product_id'=> $request->pid,'productSpecRel' => $productSpecRel);
        $data['pref_id'] = $pref_id;
        return response()->view('backend.products.stock-product-multi-pref', $data, 200);
    }
    public function productsexports()
    {
        return Excel::download(new ProductExport, 'product.xlsx');
    }
}
