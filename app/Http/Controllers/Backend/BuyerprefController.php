<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AppHead;
use App\Buyer;
use App\BuyerProductPref;
use App\ProductSpecification;
use App\BuyerPref;
use App\Product;
use App\Models\Auth\User;
use DataTables;
use DB;
use App\Exports\BuyerPrefExport;
use Maatwebsite\Excel\Facades\Excel;

class BuyerprefController extends Controller{
    
    public function index(Request $request){
        if ($request->ajax()) {
              $data = BuyerPref::with('product', 'buyer')->get();
             //echo "<pre/>"; print_r($data); die;
              return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                         $btn = ' <div class="btn-group btn-group-sm">
                                    <button type="button" class="btn btn-edit editItem" data-url="'.route('admin.buyerpref.edit', $row->id).'"><i class="fas fa-edit"></i></button>
                                    <button type="button" class="btn btn-primary viewItem" data-url="'.route('admin.buyerpref.show', $row->id).'"><i class="fas fa-eye"></i></button>
                                    <button data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="Delete" type="button" class="btn btn-danger deleteItem"><i class="fas fa-trash-alt"></i></button>
                                  </div>';
                        return $btn;
                    })
                    ->addColumn('buyer_name', function($row){
						return (@$row->buyer->username?@$row->buyer->username:'-');
					})
					->addColumn('product_name', function($row){
                        return (@$row->product->name?@$row->product->name:'-');
					})
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('backend.buyerpref.index');
    }

    public function create(){
		$product_list = Product::all()->where('status',1)->pluck('name','id');
        $productspecification_list = ProductSpecification::with('options')->where('parent_id',null)->get();
		$productSpecRel = array();
         
        foreach($productspecification_list as $spec){
            $productSpecRel[$spec->product_id][$spec->id]['name'] = $spec->display_name;
            $productSpecRel[$spec->product_id][$spec->id]['hasmany'] = $spec->buyer_hasmany;
            $productSpecRel[$spec->product_id][$spec->id]['required'] = $spec->required;
            
            if($spec->buyer_hasmany == 'Yes'){
                $productSpecRel[$spec->product_id][$spec->id]['default'] = array();
            } else {
                $productSpecRel[$spec->product_id][$spec->id]['default'] = array();
            }
            
            $productSpecRel[$spec->product_id][$spec->id]['options'] = array();
            foreach($spec->options as $option){
                if($option->default == 1){
                    $productSpecRel[$spec->product_id][$spec->id]['default'][] = $option->id;
                }
                if($spec->buyer_hasmany == 'Yes'){
                    $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['name'] = $option->value;
                    $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['premium'] = 0;
                } else {
                    $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id] = $option->value;
                }
            }
        }
        //echo "<pre/>"; print_r($productSpecRel); die;
        $first_product = current(array_keys($product_list->toArray()));
		return view('backend.buyerpref.add-edit',compact('product_list','productSpecRel','first_product'));
    }

    public function store(Request $request){
        $validate_array = ['product_id' => 'required|not_in:0', 'buyer_id' => 'required|not_in:0'];
        
        $all = $request->all();
        $productspecs = ProductSpecification::where('product_id', $all['product_id'])->with('options')->get();
        // $productspecification_list = ProductSpecification::with('options')->where('parent_id',null)->orderBy('order')->get();
        if(isset($productspecs) && !empty($productspecs)){
            $validationsname = '';
            foreach($productspecs as $required){
                if($required['required'] == 'Yes'){
                    if (count($required->options)){
                        $validate_array['specification'][$required->options[0]->product_specification_id][0] = 'required';
                    }
                    
                    
                }
            }
        }
    
        $this->validate($request, $validate_array );
        //exit;
        $tableArray = array();
        $tableArray['buyer_id']  = $all['buyer_id'];
        $tableArray['product_id']  = $all['product_id'];
        $BuyerPref = BuyerPref::create($tableArray);
        
        $buyer_pref_id = $BuyerPref->id;
       
        $specification = @$all['specification'];
        $premium = @$all['premium'];
        $collected_data = array();
       
        foreach($specification as $specKey=>$specValue)
        {
            if(is_array($specValue))
            {
                foreach($specValue as $specValueKey=>$specValueValue){
                    $premiumValue = @$premium[$specValueValue];
                    $collected_data[] = [
                        'buyer_pref_id'=>$buyer_pref_id,
                        'key' => $specKey,
                        'value' => $specValueValue,
                        'premium' => $premiumValue,
                        'created_at' => date('Y-m-d H:i:s')
                        ]; 
                }   
            }  else {
                $collected_data[] = [
                    'buyer_pref_id'=>$buyer_pref_id,
                    'key' => $specKey,
                    'value' => $specValue,
                    'premium' => NULL,
                    'created_at' => date('Y-m-d H:i:s')
                    ];    
            }                
        }
       // echo "<pre/>"; print_r($collected_data); die;
        $buyerPref = DB::table('buyer_product_prefs')->insert($collected_data);
        
        //echo "<pre/>"; print_r($collected_data); die;
       
        //variety-soil-flesh_color-packaging-purposes-defects
        /*
        $json_fields = array('size_range','location_range','price_range');
        $collected_data = array();
        foreach($request->all() as $key=>$values){
            if (in_array($key, $json_fields)) {
                foreach($values as $value){
                    if(!empty($value['from']) && !empty($value['to'])){
                        $collected_data[] = [
                            'type'=>$key,
                            'buyer_id'=>$all['buyer_id'],
                            'buyer_product_pref_id'=>$buyer_product_pref_id,
                            'key' => $key,
                            'val' => (!empty( $value ) ? json_encode( $value ) : NULL)
                        ];
                    }
                }
            }
        }
        
        */
        return response()->json(['status' => 'success', 'message' => 'Buyer pref created successfully.']);
    }

    public function show($id){
        $buyerpref = BuyerPref::where(['id' => $id])->first();
        if($buyerpref){
            $buyerprefWithproductPrefs = BuyerPref::with('product', 'buyer', 'productPrefs', 'productPrefs.productSpec', 'productPrefs.productSpecValue')->where('id',$buyerpref->id)->first();
        
            $nets = array();
            foreach($buyerprefWithproductPrefs->productPrefs as $pref){
                $nets[@$pref->productSpec->display_name][] = @$pref->productSpecValue->value;
            }
            return view('backend.buyerpref.show',compact('buyerprefWithproductPrefs','nets'));
         }else{
           $msg="Unfortunately this BuyerPref is not exist!";
          return view('backend.buyerpref.index', compact('msg'));
         } 
      }
  
      public function edit($id){
        $buyerpref = BuyerPref::where(['id' => $id])->first();
        if($buyerpref){
            $productPrefs = $buyerpref->productPrefs()->select('key','value','premium')->get()->toArray();
            $productPrefsMapping = array();
            $productPrefsMappingPremiums = array();
            foreach($productPrefs as $productPref){
                $productPrefsMapping[$productPref['key']][] = $productPref['value'];
                $productPrefsMappingPremiums[$productPref['key']][$productPref['value']] = $productPref['premium'];
            }
            $product_list = Product::all()->where('status',1)->pluck('name','id');
            $productspecification_list = ProductSpecification::with('options')->where('parent_id',null)->get();
            $productSpecRel = array();
            foreach($productspecification_list as $spec){
                $productSpecRel[$spec->product_id][$spec->id]['name'] = $spec->display_name;
                $productSpecRel[$spec->product_id][$spec->id]['hasmany'] = $spec->buyer_hasmany;
                if($spec->buyer_hasmany == 'Yes'){
                    $productSpecRel[$spec->product_id][$spec->id]['default'] = @$productPrefsMapping[$spec->id];
                } else {
                    $productSpecRel[$spec->product_id][$spec->id]['default'] = current($productPrefsMapping[$spec->id]??array());
                }
                $productSpecRel[$spec->product_id][$spec->id]['options'] = array();
                foreach($spec->options as $option){
                   if($spec->buyer_hasmany == 'Yes'){
                        $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['name'] = $option->value;
                        $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['premium'] = @$productPrefsMappingPremiums[$spec->id][$option->id];
                        $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id]['id'] = $spec->id;
                    } else {
                        $productSpecRel[$spec->product_id][$spec->id]['options'][$option->id] = $option->value;
    
                    }
                }
            }
            //echo "<pre/>"; print_r($productSpecRel); die;
            $first_product = $buyerpref->product_id;
            return view('backend.buyerpref.add-edit',compact('product_list','productSpecRel','first_product','buyerpref'));
           
         }else{
          
          $msg="Unfortunately this BuyerPref is not exist!";
          return view('backend.buyerpref.index',compact('msg'));
         } 
         
      }
  
    public function update(Request $request, BuyerPref $buyerpref){
        $request->validate([
          'buyer_id' => 'required',
          'product_id' => 'required',
        ]);
        
        $all = $request->all();
        $buyerpref->buyer_id  = $all['buyer_id'];
        $buyerpref->product_id  = $all['product_id'];
        $buyerpref->save();
        
        $buyer_pref_id = $buyerpref->id;
        $specification = @$all['specification'];
        $premium = @$all['premium'];
        $collected_data = array();
        
        $buyerExistPrefs = $buyerpref->productPrefs()->pluck('id','id')->toArray();
        //echo "<pre/>"; print_r($all); 
        //echo "<pre/>"; print_r($buyerExistPrefs);
        if(isset($specification) && !empty($specification)){
        foreach($specification as $specKey=>$specValue)
        {
            if(is_array($specValue))
            {
                foreach($specValue as $specValueKey=>$specValueValue){
                    $premiumValue = @$premium[$specValueValue];
                    
                    $buyerPP = BuyerProductPref::updateOrCreate(
                        ['buyer_pref_id' => $buyer_pref_id,'key' => $specKey,'value' => $specValueValue],
                        ['premium' => $premiumValue]
                    );
                    unset($buyerExistPrefs[$buyerPP->id]);   
                }   
            }  else {
                $premiumValue = @$premium[$specValueValue];
                $buyerPP = BuyerProductPref::updateOrCreate(
                        ['buyer_pref_id' => $buyer_pref_id,'key' => $specKey,'value' => $specValue],
                        ['premium' => $premiumValue]
                    );
                unset($buyerExistPrefs[$buyerPP->id]);       
            }
        }
        }
       // echo "<pre/>"; print_r($buyerExistPrefs); die;
        //Delete those values are updated.
        if(isset($buyerExistPrefs) && !empty($buyerExistPrefs)){
            foreach($buyerExistPrefs as $pref){
                BuyerProductPref::destroy($pref);
            }
        }
        return response()->json(['status' => 'success', 'message' => 'Buyerpref updated successfully.']);
    }

    public function destroy(BuyerPref $buyerpref){
       // echo "<pre/>"; print_r($buyerpref->productPrefs()->get()); die;
        $buyerpref->delete();
        return response()->json(['success'=>'Buyerpref deleted successfully.']);
    }

    public function buyerprefexports() 
    {
        return Excel::download(new BuyerPrefExport, 'buyerpref.xlsx');
    }
}
