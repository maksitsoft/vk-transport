<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = [ 'title', 'subject', 'email_content', 'email_content_de', 'email_content_pl', 'sms_content', 'sms_content_de', 'sms_content_pl', 'whatsapp_content', 'sent', 'status'];
}
