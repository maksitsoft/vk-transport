<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferProperty extends Model
{
    protected $fillable = ['offer_id','product_spec_id','product_spec_val_id','value','ecs'];

    public function stock(){
        return $this->hasone('App\Offer','id','offer_id');
    }
    
   public function productSpec(){
        return $this->hasone('App\ProductSpecification','id','product_spec_id');
    }

    public function productSpecValue(){
        return $this->hasone('App\ProductSpecificationValue','id','product_spec_val_id');
    }
}
