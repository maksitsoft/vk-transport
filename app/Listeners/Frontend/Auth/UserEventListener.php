<?php

namespace App\Listeners\Frontend\Auth;

/**
 * Class UserEventListener.
 */
class UserEventListener
{
    /**
     * @param $event
     */
    public function onLoggedIn($event)
    {
        $ip_address = request()->getClientIp();

        // Update the logging in users time & IP
        $event->user->fill([
            'last_login_at' => now()->toDateTimeString(),
            'last_login_ip' => $ip_address,
        ]);

        // Update the timezone via IP address
        $geoip = geoip($ip_address);

        if ($event->user->timezone !== $geoip['timezone']) {
            // Update the users timezone
            $event->user->fill([
                'timezone' => $geoip['timezone'],
            ]);
        }

        $event->user->save();

        \Log::info('User Logged In: '.$event->user->full_name);
    }

    /**
     * @param $event
     */
    public function onLoggedOut($event)
    {
        \Log::info('User Logged Out: '.$event->user->full_name);
    }

    /**
     * @param $event
     */
    public function onRegistered($event)
    {
        \Log::info('User Registered: '.$event->user->full_name);
        $locale = \App::getLocale();
            // echo 'dfd'.$locale;exit;
        $email_content = '';
        if($event->user->hasRole('seller')){
          $email_template = get_email_template('SELLER REGISTER');
          $whatsapp_number = @$event->user->whatsapp_number;
          $sms_number = @$event->user->sms_number;
        }elseif($event->user->hasRole('buyer')){
          if($event->buyer_seller->from_buyerlead==1){
            $email_template = get_email_template('BUYER SHORT REGISTER');
          }else{
            $email_template = get_email_template('BUYER REGISTER');
          }
          $whatsapp_number = @$event->user->phone;
          $sms_number = @$event->user->phone;
        }  
       if($email_template){
         \Mail::to($event->user->email,$event->user->full_name)->send(new \App\Mail\Frontend\Register\Register_email($event->user,$event->buyer_seller, $email_template));
         $whatsapp_verification_link = route('frontend.auth.whatsapp.verify', $event->user->whatsapp_verification_code);
         if($locale == 'pl'){
           $email_content = $email_template->sms_content_pl;
         }else{
           $email_content = $email_template->sms_content;
         }
          $cdata = request()->toArray();
          
          $prefered_method = array();
          if(isset($cdata['contact_sms']) && ($cdata['contact_sms']==1)){
            $prefered_method[] = "SMS";
          }
          if(isset($cdata['contact_email']) && ($cdata['contact_email']==1)){
            $prefered_method[] = "Email";
          }
          if(isset($cdata['contact_whatsapp']) && ($cdata['contact_whatsapp']==1)){
            $prefered_method[] = "WhatsApp";
          }          
           $product_name_column = (($locale == 'pl') ? 'name_pl' : 'name');
           $product_name = \App\Product::where('id',$event->user->product_id)->first()[$product_name_column];
           $email_content = str_replace("[product_name]", $product_name, $email_content);
           $email_content = str_replace("[product_sub_type]", $event->user->product_sub_type, $email_content);
           $email_content = str_replace("[name]", ucfirst($event->user->name), $email_content);
         $email_content = str_replace("[first_name]", $event->user->first_name, $email_content);
           $email_content = str_replace("[email]", $event->user->email, $email_content);
           $email_content = str_replace("[phone]", (substr(trim($event->user->phone),0,1) != '+' ? '+' : '').$event->user->phone, $email_content);
           $email_content = str_replace("[contact_preferred_method]", implode(', ',$prefered_method), $email_content);
           $email_content = str_replace("[notes]", $event->user->notes, $email_content);
           $email_content = str_replace("[verification_link]", $whatsapp_verification_link, $email_content);

          SendWhatsapp(['phone' => @$whatsapp_number, 'body' => $email_content,'is_PDF'=>false]);
          SendSMS(@$sms_number,$email_content);
      }else{
        \Log::info('Email template does not exist ');         
       }
          if(\Spatie\Permission\Models\Role::where('name','trader')->exists()){
             $roles[] = 'trader';
          }
          if(\Spatie\Permission\Models\Role::where('name','trader admin')->exists()){
             $roles[] = 'trader admin';
          }
          $users = \App\Models\Auth\User::role(@$roles)->get();
          foreach(@$users as $user){
            \Mail::to($user->email, $user->first_name.' '.$user->last_name)->send(new \App\Mail\Frontend\Register\RegistrationNotificationToTeam($event->user,$user,$event->buyer_seller, $email_template));
             // $whatsapp_message = "Dear ".ucfirst($user->first_name).", \n *We have a new seller* \n Below is the seller details: \n *Email*: ".$event->user->email." \n *Name*: ".$event->user->first_name.' '.$event->user->last_name." \n *Phone*: ".(substr(trim($event->user->phone),0,1) != '+' ? '+' : '') .$event->user->phone."  \n *View Seller*: ".route('admin.sellers.show',$event->seller->id);
              $email_content = '';
              if($event->user->hasRole('seller')){
                $email_template = get_email_template('SELLER REGISTER TO TEAM');
              }elseif ($event->user->hasRole('buyer')) {
                if($event->buyer_seller->from_buyerlead==1){
                    $email_template = get_email_template('BUYER SHORT REGISTER TO TEAM');
                }else{
                    $email_template = get_email_template('BUYER REGISTER TO TEAM');
                }
              }
              if($email_template){
                  if($locale == 'pl'){
                  $email_content = $email_template->sms_content_pl;
                  }else{
                  $email_content = $email_template->sms_content;
                  }
                  $product_name_column = (($locale == 'pl') ? 'name_pl' : 'name');
                  $product_name = \App\Product::where('id',@$event->buyer_seller->product_id)->first()[$product_name_column];
                  $email_content = str_replace("[team_member_name]", $user->name, $email_content);
                  $email_content = str_replace("[product_name]", $product_name, $email_content);
                  $email_content = str_replace("[product_sub_type]", @$event->buyer_seller->product_sub_type, $email_content);
                  $email_content = str_replace("[name]", $event->user->name, $email_content);
                  $email_content = str_replace("[email]", $event->user->email, $email_content);
                  $email_content = str_replace("[phone]", (substr(trim($event->user->phone),0,1) != '+' ? '+' : '').$event->user->phone, $email_content);
                  if($event->user->hasRole('seller')){
                    $email_content = str_replace("[view_seller_link]", route('admin.sellers.show',$event->buyer_seller->seller_id), $email_content);
                  }elseif($event->user->hasRole('buyer')){
                    $email_content = str_replace("[view_buyer_link]", route('admin.buyers.show',$event->buyer_seller->buyer_id), $email_content); 
                  }
                  SendWhatsapp(['phone' => (!empty(@$user->whatsapp_number)?$user->whatsapp_number:@$user->phone), 'body' => $email_content,'is_PDF'=>false]);
                  SendSMS(@$user->sms_number,$email_content);
              }else{
                \Log::info('Email template does not exist ');         
              }
          }
    }

    /**
     * @param $event
     */
    public function onProviderRegistered($event)
    {
        \Log::info('User Provider Registered: '.$event->user->full_name);
    }

    /**
     * @param $event
     */
    public function onConfirmed($event)
    {
        \Log::info('User Confirmed: '.$event->user->full_name);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Frontend\Auth\UserLoggedIn::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onLoggedIn'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserLoggedOut::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onLoggedOut'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserRegistered::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onRegistered'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserProviderRegistered::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onProviderRegistered'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserConfirmed::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onConfirmed'
        );
    }
}
