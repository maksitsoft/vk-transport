<?php

namespace App\Listeners\Backend;

use App\Events\Backend\BuyerCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BuyerCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckMatchesForBuyer  $event
     * @return void
     */
    public function handle(BuyerCreated $event)
    {
      // echo $event->buyer_user->name;exit;
        \Log::info('Buyer Created on '.$event->user->created_at.' by '. auth()->user()->full_name);
        // app('App\Http\Controllers\Backend\BuyerController')->buyerCreatedNotification($event->buyer);
        $locale = \App::getLocale();
        $email_content = '';
        $sms_content = '';

        $email_template = get_email_template('BUYER CREATED');
        \App\EmailTemplate::where('title', 'BUYER CREATED')->increment('sent');
        if($email_template){
          // $country_name = country_list()->toArray();
          // $country = $country_name[$event->user->country];

          if($locale == 'pl'){
            $email_content = $email_template->email_content_pl;
            $sms_content = $email_template->sms_content_pl;
          }else{
            $email_content = $email_template->email_content;
            $sms_content = $email_template->sms_content;
          }
          $email_content = str_replace("[name]", $event->user->name, $email_content);
          $email_content = str_replace("[email]", $event->user->email, $email_content);
          $email_content = str_replace("[phone]", (substr(trim($event->user->phone),0,1) != '+' ? '+' : '').$event->user->phone, $email_content);
          // $email_content = str_replace("[city]", $event->user->city, $email_content);
          // $email_content = str_replace("[postalcode]", $event->user->postalcode, $email_content);
          // $email_content = str_replace("[country]", $country, $email_content);

          $mail = $event->user->email;
          \Mail::send('frontend.mail.general', ['email_content' => $email_content], function ($message) use ($email_template, $mail) {
              $message->subject($email_template->subject);
              $message->to($mail);
          });
          $sms_content = str_replace("[name]", $event->user->name, $sms_content);
          $sms_content = str_replace("[email]", $event->user->email, $sms_content);
          $sms_content = str_replace("[phone]", (substr(trim($event->user->phone),0,1) != '+' ? '+' : '').$event->user->phone, $sms_content);
          // $sms_content = str_replace("[city]", $event->user->city, $sms_content);
          // $sms_content = str_replace("[postalcode]", $event->user->postalcode, $sms_content);
          // $sms_content = str_replace("[country]", $country, $sms_content);          

          SendWhatsapp(['phone' => @$event->user->phone, 'body' => $sms_content,'is_PDF'=>false]);
          SendSMS(@$event->user->phone,$sms_content);
       }else{
        \Log::info('Email template "BUYER CREATED" not exist ');         
       }
    }
}
