<?php

namespace App\Listeners\Backend;

use App\Events\Backend\StockUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Order;
use App\Match;
use App\Models\Auth\User;
class NotifyMatched
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  StockMatched  $event
     * @return void
     */
    public function handle($event)
    {
      // echo 'abab';class_basename($event);exit;
      // echo '<pre>';print_r($event->stock->toArray());exit;
      \Log::info('Buyerpref/Stock Updated on ');//.$event->stock->created_at.' by '. auth()->user()->full_name);
      app('App\Http\Controllers\Backend\MatchsController')->notifyMatched($event);

    }
}
