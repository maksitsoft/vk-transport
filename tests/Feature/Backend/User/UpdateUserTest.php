<?php

namespace Tests\Feature\Backend\User;

use Tests\TestCase;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use App\Events\Backend\Auth\User\UserUpdated;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Http\Middleware\VerifyCsrfToken;

class UpdateUserTest extends TestCase
{

    /** @test */
    public function an_admin_can_access_the_edit_user_page()
    {
        
        $user = factory(User::class)->create();
        $this->loginAsAdmin();
        $response = $this->get('/admin/auth/user/'.$user->id.'/edit');

        $response->assertStatus(200);
    }

    /** @test  */
    public function an_admin_can_resend_users_confirmation_email()
    {
        $this->loginAsAdmin();
        $user = factory(User::class)->states('unconfirmed')->create();
        Notification::fake();

        $this->get("/admin/auth/user/{$user->id}/account/confirm/resend");

        Notification::assertSentTo($user, UserNeedsConfirmation::class);
    }

    /** @test */
    public function a_user_can_be_updated()
    {
        $this->loginAsAdmin();
        $user = factory(User::class)->create();
        Event::fake();
        $user_new = factory(User::class)->make();
        $this->assertNotSame($user_new->first_name, $user->first_name);
        $this->assertNotSame($user_new->last_name, $user->last_name);
        $this->assertNotSame($user_new->first_name, $user->email);
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $response = $this->patch("/admin/auth/user/{$user->id}", [
            'first_name' =>$user_new->first_name,
            'last_name' => $user_new->last_name,
            'email' =>$user_new->email,
            'phone'=> '91852347806',
            'roles' => ['administrator'],
        ]);
        $this->assertSame($user_new->first_name, $user->fresh()->first_name);
        $this->assertSame($user_new->last_name, $user->fresh()->last_name);
        $this->assertSame($user_new->email, $user->fresh()->email);
        Event::assertDispatched(UserUpdated::class);

    }
}
