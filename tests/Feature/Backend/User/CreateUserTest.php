<?php

namespace Tests\Feature\Backend\User;

use Tests\TestCase;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;
use App\Events\Backend\Auth\User\UserCreated;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Http\Middleware\VerifyCsrfToken;
use Faker\Generator as Faker;
Use Exception;


class CreateUserTest extends TestCase
{
    // use RefreshDatabase;

    /** @test */
    public function an_admin_can_access_the_create_user_page()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/auth/user/create');

        $response->assertStatus(200);
    }

    /** @test */
    public function create_user_has_required_fields()
    {
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $response = $this->post('/admin/auth/user', []);

        $response->assertSessionHasErrors(['first_name', 'last_name', 'email', 'password', 'roles']);
    }

    /** @test */
    public function user_email_needs_to_be_unique()
    {
        $this->loginAsAdmin();        
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $user = factory(User::class)->make();
        factory(User::class)->create(['email' => $user->email]);
        try{

            $response = $this->post('/admin/auth/user', [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'phone' => '',
            'password' => 'password',
            'password_confirmation' => 'password',
            'active' => false,
            'confirmed' => $user->confirmed,
            'timezone' => 'UTC',
            'confirmation_email' => '1',
            'roles' => ['executive', 'user'],
        ]);
        
        $response->assertJson([
                        'message' => 'The given data was invalid.'
                    ]);
        }catch(Exception $ex){
            $this->assertSame('The given data was invalid.', $ex->getMessage());
        }
        
        // $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function admin_can_create_new_user()
    {
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        // Hacky workaround for this issue (https://github.com/laravel/framework/issues/18066)
        // Make sure our events are fired
        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        Model::setEventDispatcher($initialDispatcher);

        $user = factory(User::class)->make();

        $response = $this->post('/admin/auth/user', [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'phone' => '',
            'password' => 'OC4Nzu270N!QBVi%U%qX',
            'password_confirmation' => 'OC4Nzu270N!QBVi%U%qX',
            'active' => $user->active,
            'confirmed' => $user->confirmed,
            'timezone' => 'UTC',
            'confirmation_email' => '1',
            'roles' => ['user'],
        ]);

       $response->assertJson([
                        'message' => __('alerts.backend.users.created')
                    ]);

        // $response->assertSessionHas(['flash_success' => __('alerts.backend.users.created')]);
        Event::assertDispatched(UserCreated::class);
    }

    /** @test */
    public function when_an_unconfirmed_user_is_created_a_notification_will_be_sent()
    {
        $this->loginAsAdmin();
        Notification::fake();
        $this->withoutMiddleware(VerifyCsrfToken::class);


        $user = factory(User::class)->make();

        $response = $this->post('/admin/auth/user', [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'phone' => '',
            'password' => 'OC4Nzu270N!QBVi%U%qX',
            'password_confirmation' => 'OC4Nzu270N!QBVi%U%qX',
            'active' => '1',
            'confirmed' => '0',
            'timezone' => 'UTC',
            'confirmation_email' => '1',
            'roles' => ['user'],
            'permissions'=> ["view stock"]
        ]);
        $response->assertJson([
                        'message' => __('alerts.backend.users.created')
                    ]);

        $user = User::where('email', $user->email)->first();
        Notification::assertSentTo($user, UserNeedsConfirmation::class);
    }
}
