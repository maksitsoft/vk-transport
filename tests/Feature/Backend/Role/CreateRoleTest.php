<?php

namespace Tests\Feature\Backend\Role;

use Tests\TestCase;
use App\Models\Auth\Role;
use Illuminate\Support\Facades\Event;
use App\Events\Backend\Auth\Role\RoleCreated;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Http\Middleware\VerifyCsrfToken;

class CreateRoleTest extends TestCase
{
    // use RefreshDatabase;
    // use WithoutMiddleware;
    /** @test */
    public function an_admin_can_access_the_create_role_page()
    {
        $this->loginAsAdmin();

        $this->get('/admin/auth/role/create')->assertStatus(200);
    }

    /** @test */
    public function the_name_is_required()
    {
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $response = $this->post('/admin/auth/role', ['name' => '']);
        $response->assertSessionHasErrors('name');
        // $response->assertSessionHasErrors([],  $format = 'message', $errorBag = 'default');
    }

    /** @test */
    public function the_name_must_be_unique()
    {
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $response = $this->post('/admin/auth/role', ['name' => config('access.users.admin_role')]);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function at_least_one_permission_is_required()
    {
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $response = $this->post('/admin/auth/role', ['name' => strtolower(str_random(10))]);
        $response->assertSessionHas(['flash_danger' => __('exceptions.backend.access.roles.needs_permission')]);
    }

    /** @test */
    public function a_role_can_be_created()
    {
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->post('/admin/auth/role', ['name' => 'new role', 'permissions' => ['view backend']]);

        $role = Role::where(['name' => 'new role'])->first();

        $this->assertTrue($role->hasPermissionTo('view backend'));
    }

    /** @test */
    public function an_event_gets_dispatched()
    {
        $this->loginAsAdmin();
        Event::fake();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $rolename = strtolower(str_random(10));
        $this->post('/admin/auth/role', ['name' => $rolename , 'permissions' => ['view backend']]);
        $role = Role::where(['name' => $rolename])->first();
        Event::assertDispatched(RoleCreated::class, function ($e) use ($role) {
            return $e->role->id == $role->id;
        });
    }
}
