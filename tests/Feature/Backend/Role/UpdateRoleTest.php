<?php

namespace Tests\Feature\Backend\Role;

use Tests\TestCase;
use App\Models\Auth\Role;
use Illuminate\Support\Facades\Event;
use App\Events\Backend\Auth\Role\RoleUpdated;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Middleware\VerifyCsrfToken;

class UpdateRoleTest extends TestCase
{
    // use RefreshDatabase;

    /** @test */
    public function an_admin_can_access_the_edit_role_page()
    {
        $role = factory(Role::class)->create();
        $this->loginAsAdmin();

        $this->get("/admin/auth/role/{$role->id}/edit")->assertStatus(200);
    }

    /** @test */
    public function name_is_required()
    {
        $role = factory(Role::class)->create();
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $response = $this->patch("/admin/auth/role/".$role->id, ['name' => '']);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function at_least_one_permission_is_required()
    {

        $role = factory(Role::class)->create();
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $rolename = strtolower(str_random(10));
        $response = $this->patch("/admin/auth/role/".$role->id, ['name' => $rolename]);

        $response->assertSessionHas(['flash_danger' => __('exceptions.backend.access.roles.needs_permission')]);
    }

    /** @test */
    public function a_role_name_can_be_updated()
    {
        $role = factory(Role::class)->create();
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $rolename = strtolower(str_random(10));
        $response = $this->post("/admin/auth/role/".$role->id, ['name' => $rolename, 'permissions' => ['view backend'], '_method'=>'PATCH']);
 
        $this->assertSame($rolename, $role->fresh()->name);
    }

    /** @test */
    public function an_event_gets_dispatched()
    {
        $role = factory(Role::class)->create();
        Event::fake();
        $this->loginAsAdmin();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $rolename = strtolower(str_random(10));
        $this->patch("/admin/auth/role/{$role->id}", ['name' => $rolename, 'permissions' => ['view backend']]);

        Event::assertDispatched(RoleUpdated::class);
    }
}
