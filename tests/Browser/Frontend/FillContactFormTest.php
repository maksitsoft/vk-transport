<?php

namespace Tests\Browser\Frontend;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;
use Illuminate\Support\Facades\Mail;
use App\Mail\Frontend\Contact\SendContact;


class FillContactFormTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_the_contact_route_exists()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/contact')
                    ->assertSee('Contact Us');
        });
        
    }

    public function test_a_contact_mail_gets_sent()
    {
        Mail::fake();
       
        $faker = Faker\Factory::create();
        $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));    
          
        $this->browse(function (Browser $browser) use($faker) {
            
            $browser->visit('/contact')
                    ->assertSee('Contact Us')
                    ->type('name', $faker->name)
                    ->type('email', $faker->email)
                    ->type('phone', $faker->e164PhoneNumber)
                    ->type('message', $faker->text)
                    ->press('Send Information')
                    ->assertPathIs('/contact')
                    ->assertSee('Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.');
            Mail::assertNotSent(SendContact::class);
        });

    }

    /** @test */
    public function test_name_is_required()
    {
        Mail::fake();
        $faker = Faker\Factory::create();
        $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));    
        $this->browse(function (Browser $browser) use($faker) {
            $browser->visit('/contact')
                    ->assertSee('Contact Us')
                    // ->type('name', $faker->name)
                    ->type('email', $faker->email)
                    ->type('phone', $faker->e164PhoneNumber)
                    ->type('message', $faker->text)
                    ->press('Send Information')
                    ->assertPathIs('/contact');               
       
        });
        Mail::assertNotSent(SendContact::class);

    }

}
