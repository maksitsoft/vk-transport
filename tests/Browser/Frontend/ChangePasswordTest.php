<?php

namespace Tests\Browser\Frontend;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;

class ChangePasswordTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function test_the_password_can_be_validated()
    {
        $user = factory(User::class)->create(['password' => 'pass@Zero123']);
        $this->browse(function (Browser $browser) use($user) {
                      
            $browser->loginAs($user)
                    ->visit('account')
                    ->clickLink('Change Password')
                    ->whenAvailable('#old_password', function() use ($browser) {                       
                        $browser->type('old_password', 'pass@Zero123')                   
                        ->type('password', 'pass@New123')
                        ->type('password_confirmation', 'pass@New123')
                        ->press('Update Password')
                        ->assertPathIs('/login');
                    }, 1);       

        });
    }


    public function test_the_password_can_be_changed()
    {
        $user = factory(User::class)->create(['password' => 'pass@Zero123']);
        $this->browse(function (Browser $browser) use($user) {                      
            $browser->loginAs($user)
                    ->visit('account')
                    ->clickLink('Change Password')
                    ->whenAvailable('#old_password', function() use ($browser) {                       
                        $browser->type('old_password', 'pass@Zero123')                   
                        ->type('password', 'OC4Nzu270N!QBVi%U%qX')
                        ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX')
                        ->press('Update Password')
                        ->assertPathIs('/login');
                    }, 1);       
                     
        });
        $this->assertTrue(Hash::check('OC4Nzu270N!QBVi%U%qX', $user->fresh()->password));
    }

    public function test_the_password_must_be_confirmed()
    {
        $user = factory(User::class)->create(['password' => 'pass@Zero123']);
        $this->browse(function (Browser $browser) use($user) {                     
            $browser->loginAs($user)
                    ->visit('account')
                    ->clickLink('Change Password')
                    ->whenAvailable('#old_password', function() use ($browser) {                       
                        $browser->type('old_password', 'pass@Zero123')                   
                        ->type('password', 'pass@New123')
                        ->type('password_confirmation', 'pass')
                        ->press('Update Password')
                        ->assertPathIs('/account')
                        ->assertSee('The password confirmation does not match.');
                    }, 1);       

        });
    }

    public function test_the_old_password_is_required_in_the_password_change_form()
    {
        $user = factory(User::class)->create(['password' => 'pass@Zero123']);
        $this->browse(function (Browser $browser) use($user) {                     
            $browser->loginAs($user)
                    ->visit('account')
                    ->clickLink('Change Password')
                    ->whenAvailable('#old_password', function() use ($browser) {                       
                        $browser->type('old_password', 'pass@Zero333')                   
                        ->type('password', 'OC4Nzu270N!QBVi%U%qX')
                        ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX')
                        ->press('Update Password')
                        ->assertPathIs('/account')
                        ->assertSee('That is not your old password.');
                    }, 1);       

        });
    }


    public function test_a_user_can_not_use_the_same_password_when_history_is_on_on_account_change_password()
    {
        config(['access.users.password_history' => 3]);

        $user = factory(User::class)->create(['password' => 'OC4Nzu270N!QBVi%U%qX']);
        $this->browse(function (Browser $browser) use($user) {                    
            $browser->loginAs($user)
                    ->visit('account')
                    ->clickLink('Change Password')
                    ->whenAvailable('#old_password', function() use ($browser, $user) {                       
                        $browser->type('old_password', 'OC4Nzu270N!QBVi%U%qX')                   
                        ->type('password', 'OC4Nzu270N!QBVi%U%qX12')
                        ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX12')
                        ->press('Update Password')
                        ->assertPathIs('/login')
                        ->visit('/login')
                        ->type('email', $user->email)  
                        ->type('password', 'OC4Nzu270N!QBVi%U%qX12')
                        ->press('Login')
                        ->visit('account')                      
                        ->clickLink('Change Password')
                        ->whenAvailable('#old_password', function() use ($browser) {                       
                            $browser->type('old_password', 'OC4Nzu270N!QBVi%U%qX12')                   
                            ->type('password', 'OC4Nzu270N!QBVi%U%qX')
                            ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX')
                            ->press('Update Password')
                            ->assertPathIs('/account')
                            ->assertSee('You can not set a password that you have previously used.');
                        }, 1);      
                    }, 1);      
                    
                     
        });

    }

    public function test_a_user_can_reuse_a_password_after_it_surpasses_the_limit()
    {
        config(['access.users.password_history' => 1]);

        $user = factory(User::class)->create(['password' => 'OC4Nzu270N!QBVi%U%qX']);
        $this->browse(function (Browser $browser) use($user) {                    
            $browser->loginAs($user)
                    ->visit('account')
                    ->clickLink('Change Password')
                    ->whenAvailable('#old_password', function() use ($browser, $user) {                       
                        $browser->type('old_password', 'OC4Nzu270N!QBVi%U%qX')                   
                        ->type('password', 'OC4Nzu270N!QBVi%U%qX12')
                        ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX12')
                        ->press('Update Password')
                        ->assertPathIs('/login')
                        ->visit('/login')
                        ->type('email', $user->email)  
                        ->type('password', 'OC4Nzu270N!QBVi%U%qX12')
                        ->press('Login')
                        ->visit('account')                      
                        ->clickLink('Change Password')
                        ->whenAvailable('#old_password', function() use ($browser) {                       
                            $browser->type('old_password', 'OC4Nzu270N!QBVi%U%qX12')                   
                            ->type('password', 'OC4Nzu270N!')
                            ->type('password_confirmation', 'OC4Nzu270N!')
                            ->press('Update Password')
                            ->assertPathIs('/login');
                            
                        }, 1);

                    }, 1);
                    
            $browser->visit('/login')            
                    ->type('email', $user->email)  
                    ->type('password', 'OC4Nzu270N!')
                    ->press('Login')
                    ->visit('account')
                    ->assertPathIs('/account')
                    ->clickLink('Change Password')
                    ->whenAvailable('#old_password', function() use ($browser, $user) {
                        $browser->type('old_password', 'OC4Nzu270N!')                   
                            ->type('password', 'OC4Nzu270N!QBVi%U%qX')
                            ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX')
                            ->press('Update Password')
                            ->assertPathIs('/login');
                    }, 1);       
                    
                     
        });
    }

}  


      



