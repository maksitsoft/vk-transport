<?php

namespace Tests\Browser\Frontend;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;

class PasswordExpirationTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Laravel');
        });
    }

    public function test_a_user_is_requested_to_change_their_password_after_it_expires()
    {
        config(['access.users.password_expires_days' => 30]);
        $user = factory(User::class)->create(['password_changed_at' => now()->subMonths(2)->toDateTimeString()]);
        $this->browse(function (Browser $browser) use($user) {    
            $browser->loginAs($user)
                    ->visit('/dashboard')
                    ->assertPathIs('/password/expired');        
        });
    }

    /** @test */
    public function test_a_user_is_not_requested_to_change_their_password_if_it_not_old_enough()
    {
        config(['access.users.password_expires_days' => 30]);

        $user = factory(User::class)->create(['password_changed_at' => now()->subWeek()->toDateTimeString()]);

        $this->browse(function (Browser $browser) use($user) {    
                $browser->loginAs($user)
                        ->assertPathIs('/dashboard');    
        });
    }

    /** @test */
    public function test_a_user_is_not_requested_to_change_password_if_expiration_is_off()
    {
        config(['access.users.password_expires_days' => false]);

        $user = factory(User::class)->create(['password_changed_at' => now()->subMonths(2)->toDateTimeString()]);

        $this->browse(function (Browser $browser) use($user) {    
            $browser->loginAs($user)
                    ->assertPathIs('/dashboard');    
        });
    }

    /** @test */
    public function test_a_user_can_not_use_the_same_password_when_history_is_on_on_password_expiration()
    {
        config(['access.users.password_history' => 3]);
        config(['access.users.password_expires_days' => 30]);

        $user = factory(User::class)->create([
            'password' => ']EqZL4}zBT',
            'password_changed_at' => now()->subMonths(2)->toDateTimeString(),
        ]);

        $this->browse(function (Browser $browser) use($user) {                    
            $browser->loginAs($user)
                    ->visit('password/expired')
                    ->type('old_password', ']EqZL4}zBT') 
                    ->type('password', 'OC4Nzu270N!QBVi%U%qX')
                    ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX')
                    ->press('Update Password')
                    ->assertPathIs('/login')
                    ->type('email', $user->email)
                    ->type('password', 'OC4Nzu270N!QBVi%U%qX')
                    ->press('Login')
                    ->visit('password/expired')
                    ->type('old_password','OC4Nzu270N!QBVi%U%qX') 
                    ->type('password',']EqZL4}zBT')
                    ->type('password_confirmation', ']EqZL4}zBT')
                    ->press('Update Password')
                    ->assertSee( __('auth.password_used'));

        }); 
        
    }

    public function test_the_password_can_be_validated()
    {
        config(['access.users.password_history' => false]);
        config(['access.users.password_expires_days' => 30]);

        $user = factory(User::class)->create([
            'password' => ']EqZL4}zBT',
            'password_changed_at' => now()->subMonths(2)->toDateTimeString(),
        ]);      
        $this->browse(function (Browser $browser) use($user) {                    
                $browser->loginAs($user)
                        ->visit('password/expired')
                        ->type('old_password', ']EqZL4}zBT') 
                        ->type('password', 'secret')
                        ->type('password_confirmation', 'secret')
                        ->press('Update Password')
                        ->assertSee('The password must be at least 8 characters.');
    
        });    
    }

    public function test_a_user_can_use_the_same_password_when_history_is_off_on_password_expiration()
    {
        config(['access.users.password_history' => false]);
        config(['access.users.password_expires_days' => 30]);

        $user = factory(User::class)->create([
            'password' => 'OC4Nzu270N!QBVi%U%qX',
            'password_changed_at' => now()->subMonths(2)->toDateTimeString(),
        ]);
        
        $this->browse(function (Browser $browser) use($user) {                    
                $browser->loginAs($user)
                        ->visit('password/expired')
                        ->type('old_password','OC4Nzu270N!QBVi%U%qX') 
                        ->type('password','OC4Nzu270N!QBVi%U%qX')
                        ->type('password_confirmation', 'OC4Nzu270N!QBVi%U%qX')
                        ->press('Update Password')
                        ->assertPathIs('/login');
        });       
       
    }

}
