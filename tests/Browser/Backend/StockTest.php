<?php

namespace Tests\Browser\Backend;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;
use Tests\Browser\Components\DatePicker;

class StockTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    
    public function testCheckStocklistPermission()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/trading/stock');
            $browser->assertPathIs('/login');
        });
    }

    public function testOnlyAdminCanAccessStockList()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/stock');
            $browser->assertPathIs('/admin/trading/stock');
            $browser->assertSee('Stock');
        });
    }


    public function testCheckStockCreatePermission()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/trading/stock/create');
            $browser->assertPathIs('/login');
        });
    }

    public function testOnlyAdminCanCreateStock()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/stock/create');
            $browser->assertPathIs('/admin/trading/stock/create');
            $browser->assertSee('Add Stock');
        });
    }

    public function testSellerIsRequiredInCreateSeller()
    {
        $faker = Faker\Factory::create();
        $this->browse(function (Browser $browser) use ($faker) {
            $today = now();
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/stock/create');
            
            $browser->select('available_per_day');
            $browser->select('stock_status');
            $browser->select('load_status');
            $browser->keys('#available_from_date', $today->year);
            $browser->keys('#available_from_date', '-'.$today->month);
            $browser->keys('#available_from_date', '-'.$today->day);
            $browser->type('size_to', $faker->randomNumber(2));
            $browser->type('size_from', $faker->randomNumber(2));
            $browser->type('quantity', $faker->randomNumber($nbDigits=2));
            $browser->type('city', $faker->safeEmail);
            $browser->type('postalcode', $faker->randomNumber($nbDigits=5));
            $browser->type('street', $faker->streetAddress);
            $browser->type('price', $faker->randomNumber(2));
            $browser->type('note', $faker->safeEmail);
            $browser->attach('image[]', public_path().'/img/AdobeStock_62381867.jpeg');
            $browser->driver->executeScript('window.scrollTo(0, 1500);');
            $browser->press('Create');
            $browser->waitForText("Error");
            $browser->pause(2000);
            $browser->assertSee("Error");
            $browser->assertSee("The given data was invalid.");
            $browser->press('OK');
            $browser->driver->executeScript('window.scrollTo(1500, 0);');
            $browser->assertSee("The seller id field is required.");
            $browser->assertPathIs('/admin/trading/stock/create');
        });
    }
    public function testStockCreate()
    {
        $faker = Faker\Factory::create();
        $this->browse(function (Browser $browser) use ($faker) {
            $today = now();
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/stock/create');
            $browser->select('seller_id');
            $browser->select('product_id');
            $browser->select('available_per_day');
            $browser->select('stock_status');
            $browser->select('load_status');
            $browser->keys('#available_from_date', $today->year);
            $browser->keys('#available_from_date', '-'.$today->month);
            $browser->keys('#available_from_date', '-'.$today->day);
            $browser->type('size_to', $faker->randomNumber(2));
            $browser->type('size_from', $faker->randomNumber(2));
            $browser->type('quantity', $faker->randomNumber($nbDigits=2));
            $browser->type('city', $faker->safeEmail);
            $browser->type('postalcode', $faker->randomNumber($nbDigits=5));
            $browser->type('street', $faker->streetAddress);
            $browser->type('price', $faker->randomNumber(2));
            $browser->type('note', $faker->safeEmail);
            $browser->attach('image[]', public_path().'/img/AdobeStock_62381867.jpeg');
            $browser->driver->executeScript('window.scrollTo(0, 1500);');
            $browser->press('Create');
            $browser->waitForText("Sent");
            $browser->assertSee("Sent!");
        });
    }
    public function testStockUpdate()
    {
        $faker = Faker\Factory::create();
        $this->browse(function (Browser $browser) use ($faker) {
            $today = now();
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/stock');
            $browser->pause(5000);
            $browser->click('#stock_table > tbody > tr.odd > td:nth-child(9) > div > button.btn.btn-edit.editItem');
            $browser->pause(1000);
            $browser->assertSee('Edit Stock');
            $browser->select('seller_id');
            $browser->select('product_id');
            $browser->select('available_per_day');
            $browser->select('stock_status');
            $browser->select('load_status');
            $browser->keys('#available_from_date', $today->year);
            $browser->keys('#available_from_date', '-'.$today->month);
            $browser->keys('#available_from_date', '-'.$today->day);
            $browser->type('size_to', $faker->randomNumber(2));
            $browser->type('size_from', $faker->randomNumber(2));
            $browser->type('quantity', $faker->randomNumber($nbDigits=2));
            $browser->type('city', $faker->safeEmail);
            $browser->type('postalcode', $faker->randomNumber($nbDigits=5));
            $browser->type('street', $faker->streetAddress);
            $browser->type('price', $faker->randomNumber(2));
            $browser->type('note', $faker->safeEmail);
            $browser->attach('image[]', public_path().'/img/AdobeStock_62381867.jpeg');
            $browser->driver->executeScript('window.scrollTo(0, 1500);');
            $browser->press('Update');
            $browser->waitForText("Sent");
            $browser->pause(10000);
            $browser->assertSee("Sent!");
        });
    }
    public function testStockdelete()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/stock');
            $browser->pause(5000);
            $browser->click('#stock_table > tbody > tr.odd > td:nth-child(9) > div > button.btn.btn-danger.deleteItem');
            $browser->pause(2000);
            $browser->assertSee('Are You sure want to delete?');
            $browser->press('Yes, delete it!');
            $browser->pause(2000);
            $browser->assertSee('Deleted!');
            $browser->press('OK');
            $browser->pause(2000);
            $browser->assertSee('Stock');
        });
    }
    public function testStockview()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/stock');
            $browser->pause(5000);
            $browser->click('#stock_table > tbody > tr > td:nth-child(9) > div > button.btn.btn-primary.viewItem');
            $browser->pause(3000);
            $browser->assertSee('Stock Details');
        });
    }
}
