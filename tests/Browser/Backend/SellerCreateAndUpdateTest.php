<?php

namespace Tests\Browser\Backend;

use App\Seller;
use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;

class SellerCreateAndUpdateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateSeller()
    {
        $faker = Faker\Factory::create();

        $this->browse(function (Browser $browser) use($faker) {
            $faker2 = new Faker\Generator();
            $faker2->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));
            try{
                $browser->loginAs($this->createAdmin()); 
                $browser->visit('/admin/sellers/create');
                $browser->type('username',  $faker->userName);
                $browser->type('company',  $faker->company);
                $browser->type('vat', $faker->randomNumber($nbDigits=5));
                $browser->type('phone',   $faker2->landlineNumber(false));
                $browser->type('name',  $faker->name);
                $browser->type('email',  $faker->safeEmail);
                $browser->type('seller2_contact[phone]',   $faker2->landlineNumber(false));
                $browser->type('seller2_contact[name]', $faker->name);
                $browser->type('seller2_contact[email]', $faker->safeEmail);
                $browser->type('transport_contact[phone]',  $faker2->landlineNumber(false));
                $browser->type('transport_contact[name]', $faker->name);
                $browser->type('transport_contact[email]', $faker->safeEmail);
                $browser->type('accounts_contact[phone]',  $faker2->landlineNumber(false));
                $browser->type('accounts_contact[name]', $faker->name);
                $browser->type('accounts_contact[email]', $faker->safeEmail);
                $browser->type('city', $faker->city);
                $browser->type('postalcode', $faker->randomNumber($nbDigits=6));
                $browser->type('address', $faker->address);
                $browser->type('available_stocks', $faker->randomNumber($nbDigits=2));
                $browser->type('note', $faker->text);
                // $browser->press('Create');
                $browser->assertButtonDisabled('Create');
               $browser->waitForText("Sent");
                $browser->assertSee("Sent!");
                $browser->pause(5000);
                $browser->assertPathIs('/admin/sellers');
            }catch(Exception $ex){
                dd($ex->getMessage());  
            }
        });
    }

    public function testUpdateSeller()
    {
        $faker = Faker\Factory::create();

        $this->browse(function (Browser $browser) use($faker) {
            try{

                $faker2 = new Faker\Generator();
                $email = $faker->safeEmail;
                $faker2->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));           
                $browser->loginAs($this->createAdmin()); 
                $browser->visit('/admin/sellers/create');
                $browser->type('username',  $faker->userName);
                $browser->type('company',  $faker->company);
                $browser->type('vat', $faker->randomNumber($nbDigits=5));
                $browser->type('phone',   $faker2->landlineNumber(false));
                $browser->type('name',  $faker->name);
                $browser->type('email', $email);
                $browser->type('seller2_contact[phone]',   $faker2->landlineNumber(false));
                $browser->type('seller2_contact[name]', $faker->name);
                $browser->type('seller2_contact[email]', $faker->safeEmail);
                $browser->type('transport_contact[phone]',  $faker2->landlineNumber(false));
                $browser->type('transport_contact[name]', $faker->name);
                $browser->type('transport_contact[email]', $faker->safeEmail);
                $browser->type('accounts_contact[phone]',  $faker2->landlineNumber(false));
                $browser->type('accounts_contact[name]', $faker->name);
                $browser->type('accounts_contact[email]', $faker->safeEmail);
                $browser->type('city', $faker->city);
                $browser->type('postalcode', $faker->randomNumber($nbDigits=6));
                $browser->type('address', $faker->address);
                $browser->type('available_stocks', $faker->randomNumber($nbDigits=2));
                $browser->type('note', $faker->text);
                $browser->press('Create');
                $browser->waitForText("Sent");
                $browser->assertSee("Sent!");
                $browser->pause(5000);
                $browser->assertPathIs('/admin/sellers');
                $seller = Seller::where('email', $email)->first();
                $browser->visit('/admin/sellers/'.$seller->id.'/edit');
                $browser->type('city', $faker->city);
                $browser->type('postalcode', $faker->randomNumber($nbDigits=6));
                $browser->type('address', $faker->address);
                $browser->type('available_stocks', $faker->randomNumber($nbDigits=2));
                $browser->press('Create');
                $browser->waitForText("Sent!");
                $browser->assertSee("Seller updated successfully");
                $browser->pause(3000);
                $browser->assertPathIs('/admin/sellers');
            
            }catch(Exception $ex){
                dd($ex->getMessage());  
            }
            
        });
    }

    public function testCheckSellerlistPermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('/admin/sellers');
            $browser->assertPathIs('/login');
        });
    } 

    public function testCheckSellerCreatePermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('/admin/sellers/create');
            $browser->assertPathIs('/login');
        });
    } 

    public function testOnlyAdminCanCreateSeller(){
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/sellers/create');
            $browser->assertPathIs('/admin/sellers/create');
            $browser->assertSee('Add Sellers');
        });
    } 
    public function testOnlyAdminCanAccessSellerList(){

        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/sellers');
            $browser->assertPathIs('/admin/sellers');
            $browser->assertSee('Sellers');
        });
    } 
}
