<?php

namespace Tests\Browser\Backend;

use App\Buyer;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;

class CreateBuyerTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_CreateBuyer()
    {
        $faker = Faker\Factory::create();

        $this->browse(function (Browser $browser) use($faker) {
            
            $faker2 = new Faker\Generator();
            $faker2->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));
           
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/buyers/create')
                    ->assertPathIs('/admin/buyers/create')
                    ->assertSee('Add Buyer');
            $browser->type( 'username',  $faker->username);
            $browser->press('+');
            $browser->type('note',  $faker->text);
            $browser->type('phone',  $faker2->landlineNumber(false));
            $browser->type( 'name',  $faker->name);
            $browser->type( 'email',  $faker->safeEmail);
            $browser->type( 'buyer2_contact[phone]', $faker2->landlineNumber(false));
            $browser->type( 'buyer2_contact[name]',  $faker->name);
            $browser->type( 'buyer2_contact[email]',  $faker->safeEmail);
            $browser->type( 'transport_contact[phone]', $faker2->landlineNumber(false));
            $browser->type( 'transport_contact[name]',  $faker->name);
            $browser->type( 'transport_contact[email]',  $faker->safeEmail);
            $browser->type( 'accounts_contact[phone]', $faker2->landlineNumber(false));
            $browser->type( 'accounts_contact[name]',  $faker->name);
            $browser->type( 'accounts_contact[email]',  $faker->safeEmail);
            $browser->type( 'company',  $faker->company);            
            $browser->type( 'vat',  $faker->randomNumber($nbDigits=5));
            $faker2->addProvider(new Faker\Provider\en_US\Address($faker2));
            $browser->type( 'city',  $faker->city);
            $browser->type( 'postalcode', $faker->randomNumber($nbDigits=6));
            $browser->type( 'address', $faker->address);
            $browser->select('country');
            $browser->select('trust_level');            
            $browser->click("#formsubmit > div > div > div.row.mt-4.mb-4 > div > div:nth-child(11) > div > div > div.input-group-append > button");
            $browser->type( 'truck_quantity',  $faker->randomNumber($nbDigits=2));
            $browser->press('Create');
            $browser->waitForText("Sent");
            $browser->assertSee("Sent!");
            $browser->pause(5000);
            $browser->assertPathIs('/admin/buyers');

        });
        
    }

    public function test_UpdateBuyer()
    {
        $faker = Faker\Factory::create();

        $this->browse(function (Browser $browser) use($faker) {
            
            $faker2 = new Faker\Generator();
            $faker2->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));
            $email = $faker->safeEmail;
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/buyers/create')
                    ->assertPathIs('/admin/buyers/create')
                    ->assertSee('Add Buyer');
            $browser->type( 'username',  $faker->username);
            $browser->press('+');
            $browser->type('note',  $faker->text);
            $browser->type('phone',  $faker2->landlineNumber(false));
            $browser->type( 'name',  $faker->name);
            $browser->type( 'email', $email);
            $browser->type( 'buyer2_contact[phone]', $faker2->landlineNumber(false));
            $browser->type( 'buyer2_contact[name]',  $faker->name);
            $browser->type( 'buyer2_contact[email]',  $faker->safeEmail);
            $browser->type( 'transport_contact[phone]', $faker2->landlineNumber(false));
            $browser->type( 'transport_contact[name]',  $faker->name);
            $browser->type( 'transport_contact[email]',  $faker->safeEmail);
            $browser->type( 'accounts_contact[phone]', $faker2->landlineNumber(false));
            $browser->type( 'accounts_contact[name]',  $faker->name);
            $browser->type( 'accounts_contact[email]',  $faker->safeEmail);
            $browser->type( 'company',  $faker->company);            
            $browser->type( 'vat',  $faker->randomNumber($nbDigits=5));
            $faker2->addProvider(new Faker\Provider\en_US\Address($faker2));
            $browser->type( 'city',  $faker->city);
            $browser->type( 'postalcode', $faker->randomNumber($nbDigits=6));
            $browser->type( 'address', $faker->address);
            $browser->select('country');
            $browser->select('trust_level');            
            $browser->click("#formsubmit > div > div > div.row.mt-4.mb-4 > div > div:nth-child(11) > div > div > div.input-group-append > button");
            $browser->type( 'truck_quantity',  $faker->randomNumber($nbDigits=2));
            $browser->press('Create');
            $browser->waitForText("Sent");
            $browser->assertSee("Sent!");
            $browser->pause(5000);
            $browser->assertPathIs('/admin/buyers');

            $buyer = Buyer::where('email', $email)->first();
            $browser->visit('/admin/buyers/'.$buyer->id.'/edit');
            $browser->press('+');
            $browser->select('country');
            $browser->select('trust_level');   
            $browser->type( 'truck_quantity',  $faker->randomNumber($nbDigits=2));
            $browser->press('Update');
            $browser->waitForText("Sent");
            $browser->assertSee("Sent!");
            $browser->assertSee("Buyer updated successfully.");     
            $browser->pause(5000);
            $browser->assertPathIs('/admin/buyers');
             
        });
        
    }

    public function testCheckBuyerlistPermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('/admin/buyers');
            $browser->assertPathIs('/login');
        });
    } 

    public function testOnlyAdminCanAccessBuyerList(){

        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/buyers');
            $browser->assertPathIs('/admin/buyers');
            $browser->assertSee('Buyers');
        });
    } 
    public function testCheckBuyerCreatePermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('/admin/buyers/create');
            $browser->assertPathIs('/login');
        });
    } 

    public function testOnlyAdminCanCreateBuyer(){
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/buyers/create');
            $browser->assertPathIs('/admin/buyers/create');
            $browser->assertSee('Add Buyer');
        });
    } 
}
