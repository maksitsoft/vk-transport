<?php 
namespace Tests\Browser\Backend;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;
use Tests\Browser\Components\DatePicker;

class PurchaseTest extends DuskTestCase
{
    public function testPurchagelistPermission()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/accounts/purchaseorder');
            $browser->assertPathIs('/login');
        });
    }
    
    public function testOnlyAdminCanAccessPurchaseList()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/accounts/purchaseorder');
            $browser->assertPathIs('/admin/accounts/purchaseorder');
            $browser->assertSee('Purchase Order');
        });
    }


    public function testCheckPurchaseCreatePermission()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/accounts/purchaseorder');
            $browser->assertPathIs('/login');
        });
    }

    public function testOnlyPurchaseCanCreateStock()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/accounts/purchaseorder');
            $browser->assertPathIs('/admin/accounts/purchaseorder');
            $browser->assertSee('Purchase Order');
        });
    }
    public function testPurchaseCreate(){
        $faker = Faker\Factory::create();
        $this->browse(function(Browser $browser)use($faker)
        {
            $today = now();
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('admin/accounts/purchaseorder');
            $browser->waitfor('@create-click');
            $browser->click('@create-click'); 
            $browser->assertSee('Add Purchase Order');
            $browser->select('stock_id');
            $browser->select('buyer_id');
            $browser->select('seller_id');
            $browser->type('price', $faker->randomNumber(2)); 
            $browser->type('delivery_date', '2019-11-26');
            $browser->press('Create');
            $browser->pause(5000);
            $browser->assertSee('Purchase Order');
            $browser->assertPathIs('/admin/accounts/purchaseorder');
        });
    }
}
