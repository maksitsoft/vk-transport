<?php

namespace Tests\Browser\Backend;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;
use Tests\Browser\Components\DatePicker;

class MatchTest extends DuskTestCase
{
    public function testMatchPermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('/admin/trading/matches');
            $browser->assertPathIs('/login');
        });
    }
    public function testOnlyAdminCanAccessMatchPermission(){
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/trading/matches');
            $browser->assertPathIs('/admin/trading/matches');
            $browser->assertSee('Stock Matches');

        });
    }
    public function testMakeSale()
    {
        $faker = Faker\Factory::create();
        $this->browse(function (Browser $browser) use ($faker) {
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/matches');
            $browser->assertSee('Stock Matches');
            $browser->pause(2000);
            $browser->press('Make Sale');
            $browser->pause(2000);
            $browser->select('buyer_id');
            $browser->type('match_id',$faker->randomNumber($nbDigits=5));
            $browser->select('stock_id');
            $browser->select('payment_options');
            $browser->type('defect_percentage', $faker->randomNumber(2));
            $browser->select('payment_type');
            $browser->select('payment_currency');
            $browser->radio('status', 'ordered');
            $browser->press('Create');
            $browser->pause(5000);
            $browser->assertSee('Orders');
        });
    }
    

}