<?php

namespace Tests\Browser\Backend;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;
use Tests\Browser\Components\DatePicker;

class OrderTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    
    public function testOrderPermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('/admin/trading/order2');
            $browser->assertPathIs('/login');
        });
    } 

    public function testOnlyAdminCanAccessOrderList(){

        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/trading/order2');
            $browser->assertPathIs('/admin/trading/order2');
            $browser->assertSee('Order');
        });
    } 


    public function testCheckStockCreatePermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('/admin/trading/order2/create');
            $browser->assertPathIs('/login');
        });
    } 

    public function testOnlyAdminCanCreateOrder(){
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/trading/order2/create');
            $browser->assertPathIs('/admin/trading/order2/create');
            $browser->assertSee('Add Order');
        });
    } 

    public function testCreateOrder(){
        $faker = Faker\Factory::create();
        $this->browse(function (Browser $browser)use($faker) {
          
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/trading/order2/create');
            $browser->assertSee('Add Order');
            $browser->select('buyer_id');
            $browser->select('stock_id');
            $browser->type('transport_id', $faker->randomNumber(2));
            $browser->type('payment_id', $faker->randomNumber(2));
            $browser->type('message_id', $faker->randomNumber(2));
            $browser->radio('payment_status', 'Paid');
            $browser->press('Create');
            $browser->pause(5000);
            $browser->assertSee('Order');
            $browser->assertPathIs('/admin/trading/order2');
        });
    }
    public function testUpdateOrder(){
        $faker = Faker\Factory::create();
        $this->browse(function (Browser $browser)use($faker) {
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('/admin/trading/order2');
            $browser->pause(5000);
            $browser->click(' #order2_table > tbody > tr:nth-child(1) > td.sorting_1');
            $browser->click('#order2_table > tbody > tr.child > td > ul > li > span.dtr-data > div > button.btn.btn-edit.editItem');
            $browser->assertSee('Edit Order');
            $browser->select('buyer_id');
            $browser->type('stock_id',$faker->randomNumber(2));
            $browser->type('transport_id', $faker->randomNumber(2));
            $browser->type('payment_id', $faker->randomNumber(2));
            $browser->type('message_id', $faker->randomNumber(2));
            $browser->radio('payment_status', 'Paid');
            $browser->press('Create');
            $browser->pause(5000);
            $browser->assertSee('Order');
        });
    }
    public function testDeleteOrder(){
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin());
            $browser->visit('/admin/trading/order2');
            $browser->pause(5000);
            $browser->click(' #order2_table > tbody > tr:nth-child(1) > td.sorting_1');
            $browser->click('#order2_table > tbody > tr.child > td > ul > li > span.dtr-data > div > button.btn.btn-danger.deleteItem');
            $browser->assertSee('Are You sure want to delete?');
            $browser->press('Yes, delete it!');
            $browser->pause(2000);
            $browser->assertSee('Deleted!');
            $browser->press('OK');
            $browser->pause(5000);
            $browser->assertSee('order');
        });
    }
    public function testviewpdf()
    {
       
        $this->browse(function(Browser $browser){
         $browser->loginAs($this->createAdmin());   
         $browser->visit('/admin/trading/order2');
         $browser->pause(5000);
        $browser->click(' #order2_table > tbody > tr:nth-child(1) > td.sorting_1');
        $browser->click('#order2_table > tbody > tr.child > td > ul > li > span.dtr-data > div > button.btn.btn-success.sendInvoice');
        $browser->pause(2000);
        $browser->assertSee('Are you sure?');
        $browser->press('View PDF');
        $browser->pause(2000); 
        $window = collect($browser->driver->getWindowHandles())->last();
        $browser->driver->switchTo()->window($window);
      
        });
    }

}

