<?php

namespace Tests\Browser\Backend;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Faker;


class InquiriesOperationsTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCheckInquirieslistPermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('admin/trading/orders');
            $browser->assertPathIs('/login');
        });
    } 

    public function testOnlyAdminCanAccessInquiriesList(){

        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('admin/trading/orders');
            $browser->assertPathIs('admin/trading/orders');
            $browser->assertSee('Stock');
        });
    } 


    public function testCheckInquiriesCreatePermission(){

        $this->browse(function (Browser $browser){
            $browser->visit('admin/trading/orders/create');
            $browser->assertPathIs('/login');
        });
    } 

    public function testOnlyAdminCanCreateInquiries(){
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('admin/trading/orders/create');
            $browser->assertPathIs('/admin/trading/orders/create');
            $browser->assertSee('Add Order');
        });
    } 

    public function testInquiriesCreate()
    {
        $faker = Faker\Factory::create();
        $this->browse(function (Browser $browser)use($faker) {
            $today = now();
            $browser->loginAs($this->createAdmin()); 
            $browser->visit('admin/trading/orders/create');
            $browser->select('buyer_id');
            $browser->select('product_id');
            $browser->select('variety');       
            $browser->select('packing');
            $browser->select('flesh_color');                    
            $browser->type('size_range[0][from]', 2);
            $browser->type('size_range[0][to]', $faker->randomNumber(2));
            $browser->type('quantity', $faker->randomNumber($nbDigits=2));
            $browser->type('location_range[0][from]', 2);
            $browser->type('location_range[0][to]', $faker->randomNumber(2));
            $browser->type('price_range[0][from]', 2);
            $browser->type('price_range[0][to]', $faker->randomNumber(2));
            $browser->press('Create');
            $browser->waitForText("Sent");
            $browser->assertSee("Sent");
            $browser->pause(2000);
            $browser->assertPathIs('/admin/trading/orders');
        });

    }
}
