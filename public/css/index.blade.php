@extends('frontend.layouts.app-fullwidth')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('classes', 'home has-slider')
@section('slider')

<div class="page-header mb-0 pb-0 pt-0">
	<input type="hidden" name="ip" id="ip" value="">
	<!--<div class="overlay"></div>-->
	<!--<div class="page-header-bg" style="background-image: url('{{ asset('img/british-flag-1907933.jpg')}}');background-position: bottom center;"></div>
	<div class="container h-100">
	<div class="d-flex h-100 content text-center align-items-center align-self-center">
	<div class="w-100 text-white">
	<h1 class="display-4 text-white mb-3">Welcome to <span class="script-font">{{app_name()}}</span></h1>
	<h5 class="mb-0 text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
	Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </h5>
	<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase" href="{{ route('frontend.buyercontact') }}">Get A Quote!</a>
</div>
</div>
</div>-->
<div id="carouselidControls" class="carousel slide HomepageCarosel" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<div class="page-header-bg" style="background-image: url('{{ asset('img/home-banne1920.jpg')}}');"></div>
			<div class="container h-100">
				<div class="d-flex h-100 content text-center align-items-center align-self-center">
					<div class="w-100 text-white">
						<h1 class="display-4 text-white mb-3"><!--Welcome to --><span class="script-font"><!--{{app_name()}}--></span></h1>
						<h2 class="mb-0 text-center" style="margin-top: 6%;">@lang('inner-content.frontend.homepage-content.slider-title')</h2>
                        <div class="VegkingBannerBtns">
							<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase"  href="{{route('frontend.buyercontact')}}" >@lang('inner-content.frontend.homepage-content.buy')</a>
							<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase" href="javascript:;">@lang('inner-content.frontend.homepage-content.sell')</a>
						</div>
						</div>
					</div>
				</div>
			</div>
		<!--	<div class="carousel-item">
				<div class="page-header-bg" style="background-image: url('{{ asset('img/AdobeStock_62381867.jpeg')}}');background-position: bottom center;"></div>
				<div class="container h-100">
					<div class="d-flex h-100 content text-center align-items-center align-self-center">
						<div class="w-100 text-white">
							<h1 class="display-4 text-white mb-3">@lang('inner-content.frontend.homepage-content.welcome')<span class="script-font">{{app_name()}}</span></h1>
							<h5 class="mb-0 text-center">@lang('inner-content.frontend.homepage-content.welcome-content') </h5>
								<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase"  href="{{route('frontend.buyercontact')}}" >@lang('inner-content.frontend.homepage-content.buy')</a>
								<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase" href="javascript:;">@lang('inner-content.frontend.homepage-content.sell')</a>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item">
					<div class="page-header-bg" style="background-image: url('{{ asset('img/AdobeStock_207035862.jpeg')}}');background-position: bottom center;"></div>
					<div class="container h-100">
						<div class="d-flex h-100 content text-center align-items-center align-self-center">
							<div class="w-100 text-white">
								<h1 class="display-4 text-white mb-3">@lang('inner-content.frontend.homepage-content.welcome') <span class="script-font">{{app_name()}}</span></h1>
								<h5 class="mb-0 text-center">@lang('inner-content.frontend.homepage-content.welcome-content')</h5>
									<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase"  href="{{route('frontend.buyercontact')}}" >@lang('inner-content.frontend.homepage-content.buy')</a>
									<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase" href="javascript:;">@lang('inner-content.frontend.homepage-content.sell')</a>
								</div>
							</div>
						</div>
					</div>
					<div class="carousel-item">
						<div class="page-header-bg" style="background-image: url('{{ asset('img/british-flag-1907933.jpg')}}');background-position: bottom center;"></div>
						<div class="container h-100">
							<div class="d-flex h-100 content text-center align-items-center align-self-center">
								<div class="w-100 text-white">
									<h1 class="display-4 text-white mb-3">@lang('inner-content.frontend.homepage-content.welcome') <span class="script-font">{{app_name()}}</span></h1>
									<h5 class="mb-0 text-center">@lang('inner-content.frontend.homepage-content.welcome-content')</h5>
										<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase"  href="{{route('frontend.buyercontact')}}" >@lang('inner-content.frontend.homepage-content.buy')</a>
										<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase" href="javascript:;">@lang('inner-content.frontend.homepage-content.sell')</a>
									</div>
								</div>
							</div>
						</div>
	-->
					</div>
					<!--<a class="carousel-control-prev" href="#carouselidControls" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">@lang('inner-content.frontend.homepage-content.previous')</span>
					</a>
					<a class="carousel-control-next" href="#carouselidControls" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">@lang('inner-content.frontend.homepage-content.next')</span>
					</a>-->
				</div>
			</div>
			@endsection
			@section('content')
			<section id="aboutus" class="about pt-5 pb-5">
				<div class="container">
					<div class="row pb-3">
						<div class="col-md-6">
							<div class="author-image mb-4">
								<!--<img src="{{asset('img/british-flag-1907933.jpg')}}" width="100%;" height="100%;">-->
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="http://vegking.eu/wp-content/uploads/2019/08/VEG-KING-2_2-1.mp4" allowfullscreen></iframe>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!--<h2>ABOUT US</h2>
							<h4 class="veg_king_abuot">VEG KING is a professional vegetable distribution chain in Europe.</h4>
							<h5 class="veg_king_abuot">We have been supplying European producers of the vegetable industry for years.</h5>-->
							@lang('inner-content.frontend.homepage-content.section-1')
							
						<button class="btn btn-outline-success btn-lg text-uppercase" id="LearnMoreBtnn">@lang('inner-content.frontend.homepage-content.read-more')</button>
						
                        </div>
						<div class="col-md-12" id="LearnMoredescription" style="display: none;">
							@lang('inner-content.frontend.homepage-content.read-more-content')
							
						</div>


					</div>
				</div>
			</section>



			<section class="services-import section-first">
				<div class="container" id="import-link">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col section-short">
							<h2>@lang('inner-content.frontend.section-short-inner.import')</h2>
							<div class="section-short-inner">
								<figure> <img src="{{ asset('img/offer.png') }}" alt="" width="93" height="94"> </figure>
								<h5>@lang('inner-content.frontend.section-short-inner.heading-col-1')</h5>
								<span class="line"><em></em></span>
								<div>
									<p>@lang('inner-content.frontend.section-short-inner.content-col-1')</p>
								</div>
								<a href="" class="btn"></a> </div>
								<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-4 col section-short">
								<h2></h2>
								<div class="section-short-inner">
									<figure> <img src="{{ asset('img/import-2-yellow.png') }}" alt="" width="80" height="95"> </figure>
									<h5>@lang('inner-content.frontend.section-short-inner.heading-col-2')</h5>
									<span class="line"><em></em></span>
									<div>
										<p>@lang('inner-content.frontend.section-short-inner.content-col-2')</p>
									</div>
									<a href="" class="btn"></a> </div>
									<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col section-short">
									<h2></h2>
									<div class="section-short-inner">
										<figure> <img src="{{ asset('img/vegetablesyellow.png') }}" alt="" width="95" height="101"> </figure>
										<h5>@lang('inner-content.frontend.section-short-inner.heading-col-3')</h5>
										<span class="line"><em></em></span>
										<div>
											<p>@lang('inner-content.frontend.section-short-inner.content-col-3')</p>
										</div>
										<a href="" class="btn"></a> </div>
										<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
									</div>
								</div>
							</div>
						</section>


						<section class="services-export">
							<div class="container" id="export-link">
								<div class="row"><div class="col-xs-12 col-sm-12 col-md-4 col section-short">
									<h2>@lang('inner-content.frontend.section-short-inner.export')</h2>
									<div class="section-short-inner">
										<figure>
											<img src="{{ asset('img/export-1.png') }}" alt="" width="92" height="94">
										</figure>
										<h5>@lang('inner-content.frontend.section-short-inner.heading-row-1')</h5>
										<span class="line"><em></em></span>
										<div>
											<p>@lang('inner-content.frontend.section-short-inner.content-row-1')</p>
										</div>
										<a href="" class="btn"></a>
									</div>
									<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
								</div><div class="col-xs-12 col-sm-12 col-md-4 col section-short">
									<h2></h2>
									<div class="section-short-inner">
										<figure>
											<img src="{{ asset('img/fast-delivery-yellow.png') }}" alt="" width="94" height="96">
										</figure>
										<h5>@lang('inner-content.frontend.section-short-inner.heading-row-2')</h5>
										<span class="line"><em></em></span>
										<div>
											<p>@lang('inner-content.frontend.section-short-inner.content-row-2')</p>
										</div>
										<a href="" class="btn"></a>
									</div>
									<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
								</div><div class="col-xs-12 col-sm-12 col-md-4 col section-short">
									<h2></h2>
									<div class="section-short-inner">
										<figure>
											<img src="{{ asset('img/export-3.png') }}" alt="" width="92" height="94">
										</figure>
										<h5>@lang('inner-content.frontend.section-short-inner.heading-row-3')</h5>
										<span class="line"><em></em></span>
										<div>
											<p>@lang('inner-content.frontend.section-short-inner.content-row-3')</p>
										</div>
										<a href="" class="btn"></a>
									</div>
									<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
								</div></div>
							</div>
						</section>



						<!--<section class="services-mission">
							<div class="page-header mb-0">
								<div class="page-header-bg mission" style="background-image: url('{{ asset('img/baby-potatoes-farm-farming-775707.jpg')}}');background-position: left center;"></div>
								<div class="container h-100 c_mission">
									<div class="d-flex h-100 align-items-center align-self-center">
										<div class="row">
											<div class="w-100 text-white col-md-8">
												<h2 class="text-white mb-4">MISSION</h2>
												<h3 class="mb-4">Join Veg King and become a leader in the food industry thanks to:</h3>
												<ul class="p-0 truck_linecon">
													<li><i class="fa fa-truck linecon-icon-truck extra-color-3"></i>
														<h4>permanent vegetable supplier all year round</h4>
													</li>
													<li><i class="fa fa-truck linecon-icon-truck extra-color-3"></i>
														<h4>shortening processes logistics through liquid supply chain</h4>
													</li>
													<li><i class="fa fa-truck linecon-icon-truck extra-color-3"></i>
														<h4>cost optimization, in production for a bigger one scale</h4>
													</li>
													<li><i class="fa fa-truck linecon-icon-truck extra-color-3"></i>
														<h4>timely delivery regardless of market forecasts or weather conditions</h4>
													</li>
												</ul>
												<a class="btn btn-success btn-lg mt-1 pl-4 pr-4 mr-3 text-uppercase" href="{{ route('frontend.contact') }}">Learn more</a>
											</div>
											<div class="col-md-4"></div>
										</div>
									</div>
								</div>
                                </div>
							</section>
							<section class="smart-delivery pt-5 pb-4">
								<div class="container">
									<div class="row">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<div class="author-image mb-4 text-center">
												<!--<img src="{{asset('img/british-flag-1907933.jpg')}}" width="100%;" height="100%;"
												<h2 class="text-center mb-4">SMART DELIVERY</h2>
												<p class="text-center mb-4" style="color: rgba(0,0,0,0.7);">Many years of experience in cooperation with the largest food producers, helped us create a personalized delivery program.</p>
												<p class="text-center mb-5" style="color: rgba(0,0,0,0.7);">VEG KING guarantee one point at contact dedicated to your company, as well as an intelligent system supply and inventory management.</p>
												<a class="btn btn-success btn-lg mt-1 pl-4 pr-4 mr-3 mb-2 text-uppercase" href="{{ route('frontend.contact') }}">Learn more</a>
											</div>
										</div>
										<div class="col-md-2"></div>
									</div>
								</div>
							</section>
							<section class="section-bg" style="background-image: url('{{ asset('img/joao-marcelo-marques-Qp0lt8ehfjg-unsplash.jpg')}}');">
								<div class="overlay"></div>
								<div class="text-white pt-5 pb-5">
									<div class="container">
										<div class="row">
											<br/>
											<div class="col text-center">
												<h2 class="mb-4">EXPERIENCE</h2>
												<hr class="w-25 m-auto hr-bg-color">
												<div class="row pt-5 pb-5">
													<div class="col-md-6 text-left mt-4">
														<h3 class="mb-4 experience-title">Trusted Company</h3>
														<p>Wybieramy partnerów biznesowych, którzy wywiązują się ze swoich zobowiązań przez 365 dni w roku, by nie zakłócić codziennego funkcjonowania Twojego przedsiębiorstwa.</p>
														<p>Dzięki wypracowanym relacjom biznesowym oraz współpracy w modelu klastra, możemy pomóc Twojej działalności w spełnieniu celów sprzedażowych, również w pozostałych kategoriach  warzyw nieprzetworzonych.</p>
													</div>
													<div class="col-md-6 text-left mt-4">
														<h3 class="mb-4 experience-title">Trusted Company</h3>
														<p>W VEG KING gwarantujemy jeden punkt kontaktowy dedykowany Twojej firmie, a także inteligentny system zarządzania dostawami i zapasami.</p>
														<p>Skontaktuj się z polskim odziałem VEG KING, aby zapytać o aktualną ofertę.</p>
														<a href="" class="btn btn-outline-success btn-lg text-uppercase"><span>SKONTAKTUJ SIĘ</span></a>
													</div>
												</div>
											</div>
										</div>
										<div class="row text-center pt-4 pb-5">
											<div class="col">
												<div class="counter">
													<img src="{{asset('img/growth.svg')}}" width="50" height="50" class="mb-4">
													<h2 class="timer count-title count-number" data-to="97" data-speed="1500"></h2>
													<p class="count-text ">Satisfied customers</p>
												</div>
											</div>
											<div class="col">
												<div class="counter">
													<img src="{{asset('img/shopping-cart.svg')}}" width="50" height="50" class="mb-4">
													<h2 class="timer count-title count-number" data-to="4800" data-speed="1500"></h2>
													<p class="count-text ">Successful delivery</p>
												</div>
											</div>
											<div class="col">
												<div class="counter">
													<img src="{{asset('img/tractor.svg')}}" width="50" height="50" class="mb-4">
													<h2 class="timer count-title count-number" data-to="48" data-speed="1500"></h2>
													<p class="count-text ">Providers</p>
												</div>
											</div>
											<div class="col">
												<div class="counter">
													<img src="{{asset('img/bomb-detonation.svg')}}" width="50" height="50" class="mb-4">
													<h2 class="timer count-title count-number" data-to="15" data-speed="1500"></h2>
													<p class="count-text ">Products</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section-->
							<!--<section class="smart-delivery pt-5 pb-5 mt-4 mb-4">
								<div class="container">
									<div class="row">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<div class="author-image mb-4 text-center">												
												<a href="{{ route('frontend.contact') }}" ><h3 class="text-center mb-4" style="color:#105640;">KATEGORIE WARZYW</h3></a>

											</div>
										</div>
										<div class="col-md-2"></div>
									</div>
								</div>
							</section>
							<section class="currentoffersection" style="background-image: url('{{ asset('img/potatoes-411975.jpg')}}');background-position: center bottom;background-size: cover;
    background-position: center center;">
								<div class="page-header mb-0">
									<div class="page-section-bg"></div>
									<div class="container h-100">
										<div class="d-flex h-100 content text-center align-items-center align-self-center">
											<div class="w-100 text-white">
												<small class="text-center">Discover the current offer at the best price.</small>
												<h2 class="mb-0 text-center">CURRENT OFFER</h2>
												<small>These vegetables can be at your place even in 24h from ordering!</small></br>
												<a class="btn btn-success btn-lg mt-5 pl-4 pr-4 mr-3 text-uppercase" href="{{ route('frontend.contact') }}">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
							</section-->
                            
                            <section class="container-fluid NewsLetterSec">								
									<div class="row">
                                  
                                    <div class="col-md-4">
                                    <div class="SignUpSec">
                                    
                                    <h2>@lang('inner-content.frontend.newlettersec.newsletter')</h2>
                                    <p>@lang('inner-content.frontend.newlettersec.newsletter-content')</p>
                                    <div class="EqualLabelFiled">
                                    <input type="email" placeholder="E-MAIL">
                                    <button class="signupbtn"><img src="<?php echo URL::to('/') ?>/img/envelopegreen.png"></button>
                                    </div>
                                    
                                    
                                   </div> 
                                    </div>
                                    <div class="col-md-8">
                                    
                                    
                                    
                                    <div class="row NewproductSecmain">
                                    
                                    
                                    
                                                                   
                                    <div class="col-md-6">                                    
                                    <div class="NewproductSec">                                    
                                    <h2>@lang('inner-content.frontend.newlettersec.poffers')</h2>
                                    <h6>@lang('inner-content.frontend.newlettersec.beets')</h6>
                                    <p>@lang('inner-content.frontend.newlettersec.beets-content')</p>
                                    <button class="Seeofferbtn">@lang('inner-content.frontend.newlettersec.see-offer')</button>                          
                                   </div>                           
                                    </div>                                    
                                    <div class="col-md-6">
                                    <div class="productimageSec" style="background-image:url('<?php echo URL::to('/') ?>/img/beets.png');">
                                    </div>
                                    </div>                                                                   
                                    </div>
                                    
                                    </div>
									</div>
							</section>
                            
                            <section class="salesHomesec">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="ContactPageHeading">@lang('inner-content.frontend.salessec.title-1')</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="SinglePersonDetails"> <img src="<?php echo URL::to('/') ?>/img/c-user-profile-green.png" class="UserProfileimg">
          <ul class="ProfileDetails">
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-user-profile-name.png" class="userprofileicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.name-1') </span></li>
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.phone-1')</span></li>
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.email-1')</span></li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <div class="SinglePersonDetails"> <img src="<?php echo URL::to('/') ?>/img/c-user-profile-green.png" class="UserProfileimg">
          <ul class="ProfileDetails">
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-user-profile-name.png" class="userprofileicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.name-2')</span></li>
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.phone-2')</span></li>
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.email-2')</span></li>
          </ul>
        </div>
      </div>
    </div>
    </div>
    </section>
    
    
    
    
    
    <section class="ContactHomeSec">
     <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="ContactPageHeading">@lang('inner-content.frontend.contactsec.heading')</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="LocationaddressSec">
          <ul>
            <li class="polandAlignText"><span class="righttext"><img src="<?php echo URL::to('/') ?>/img/c-polland-flag.png" class="LocationFlag"></span><span class="lefttext">@lang('inner-content.frontend.contactsec.country-1')</span></li>
          </ul>
          <p class="AddressText">@lang('inner-content.frontend.contactsec.address-1')
          </p>
        </div>
        <div class="SinglePersonDetails">
          <ul class="ProfileDetails">
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><span class="righttext">@lang('inner-content.frontend.contactsec.phone-1')</span></li>
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><span class="righttext">@lang('inner-content.frontend.contactsec.email-1')</span></li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <div class="LocationaddressSec">
          <ul>
            <li><span class="righttext"><img src="<?php echo URL::to('/') ?>/img/c-us-flag.png" class="LocationFlag"></span><span class="lefttext">@lang('inner-content.frontend.contactsec.country-2')</span></li>
          </ul>
          <p class="AddressText">@lang('inner-content.frontend.contactsec.address-2')</p>
        </div>
        <div class="SinglePersonDetails">
          <ul class="ProfileDetails">
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><span class="righttext">@lang('inner-content.frontend.contactsec.phone-2')</span></li>
            <li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><span class="righttext">@lang('inner-content.frontend.contactsec.email-2')</span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p class="footerbottomText">@lang('inner-content.frontend.contactsec.content')</p>
      </div>
    </div>
    </div>
    </section>
    
                            
@endsection

@push('after-scripts')
<script>
$(".section-short").hover(function(){
$(this).addClass('hovered');
$(this).find('.bg').css({"left": "0", "top": "0"});
}, function(){
$(this).removeClass('hovered');
$(this).find('.bg').css({"left": "100%", "top": "0"});
});
</script>
<script src="https://www.jqueryscript.net/demo/Detect-Browser-Device-OS/dist/jquery.device.detector.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
var user_id = {{ $user_id }};
$.ajax({
url: "https://jsonip.com",
type: 'get',
cache: false,
success: function(res){
console.log(res.ip)	;   
$("#ip").val(res.ip);
var ip = $("#ip").val();	

var instance = $.fn.deviceDetector;

$.ajax({
url: "{{ route('frontend.referrer') }}",
type: 'get',
cache: false,
data:{'_token':$('meta[name="csrf-token"]').attr('content'), 'browser_name':instance.getBrowserName(),'os_name':instance.getOsName(),'os_version': instance.getOsVersion(),'ip':ip ,'user_id':user_id },
success: function(result){




}
});
}
});
});

</script>
<script type="text/javascript">


$(function(){
$('#LearnMoreBtnn').click(function(){
 $('#LearnMoredescription').toggle("slow");
});
});
</script>
@endpush