<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Zugriff',

            'roles' => [
                'all' => 'Alle Rollen',
                'create' => 'Rolle erstellen',
                'edit' => 'Rolle bearbeiten',
                'management' => 'Rollenverwaltung',
                'main' => 'Rollen',
            ],

            'users' => [
                'all' => 'Alle Nutzer',
                'change-password' => 'Passwort ändern',
                'create' => 'Benutzer erstellen',
                'deactivated' => 'Deaktivierte Benutzer',
                'deleted' => 'Gelöschte Benutzer',
                'edit' => 'Benutzer bearbeiten',
                'main' => 'Benutzer',
                'view' => 'Benutzer anzeigen',
            ],
        ],


        'trading' => [
            'title' => 'Handel',

            'offers' => [
                'all' => 'Stock',
                'create' => 'Aktie hinzufügen',
                'edit' => 'Lager bearbeiten',
                'show' => 'Lagerbestand anzeigen',
            ],

            'order2' => [
                'all' => 'Bestellung',
                'create' => 'Bestellung hinzufügen',
                'edit' => 'Bestellung bearbeiten',
                'show' => 'Reihenfolge anzeigen',
            ],

            'buyerpref' => [
                'all' => 'Käufer Pref',
                'create' => 'Käuferpräferenz hinzufügen',
                'edit' => 'Käuferpräferenz bearbeiten',
                'show' => 'Käuferpräferenz anzeigen',
            ],

            'productspecs' => [
                'all' => 'Produktspezifikation',
                'create' => 'Produktspezifikation hinzufügen',
                'edit' => 'Produktspezifikation bearbeiten',
                'show' => 'Produktspezifikation anzeigen',
            ],

            'productspecvalues' => [
                'all' => 'Produktspezifikationswerte',
                'create' => 'Produktspezifikationswerte hinzufügen',
                'edit' => 'Produktspezifikationswerte bearbeiten',
                'show' => 'Produktspezifikationswerte anzeigen',
            ],

            'offersent' => [
                'all' => 'Gesendete Angebote',
                'create' => 'Gesendete Angebote hinzufügen',
                'edit' => 'Gesendete Angebote bearbeiten',
                'show' => 'Gesendete Angebote anzeigen',
            ],

            'requests' => [
                'all' => 'Anfragen',
                'create' => 'Anfrage hinzufügen',
                'edit' => 'Anfrage bearbeiten',
                'show' => 'Anfrage anzeigen',
            ],

            'products' => [
                'all' => 'Produkte',
                'create' => 'Produkt hinzufügen',
                'edit' => 'Produkt bearbeiten',
            ],

            'sales' => [
                'all' => 'Der Umsatz',
                'create' => 'Verkauf hinzufügen',
                'edit' => 'Verkauf bearbeiten',
            ],
			      'variety' => [
                'all' => 'Köpfe',
                'create' => 'Köpfe hinzufügen',
                'edit' => 'Köpfe bearbeiten',
            ],
            'orders'=>[
                'all' => 'Anfragen',
                'create' => 'Anfrage hinzufügen',
                'edit' => 'Anfrage bearbeiten',
                'show' => 'Anfrage anzeigen',
            ],
            'warehouse' => [
                'all' => 'Warehouse',
                'create' => 'Add Warehouse',
                'edit' => 'Edit Warehouse',
                'show' => 'Show Warehouse',
            ],
        ],

        'setting'=>[
      			 'all' => 'Rahmen',
      			],
        			'pages'=>[
        			   'all' => 'Seiten',
        			   'create' => 'Seite hinzufügen',
        			   'edit' => 'Seite bearbeiten',

        			 ],

        'log-viewer' => [
            'main' => 'Eintrags Ansicht',
            'dashboard' => 'Instrumententafel',
            'logs' => 'Protokolle',
        ],

        'sidebar' => [
            'dashboard' => 'Instrumententafel',
            'general' => 'Allgemeines',
            'history' => 'Geschichte',
            'system' => 'System',
        ],

		'homepage-content' => [
            'slider-title' => 'Ihre Quelle der besten Produkte und Dienstleistungen',
			],


    ],


	'frontend' => [
			'nav' => [
				'login' => 'Einloggen',
				'register' => 'Registrieren',
				'contact' => 'Kontakt',
			],
		'menu' => [
				'about-us' => 'ÜBER UNS',
				'offer' => 'Unser Angebot',
				'contact' => 'KONTAKT',
			],

       'homepage-content' => [
            'slider-title' => 'Ihre Quelle der besten Produkte und Dienstleistungen',
            'buy' => 'KAUFEN',
            'sell' => 'VERKAUFEN',
            'welcome' => 'Willkommen zu',
            'welcome-content' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
			'previous' => 'Bisherige',
			'next' => 'Nächster',
			'section-1' => '<p>Wir sind ein führender Händler von Obst und Gemüse. Wir wirken auf dem internationalen Markt, wo wir unser Wissen und unsere Erfahrung einsetzen – wir liefern eine ganze Palette hochqualitativer Produkte.</p>
			<p>Seit über 10 Jahren arbeiten wir mit auf dem Markt anerkannten Obst- und Gemüseerzeugern zusammen. Damit gewährleisten wir die Ausführung der Aufträge auf hohem Niveau.</p>
			<p>Langjährige Erfahrung im globalen Risikomanagement sowie in Management der Versorgungskette von Veg King Europe GmbH (Sp. z o.o ) lässt uns die Markterwartungen erfüllen. Wir bilden ein Paket vorteilhafter Produkte und Dienstleistungen, das uns erlaubt, die Qualitätsskala sowie das Verkaufsvolumen stets zu erhöhen. </p>',
			'read-more' => 'Mehr anzeigen',
			'read-more-content' => '<p>Durch Kauf unserer Produkte haben Sie die Sicherheit, hochqualitative Produkte zu erhalten. Der Hauptbereich unserer Firma ist ein effektiver Vertrieb von frischem Obst und Gemüse, mit Berücksichtigung sowohl der Bedürfnisse als auch der Zufriedenheit unserer Lieferanten sowie Kunden.</p>
			<p>Stetige Weiterentwicklung und der Erhalt starker Position auf dem Markt sind unsere Mission und Ambition. Höchstqualität, Sicherheit von Produkten und fristgerechte Lieferung gehören zu unseren Prioritäten.</p>
			<p>Unseren Erfolg verdanken wir unserer Erfahrung, Professionalität, Zuverlässigkeit sowie dem immer konkurrierenden Angebot.</p>
			<p>Wir sind uns sicher, dass gemeinsame Arbeit und Qualität der Produkte ein Weg zum Erfolg ist nämlich zum gemeinsamen Erfolg.</p>
			<p>Wir freuen uns auf die Zusammenarbeit mit Ihnen.</p>'
		],

		'section-short-inner' => [
			'import' => 'Einführen',
            'heading-col-1' => '<strong>Die besten</strong> Preise',
            'content-col-1' => 'Unser Vorteil sind die wettbewerbsfähigen Preise. Wir behalten den internationalen Markt im Auge, um immer die höchste Qualität zum erschwinglichen Preis anbieten zu können.',
			'heading-col-2' => '<strong>Lieferanten</strong>',
            'content-col-2' => 'Wir glauben an die Kraft der einheimischen Erzeuger und die höchste Qualität ihrer angebotenen Produkte, mit den wir handeln. Wir wählen stets zuverlässige Quellen unserer Käufe und Lieferungen.',
			'heading-col-3' => '<strong>Produktpalette</strong>',
            'content-col-3' => 'Der wachsende Bedarf an breiter Produktpalette ist der Grund dafür, dass wir uns stets weiterentwickeln. Wir suchen nach Neuheiten und dabei bieten wir die meistbeliebten Produkte in der Kategorie: Obst und Gemüse.',


			'export' => 'Export',
			'heading-row-1' => '<strong>Sichere </strong>Lieferung',
            'content-row-1' => 'Am wichtigsten sind für uns die Sicherheit und eine fristgerechte Lieferung. Bei der Zusammenarbeit mit uns können Sie sich darauf verlassen, dass der Transport schnell und sicher ist.',
			'heading-row-2' => '<strong>Schnelle</strong> Lieferung',
            'content-row-2' => 'Ihre Zeit ist uns wichtig. Unsere Lieferungsstandards, professionelles Team und rasche Reaktion sind Folgen der Zufriedenheit unserer Kunden.',
			'heading-row-3' => '<strong>Globale</strong>Reichweite',
            'content-row-3' => 'Wir sind auf internationaler Ebene tätig. Unsere Erfahrung und unser Wissen lassen uns effektiv und absichtlich sogar sehr anspruchsvolle Aufträge realisieren.',

			],

			'newlettersec' => [
				'newsletter' => 'Newsletter',
				'newsletter-content' => 'Melden Sie sich an, dann sind Sie auf dem Laufenden mit unseren Angeboten',
				'poffers' => 'Ein neues Produkt in unserem Angebot',
				'beets' => 'Kartoffeln',
				'beets-content' => 'Die in unserem Angebot angebotenen Rüben werden auf kalziumreichen Böden angebaut.',
				'see-offer' => 'Siehe Angebote',

			],

			'salessec' => [
				'title-1' => 'Verkaufsabteilung',
				'name-1' => 'AGNIESZKA CIUNAJTIS',
				'phone-1' => '+ 48 790 239 292',
				'email-1' => 'agnieszka@vegking.eu',

				'name-2' => 'KAMIL ANIKIEJ',
				'phone-2' => '+48 608 055 152',
				'email-2' => 'kamil@vegking.eu',
				'title-2' => 'Verkauf-E-Mail',
                'title-2-1' => 'Buchhaltung E-Mail',

			],

			'contactsec' => [
				'heading' => 'Kontaktieren Sie uns',
				'country-1' => 'POLEN',
				'address-1' => 'Veg King Europe Sp. z o.o.<br>ul. Grzybowska 80/82<br>00-844 Warschau',
				'phone-1' => '+ 48 22 122 87 69',
				'email-1' => 'info@vegking.eu',

				'country-2' => 'GROSSBRITANNIEN',
				'address-2' => 'Veg King Europe Ltd.<br>1000 Great West Rd.<br>TW8 9DW BRENTFORD, UK<br>VAT:  330 5004 61',
				'phone-2' => '+44 2080 896560',
				'email-2' => 'info@vegking.eu',
				'content' => 'Veg King Europe Sp.z.o.o mit Sitz in Warschau (00-844). Grzybowska 80/82 Straße, NIP-Nummer 5272869217, REGON-Nummer 381654284, wurde in den Unternehmensregister des polnischen Gerichtsregisters eingetragen, Amtsgericht der Hauptstadt Warschau in Warschau, XII Wirtschaftsabteilung Nummer 0000754846',

			],

			'footer' => [
				'about-property' => 'Über Eigentum',
				'about-us' => 'Über uns',
				'privacy-policy' => 'Datenschutzerklärung',
				//'disclaimer' => 'Disclaimer',
				'contact-info' => 'Kontaktinformationen',
				'terms' => 'Nutzungsbedingungen',
				'contact' => 'Kontakt',
				'copyright'=>'Urheberrechte',
				'copyright-content'=>'Alle Rechte vorbehalten',
				'fulladdress'=>'HUB in Polen<br>Veg King Europe Sp. z o.o.<br>ul. Grzybowska 80/82',
			],
			'popup' => [
				'heading' => 'DIESE WEBSITE VERWENDET COOKIES',
				'content' => '<span id="cookie_pre_text" class="pre">Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren, Funktionen für soziale Medien bereitzustellen und unseren Datenverkehr zu analysieren.</span> <span id="more_cookies" style="display:none">Wir teilen auch Informationen über Ihre Nutzung unserer Website mit unseren Partnern für soziale Medien, Werbung und Analyse, die diese möglicherweise mit anderen Informationen kombinieren, die Sie ihnen zur Verfügung gestellt haben oder die sie aus Ihrer Nutzung ihrer Dienste gesammelt haben. Sie stimmen unseren Cookies zu, wenn Sie unsere Website weiterhin nutzen.</span> <span id="more_cookies_btn">[Weiterlesen]</span>',
				'agreebutton' => 'Genau',
                'product_modal_text' => 'Wir bieten Produkte von höchster Qualität',
                'product_modal_Buy_button' => '<a id="open_buyercontact_modal" href="javascript:;">KAUFEN</a>',
                'product_modal_sell_button' => '<a id="open_sellercontact_modal" href="javascript:;">VERKAUFEN</a>',
                'potatoes' => 'KARTOFFELN',
                'onion' => 'ZWIEBEL',
                'cauliflower' => 'BLUMENKOHL',
                'cabbage' => 'KOHL',
                'broccoli' => 'BROKKOLI',
                'beets' => 'RÜBEN',
                'other' => 'ANDERE',
			],
      'sell_popup' => [
				'heading' => 'Verkaufen',
                'heading1' => 'Wir bieten Produkte von höchster Qualität',
				'choose_products' => 'Wählen Sie Produkte',
				'choose_type' => 'Wählen Sie Typ',
                'name' => 'Name',
				'phone' => 'Telefon',
                'email' => 'E-Mail',
                'password' => 'Passwort',
                'confirm_password' => 'Passwort bestätigen',
                'location' => 'Ort',
                'postal_code' => 'Postleitzahl',
                'prefered_method' => 'Bevorzugte Methode',
                'pm_email' => 'Email',
                'pm_sms' => 'SMS',
                'pm_phone' => 'Telefon',
                'pm_whatsapp' => 'WhatsApp',
                'yes' => 'Ja',
                'no' => 'Nein',
                'notes' => 'Anmerkungen',
                'submit' => 'Sende Informationen',
                'country_code' => 'Landesvorwahl',
			],
      'buy_popup' => [
				'heading' => 'Kaufen',
                'back_Button' => 'Zurück',
                'next_Button' => 'Weiter',
                'heading1' => 'Wir bieten Produkte von höchster Qualität',
				'choose_products' => 'Wählen Sie Produkte',
                'choose_type' => 'Wählen Sie Typ',
                'company_name' => 'Name der Firma',
                'contact_name' => 'Kontaktname',
				'phone' => 'Telefon',
        'email' => 'E-Mail',
        'prefered_method' => 'Bevorzugte Methode',
        'pm_email' => 'Email',
        'pm_sms' => 'SMS',
        'pm_phone' => 'Telefon',
        'pm_whatsapp' => 'WhatsApp',
        'yes' => 'Ja',
        'no' => 'Nein',
        'notes' => 'Anmerkungen',
        'agreecheckbox' => 'Hiermit erkläre ich mich mit den <a class="termsacnchor" href='.url('terms-conditions').' target="_blank">Geschäftsbedingungen und der </a> & <a class="termsacnchor" href='.url('privacy-policy').' target="_blank">Datenschutzerklärung einverstanden</a>',
        'submit' => 'Sende Informationen',
			],


    ],

    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar' => 'Arabic',
            'az' => 'Azerbaijan',
            'zh' => 'Chinese Simplified',
            'zh-TW' => 'Chinese Traditional',
            'da' => 'Danish',
            'de' => 'German',
            'el' => 'Greek',
            'en' => 'English',
            'es' => 'Spanish',
            'fa' => 'Persian',
            'fr' => 'French',
            'he' => 'Hebrew',
            'id' => 'Indonesian',
            'it' => 'Italian',
            'ja' => 'Japanese',
            'nl' => 'Dutch',
            'no' => 'Norwegian',
            'pt_BR' => 'Brazilian Portuguese',
            'pl' => 'Polish',
            'ru' => 'Russian',
            'sv' => 'Swedish',
            'th' => 'Thai',
            'tr' => 'Turkish',
            'uk' => 'Ukrainian',
        ],
    ],
];
