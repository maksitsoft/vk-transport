<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Access',

            'roles' => [
                'all' => 'All Roles',
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'management' => 'Role Management',
                'main' => 'Roles',
            ],

            'users' => [
                'all' => 'All Users',
                'change-password' => 'Change Password',
                'create' => 'Create User',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'edit' => 'Edit User',
                'main' => 'Users',
                'view' => 'View User',
            ],
        ],


        'trading' => [
            'title' => 'Trading',

            'offers' => [
                'all' => 'Stock',
                'create' => 'Add Stock',
                'edit' => 'Edit Stock',
                'show' => 'Show Stock',
            ],

            'order2' => [
                'all' => 'Order',
                'create' => 'Add Order',
                'edit' => 'Edit Order',
                'show' => 'Show Order',
            ],

            'buyerpref' => [
                'all' => 'Buyer Pref',
                'create' => 'Add Buyer Pref',
                'edit' => 'Edit Buyer Pref',
                'show' => 'Show Buyer Pref',
            ],

            'productspecs' => [
                'all' => 'Product Specification',
                'create' => 'Add Product Specification',
                'edit' => 'Edit Product Specification',
                'show' => 'Show Product Specification',
            ],

            'productspecvalues' => [
                'all' => 'Product Specification Values',
                'create' => 'Add Product Specification Values',
                'edit' => 'Edit Product Specification Values',
                'show' => 'Show Product Specification Values',
            ],

            'offersent' => [
                'all' => 'Offers Sent',
                'create' => 'Add Offers Sent',
                'edit' => 'Edit Offers Sent',
                'show' => 'Show Offers Sent',
            ],

            'requests' => [
                'all' => 'Inquiries',
                'create' => 'Add Inquiry',
                'edit' => 'Edit Inquiry',
                'show' => 'Show Inquiry',
            ],

            'products' => [
                'all' => 'Products',
                'create' => 'Add Product',
                'edit' => 'Edit Product',
            ],

            'sales' => [
                'all' => 'Sales',
                'create' => 'Add Sale',
                'edit' => 'Edit Sale',
            ],
			      'variety' => [
                'all' => 'Heads',
                'create' => 'Add Heads',
                'edit' => 'Edit Heads',
            ],
            'orders'=>[
                'all' => 'Inquiries',
                'create' => 'Add Inquiry',
                'edit' => 'Edit Inquiry',
                'show' => 'Show Inquiry',
            ],
            'warehouse' => [
                'all' => 'Warehouse',
                'create' => 'Add Warehouse',
                'edit' => 'Edit Warehouse',
                'show' => 'Show Warehouse',
            ],
        ],

        'setting'=>[
      			 'all' => 'Setting',
      			],
        			'pages'=>[
        			   'all' => 'Pages',
        			   'create' => 'Add Page',
        			   'edit' => 'Edit Page',

        			 ],

        'log-viewer' => [
            'main' => 'Log Viewer',
            'dashboard' => 'Dashboard',
            'logs' => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general' => 'General',
            'history' => 'History',
            'system' => 'System',
        ],

		'homepage-content' => [
            'slider-title' => 'Your source of the best products and services',
			],


    ],


	'frontend' => [
			'nav' => [
				'login' => 'Login',
				'register' => 'REGISTER',
				'contact' => 'Contact',
			],
		'menu' => [
				'about-us' => 'ABOUT US',
				'offer' => 'Offer',
				'contact' => 'CONTACT',
			],

       'homepage-content' => [
            'slider-title' => 'Your source of the best products and services',
            'buy' => 'BUY',
            'sell' => 'SELL',
            'welcome' => 'Welcome to',
            'welcome-content' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
			'previous' => 'Previous',
			'next' => 'Next',
			'section-1' => '<p>Veg King Europe Sp. z o.o. is a leading distributor of vegetables and fruits, operating on international markets, based on knowledge and experience - it provides a full range of high-quality products per 365 days a year.</p>
			<p>For over 10 years we have been cooperating with recognized fruit and vegetable producers, thus guaranteeing the highest level of order fulfillment.</p>
			<p>Competitive experience in global risk and supply chain management Veg King Europe Sp. z o.o. - allows you to meet market expectations and create a package of beneficial products and services, which allows you to constantly increase the scale of quality and sales volume.</p>',
			'read-more' => 'Read more',
			'read-more-content' => '<p>By buying our products you can be sure that you are buying the highest quality products.</p>
			<p>The main area of our activity is the effective distribution of fresh fruit and vegetables, taking into account both the needs and the satisfaction of our suppliers and customers.</p>
			<p>Our mission and ambition is continuous development and maintaining a strong market position. The highest quality, product safety and timely delivery - these are our priorities.</p>
			<p>We owe our success to experience, professional service and always competitive commercial offer.</p>
			<p>We believe that cooperation work and product quality are the path to success, collaborated success.</p>
			<p>We invite you to cooperation.</p>',

			],

		'section-short-inner' => [
			'import' => 'Import',
            'heading-col-1' => '<strong>The best</strong>prices',
            'content-col-1' => 'We monitor international market to make sure our prices are always competitive. We buy goods at the best prices. It is our goal and advantage.',
			'heading-col-2' => '<strong>Qualified</strong>suppliers',
            'content-col-2' => 'We strongly believe in the power of worldwide known brands and the best quality of the products we trade. We select carefully the sources of our purchase and delivery.',
			'heading-col-3' => '<strong>Full range</strong>of products',
            'content-col-3' => 'We answer to growing market demand for the full range of products. Always looking for novelty and constantly provide vegetables and fruits best sellers.',


			'export' => 'Export',
			'heading-row-1' => '<strong>Safe</strong>shipping',
            'content-row-1' => 'We care about exported goods. They are shipped quickly and safely all over the world. You always know where your money is and decide about your order conditions.',
			'heading-row-2' => '<strong>Fast</strong> delivery',
            'content-row-2' => 'We understand the needs of our Clients and remember that time is money. Simplified processes, professional Team and prompt actions support delivery standards',
			'heading-row-3' => '<strong>Worldwide</strong>range',
            'content-row-3' => 'We cooperate with our Clients on a global scale. Our experience and knowledge allows us to serve you in comprehensive, effective and professional way all over the world.',

			],

			'newlettersec' => [
				'newsletter' => 'Newsletter',
				'newsletter-content' => 'Subscribe to our newsletter and stay up to date',
				'poffers' => 'New product in our offer',
				'beets' => 'POTATOES',
				'beets-content' => 'Beets available in our offer are grown in soils rich in calcium.',
				'see-offer' => 'SEE OFFER',

			],

			'salessec' => [
				'title-1' => 'Sales Department',
				'name-1' => 'AGNIESZKA CIUNAJTIS',
				'phone-1' => '+ 48 790 239 292',
				'email-1' => 'agnieszka@vegking.eu',

				'name-2' => 'KAMIL ANIKIEJ',
				'phone-2' => '+48 608 055 152',
				'email-2' => 'kamil@vegking.eu',
				'title-2' => 'Sales Email',
                'title-2-1' => 'Accounts Email',

			],

			'contactsec' => [
				'heading' => 'CONTACT US',
				'country-1' => 'POLAND',
				'address-1' => 'Veg King Europe Sp. z o.o.<br>ul. Grzybowska 80/82<br>00-844 Warszawa',
				'phone-1' => '+ 48 22 122 87 69',
				'email-1' => 'info@vegking.eu',

				'country-2' => 'UNITED KINGDOM',
				'address-2' => 'Veg King Europe ltd.<br>1000 Great West Rd.<br>Brentford TW8 9HH, UK<br>VAT:  GB313177820',
				'phone-2' => '+44 2080 896560',
				'email-2' => 'info@vegking.eu',
				'content' => 'Veg King Europe Sp. z o.o. with its registered office in Warsaw (00-844) ul. Grzybowska 80/82, NIP 527-286-9217, REGON 381654284, entered into the Register of Entrepreneurs of the National Court Register, District Court for the Capital City of Warsaw Warsaw in Warsaw, XII Commercial Department number 0000754846.',

			],

			'footer' => [
				'about-property' => 'About Property',
				'about-us' => 'About Us',
				'privacy-policy' => 'Privacy Policy',
				//'disclaimer' => 'Disclaimer',
				'contact-info' => 'Contact Info',
				'terms' => 'Terms of service',
				'contact' => 'Contact',
				'copyright'=>'Copyright',
				'copyright-content'=>'All rights Reserved',
				'fulladdress'=>'HUB in Poland<br>Veg King Europe Sp. z o.o.<br>ul. Grzybowska 80/82<br>00-844 Warszawa',
			],
			'popup' => [
				'heading' => 'THIS WEBSITE USES COOKIES',
				'content' => '<span id="cookie_pre_text" class="pre">We use cookies to personalise content and ads, to provide social media features and to analyse our traffic.</span> <span id="more_cookies" style="display:none">We also share information about your use of our site with our social media, advertising and analytics partners who may combine it with other information that you’ve provided to them or that they’ve collected from your use of their services. You consent to our cookies if you continue to use our website.</span> <span id="more_cookies_btn">[Read More]</span>',
				'agreebutton' => 'I agree',
        'product_modal_text' => 'We offer the highest quality products',
        'product_modal_Buy_button' => '<a id="open_buyercontact_modal" href="javascript:;">BUY</a>',
        'product_modal_sell_button' => '<a id="open_sellercontact_modal" href="javascript:;">SELL</a>',
        'potatoes' => 'POTATOES',
        'onion' => 'ONION',
        'cauliflower' => 'CAULIFLOWER',
        'cabbage' => 'CABBAGE',
        'broccoli' => 'BROCCOLI',
        'beets' => 'BEETS',
        'other' => 'OTHER',
			],
      'sell_popup' => [
				'heading' => 'Sell',
                'heading1' => 'We offer the highest quality products',
				'choose_products' => 'Choose Products',
				'choose_type' => 'Choose Type',
        'name' => 'Name',
				'phone' => 'Phone',
        'email' => 'E-mail Address',
        'password' => 'Password',
        'confirm_password' => 'Confirm Password',
        'location' => 'Location',
        'postal_code' => 'Postal Code',
        'prefered_method' => 'Prefered Method',
        'pm_email' => 'Email',
        'pm_sms' => 'SMS',
        'pm_phone' => 'Phone',
        'pm_whatsapp' => 'WhatsApp',
        'yes' => 'Yes',
        'no' => 'No',
        'notes' => 'Notes',
        'submit' => 'Send Information',
        'country_code' => 'Country Code',
			],
      'buy_popup' => [
				'heading' => 'Buy',
                'back_Button' => 'Back',
                'next_Button' => 'Next',
                'heading1' => 'We offer the highest quality products',
				'choose_products' => 'Choose Products',
                'choose_type' => 'Choose Type',
        'company_name' => 'Company Name',
        'contact_name' => 'Contact Name',
				'phone' => 'Phone',
        'email' => 'E-mail Address',
        'prefered_method' => 'Prefered Method',
        'pm_email' => 'Email',
        'pm_sms' => 'SMS',
        'pm_phone' => 'Phone',
        'pm_whatsapp' => 'WhatsApp',
        'yes' => 'Yes',
        'no' => 'No',
        'notes' => 'Notes',
        'agreecheckbox' => 'I agree and accept with all <a class="termsacnchor" href='.url('terms-conditions').' target="_blank">Terms of Services</a> & <a class="termsacnchor" href='.url('privacy-policy').' target="_blank">Privacy Policy</a>',
        'submit' => 'Send Information',
			],
      'buyerlead_popup' => [
         'heading' => 'Buy',
         'back_Button' => 'Back',
         'next_Button' => 'Next',
         'heading1' => "To get :percent discount prices <br>Fill in your information",
         'choose_products' => 'Choose Products',
         'choose_type' => 'Choose Type',
        'company_name' => 'Company Name',
        'name' => 'Name',
         'phone' => 'Phone',
        'email' => 'E-mail Address',
        'agreecheckbox' => 'Receive our actual offer on your email',
        'submit' => 'Submit',
         ],



    ],

    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar' => 'Arabic',
            'az' => 'Azerbaijan',
            'zh' => 'Chinese Simplified',
            'zh-TW' => 'Chinese Traditional',
            'da' => 'Danish',
            'de' => 'German',
            'el' => 'Greek',
            'en' => 'English',
            'es' => 'Spanish',
            'fa' => 'Persian',
            'fr' => 'French',
            'he' => 'Hebrew',
            'id' => 'Indonesian',
            'it' => 'Italian',
            'ja' => 'Japanese',
            'nl' => 'Dutch',
            'no' => 'Norwegian',
            'pt_BR' => 'Brazilian Portuguese',
            'pl' => 'Polish',
            'ru' => 'Russian',
            'sv' => 'Swedish',
            'th' => 'Thai',
            'tr' => 'Turkish',
            'uk' => 'Ukrainian',
        ],
    ],
];
