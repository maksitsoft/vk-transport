@extends('backend.layouts.app')

@section('title', 'UserTracking'. ' | '.app_name() )

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                  User Tracking <small class="text-muted"></small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                   <table class="table table-bordered data-table usertracking_tbl">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>User Id</th>
                            <th>Page</th>
                            <th>Button</th>
                            <th>Name</th>
                            <th>Data</th>
                            <th>Time</th>
							<th>Date</th>
                            
                        </tr>
                        </thead>
                    
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                   
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
@push('after-scripts')
<script type="text/javascript">
    $(function () {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            responsive: true,
            ajax: "{{ route('admin.user-ips.usertracking') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'user_id', name: 'user_id'},
                {data: 'page', name: 'page'},
                {data: 'button', name: 'button'},
                {data: 'name', name: 'Country'},
                {data: 'data', name: 'data'},
                {data: 'date_time', name: 'date_time'},
				{data: 'date', name: 'date'},
				{data: 'ip', name: 'ip'},
				{data: 'city', name: 'city'},
				{data: 'country', name: 'country'},
            ]
        });
      
    });
  </script>
  @endpush