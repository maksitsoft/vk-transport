@extends('backend.layouts.app')
@if(@$data->id)
 @section('title',__('Edit Email Template') . ' :: ' . app_name())
@else
 @section('title',__('Create Email Template') . ' :: ' . app_name())
@endif
@section('content')
    {{ html()->form((isset($data->id) ? 'PUT' : 'POST') )->id('form_email_template_submit')->class('form-horizontal')->open() }}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                          @if(@$data->id)
                            Edit Email Template
                          @else
                            Create Email Template
                          @endif
                            <small class="text-muted"></small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr>

                <div class="row mt-4 mb-4">
                    <div class="col">

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.title'))->class('col-md-2 form-control-label')->for('title') }}
                            <div class="col-md-10">
                                {{ html()->text('title')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.title'))
                                    ->value(old('title', @$data->title))
                                    ->attribute('maxlength', 191)}}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.subject'))->class('col-md-2 form-control-label')->for('subject') }}
                            <div class="col-md-10">
                                {{ html()->text('subject')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.subject'))
                                    ->value(old('title', @$data->subject))
                                    ->attribute('maxlength', 191)}}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.shortcodes'))->class('col-md-2 form-control-label')->for('shortcodes') }}
                            <div class="col-md-10">
                                @if(@$data->id)
                                    @section('title',__('Edit Email Template') . ' :: ' . app_name())
                                        <code>{{ @$data->shortcodes }}</code>
                                    @else
                                        <code>[first_name], [name], [upload_stock_link], [english_phone_number], [phone], [email], [verification_link], [team_member_name], [view_seller_link], [product_name], [contact_preferred_method], [notes], [view_buyer_link], [username], [password]</code>
                                @endif
                            </div><!--col-->
                        </div><!--form-group-->



                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.email_content'))->class('col-md-2 form-control-label')->for('email_content') }}
                            <div class="col-md-10">
                                {{ html()->textarea('email_content')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.email_content'))
                                    ->value(old('email_content', @$data->email_content))
                                }}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.email_content_de'))->class('col-md-2 form-control-label')->for('email_content_de') }}
                            <div class="col-md-10">
                                {{ html()->textarea('email_content_de')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.email_content_de'))
                                    ->value(old('email_content_de', @$data->email_content_de))
                                }}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.email_content_pl'))->class('col-md-2 form-control-label')->for('email_content_pl') }}
                            <div class="col-md-10">
                                {{ html()->textarea('email_content_pl')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.email_content_pl'))
                                    ->value(old('email_content_pl', @$data->email_content_pl))
                                }}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.sms_content'))->class('col-md-2 form-control-label')->for('sms_content') }}
                            <div class="col-md-10">
                                {{ html()->textarea('sms_content')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.sms_content'))
                                    ->value(old('sms_content', @$data->sms_content))
                                    ->attribute('rows', 8)
                                }}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.sms_content_de'))->class('col-md-2 form-control-label')->for('sms_content_de') }}
                            <div class="col-md-10">
                                {{ html()->textarea('sms_content_de')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.sms_content_de'))
                                    ->value(old('sms_content_de', @$data->sms_content_de))
                                    ->attribute('rows', 8)
                                }}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.backend.templates.sms_content_pl'))->class('col-md-2 form-control-label')->for('sms_content_pl') }}
                            <div class="col-md-10">
                                {{ html()->textarea('sms_content_pl')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.templates.sms_content_pl'))
                                    ->value(old('sms_content', @$data->sms_content_pl))
                                    ->attribute('rows', 8)
                                }}
                                <div class="invalid-feedback"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                          {{ html()->label('Status')->class('col-md-2 form-control-label') }}
                          <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                              {{ html()->label(
                                html()->checkbox('status',  @$data->status ?? 1, '1')
                                ->class('switch-input pref_check')
                                ->id('status')
                                . '<span class="switch-slider" data-checked="On" data-unchecked="Off"></span>')
                                ->class('switch switch-label switch-pill switch-success mr-2')
                                ->for('status') }}
                              </div>
                            </div>
                        </div>


                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.email-templates.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        <input type="hidden" name="id" value="{{@$data->id}}">
                    @if(@$data->id)
                    {{ form_submit(__('buttons.general.crud.update')) }}
                    @else
                    {{ form_submit(__('buttons.general.crud.create')) }}
                    @endif
                        
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
    @php
      $url = route('admin.email-templates.store');
      $redirect_url = route('admin.email-templates.index');
    @endphp
@endsection
@push('after-scripts')

@push('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.9/tinymce.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.9/jquery.tinymce.min.js"></script>
   <script>
    var editor_config = {
          path_absolute : "",
          selector: "#email_content, #email_content_pl, #email_content_de",
          /* plugins:['advlist lists autolink link table wordcount searchreplace imagetools',
         'template paste textcolor colorpicker textpattern media','code','image imagetools'], */
    		 plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
    		'searchreplace wordcount visualblocks visualchars code fullscreen',
    		'insertdatetime media nonbreaking save table contextmenu directionality',
    		'emoticons template paste textcolor colorpicker textpattern imagetools'
    		],
    	   toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    	   toolbar2: 'print preview media | forecolor backcolor emoticons',
    	   image_advtab: true,


          relative_urls: false,
          height:400,
          file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
            if (type == 'image') {
              cmsURL = cmsURL + "&type=Images";
            } else {
              cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
              file : cmsURL,
              title : 'Filemanager',
              width : x * 0.8,
              height : y * 0.8,
              resizable : "yes",
              close_previous : "no"
            });
          }
        };

        tinymce.init(editor_config);

    </script>
@endpush



<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#formsubmit2').on('submit', function(event) {
        event.preventDefault();

        $('.has-danger').next().children().children().css({
            "border": ""
        });
        $('.is-invalid').removeClass("is-invalid");
        $('.invalid-feedback').html("");
        $('.has-danger').removeClass("has-danger");

        var formData = new FormData($(this)[0]);

        $( ".pref_check" ).each(function( key, value ) {
            if(value.value == 0){
                formData.append(value.name, value.value);
            }
        });

        $.ajax({
            url: "{{ $url }}",
            method: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json",
            beforeSend: function() {
             $('.loading').removeClass('loading_hide');
            },
            success: function(data) {
                $('.loading').addClass('loading_hide');
                if (data.status == 'success') {
                    Swal.fire('Sent!', data.message, 'success');
                    setTimeout(function() {
                        window.location.href = "{{ $redirect_url }}";
                    }, 500);
                }
                if (data.status == 'error') {
                    Swal.fire('Error!', data.message, 'error');
                }
            },
            error: function(data) {
                $('.loading').addClass('loading_hide');
                if (data.status === 422) {
                    Swal.fire('Error!', data.responseJSON.message, 'error');
                    $('.btn-success').removeAttr('disabled');
                    var errors = [];
                    errors = data.responseJSON.errors
                    $.each(errors, function(key, value) {
                        $('#' + key).parent().addClass('has-danger');
                        $('#' + key).addClass('is-invalid');
                        $('#' + key).parent('.has-danger').find('.invalid-feedback').html(value);
                        $('#' + key).next().children().children().css({
                            "border": "1px solid #f86c6b"
                        });
                    })
                }
            }
        });
    });

    $('body').on('click', '.pref_check', function(){
        if(this.checked){
            $(this).val(1);
            $(this).attr('checked','checked');
        }else{
            $(this).val(0);
            $(this).removeAttr('checked');
        }
    })
</script>
 <script type="text/javascript">
    $(document).ready(function() {
    $('#form_email_template_submit').on('submit', function(event) {
      event.preventDefault();
      var formData = new FormData($(this)[0]);
      $.ajax({
        url: "{{ (isset($data->id)) ? route('admin.email-templates.update', $data->id) : route('admin.email-templates.store') }}",
        method: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function(){
          $('.loading').removeClass('loading_hide');
        },
        success: function(data)
        {
          $('.loading').addClass('loading_hide');
          if(data.status == 'success'){
            $('.loading').addClass('loading_hide');
            Swal.fire('Sent!', data.message, 'success');
            setTimeout(function(){
              window.location.href = "{{  route('admin.email-templates.index') }}";
            }, 2000);
          }
          if(data.status == 'error'){
            $('.loading').addClass('loading_hide');
            Swal.fire('Error!', data.message, 'error');
            $('.btn-success').removeAttr('disabled');
          }
        },
        error :function( data ) {
          $('.loading').addClass('loading_hide');
          if( data.status === 422 ) {
            $('.loading').addClass('loading_hide');
            Swal.fire('Error!', data.responseJSON.message, 'error');
            $('.btn-success').removeAttr('disabled');
            var errors = [];
            errors = data.responseJSON.errors
            $.each(errors, function (key, value) {
              $('#'+key).parent().addClass('has-danger');
              $('#'+key).addClass('is-invalid');
              $('#'+key).parent('.has-danger').find('.invalid-feedback').html(value);
              $('#'+key).next().children().children().css({"border": "1px solid #f86c6b"});
            })
          }
        }
      });
    });
  });
    </script>
@endpush
