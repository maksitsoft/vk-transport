@extends('backend.layouts.app')
@section('title', 'Matches  :: ' . app_name())
@section('content')
@if(!empty($msg))
    <div class="card-body alert-danger">
        <div class="row">
            <div class="col-sm-12">
                <div>{{ $msg }}</div>
            </div>
        </div>
    </div>
@endif
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group row">
          <label class="col-md-12 form-control-label" for="type">Filter Matches</label>
          <div class="col-md-2">
            <select class="form-control select2" id="stock_filter" name="stock_filter">
              <option value="">Select Stock</option>
              @foreach($stock_list as $stock)
              <option value="{{$stock->id}}" @if(isset($_GET['stock']) && $_GET['stock'] == $stock->id) selected @endif>
                #{{ @$stock->id }} {{ @$stock->product->name}} [{{ @$stock->seller->username}}]
              </option>
              @endforeach
            </select>
          </div>

          <div class="col-md-2">
            <select class="form-control select2" id="buyer_filter" name="buyer_filter">
              <option value="">Select Buyer</option>
              @foreach($buyers_list as $buyer)
              <option value="{{$buyer->id}}" @if(isset($_GET['buyer']) && $_GET['buyer'] == $buyer->id) selected @endif>
                {{ @$buyer->username}}
              </option>
              @endforeach
            </select>
          </div>

          <div class="col-md-2">
            <select class="form-control select2" id="product_id" name="product_id">
              @foreach($products as $key => $val)

                 <option value="{{$key}}" @if(in_array($key, explode(',',@$_GET['product_id']))) selected @endif>{{ $val}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-2">
            <select class="form-control select2" id="match_type" name="match_type" multiple="multiple" data-placeholder="Match Requirement">
              @foreach($product_specifications as $key => $val)
                 <option value="{{$key}}" @if(in_array($key, explode(',',@$_GET['match_type']))) selected @endif>{{ $val}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-6">
            <div class="matches_header">
              <button class="btn btn-success float-left" type="submit" name="filter_matches" onclick="window.location.href='{{ route('admin.matches.index') }}?stock='+$('#stock_filter').val()+'&buyer='+$('#buyer_filter').val()+'&product_id='+$('#product_id').val()+'&match_type='+$('#match_type').val()+'@if(isset($_GET['show_matched']))&show_matched={{$_GET['show_matched']}} @endif'"><i class="fas fa-filter"></i>Filter</button>
            </div>
            <div class="matches_header"> 
              <button class="btn btn-primary float-left"  style="margin-left:20px"type="submit" id="clear_filter" name="clear_filter"><i class="fas fa-filter"></i>Clear</button>
            </div>
            <div class="matches_header">
              <a href="{{ route('admin.matches.updateAll') }}" style="margin-left:20px" class="btn btn-primary" role="button">Update All Matches</a>
              <button type="button" class="btn btn-primary sendtoall" style="margin-left:20px" title="Sent Invoice to All" data-url="{{ route('admin.matches.InvoiceSendtoAll') }}"><i class="fas fa-file-invoice"></i></button>
              <a href="{{ route('admin.matches.matchesexports') }}?@if(isset($_GET['stock']))stock={{$_GET['stock']}}&buyer={{$_GET['buyer']}}@endif @if(isset($_GET['match_type']))&match_type={{$_GET['match_type']}} @endif @if(isset($_GET['show_matched']))&show_matched={{$_GET['show_matched']}} @endif" class="btn btn-primary ml-1" data-toggle="tooltip" title="Export Excel"><i class="fa fa-download"></i></a>
            </div>
          </div>
        </div>


      </div>
      <div class="col-sm-12">
        <h4 class="card-title mb-0">
          Stock Matches <small class="text-muted"></small>
        </h4>
      </div><!--col-->
      <div id="show_matched_div" class="row col-md-12 mt-3">
          <ul class="nav nav-tabs col-md-12" role="tablist">
              <li class="nav-item"><a class="nav-link @if(request()->input('show_matched') != 'no') active @endif " href="yes" data-toggle="tab" role="tab">Matched</a></li>
              <li class="nav-item"><a class="nav-link @if(request()->input('show_matched') == 'no') active @endif" href="no" data-toggle="tab" role="tab">Mismatched</a></li>
         </ul>
      </div>
    </div><!--row-->
    <div class="row mt-2">
      <div class="col">
        <div class="table-offers">
          <table class="table table-bordered data-table">
            <thead>
              <tr>
                <th></th>
                <th>Id</th>
                <th>Buyer</th>
                <th>Seller</th>
                <th>StockID</th>
                <th>Product</th>
                 @foreach($ProdSpecArr as $spec)
                     <th>{{ $spec }}</th>
                 @endforeach
                <th>P/Ton</th>
                {{-- <th>P/Ton Calculation</th> --}}
                <!-- <th>Profit</th> -->
                <th>#MM</th>
                <th>MM</th>
                <th>TSB</th>
                <th>TSS</th>
                <th>TSC</th>
                <th>Added</th>
                <th>Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div><!--col-->
    </div><!--row-->
  </div><!--card-body-->
</div><!--card-->

  <!-- Modal -->
  <div class="modal fade" id="notifyModel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Notify Buyer & Seller</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Are you sure ? want to <span class="badge badge-info">Notify Buyer & Seller</span> or <span class="badge badge-success">Make Sale</span> for:
            <br>Stock ID: <span class="badge badge-info" id="stock_id"></span>
            <br>Order ID: <span class="badge badge-info" id="order_id"></span>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-info pull-left notifyBTN" data-loading-text="Notifying…"><i class="fa fa-paper-plane"></i> Notify Buyer & Seller</button>
          <button type="button" class="btn btn-sm btn-success pull-left makesaleBTN" data-loading-text="Making Sale…"><i class="fa fa-paper-plane"></i> Make Sale</button>
          <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
        </div>
      </div>

    </div>
  </div>
   <div class="modal fade" id="viewMismatches" role="dialog">
       <div class="modal-dialog">

         <!-- Modal content-->
         <div class="modal-content">
           <div class="modal-header">
             <h4 class="modal-title">View Mismatches</h4>
             <button type="button" class="close" data-dismiss="modal">&times;</button>
           </div>
           <div class="modal-body">
             <p>
             </p>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
           </div>
         </div>

       </div>
     </div>
   <div class="modal fade" id="ptonModal" role="dialog">
       <div class="modal-dialog modal-lg">

         <!-- Modal content-->
         <div class="modal-content">
           <div class="modal-header">
             <h4 class="modal-title">Profit per ton calculation</h4>
             <button type="button" class="close" data-dismiss="modal">&times;</button>
           </div>
           <div class="modal-body">
             <p>
             </p>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
           </div>
         </div>

       </div>
     </div>
@endsection

@push('after-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  
$("body").on("click", ".expand_row", function(event){
  var id = $(this).attr('id_v');
 $('#tab_show'+id).toggle();
});  
$("body").on("click", ".tabs li a", function(event){
  var t = $(this).attr('id');

  if($(this).hasClass('inactive')){ //this is the start of our condition 
    $('.tabs li a').addClass('inactive');           
    $(this).removeClass('inactive');
    $(this).addClass('active');

    $('.containerss').hide();
   
    $('#'+ t + 'C').fadeIn('slow');
 }
});
});
</script>
<script type="text/javascript">
$(function () {
  setTimeout(function() {
        $(".alert-danger").hide();
    }, 3000);
  var table = $('.data-table').DataTable({
    processing: true,
    serverSide: true,
    autoWidth: false,
    responsive: true,
    order: [],
    ajax: "{{ route('admin.matches.index') }}?@if(isset($_GET['stock']))stock={{$_GET['stock']}}&buyer={{$_GET['buyer']}}@endif  &product_id=@if(isset($_GET['product_id'])){{$_GET['product_id']}} @else "+$('#product_id').val()+" @endif @if(isset($_GET['match_type']))&match_type={{$_GET['match_type']}} @endif @if(isset($_GET['show_matched']))&show_matched={{$_GET['show_matched']}} @endif",
    columns: [
        {data: 'checkbox', name: 'checkbox', selectRow: true},
        {data: 'id', name: 'id'},
        {data: 'buyer_show_url', name: 'buyer_show_url'},
        {data: 'seller_show_url', name: 'seller_show_url'},
        {data: 'stock_show_url', name: 'stock_show_url'},
        {data: 'stock.product.name', name: 'stock.product.name'},
         @if(isset($ProdSpecArrNames['field1']) && $ProdSpecArrNames['field1'] != '')
         {data: 'field1', name: 'field1'},
         @endif
         @if(isset($ProdSpecArrNames['field2']) && $ProdSpecArrNames['field2'] != '')
         {data: 'field2', name: 'field2'},
         @endif
         @if(isset($ProdSpecArrNames['field3']) && $ProdSpecArrNames['field3'] != '')
         {data: 'field3', name: 'field3'},
         @endif
        // {data: 'variety_name', name: 'variety_name'},
        // {data: 'flesh_color2', name: 'flesh_color2'},
        // {data: 'purposes2', name: 'purposes2'},
        {data: 'profit_per_ton', name: 'profit_per_ton'},
        // {data: 'pton_calculation', name: 'pton_calculation'},
        // {data: 'total_profit', name: 'total_profit'},
        {data: 'numofmismatches', name: 'numofmismatches'},
        {data: 'mismatches', name: 'mismatches'},
        {data: 'trust_level_buyer', name: 'trust_level_buyer'},
        {data: 'trust_level_seller', name: 'trust_level_seller'},
        {data: 'trust_level_combined', name: 'trust_level_combined'},    
        {data: 'added', name: 'added'},    
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('body').on('click', '.pton', function () {
    $("#ptonModal .modal-body p").html($(this).next('div.pton_calculation').html());
    $("#ptonModal").modal({
            backdrop: 'static',
            keyboard: false
          });
  });
  $('body').on('click', '.editItem', function () {
    var item_url = $(this).data("url");
    window.location.href = item_url;
  });

  $('body').on('click', '.viewItem', function () {
    var item_url = $(this).data("url");
    window.location.href = item_url;
  });

  $('body').on('click', '.notifyItem', function () {
    var stock_id = $(this).data("stock_id");
    var order_id = $(this).data("order_id");
    var seller_id = $(this).data("seller_id");
    var buyer_id = $(this).data("buyer_id");
    $("#notifyModel #order_id").text(order_id);
    $("#notifyModel #stock_id").text(stock_id);
    $("#notifyModel").modal({
            backdrop: 'static',
            keyboard: false
          });
  });

  $('body').on('click', '.notifyBTN', function () {
    var order_id = $("#notifyModel #order_id").text();
    var stock_id = $("#notifyModel #stock_id").text();
    $.ajax({
      type: "POST",
      url: "{{ url('admin/matches-notify') }}/"+ order_id +"/"+stock_id,
      success: function (data) {
        console.log(data);
        Swal.fire('Success!', 'Notification sent.', 'success');
        table.draw();
      },
      error: function (data) {
        Swal.fire('Error!', 'Notification not sent', 'error');
      }
    });
  });

  $('body').on('click', '.makesaleBTN', function () {
    var order_id = $("#notifyModel #order_id").text();
    var stock_id = $("#notifyModel #stock_id").text();
    $.ajax({
      type: "POST",
      url: "{{ url('admin/matches-makesale') }}/"+ order_id +"/"+stock_id,
      success: function (data) {
        console.log(data);
        $('#notifyModel').modal('hide');
        Swal.fire('Success!', 'Sale has made.', 'success');
        table.draw();
      },
      error: function (data) {
        $('#notifyModel').modal('hide');
        Swal.fire('Error!', 'Sale has not made', 'error');
      }
    });
  });

  var viewinvoiceUrl,sendInvoiceUrl;
    $('body').on('click','.sendInvoice',function(e){
        e.preventDefault();
         viewinvoiceUrl = $(this).data("viewurl");
         sendInvoiceUrl = $(this).data("url");
        Swal.fire({
            title: "Are you sure?",
            html: '<div><button class="btn btn-primary" value="send" id="sendPdf">Send</button> <button class="btn btn-success" id="viewPdf">View PDF</button> <button class="btn btn-secondary" id="cancel">Cancel</button></div>',
            type: "warning",
            showConfirmButton: false,
            showCancelButton: false
        });
    })

    $('body').on('click','#sendPdf',function(e){
        e.preventDefault();
        window.location.href = sendInvoiceUrl;
        Swal.close();
    });

    // View pdf
    $('body').on('click','#viewPdf',function(e){
        e.preventDefault();
        window.open(viewinvoiceUrl, '_blank');
    });

    $('body').on('click','#cancel',function(e){
        e.preventDefault();
        Swal.close();
    });
    
    $('body').on('click', '.sendtoall', function (e) {
      e.preventDefault();
      var array_main = [];
      // Read all checked checkboxes
      $("input:checkbox[type=checkbox]:checked").each(function () {
        var id  = $(this).data('id');
        array_main.push({
              match_id: id,
      		});
      });
      if(array_main.length === 0){
        Swal.fire('Error!','Please select at least 1 record to send notification.', 'error');
      }else if(array_main.length > 10){
        Swal.fire('Error!', 'Maximum 10 notification can be send at a time.', 'error');
      } else{
        var jsonString = JSON.stringify(array_main);
        $.ajax({
          type: "POST",
          url: "{{ url('admin/InvoiceSendtoAll') }}",
          data: {array_main},
          dataType:'json',
          success: function (data) {
            Swal.fire('Sent!', data.success, 'success');
            $("input:checkbox[type=checkbox]").each(function () {
              $(this).prop('checked',false);
            }); 
          },
          error: function (data) {
            Swal.fire('Error!', '', 'error');
          }
        });
      }
      
      

      // sendBuyerAllInvoice = $(this).data("buyer");
      // sendSellerAllInvoice = $(this).data("seller");
      // console.log(sendSellerAllInvoice);;
      
        // Swal.fire({
        //   title: 'Are You sure want to sent?',
        //   text: '',
        //   type: 'warning',
        //   showCancelButton: true,
        //   confirmButtonText: 'Yes, sent it!',
        //   cancelButtonText: 'No, keep it'
        // }).then((result) => {
        //   if (result.value){
        //     window.location.href = $(this).data("url");
        //     Swal.close();
        //   }
        // });
    });
   $('#show_matched_div a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var show_matched = $(e.target).attr("href");
      window.location.href = "{{ route('admin.matches.index') }}?stock="+$('#stock_filter').val()+"&buyer="+$('#buyer_filter').val()+"&product_id="+$('#product_id').val()+"&match_type="+$('#match_type').val()+"&show_matched="+show_matched;
   });

   $("#clear_filter").bind("click", function () {
      $("#stock_filter,#buyer_filter,#match_type").val("");
      $("#stock_filter,#buyer_filter,#match_type").select2().trigger('change');
   });
});
</script>
@endpush
