@extends('backend.layouts.app')

@section('title', __('menus.backend.trading.offers.all') . ' :: ' .app_name())
@push('after-styles')
    <style>
        .float-right form {
            position: relative;
        }

        #upload_stock > input.btn.btn-info.ml-1 {
            position:absolute;
            top:0;
            right:0;
            margin:0;
            bottom:0;
            opacity:0;
            filter:alpha(opacity=0);
            font-size:5px;
            cursor:pointer;
            width: 80px;
        }
       
td.details-control {
    padding:13px !important;
    background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.details td.details-control {
     padding:13px !important;
    background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
}

    </style>
@endpush
@role('seller')
@php $route_pre = 'seller'; @endphp
@else
@php $route_pre = 'admin'; @endphp
@endif
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <h4 class="card-title mb-0">
                    {{ __('menus.backend.trading.offers.all') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->
            <div class="col-sm-4">
                <div class="form-group row">
                   
                  <div class="col-md-10">
                    {{ html()->select('product_id')
                    ->class('select2 form-control')
                    ->attribute('maxlength', 191)
                    ->options($products)
                    ->value(@$_GET['product_id'])
                      ->attribute('onchange', 'fetch_select(this.value)')
                    
                  }}

                </div>
            </div>
            </div>
            <div class="col-sm-4">
                <div class="btn-toolbar float-right" role="toolbar" >
                @role('seller')
                @else
                @can('export stocks')
                <a href="{{ route('admin.stock.stocksexports') }}" class="btn btn-warning ml-1" data-toggle="tooltip" title="Export Excel"><i class="fa fa-download">Download</i></a>
                 @endcan
                @endif
                </div>
                
              <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
                @can('add stock')  
                <a id="addstocks" class="btn btn-success ml-1" data-toggle="tooltip" title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i></a>
                @endcan
            </div><!--btn-toolbar-->
              <div class="btn-toolbar float-right">
                @role('seller')
                @else
                <form action="{{ route('admin.stock.stockimport') }}" method="POST" enctype="multipart/form-data" id="upload_stock">
                    {{ csrf_field() }}
                    <span class="fileupload-exists btn btn-primary ml-1"><i class="fa fa-upload">Upload</i></span>    
                    <input type="file" name="stock_file" class="btn btn-info ml-1" onchange="$('#upload_stock').submit();" >
                    
                </form>
                @endif
            </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-2">
            <div class="col">
                <div class="table-offers">
                 <table id="stock_table" class="table table-bordered data-table">
                        <thead>
                            <tr>
                            <th></th>
                                <th>Product</th>
                                <th>Status</th>
                                <th>Variety</th>
                                <th>Packing</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Quality</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script type="text/javascript">
var table;
var products = JSON.parse('@json($products)');
var selectOpt = '<option value="" selected="selected">Choose Product</option>';
$.each(products,function(key,value){
   selectOpt += '<option value="'+key+'">'+value+'</option>'
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var editMode = '<div class="row mt-3 ml-2 mr-2"><label class="col-md-2">Product</label><div class="col-md-4"><select name="product_id" class="edit_product edit_product_id form-control" required>'+selectOpt+'</select></div><label class="col-md-2">Status</label><div class="col-md-4"><select name="stock_status" class="status form-control" ><option value>Choose Stock Status</option><option value="unavailable">Unavailable</option><option value="available">Available</option><option value="upcoming_stock">Upcoming Stock</option></select></div></div><div class="row mt-3 ml-2 mr-2"><label class="col-md-2">Size</label><div class="col-md-2"><input name="size_from" type="text" class="form-control sizefrom" /></div><div class="col-md-2"><input type="text" name="size_to" class="sizeto form-control" /></div><label class="col-md-2">Price</label><div class="col-md-4"><input name="price" type="text" class="price form-control" /></div></div>';

var append_data = {"msg":"appened_row","data":{"expand":"","product_name":'<select class="product product_id form-control" required>'+selectOpt+'</select>',
    "stock_status":"<select class='status form-control' ><option value>Choose Stock Status</option><option value='unavailable'>Unavailable</option><option value='available'>Available</option><option value='upcoming_stock'>Upcoming Stock</option></select>",
    "field1":"<select class='field1 form-control' ><option value=''>Select</option></select>",
    "field2":"<select class='field2 form-control' ><option value=''>Select</option></select>",
    "field3":"<select class='field3 form-control' ><option value=''>Select</option></select>",
    "size":"<input style='width:50px;'  type='text' class='form-control sizefrom' /><input style='width:50px;' type='text' class='sizeto form-control' />",
    "price":"<input style='width:50px;'  type='text' class='price form-control' />",
    "action":"<input class='btn btn-primary savestock' value='Save' type='button'/>"}};
  $(function () {

	$('#stock_table #filter th').each( function () {
        var title = $(this).attr('data-title');
        if(title != '')
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );

     table = $('.data-table').DataTable({
        paging: true,
        searching: false,
        processing: true,
        serverSide: false,
        autoWidth: false,
        responsive: true,
        ajax: "{{ route($route_pre.'.stockv2.index') }}"+'?@if(isset($_GET['product_id']))product_id={{$_GET['product_id']}}@endif',
        columns: [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           "expand",
                "defaultContent": " "
            },
            {data: 'product_name'},
            {data: 'stock_status'},
            {data: 'field1'},
            {data: 'field2'},
            {data: 'size'},
            {data: 'price'},
            {data: 'field3'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });


    $('tbody').on('change', '.edit_product', function () {
        pid = $(this).val();
        parent_tr = $(this).parents('tr');
        $.ajax({
            type: "POST",
            url: "{{ route($route_pre.'.trading.getproduct') }}",
            data: {pid:pid},
            success: function (data) {
               parent_tr.find('.edit_nets').html(data);
            }
        });
    });
    $('tbody').on('click', 'td.details-control', function () {
        var tr = $(this).parents('tr');
        if(tr.hasClass('details') == false){
            var row = table.row( tr );
            row_data = row.data();
            console.log(row_data);
            tr.after('<tr></tr>');
            tr3 = tr.next();
            var th = this;
            $.ajax({
                type: "POST",
                url: "{{ route($route_pre.'.trading.getproduct') }}",
                data: {stock_id:row.data().id},
                success: function (data) {
                    $tdHtml = "<form>"+editMode +"<div class='edit_nets'>"+ data + "</div><div class='form-group row mt-3 ml-2 mr-2'><div class='col-md-6'> <input type='button' class='btn btn-primary update_stock' value='Save'/><input type='hidden' name='stock_id' value='"+row.data().id+"'/> </div> </div></form>";
                    tr3.html("<td colspan='10'>"+$tdHtml+"</td>");
                    tr3.find('.edit_product_id').val(row_data.product_id);
                    tr3.find('.sizefrom').val(row_data.size_from);
                    tr3.find('.sizeto').val(row_data.size_to);
                    tr3.find('.price ').val(row_data.raw_price);
                    tr3.find('.status').val(row_data.raw_stock_status);
                    console.log(row_data);
                    tr.addClass("details");
                }
            });
        } else {
           tr.next().remove();
           tr.removeClass('details');
        }
    });
    
    $('body').on('click', '.update_stock', function () {
        pr = $(this).parents('tr');
        var pid = pr.find('.edit_product_id').val();
        var form = $(this).parents('form');
        var stock_id = pr.find('input[name="stock_id"]').val();
        //var formData = $(this).parents('form').serializeArray();
        var formData = new FormData($(this).parents('form')[0]);
        formData.append('_method', 'PUT');
        $.ajax({
                url: "{{ route($route_pre.'.stockv2.index') }}/"+stock_id,
                method: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                beforeSend: function(){
                    $('.loading').removeClass('loading_hide');
                },
                success: function(data)
                {
                   if(data.status == 'success'){
                        $('.loading').addClass('loading_hide');
                        Swal.fire('Sent!', data.message, 'success');
                        setTimeout(function(){
                            window.location.reload();
                        }, 2000);
                    }
                    if(data.status == 'error'){
                        $('.loading').addClass('loading_hide');
                        Swal.fire('Error!', data.message, 'error');
                        $('.btn-success').removeAttr('disabled');
                    }
                }
                
        });
                    
       
    });
    $('body').on('click', '.editItem', function () {
        var item_url = $(this).data("url");
        window.location.href = item_url;
    });

    $('body').on('click', '.viewItem', function () {
        var item_url = $(this).data("url");
        window.location.href = item_url;
    });

    $('body').on('click', '.deleteItem', function () {
        var product_id = $(this).data("id");
        Swal.fire({
          title: 'Are You sure want to delete?',
          text: 'You will not be able to recover this offer!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it'
        }).then((result) => {
          if (result.value) {
            $.ajax({
                type: "DELETE",
                url: "{{ url($route_pre.'/trading/stock') }}"+'/'+product_id,
                success: function (data) {
                    Swal.fire('Deleted!', 'Offer has been deleted.', 'success');
                    table.draw();
                },
                error: function (data) {
                  Swal.fire('Error!', 'Offer not deleted', 'error');
                }
            });
          }
        });
    });

  });
function fetch_select(id){
    var rt = '{{ route($route_pre.'.stockv2.index') }}';
    window.location.href=rt+'?product_id='+id;
}
$(document).on("click","#addstocks",function(){
   

    table.row.add( append_data.data ).draw( false );
    /*
    var html = $('.data-table tr:last').html();
    html = "<tr>"+html+"</tr>"; 
    $('.data-table tbody').prepend(html);       
    */       
});


$(document).on('change','.product',function(){
   var productid = $(this).val();
   var ths = $(this);
   $.ajax({
        url: "{{ route($route_pre.'.trading.getproductforstock') }}",
        method: 'POST',
        data: {'productid':productid},
        success: function(data)
        {
            var selectvariety = '<option value="">Select</option>';
            var selectpacking = '<option value="">Select</option>';
            var selectquality = '<option value="">Select</option>';
           
            $.each(data.Variety,function(key,value){
                selectvariety += '<option value="'+key+'">'+value+'</option>'
            });
            console.log(selectvariety);
            ths.parents('tr').find(".field1").html(selectvariety);
            ths.parents('tr').find(".field1").attr('name',"fields["+data.Variety_id+"]");
            ths.parents('tr').find(".field1").attr('data-id',data.Variety_id);
            
            $.each(data.Packing,function(key,value){
                selectpacking +='<option value="'+key+'">'+value+'</option>'
            })
            ths.parents('tr').find(".field2").html(selectpacking);
            ths.parents('tr').find(".field2").attr('name',"fields["+data.Packing_id+"]");
            ths.parents('tr').find(".field2").attr('data-id',data.Packing_id);
            
            $.each(data.Quality,function(key,value){
                selectquality +='<option value="'+key+'">'+value+'</option>'
            })
            ths.parents('tr').find(".field3").html(selectquality);
            ths.parents('tr').find(".field3").attr('name',"fields["+data.Quality_id+"]");
            ths.parents('tr').find(".field3").attr('data-id',data.Quality_id);
        },
        error :function( data ) {
            
        }
    });
});

$(document).on('click', '.savestock', function(){
    var ths = $(this);
    var row = ths.parents('tr');
    row_data = row.data();
    var product          =  row.find(".product option:selected").val();
    var product_text     =  row.find(".product option:selected").text();
    var status           =  row.find(".status option:selected").val();
    var sizefrom         =  row.find(".sizefrom").val();
    var sizeto           =  row.find(".sizeto").val();
    var price            =  row.find(".price").val();
    var quality          =  row.find(".quality option:selected").val();
    var field1id         =  row.find(".field1").attr('data-id');
    var field1val        =  row.find(".field1").val();
    var field1_text      =  row.find(".field1 option:selected").text();
    var field2id         =  row.find(".field2").attr('data-id');
    
    var field2val        =  row.find(".field2").val();
    
    var field2_text      =  row.find(".field2 option:selected").text();
    var field3id         =  row.find(".field3").attr('data-id');
    var field3val        =  row.find(".field3").val();
    var field3_text      =  row.find(".field3 option:selected").text();
    $.ajax({
        url: "{{route('admin.stockv2.store')}}",
        method: 'POST',
        data: {'product':product,'stock_status':status,'sizefrom':sizefrom,'sizeto':sizeto,'price':price,'quality':quality,'field1id':field1id,'field1val':field1val,'field2id':field2id,'field2val':field2val,'field3id':field3id,'field3val':field3val},
        success: function(data)
        {
            $(".product").parent().html(product_text);
            $(".status").parent().html(status);
            $(".sizefrom").parent().html(sizefrom+"-"+sizeto);
            $(".price").parent().html(price);
            $(".quality").parent().html(quality);
            $(".field1").parent().html(field1_text);
            $(".field2").parent().html(field2_text);
            $(".field3").parent().html(field3_text);
            ths.hide();
            row_data.id = 118;
            row.data(row_data)
            console.log(row_data);
            ths.parents('tr').find('td:last-child').append('<div class="btn-group btn-group-sm"><button data-toggle="tooltip" data-id="93" data-original-title="Delete" type="button" class="btn btn-danger deleteItem"><i class="fas fa-trash-alt"></i></button></div>');
        },
        error :function( data ) {
            if( data.status === 422 ) {
                $('.loading').addClass('loading_hide');
               Swal.fire('Error!', data.responseJSON.message, 'error');
                $('.btn-success').removeAttr('disabled');
                var errors = [];
                errors = data.responseJSON.errors
                $.each(errors, function (key, value) {
                    
                    var n = key.search(".");
                    var res = key.split(".");
                    if(res.length > 1){
                        key = res[0];
                        for(i=1;i<res.length;i++){
                            key += "["+res[i]+"]";
                        }
                    }
                    console.log(key);
                    $("."+key).parent().addClass('has-danger');
                    $("."+key).addClass('is-invalid');
                    $('.'+key).next().children().children().css({"border": "1px solid #f86c6b"});
                })
            }
        }
    });
});
</script>
@endpush