<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-title">
        @lang('menus.backend.sidebar.general')
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::path() == 'admin/dashboard' ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          @lang('menus.backend.sidebar.dashboard')
        </a>
      </li>
		@can('view user')
    <li class="nav-item nav-dropdown {{ Request::segment(2) == 'auth' ? 'open' : '' }}">
      <a class="nav-link nav-dropdown-toggle {{ Request::path() == 'admin/auth*' ? 'active' : '' }}" href="#">
        <i class="nav-icon far fa-user"></i>
        @lang('menus.backend.access.title')

        @if ($pending_approval > 0)
        <span class="badge badge-danger">{{ $pending_approval }}</span>
        @endif
      </a>

      <ul class="nav-dropdown-items">
        <li class="nav-item">
          <a class="nav-link {{ Request::path() == 'admin/auth/user*' ? 'active' : '' }}" href="{{ route('admin.auth.user.index') }}">
            @lang('labels.backend.access.users.management')
            @if ($pending_approval > 0)
              <span class="badge badge-danger">{{ $pending_approval }}</span>
            @endif
          </a>
        </li>
        @if ($logged_in_user->isAdmin())
           
          <li class="nav-item">
            <a class="nav-link {{ Request::path() == 'admin/auth/role*' ? 'active' : '' }}" href="{{ route('admin.auth.role.index') }}">
              @lang('labels.backend.access.roles.management')
            </a>
          </li>
        @endif
      </ul>
    </li>
    @endcan	
	
	@if($logged_in_user->hasAnyPermission(['view carrier list','view transport list']))
	  <li class="nav-item nav-dropdown {{ Request::segment(2) == 'transport' ? 'open' : '' }}">
      <a class="nav-link nav-dropdown-toggle {{ Request::path() == 'admin/transport*' ? 'active' : '' }}" href="#">
        <i class="nav-icon fas fa-truck"></i>
        Transport List
      </a>
      <ul class="nav-dropdown-items">
        @can('view carrier list')
        <li class="nav-item">
          <a class="nav-link {{ Request::path() == 'admin/transport/transportlist*' ? 'active' : '' }}" href="{{ route('admin.transportlist.index') }}">
           Transport List
          </a>
        </li>
        @endcan
        @can('view transport list')
		 <li class="nav-item">
          <a class="nav-link {{ Request::path() == 'admin/transport/carrier*' ? 'active' : '' }}" href="{{ route('admin.carrier.index') }}">
           Carrier List
          </a>
        </li>
        @endcan
      </ul>
    </li>
  @endif
      
  </ul>
</nav>

<button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
