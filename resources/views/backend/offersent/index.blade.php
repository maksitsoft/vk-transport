@extends('backend.layouts.app')
@section('title', __('menus.backend.trading.offersent.all'). ' :: ' . app_name())
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">

            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('menus.backend.trading.offersent.all') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->
            <form action="{{ route('admin.offersent.index') }}" name="filter_matches">
            <div class="col-sm-12">
                <div class="form-group row">
                    <label class="col-md-12 form-control-label" for="type">Filter Matches</label>
                    <div class="col-md-3">
                        <select class="form-control select2" id="stock_filter" name="stock_filter">
                        <option value="">Select Stock</option>
                        @foreach($stock_list as $stock)
                        <option value="{{$stock->id}}" @if(isset($_GET['stock']) && $_GET['stock'] == $stock->id) selected @endif>
                            #{{ @$stock->id }} {{ @$stock->product->name}} [{{ @$stock->seller->username}}]
                        </option>
                        @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select class="form-control select2" id="buyer_filter" name="buyer_filter">
                        <option value="">Select Buyer</option>
                        @foreach($buyers_list as $buyer)
                        <option value="{{$buyer->id}}" @if(isset($_GET['buyer']) && $_GET['buyer'] == $buyer->id) selected @endif>
                            {{ @$buyer->username}}
                        </option>
                        @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select class="form-control select2" id="seller_filter" name="seller_filter">
                        <option value="">Select Seller</option>
                        @foreach($sellers_list as $seller)
                        <option value="{{$seller->id}}" @if(isset($_GET['seller']) && $_GET['seller'] == $seller->id) selected @endif>
                            {{ @$seller->username}}
                        </option>
                        @endforeach
                        </select>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <input class="form-control offer_sent" id="offer_sent" name="offersent" autocomplete="off" value="<?php echo (isset($_GET['offer_sent_date'])) ? $_GET['offer_sent_date'] : ''?>" placeholder="Offer Sent Date" type="text"/>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    
                     <div class="col-md-3">
                        <select class="form-control select2" id="product_id" name="product_id">
                        <option value="">Select Product</option>
                        @foreach($products as $key => $product)
                        <option value="{{$key}}" @if(isset($_GET['product_id']) && $_GET['product_id'] == $key) selected @endif>
                            {{ @$product}}
                        </option>
                        @endforeach
                        </select>
                    </div>
                    @php $field_num = 1; @endphp
                    @foreach($ProdSpecArrKeys as $key_id)
                    <div class="col-md-3">
                        {{ html()->select('field'.$field_num)
                            ->class('select2 form-control')
                            ->attribute('maxlength', 191)
                            ->options(@$product_spec_values_arr[$key_id])
                            ->value(@$_GET['field'.$field_num])
                            ->placeholder('Select '.$ProdSpecArr[$key_id])
                        }}
                    </div>
                    @php $field_num++; @endphp
                    @endforeach
                    
                    <div class="col-md-12">
                        <button class="btn btn-success float-left" type="submit"><i class="fas fa-filter"></i>Filter</button>
                        <div class="btn-toolbar float-right" role="toolbar" >
                            <a href="{{ route('admin.offersent.offersentexports') }}" class="btn btn-primary ml-1" data-toggle="tooltip" title="Export Excel"><i class="fa fa-download"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <div class="col-sm-7">
              
              <!--btn-toolbar-->
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-2">
            <div class="col">
                <div class="table-offers">
                      <table id="order2_table" class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.trading.offers.table.id')</th>
                            <th>Buyer</th>
                            <th>Company</th>
                            <th>Product Name</th>
                            @foreach($ProdSpecArr as $spec)
                                <th>{{ $spec }}</th>
                            @endforeach
                            <th>Size From</th>
                            <th>Size To</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <!-- <th>@lang('labels.backend.trading.offers.table.date')</th> -->
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tfoot>
                            <tr id="filter">
                                <th data-title="@lang('labels.backend.trading.offers.table.id')"></th>
                                <th data-title="Buyer"></th>
                                <th data-title="Company"></th>
                                <th data-title="Product Name"></th>
                                @foreach($ProdSpecArr as $spec)
                                    <th data-title="{{ $spec }}"></th>
                                @endforeach
                                <th data-title="Size From"></th>
                                <th data-title="Size To"></th>
                                <th data-title="Quantity"></th>
                                <th data-title="Price"></th>
                                <!-- <th data-title="@lang('labels.backend.trading.offers.table.date')"></th> -->
                                <th data-title=""></th>
                            </tr>
                        </tfoot>   
					</table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script type="text/javascript">
  $(function () {
    $('#order2_table #filter th').each( function () {
        var title = $(this).attr('data-title');
        if(title != '')
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        responsive: true,
        ajax: "{{ route('admin.offersent.index') }}?@if(isset($_GET['product_id']))stock={{@$_GET['stock']}}&buyer={{@$_GET['buyer']}}&seller={{@$_GET['seller']}}&product_id={{@$_GET['product_id']}}&field1={{@$_GET['field1']}}&field2={{@$_GET['field2']}}&packing={{@$_GET['packing']}}&offer_sent_date={{@$_GET['offer_sent_date']}}@endif",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'buyer_name', name: 'buyer_name'},
            {data: 'buyer_company', name: 'buyer_company'},
            {data: 'product_name', name: 'product_name'},
             @if(isset($ProdSpecArrNames['field2']) && $ProdSpecArrNames['field2'] != '')
            {data: 'field1', name: 'field1'},
            @endif
            @if(isset($ProdSpecArrNames['field2']) && $ProdSpecArrNames['field2'] != '')
            {data: 'field2', name: 'field2'},
            @endif
            @if(isset($ProdSpecArrNames['field3']) && $ProdSpecArrNames['field3'] != '')
            {data: 'field3', name: 'field3'},
            @endif
            {data: 'size_from', name: 'size_from'},
            {data: 'size_to', name: 'size_to'},
            {data: 'quantity', name: 'quantity'},
            {data: 'price', name: 'price'},
            // {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    table.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('body').on('click', '.editItem', function () {
        var item_url = $(this).data("url");
        window.location.href = item_url;
    });

    $('body').on('click', '.viewItem', function () {
        var item_url = $(this).data("url");
        window.location.href = item_url;
    });

    $('body').on('click', '.deleteItem', function () {
        var item_id = $(this).data("id");
        Swal.fire({
          title: 'Are You sure want to delete?',
          text: 'You will not be able to recover this order2!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it'
        }).then((result) => {
          if (result.value) {
            $.ajax({
                type: "DELETE",
                url: "{{ url('admin/trading/order2') }}"+'/'+item_id,
                success: function (data) {
                    Swal.fire('Deleted!', 'Sale has been deleted.', 'success');
                    table.draw();
                },
                error: function (data) {
                  Swal.fire('Error!', 'Sale not deleted', 'error');
                }
            });
          }
        });
    });

    var viewinvoiceUrl,sendInvoiceUrl;
    $('body').on('click','.sendInvoice',function(e){
        e.preventDefault();
         viewinvoiceUrl = $(this).data("viewurl");
         sendInvoiceUrl = $(this).data("url-send");
        Swal.fire({
            title: "Are you sure?",
            html: '<div><button class="btn btn-primary" value="send" id="sendPdf">Send</button> <button class="btn btn-success" id="viewPdf">View PDF</button> <button class="btn btn-secondary" id="cancel">Cancel</button></div>',
            type: "warning",
            showConfirmButton: false,
            showCancelButton: false
        });
    })

    $('body').on('click','#sendPdf',function(e){
        e.preventDefault();
        window.location.href = sendInvoiceUrl;
        Swal.close();
    });

    // View pdf
    $('body').on('click','#viewPdf',function(e){
        e.preventDefault();
        window.open(viewinvoiceUrl, '_blank');
    });

    $('body').on('click','#cancel',function(e){
        e.preventDefault();
        Swal.close();
    });
     $(".offer_sent").datepicker({
		format: "mm/dd/yyyy",
		weekStart: 0,
		calendarWeeks: true,
		autoclose: true,
	});
  });
  </script>
  @endpush
