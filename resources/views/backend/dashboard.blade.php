@extends('backend.layouts.app')

@section('title',  __('strings.backend.dashboard.title'). ' :: ' . app_name())

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong>
                </div><!--card-header-->
                <div class="card-body">
                  @if (\Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! \Session::get('success') !!}
                    </div>
                  @endif
                   @if(auth()->user()->confirmed != 1)
                     <div class="alert alert-warning">
                        {!! __('exceptions.frontend.auth.confirmation.resend', ['url' => route('frontend.auth.account.confirm.resend', e(auth()->user()->{auth()->user()->getUuidName()}) )]) !!}
                     </div>
                   @endif
                  @role('seller')
                  <span class="btn" style="font-size: 16px;font-weight: 600;padding: 0;">Please click to </span>
                  <a href="{{ route('seller.stock.create') }}" class="btn btn-success ml-1" title="Add a New Stock" data-toggle="tooltip" ><i class="fas fa-plus-circle"></i> Add a New Stock</a>
                  @elserole('buyer')
                    <span class="btn" style="font-size: 16px;font-weight: 600;padding: 0;">Please click to </span>
                    <a href="{{ route('buyer.get_quote') }}" class="btn btn-success ml-1" @if(auth()->user()->confirmed != 1) onclick="Swal.fire('Error!', 'Please verify your email first to Get Quote, 'info'); return false;" @endif title="Get Quote" data-toggle="tooltip" ><i class="fas fa-paper-plane"></i> Get Quote</a>
                    <div class="form-group row mt-3">
                      <div class="col-md-12">
                        <button style="bottom:0px; left:15px;" type="button" class="pro-btnAdd btn btn-success btn-md">Add your Prefrences +</button>
                      </div>
                    </div>
                    {{ html()->form('POST')->id('formsubmit')->class('form-horizontal')->open() }}
                    @php $tab_key = 1; @endphp
                    @php $tab_key_heading = $tab_key_heading2 = 1; @endphp
                    <div id="product_prefs_box2">
                      <hr class="mb-4">
                      <div id="products_specs">
                          <div class="row">
                              <div class="col-md-12" id="tabs">
                                  <ul class="nav nav-tabs ui-sortable">
                                      @if(isset($productPrefRel))
                                      @foreach(@$productPrefRel as $SPkey=>$productSpecRel)
                                          <li class="nav-item "><a class="nav-link {{ ($tab_key_heading ==1)?'active':'' }}" href="#pref{{ $tab_key_heading }}" data-toggle="tab">Pref #{{ $tab_key_heading }}</a></li>
                                      @php $tab_key_heading++; @endphp   
                                      @endforeach 
                                      @else
                                          <li class="nav-item "><a class="nav-link active" href="#pref1" data-toggle="tab">Pref #1</a></li>
                                      @endif
                                  </ul>
                                  <div class="tab-content">
                                  @if(isset($productPrefRel))
                                      @foreach(@$productPrefRel as $SPkey=>$productSpecRel)
                                      <div id="pref{{ $tab_key_heading2 }}" class="tab-pane {{ ($tab_key_heading2 ==1)?'active':'' }} product-group">
                                     <div class="form-group row">
                                           {{ html()->label('<strong>Product</strong>')->class('col-md-2 form-control-label')->for("product[".$tab_key_heading2."][product_name]") }}
                                          <div class="col-md-10">
                                          {{ html()->select("old_product[".$SPkey."][product_name]")->id("product_".$tab_key_heading2."_product_name")
                                            ->class('select2 form-control products-details')
                                            ->options($products)
                                            ->value(@$productProdRel[$SPkey][product_id])
                                            ->attribute('data-pref-id', $SPkey)
                                            ->placeholder('Choose Product')
                                          }}
                                          </div>
                                          </div>
                                          <div class="col-md-12">
                                              <div class="product-nets">
                                                  @include('backend.products.stock-product-multi-pref', ['productSpecRel' => $productSpecRel,'pref_id' => $SPkey])
                                              </div>
                                              @if(!empty($productSpecRel))
                                              
                                            @foreach($productSpecRel as $pKey=>$productSpec)
                                            @if(isset($productSpec['field_type']) && $productSpec['field_type'] == 'optionrange' )
                                            <div class="form-group row">
                                          {{ html()->label('<strong>Size ranges</strong>')->class('col-md-12 form-control-label')->for('size_ranges') }}
                                          <div id="size_ranges" class="col-md-12" style="padding-bottom:40px; position:relative">
                                            <div class="r-group form-group row">
                                              <div class="col-md-3">
                                                <label class="form-control-label">Min</label>
                                                <input class="form-control" type="text" value="{{@$stock->size_from ?? '45'}}" placeholder="From" name="size_range[{{ $pKey }}][size_from]" id="size_range_0_from" data-pattern-name="size_range[++][from]" data-pattern-id="size_range_++_from" />
                                              </div>
                                              <div class="col-md-3">
                                                <label class="form-control-label">Max</label>
                                                <input class="form-control" type="text" value="{{@$stock->size_to ?? '65'}}" placeholder="to" name="size_range[{{ $pKey }}][size_to]" id="size_range_0_to" data-pattern-name="size_range[++][to]" data-pattern-id="size_range_++_to" />
                                              </div>
                                              <div class="col-md-3">
                                                <label class="form-control-label">Premium</label>
                                                <input class="form-control" type="number" name="size_range[0][premium]" id="size_range_0_premium" data-pattern-name="size_range[++][premium]" data-pattern-id="size_range_++_premium" value="0" data-decimals="0" min="-10" max="10" step="1"/>
                                              </div>
                                              <div class="col-md-3">
                                                <label class="form-control-label d-block">&nbsp;</label>
                                                <button type="button" class="r-btnRemove btn btn-danger btn-md">Remove -</button>
                                              </div>
                                            </div>
                                            <button style="position:absolute; bottom:0px; left:15px;" type="button" class="r-btnAdd btn btn-success btn-md">Add +</button>
                                          </div>
                                        </div>
                                            
                                            <!--form-group-->
                                            @endif
                                            @endforeach
                                            @endif
                                          </div>
                                      </div>
                                      @php $tab_key = $SPkey; $tab_key++; $tab_key_heading2++; @endphp
                                      @endforeach
                                  @else
                                      <div id="pref1" class="tab-pane active product-group">
                                   <div class="form-group row">
                                           {{ html()->label('<strong>Product</strong>')->class('col-md-2 form-control-label')->for("product[".$tab_key."][product_name]") }}
                                          <div class="col-md-10">
                                          {{ html()->select("product[".$tab_key."][product_name]")->id("product_".$tab_key."_product_name")
                                            ->class('select2 form-control products-details')
                                            ->options($products)
                                            ->value(@$buyer->product->id)
                                            ->attribute('data-pref-id', $tab_key)
                                            ->placeholder('Choose Product')
                                          }}
                                          </div>
                                          </div>
                                          <div class="col-md-12">
                                              <div class="product-nets"></div>
                                          </div>
                                      </div>
                                      @php $tab_key++; @endphp
                                      @php $tab_key_heading++; @endphp   
                                  @endif
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  @else
                  {!! __('strings.backend.welcome') !!}
                  @endrole
                  @if(!empty($buyer->id))
                    @php 
                      $url =  route('buyer.updateBuyerPref');
                      $buyerid = $buyer->id;
                    @endphp
                  @endif
                </div><!--card-body-->
                @role('buyer')
                <div class="card-footer clearfix">
                  <div class="row">
                    <div class="col">
                      {{ form_cancel(route('admin.buyers.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                      {{ form_submit(__('buttons.general.crud.update')) }}
                    </div><!--col-->
                  </div><!--row-->
                </div><!--card-footer-->
                {{ html()->form()->close() }}
                @endrole
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
@push('after-scripts')
@role('buyer')
  <link rel="stylesheet" href="{{ asset('css/bootstrap-dynamic-tabs.css') }}"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"/>
   <style>.flex_item{display: inline-block;width: auto;}.checkbox .input-group{margin-right:10px}.more {display: none;}</style>
  <script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
    crossorigin="anonymous"></script>
  <script src="{{ asset('js/bootstrap-dynamic-tabs.js') }}"></script>
  <script type="text/javascript">
  var tabn = {{$tab_key}};
  var tab_key_heading = {{$tab_key_heading}};
  var tabs = $('#tabs').bootstrapDynamicTabs();

  $("body").on('click','.remove-this-tab',function(){
    $(this).parent().remove();
    content_id = $(this).parent().attr('data-content-id');
    $('.tab-content').find('#pref'+content_id).remove();
    $('.nav-tabs a:first').tab('show');
  });  

  $('body').on('DOMNodeInserted', 'select', function () {
    $(this).select2();
  }); 

  var products = JSON.parse('@json($products)');
  var selectOpt = '<option value="" selected="selected">Choose Product</option>';
  $.each(products,function(key,value){
    selectOpt += '<option value="'+key+'">'+value+'</option>'
  });

  $(".pro-btnAdd").click(function(){
    $('.nav-tabs').append('<li class="nav-item "><a class="nav-link" data-content-id="'+tabn+'" href="#pref'+tabn+'" data-toggle="tab">Pref#'+tab_key_heading+' <button class="close remove-this-tab" type="button">x</button></a></li>');
    $('.tab-content').append('<div id="pref'+tabn+'" class="tab-pane product-group"> <div class="form-group row"><label class="col-md-2 form-control-label" for="product[0][product_name]"><strong>Product</strong></label><div class="col-md-10">  <select class="select2 form-control products-details" data-pref-id="'+tabn+'" name="product['+tabn+'][product_name]" id="product_'+tabn+'_product_name" pref-id="0" tabindex="-1" aria-hidden="true">'+selectOpt+'</select></div></div><div class="col-md-12"> <div class="product-nets"></div></div></div></div>');
      tabn++;
      tab_key_heading++;
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#formsubmit').on('submit', function(event) {
      event.preventDefault();

      var buyerid = {{ $buyer->id }};
      var formData = new FormData(this);      

      $( ".pref_contact" ).each(function( key, value ) {
        if(value.value == 0){
          formData.append(value.name, value.value);
        }
      });
      
      if(buyerid != 0)
      {
        formData.append('_method', 'PUT');
      }
      $.ajax({
        url: "{{ $url }}",
        method: 'POST',
        data: formData,
        contentType: false,             
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function(){
        $('.loading').removeClass('loading_hide');
        },
        success: function(data)
        {
          $('.loading').addClass('loading_hide');
          if(data.status == 'success'){
            Swal.fire('Sent!', data.message, 'success');
            setTimeout(function(){
              window.location.href = "{{ route('admin.buyers.index') }}"; 
            }, 5000);
          }
          if(data.status == 'error'){
            Swal.fire('Error!', data.message, 'error');
            $('.btn-success').removeAttr('disabled');
          }
        },
        error :function( data ) {
          $('.loading').addClass('loading_hide');
          if( data.status === 422 ) {
            Swal.fire('Error!', data.responseJSON.message, 'error');
            $('.btn-success').removeAttr('disabled');
            var errors = [];
            errors = data.responseJSON.errors
            $.each(errors, function (key, value) {
                console.log(key);
                var n = key.search(".");
                var res = key.split(".");
                if(res.length > 1){
                    key = res[0]+"_"+res[1];
                }
                $('#'+key).parent().addClass('has-danger');
                $('#'+key).addClass('is-invalid');
                $('#'+key).parent('.has-danger').find('.invalid-feedback').html(value);
                $('#'+key).next().children().children().css({"border": "1px solid #f86c6b"});
            })
          }
        }
      });
    });
    
  $('body').on('change', '.products-details', function(){
    var val = $(this).val();
    //console.log($(this).parent().next().next().find('.product-nets'));
    console.log($(this).parents('.product-group').find('.product-nets'));
    if(val == '' || val == 'undefined')
    {
        return false;
    }
    var pref_id = $(this).attr('data-pref-id');
   
    var ths = $(this);
    $(this).attr('data-pref-id', pref_id);
    $.ajax({
        type: "POST",
        url: "{{ route('buyer.trading.getproductmultiple') }}",
        data: {pid:val, pref_id:pref_id},
        success: function (data) {
            ths.parents('.product-group').find('.product-nets').html(data);
            ths.parents('.product-group').find(".product-nets input[type='number']").inputSpinner();
            $( ".checkbox.switch-box .switch input" ).each(function( index,element ) {
                switchPremium(element.id);
            });
            $('.select2').select2();
          return false;
        }
    });
    $('.switch').click(function() {
      delivery_same();
      product_prefs();
      //select_all_soil();
      light_same();
      //var id = $(this).children('input').attr('id');
      switchPremium($(this).children('input').attr('id'));
    });
    $( ".checkbox.switch-box .switch input" ).each(function( index,element ) {
      switchPremium(element.id);
    });

    $("body").on("click",".any_type_selected",function(){
    dataGroup = $(this).attr('data-group');
    state = $(this).prop('checked');
    $(this).parents(".app-head-group-outer").find(".switch_select_sub_item").each(function(){
        console.log(state);
        if(state == true){
            $(this).find('input[type=number]').prop('disabled', false);
            $(this).find('input[type=checkbox]').prop('checked', true);
        } else {
            $(this).find('input[type=number]').prop('disabled', true);
            $(this).find('input[type=checkbox]').prop('checked', false);
        }
        });
    });

    $("body").on("click",".switch_select_sub_item_cb",function(){
        state = $(this).prop('checked');
        if(state == false){
            $(this).parents(".app-head-group-outer").find(".any_type_selected").prop('checked', false);
        }
    });
    $('body').on('click', '.pref_contact', function(){
      if(this.checked){
        $(this).val(1);
        $(this).attr('checked','checked');
      }else{
        $(this).val(0);
        $(this).removeAttr('checked');
      }
    });
    $('body').on('click','.switch',function() {
       switchPremium($(this).children('input').attr('id'));
    });
  });
  function switchPremium(id){
  if ($('input#'+id).prop('checked')) {
    $('input#'+id).parent().parent().find('input[type=number]').prop('disabled', false);
    $('input#'+id).closest('.accept_all').parent().find('select[name^=specification]').removeAttr('required');
  } else {
    $('input#'+id).parent().parent().find('input[type=number]').prop('disabled', true);
    $('input#'+id).closest('.accept_all').parent().find('select[name^=specification]').attr('required',true);
  }
}
function light_same(){
  var checkbox = $('input#light');
  var size_range_0_to =  $('#size_range_0_to').val();
  if ($(checkbox).prop('checked') && size_range_0_to > 65) {
      $('#export').prop('checked', true).change();
  } else {
    $('#export').prop('checked', false).change();
  }
}
  </script>
  @endrole
@endpush