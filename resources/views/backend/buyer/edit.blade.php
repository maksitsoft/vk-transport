@extends('backend.layouts.app')

@section('title', 'Create Offer :: '.app_name())
@php
$company = json_decode($buyer->company);
@endphp
@section('content')
<style>.show_r_btn{display:block!important}</style>
{{ html()->form('PUT', route('admin.buyers.update', $buyer->id))->class('form-horizontal')->open() }}
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-5">
        <h4 class="card-title mb-0">
          Edit Buyer
          <small class="text-muted"></small>
        </h4>
      </div><!--col-->
    </div><!--row-->

    <hr>

    <div class="row mt-4 mb-4">
      <div class="col">

        <div class="form-group row">
          {{ html()->label('<strong>Username</strong> <span style="color:red">*</span>')->class('col-md-2 form-control-label')->for('username') }}

          <div class="col-md-2">
            <div class="form-group">
            {{ html()->text('username')
            ->class('form-control')
            ->value($buyer->username)
            ->placeholder('Username')
            ->attribute('maxlength', 191)
            ->required() }}
          </div>
          </div><!--col-->
          <div class="col-md-4 row">
              {{ html()->label('<strong>Discount (%)</strong>')->class('col-md-4 form-control-label float-left pl-4 pr-0')->for('transportation') }}
              <div class="col-md-8 float-left pl-md-0">
                 <input type="number" name="disc_upsc" value="{{$buyer->disc_upsc}}" data-decimals="2" min="0" max="100" step="0.1" class="float-left" />
              </div>
          </div>
            @php $price =  (isset($price->price) ? '+'.$price->price : '0' ) @endphp

          {{ html()->label('<strong>Transportation Cost</strong> &nbsp;&nbsp;'.$price)->class('col-md-4 form-control-label  pl-4 pr-0')->for('transportation_cost') }}
          <!--col-->
        </div><!--form-group-->
      <div class="form-group row">
        <label class="col-md-2 form-control-label" for="note"><strong>Notes</strong></label>
        <div class="col-md-10">
         <textarea class="form-control" name="note" rows="3" placeholder="Notes" id="note">{{$buyer->note}}</textarea>
        </div>
        </div>
        <div class="form-group row">
          {{ html()->label('<strong>Buyer 1 Contact Info</strong>')->class('col-md-2 form-control-label')->for('name') }}

          <div class="col-md-10">
            <div class="row">


              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Phone <small><i>(with county code)</i></small> <span style="color:red">*</span>')->class('form-control-label')->for('phone') }}
                  {{ html()->text('phone')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.phone_placeholder'))
                    ->value($buyer->phone)
                    ->attribute('maxlength', 191)
                    ->required()
                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Name')->class('form-control-label')->for('name') }}
                  {{ html()->text('name')
                    ->class('form-control')
                    ->value($buyer->name)
                    ->placeholder('Name')
                    ->attribute('maxlength', 191)
                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('E-mail Address <span style="color:red">*</span>')->class('form-control-label')->for('email') }}
                  {{ html()->email('email')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                    ->value($buyer->email)
                    ->attribute('maxlength', 191)
                  }}
                </div>
              </div>


            </div>
          </div><!--col-->
        </div><!--form-group-->

        @php
        $buyer2_contact = json_decode($buyer->buyer2_contact);
        $transport_contact = json_decode($buyer->transport_contact);
        $accounts_contact = json_decode($buyer->accounts_contact);
        @endphp
        <div class="form-group row">
          {{ html()->label('<strong>Buyer 2 Contact Info </strong>')->class('col-md-2 form-control-label')->for('buyer2_contact_phone') }}

          <div class="col-md-10">
            <div class="row">


              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Phone <small><i>(with county code)</i></small>')->class('form-control-label')->for('buyer2_contact_phone') }}
                  {{ html()->text('buyer2_contact[phone]')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.phone_placeholder'))
                    ->attribute('maxlength', 191)
                    ->value(@$buyer2_contact->phone)

                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Name')->class('form-control-label')->for('buyer2_contact_name') }}
                  {{ html()->text('buyer2_contact[name]')
                    ->class('form-control')
                    ->placeholder('Name')
                    ->attribute('maxlength', 191)
                    ->value(@$buyer2_contact->name)
                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('E-mail Address')->class('form-control-label')->for('buyer2_contact_email') }}
                  {{ html()->email('buyer2_contact[email]')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                    ->attribute('maxlength', 191)
                    ->value(@$buyer2_contact->email)
                  }}
                </div>
              </div>

            </div>
          </div><!--col-->
        </div><!--form-group-->
        <!--Transport-->
        <div class="form-group row">
          {{ html()->label('<strong>Transport Contact Info</strong>')->class('col-md-2 form-control-label')->for('transport_contact_phone') }}

          <div class="col-md-10">
            <div class="row">


              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Phone <small><i>(with county code)</i></small>')->class('form-control-label')->for('transport_contact_phone') }}
                  {{ html()->text('transport_contact[phone]')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.phone_placeholder'))
                    ->attribute('maxlength', 191)
                    ->value(@$transport_contact->phone)
                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Name')->class('form-control-label')->for('transport_contact_name') }}
                  {{ html()->text('transport_contact[name]')
                    ->class('form-control')
                    ->placeholder('Name')
                    ->attribute('maxlength', 191)
                    ->value(@$transport_contact->name)
                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('E-mail Address')->class('form-control-label')->for('transport_contact_email') }}
                  {{ html()->email('transport_contact[email]')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                    ->attribute('maxlength', 191)
                    ->value(@$transport_contact->email)
                  }}
                </div>
              </div>

            </div>
          </div><!--col-->
        </div>
         <!--Accounts-->
        <div class="form-group row">
          {{ html()->label('<strong>Accounts Contact Info</strong>')->class('col-md-2 form-control-label')->for('accounts_contact_phone') }}

          <div class="col-md-10">
            <div class="row">


              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Phone <small><i>(with county code)</i></small>')->class('form-control-label')->for('accounts_contact_phone') }}
                  {{ html()->text('accounts_contact[phone]')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.phone_placeholder'))
                    ->attribute('maxlength', 191)
                    ->value(@$accounts_contact->phone)

                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('Name')->class('form-control-label')->for('accounts_contact_name') }}
                  {{ html()->text('accounts_contact[name]')
                    ->class('form-control')
                    ->placeholder('Name')
                    ->attribute('maxlength', 191)
                    ->value(@$accounts_contact->name)
                  }}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  {{ html()->label('E-mail Address')->class('form-control-label')->for('accounts_contact_email') }}
                  {{ html()->email('accounts_contact[email]')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                    ->attribute('maxlength', 191)
                    ->value(@$accounts_contact->email)
                  }}
                </div>
              </div>

            </div>
          </div><!--col-->
        </div>


        <div class="form-group row">
          {{ html()->label('<strong>Company</strong>')->class('col-md-2 form-control-label')->for('company_name') }}
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                {{ html()->label('Company Name')->class('form-control-label')->for('email') }}
                {{ html()->text('company')->value($buyer->company)->class('form-control')->placeholder('Company Name')->attribute('maxlength', 191) }}
                </div>
              </div><!--col-->
              <div class="col-md-4">
                <div class="form-group">
                {{ html()->label('Company VAT')->class('form-control-label')->for('vat') }}
                {{ html()->text('vat')->value($buyer->vat)->class('form-control')->placeholder('Company VAT')->attribute('maxlength', 191) }}
                </div>
              </div><!--col-->
            </div><!--form-group-->
          </div>
        </div>



        <div class="form-group row">
          {{ html()->label('<strong>Company Address</strong>')->class('col-md-2 form-control-label')->for('address') }}
          <div class="col-md-10">
            <div class="row">

              <div class="col-md-3">
                <div class="form-group">
                {{ html()->label('City <span style="color:red">*</span>')->class('form-control-label')->for('city') }}
                {{ html()->text('city')->value(@$buyer->city)->class('form-control')->placeholder('City')->attribute('maxlength', 191) }}
                </div>
              </div><!--col-->

              <div class="col-md-3">
                <div class="form-group">
                {{ html()->label('Postal Code <span style="color:red">*</span>')->class('form-control-label')->for('postalcode') }}
                {{ html()->text('postalcode')->value($buyer->postalcode)->class('form-control')->placeholder('Postal Code')->attribute('maxlength', 191) }}
                </div>
              </div><!--col-->

              <div class="col-md-3">
                <div class="form-group">
                {{ html()->label('Street Address')->class('form-control-label')->for('address') }}
                {{ html()->text('address')->value($buyer->address)->class('form-control')->placeholder('Street Address')->attribute('maxlength', 191) }}
                </div>
              </div><!--col-->

              <div class="col-md-3">
                <div class="form-group">
                {{ html()->label('Country')->class('form-control-label')->for('email') }}
                {{ html()->select('country')
                ->class('select2 form-control')
                ->options(country_list())
                ->attribute('maxlength', 191)
                ->value($buyer->country)
                ->attribute('onblur', 'changeRequered()')
              }}
                </div>
              </div><!--col-->


            </div><!--form-group-->
          </div>
        </div>

        <div class="form-group row">
          {{ html()->label('<strong>Delivery Address Is Same ?</strong>')->class('col-md-2 form-control-label')->for('email') }}
          <div class="col-md-10">
            <div class="checkbox d-flex align-items-center">
              {{ html()->label(
                html()->checkbox('delivery_same', in_array($buyer->delivery_same, array('1')), 1)
                ->class('switch-input')
                ->id('delivery_same')
                . '<span class="switch-slider" data-checked="On" data-unchecked="Off"></span>')
                ->class('switch switch-label switch-pill switch-success mr-2')
                ->for('delivery_same') }}
              </div>
            </div>
          </div>


        <div class="form-group row">
          {{ html()->label('<strong>Delivery Address</strong>')->class('col-md-2 form-control-label')->for('delivery_address') }}
          <div id="delivery_address" class="col-md-10" style="padding-bottom:40px; position:relative">
            @foreach(json_decode($buyer->delivery_address) as $key => $val)
            <div class="del-group form-group row">




              <div class="col-md-3">
                <div class="form-group">
                {{ html()->label('City <span style="color:red">*</span>')->class('form-control-label')->for('delivery_address_'.$key.'_city') }}

                <input class="form-control" type="text" placeholder="City" value="{{@$val->city}}"
                name="delivery_address[{{$key}}][city]" id="delivery_address_{{$key}}_city"
                data-pattern-name="delivery_address[++][city]"
                data-pattern-id="delivery_address_++_city" />

                </div>
              </div><!--col-->

              <div class="col-md-2">
                <div class="form-group">
                {{ html()->label('Postal Code <span style="color:red">*</span>')->class('form-control-label')->for('delivery_address_'.$key.'_postalcode') }}
                <input class="form-control" type="text" placeholder="City" value="{{@$val->postalcode}}"
                name="delivery_address[{{$key}}][postalcode]" id="delivery_address_{{$key}}_postalcode"
                data-pattern-name="delivery_address[++][postalcode]"
                data-pattern-id="delivery_address_++_postalcode" />
                </div>
              </div><!--col-->

              <div class="col-md-3">
                <div class="form-group">
                {{ html()->label('Street Address')->class('form-control-label')->for('delivery_address_'.$key.'_address') }}
                <input class="form-control" type="text" placeholder="City" value="{{@$val->address}}"
                name="delivery_address[{{$key}}][address]" id="delivery_address_{{$key}}_address"
                data-pattern-name="delivery_address[++][address]"
                data-pattern-id="delivery_address_++_address" />
                </div>
              </div><!--col-->

              <div class="col-md-2">
                <div class="form-group">
                {{ html()->label('Country')->class('form-control-label')->for('delivery_address_'.$key.'_country') }}
                <input class="form-control" type="text" placeholder="City" value="{{@$val->country}}"
                name="delivery_address[{{$key}}][country]" id="delivery_address_{{$key}}_country"
                data-pattern-name="delivery_address[++][country]"
                data-pattern-id="delivery_address_++_country" />
                </div>
              </div><!--col-->

              <div class="col-md-2">
                 <button type="button" class="del-btnRemove btn btn-danger btn-md remove_add_bt mt-4 @if($key > 0) show_r_btn @endif" style="">Remove </button>
              </div>
            </div>
            @endforeach
            <button style="position:absolute; bottom:0px; left:15px;" type="button" class="del-btnAdd btn btn-success btn-md">Add +</button>
          </div>
        </div>

        <div class="form-group row">
          {{ html()->label('Credit Limit in PLN')->class('col-md-2 form-control-label')->for('credit_limit') }}
          <div class="col-md-2">
            <input type="number" name="credit_limit" value="{{$buyer->credit_limit}}" data-decimals="2" min="0" max="25000" step="10"/>
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label('Transportation Cost')->class('col-md-2 form-control-label')->for('credit_limit') }}
          <div class="col-md-2">
            <input type="number" name="transportation" value="{{$buyer->transportation}}" data-decimals="0" min="0" step="1"/>
          </div><!--col-->
        </div><!--form-group-->






<hr class="mb-4">

        <div class="form-group row">
          {{ html()->label('<strong>Customer Prefs ?</strong>')->class('col-md-2 form-control-label')->for('product_prefs') }}
          <div class="col-md-10">
            <div class="checkbox d-flex align-items-center">
              {{ html()->label(
                html()->checkbox('product_prefs', in_array($buyer->product_prefs, array('1')), 1)
                ->class('switch-input')
                ->id('product_prefs')
                . '<span class="switch-slider" data-checked="On" data-unchecked="Off"></span>')
                ->class('switch switch-label switch-pill switch-success mr-2')
                ->for('product_prefs') }}
              </div>
            </div>
          </div>

          <div id="product_prefs_box">
            <hr class="mb-4">

            <div class="form-group row">
              {{ html()->label('Product')->class('col-md-2 form-control-label')->for('product') }}
              <div class="col-md-10">
                {{ html()->select('product')
                ->class('select2 form-control')
                ->options(products_list())
                ->value('34')
                ->attribute('maxlength', 191)
              }}
            </div>
          </div>

		 <div class="form-group row">
		  {{ html()->label('<strong>Variety 7% premium list</strong>')->class('col-md-12 form-control-label')->for('variety') }}
		  <div class="col-xl-10 col-lg-8">
			<label class="form-control-label">Variety</label>
      <select name="variety[]" id="variety" class="select2-multiple form-control" multiple>
          <option value="">Select Variety</option>
          @foreach(variety_list() as $key => $value)
            <option value="{{$key}}" @if(is_array(json_decode($buyer->variety,true)) && in_array($key, json_decode($buyer->variety,true)))selected @endif>{{$value}}</option>
          @endforeach
      </select>
    </div>
    <div class="col-xl-2 col-lg-4">
      <label class="form-control-label">Premium %</label>
      <input type="number" name="variety_premium" value="{{$buyer->variety_premium}}" data-decimals="0" min="-15" max="15" step="1"/>
    </div>
  </div>


<div class="row">
    <div class="col-md-4">
    <div class="form-group row">
      {{ html()->label('<strong>Soil</strong>')->class('col-md-12 form-control-label')->for('soil') }}
      <div class="col-md-12">
        <div class="row">
          @php

		  if(!empty($buyer->soil)){
			  $soil_json = json_decode($buyer->soil,true);
		  }else{
			 $soil_json =  head_default('soil');
		  }



          $soil_list_selected = $soil_json;


          $soil_list = soil_list();
          @endphp

          <div class="flex_item form-group">
            <div class="checkbox switch-box d-flex align-items-center">
              {{ html()->label(
                html()->checkbox('soil[]', in_array('Any', $soil_list_selected), 'Any')
                ->class('switch-input')
                ->id('any_soil')
                . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                ->class('switch switch-label switch-pill switch-primary mr-2')
                ->for('any_soil')
              }}
              {{ html()->label('Any')->for('any_soil')->class('flex-1') }}
            </div>
          </div>

          @foreach($soil_list as $soil)
          @php $soil_key = str_slug($soil, "_"); @endphp

          000: @php print_r($soil) @endphp
          <div class="flex_item form-group">
            <div class="checkbox switch-box d-flex align-items-center">
              {{ html()->label(
                html()->checkbox('soil['.$soil_key.']', in_array($key, array_keys($soil_list_selected)), $key)
                ->class('switch-input')
                ->id($soil_key)
                . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                ->class('switch switch-label switch-pill switch-primary mr-2')
                ->for($soil_key)
              }}
              <input type="number" name="soil[{{$soil_key}}]" value="{{$soil_json["$soil_key"] ?? 0}}" data-decimals="0" min="-15" max="15" step="1"/>
              {{ html()->label(ucwords($soil))->for($soil_key)->class('flex-1') }}
            </div>
          </div>

          @endforeach
        </div>
      </div>
     </div>
    </div>
    <div class="col-md-4"><div class="form-group row">
      {{ html()->label('<strong>Flesh Color</strong>')->class('col-md-12 form-control-label')->for('soil') }}
      <div class="col-md-12">
        <div class="row">
          @php
		  if(!empty($buyer->flesh_color)){
		   $flesh_color_json = json_decode($buyer->flesh_color,true);
		  }else{
			  $flesh_color_json =  head_default('flesh_color');
		  }

          $flesh_color_list_selected = $flesh_color_json;
          $flesh_color_list = color_list();
          @endphp
          @foreach($flesh_color_list as $key => $flesh_color)
          @php $flesh_color_key = str_slug($flesh_color, "_"); @endphp
          <div class="flex_item form-group">
            <div class="checkbox switch-box d-flex align-items-center">
              {{ html()->label(
                html()->checkbox('flesh_color['.$flesh_color_key.']', in_array($flesh_color, $flesh_color_list_selected), $flesh_color)
                ->class('switch-input')
                ->id(str_slug($flesh_color, '_'))
                . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                ->class('switch switch-label switch-pill switch-primary mr-2')
                ->for(str_slug($flesh_color, '_'))
              }}

              <input type="number" name="flesh_color[{{$flesh_color_key}}]" value="{{$flesh_color_json["$flesh_color_key"] ?? 0}}" data-decimals="0" min="-15" max="15" step="1"/>
              {{ html()->label(ucwords($flesh_color))->for($flesh_color_key)->class('flex-1') }}
            </div>
          </div>

          @endforeach
        </div>
      </div>
    </div>
  </div>
    <div class="col-md-4">
      <div class="form-group row">
        {{ html()->label('<strong>Packing</strong>')->class('col-md-12 form-control-label')->for('soil') }}
        <div class="col-md-12">
          <div class="row">
            @php
			if(!empty($buyer->packaging)){
		      $packaging_json = json_decode($buyer->packaging,true);
		    }else{
			  $packaging_json =  head_default('packaging');
		    }

            $packaging_list_selected = $packaging_json;
            $packaging_list = packaging_list();

            @endphp
            @foreach($packaging_list as $key => $packaging)
            @php $packaging_key = str_slug($packaging, "_"); @endphp
            <div class="flex_item form-group">
              <div class="checkbox switch-box d-flex align-items-center">
                {{ html()->label(
                  html()->checkbox('packaging['.$packaging_key.']', in_array($packaging, array_values($packaging_list_selected)), $packaging)
                  ->class('switch-input')
                  ->id($packaging_key)
                  . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                  ->class('switch switch-label switch-pill switch-primary mr-2')
                  ->for($packaging_key)
                }}

                <input type="number" name="packaging[{{$packaging_key}}]" value="{{$packaging_json["$packaging_key"] ?? 0}}" data-decimals="0" min="-15" max="15" step="1"/>
                {{ html()->label(ucwords($packaging))->for($packaging_key)->class('flex-1') }}
              </div>
            </div>

            @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group row">
        {{ html()->label('<strong>Purpose</strong>')->class('col-md-12 form-control-label')->for('purposes') }}
        <div class="col-md-12">
          <div class="row">
            @php
			if(!empty($buyer->purposes)){
		       $purposes_json = json_decode($buyer->purposes,true);
		    }else{
			  $purposes_json =  head_default('purposes');
		    }

            $purposes_list_selected = $purposes_json;
            $purposes_list = purpose_list();
            @endphp
            @foreach($purposes_list as $key => $purpose)
            @php $purpose_key = str_slug($purpose, "_"); @endphp
            <div class="flex_item form-group">
              <div class="checkbox switch-box d-flex align-items-center">
                {{ html()->label(
                  html()->checkbox('purposes['.$purpose_key.']', in_array($purpose, $purposes_list_selected), $purpose)
                  ->class('switch-input')
                  ->id($purpose_key)
                  . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                  ->class('switch switch-label switch-pill switch-primary mr-2')
                  ->for($purpose_key)
                }}

                <input type="number" name="purposes[{{$purpose_key}}]" value="{{$purposes_json["$purpose_key"] ?? 0}}" data-decimals="0" min="-10" max="10" step="1"/>
                {{ html()->label(ucwords($purpose))->for($purpose_key)->class('flex-1') }}
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
</div>

















<div class="form-group row">
  {{ html()->label('<strong>Defects</strong>')->class('col-md-12 form-control-label')->for('defects') }}
  <div class="col-md-12">
    <div class="row">
      @php
	  if(!empty($buyer->defects)){
		      $defects_json = json_decode($buyer->defects,true);
	  }else{
			  $defects_json =  head_default('defects');
	  }

      $defects_list_selected = $defects_json;
      $defects_list = defects_list();
      @endphp
      @foreach($defects_list as $key => $defect)
      @php $defect_key = str_slug($defect, "_"); @endphp
      <div class="flex_item form-group">
        <div class="checkbox switch-box d-flex align-items-center">
          {{ html()->label(
            html()->checkbox('defects['.$defect_key.']', in_array($defect, $defects_list_selected), $defect)
            ->class('switch-input')
            ->id($defect_key)
            . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
            ->class('switch switch-label switch-pill switch-primary mr-2')
            ->for($defect_key)
          }}
          {{ html()->label(ucwords($defect))->for($defect_key)->class('flex-1') }}
          <input type="number" name="defects[{{$defect_key}}]" value="{{$defects_json["$defect_key"] ?? 0}}" data-decimals="0" min="-10" max="10" step="1"/>
        </div>
      </div>
      @endforeach
    </div>
  </div>

</div>


<div class="form-group row">
  {{ html()->label('<strong>Transportation Cost Estimate (%)</strong>')->class('col-md-2 form-control-label')->for('transportation') }}
  <div class="col-xl-2 col-md-4">
    <input type="number" name="transportation_cost_estimate" value="{{$buyer->transportation_cost_estimate}}" data-decimals="2" min="0" max="100" step="0.1"/>
  </div><!--col-->
</div><!--form-group-->

{{-- <div class="form-group row">
  {{ html()->label('<strong>Disc/Upsc (%)</strong>')->class('col-md-2 form-control-label')->for('transportation') }}
  <div class="col-xl-2 col-md-4">
    <input type="number" name="disc_upsc" value="{{$buyer->disc_upsc}}" data-decimals="2" min="0" max="100" step="0.1"/>
  </div><!--col-->
</div> --}}<!--form-group-->
@php
$sizerange = json_decode($buyer->size_range);
@endphp

<div class="form-group row">
  {{ html()->label('Size ranges')->class('col-md-12 form-control-label')->for('size_ranges') }}
  <div id="size_ranges" class="col-md-12" style="padding-bottom:40px; position:relative">
   @foreach($sizerange as $key=>$val)
    <div class="r-group form-group row">
      <div class="col-md-2">
        <label class="form-control-label">&nbsp;</label>
        <label class="form-control-label d-block">Preference #{{$key+1}}</label>
      </div>
      <div class="col-md-2">
        <label class="form-control-label">Min</label>
        <input class="form-control" type="text" value="{{ isset($val->from)? $val->from : '' }}" placeholder="From" name="size_range[0][from]" id="size_range_0_from" data-pattern-name="size_range[++][from]" data-pattern-id="size_range_++_from" />
      </div>
      <div class="col-md-2">
        <label class="form-control-label">Max</label>
        <input class="form-control" type="text" value="{{ isset($val->to)? $val->to : '' }}" placeholder="to" name="size_range[0][to]" id="size_range_0_to" data-pattern-name="size_range[++][to]" data-pattern-id="size_range_++_to" />
      </div>
      <div class="col-md-2">
        <label class="form-control-label">Premium</label>
        <input class="form-control" type="number" value="{{ isset($val->premium)? $val->premium : '' }}" name="size_range[0][premium]" id="size_range_0_premium" data-pattern-name="size_range[++][premium]" data-pattern-id="size_range_++_premium" value="0" data-decimals="0" min="-10" max="10" step="1"/>
      </div>
      <div class="col-md-2">
        <label class="form-control-label d-block">&nbsp;</label>
        <button type="button" class="r-btnRemove btn btn-danger btn-md">Remove -</button>
      </div>
    </div>
    @endforeach
    <button style="position:absolute; bottom:0px; left:15px;" type="button" class="r-btnAdd btn btn-success btn-md">Add +</button>
  </div>
</div>












          </div><!--col-->





    <div class="form-group row">
      {{ html()->label('Buyer Status')->class('col-md-2 form-control-label')->for('status') }}
      <div class="col-md-10">
        <div class="checkbox d-flex align-items-center">
          {{ html()->label(
            html()->checkbox('status', '0', '0')
            ->class('switch-input')
            ->id('status')
            . '<span class="switch-slider" data-checked="Ban" data-unchecked="act"></span>')
            ->class('switch switch-label switch-pill switch-danger mr-2')
            ->for('status') }}
          </div>
        </div>
      </div>


  </div><!--col-->
</div><!--row-->
</div><!--card-body-->

<div class="card-footer clearfix">
  <div class="row">
    <div class="col">
      {{ form_cancel(route('admin.buyers.index'), __('buttons.general.cancel')) }}
    </div><!--col-->

    <div class="col text-right">
      {{ form_submit(__('buttons.general.crud.update')) }}
    </div><!--col-->
  </div><!--row-->
</div><!--card-footer-->
</div><!--card-->
{{ html()->form()->close() }}
@endsection

@push('after-scripts')
<script>
/* $('#size_ranges').repeater({
  btnAddClass: 'r-btnAdd',
  btnRemoveClass: 'r-btnRemove',
  groupClass: 'r-group',
  minItems: 1,
  maxItems: 0,
  startingIndex: 0,
  showMinItemsOnLoad: true,
  reindexOnDelete: true,
  repeatMode: 'append',
  animation: null,
  animationSpeed: 400,
  animationEasing: 'swing',
  clearValues: true
});

$('#delivery_address').repeater({
  btnAddClass: 'del-btnAdd',
  btnRemoveClass: 'del-btnRemove',
  groupClass: 'del-group',
  minItems: 1,
  maxItems: 0,
  startingIndex: 0,
  showMinItemsOnLoad: true,
  reindexOnDelete: true,
  repeatMode: 'append',
  animation: null,
  animationSpeed: 400,
  animationEasing: 'swing',
  clearValues: false,
  beforeAdd: function($doppleganger) {
    return $doppleganger;
  },
  afterAdd: function($elem) {
    //$elem.val() = '';
  },
  beforeDelete: function($elem) {},
  afterDelete: function() {}
});

$(function() {
  delivery_same();
  product_prefs();
  changeRequered();
  Purpose_Check();
});

$('.switch').click(function() {
  delivery_same();
  product_prefs();
  //var id = $(this).children('input').attr('id');
  switchPremium($(this).children('input').attr('id'));
});

$("#dry_matter_content_to, #soil, #size_range_0_to").on('keydown keyup keypress blur', function(){
  Purpose_Check();
});

$('#country').on('select2:select', function (e) {
  changeRequered();
});
function changeRequered(){
  var val = $('#country').val();
  if(val === 'PL'){
    //$('#city').prop('required',true);
    $('#city').parent('.form-group').find('span').show();
    //$('#postalcode').prop('required',true);
    $('#postalcode').parent('.form-group').find('span').show();
    $('#phone').prop('required',true);
    $('#phone').parent('.form-group').find('span').show();
    $('#email').prop('required',false);
    $('#email').parent('.form-group').find('span').hide();
    $('#username').prop('required',true);
    $('#username').parent('.form-group').parent().parent('.form-group').find('span').show();
  }else{
    $('#city').prop('required',false);
    $('#city').parent('.form-group').find('span').hide();
    $('#postalcode').prop('required',false);
    $('#postalcode').parent('.form-group').find('span').hide();
    $('#phone').prop('required',false);
    $('#phone').parent('.form-group').find('span').hide();
    $('#email').prop('required',false);
    $('#email').parent('.form-group').find('span').show();
    $('#username').prop('required',false);
    $('#username').parent('.form-group').parent().parent('.form-group').find('span').hide();

    /*$( ".form-group" ).each(function() {
    $( this ).find( "input" ).prop('required',false);
    $( this ).find( "span" ).hide();
    $('#email').prop('required',true);
    $('#email').parent('.form-group').find('span').show();
  });
}
}

function delivery_same(){
  var checkbox = $('input#delivery_same');
  if ($(checkbox).prop('checked')) {
    $("#delivery_address").parent().hide();
  } else {
    $("#delivery_address").parent().show();
  }
}

function product_prefs(){
  var checkbox = $('input#product_prefs');
  if ($(checkbox).prop('checked')) {
    $("#product_prefs_box").show();
  } else {
    $("#product_prefs_box").hide();
  }
}

function Purpose_Check(){
  var dmc = $('#dry_matter_content_to').val();
  var soil = $('#soil').val();
  var size1 = $('#size_range_0_to').val();
  var size2 = $('#size_range_1_to').val();
  var size3 = $('#size_range_2_to').val();
  var size4 = $('#size_range_3_to').val();
  var size = Math.max(size1, size2, size3, size4);

  if(soil == '28' && size1 > 65){
    $('#Export').prop('checked', true).change();
  }else{
    $('#Export').prop('checked', false).change();
  }

  if(dmc > 22){
    $('#Peeling').prop('checked', false).change();
  }else{
    $('#Peeling').prop('checked', true).change();
  }
  console.log('Val Changed! '+size1);
}


$( ".checkbox.switch-box .switch input" ).each(function( index,element ) {
  switchPremium(element.id);
  /*if ($('input#'+element.id).prop('checked')) {
  $('input#'+element.id).parent().parent().find('input[type=number]').prop('disabled', false);
  console.log( element.value + " Checked" );
} else {
$('input#'+element.id).parent().parent().find('input[type=number]').prop('disabled', true);
console.log( element.value + " Not Checked" );
}
});

function switchPremium(id){
  if ($('input#'+id).prop('checked')) {
    $('input#'+id).parent().parent().find('input[type=number]').prop('disabled', false);
  } else {
    $('input#'+id).parent().parent().find('input[type=number]').prop('disabled', true);
  }
} */
$('#size_ranges').repeater({
  btnAddClass: 'r-btnAdd',
  btnRemoveClass: 'r-btnRemove',
  groupClass: 'r-group',
  minItems: 1,
  maxItems: 0,
  startingIndex: 0,
  showMinItemsOnLoad: true,
  reindexOnDelete: true,
  repeatMode: 'append',
  animation: null,
  animationSpeed: 400,
  animationEasing: 'swing',
  clearValues: true
});

$('#delivery_address').repeater({
  btnAddClass: 'del-btnAdd',
  btnRemoveClass: 'del-btnRemove',
  groupClass: 'del-group',
  minItems: 1,
  maxItems: 0,
  startingIndex: 0,
  showMinItemsOnLoad: true,
  reindexOnDelete: true,
  repeatMode: 'append',
  animation: null,
  animationSpeed: 400,
  animationEasing: 'swing',
  clearValues: false,
  beforeAdd: function($doppleganger) {
    return $doppleganger;
  },
  afterAdd: function($elem) {
    //$elem.val() = '';
  },
  beforeDelete: function($elem) {},
  afterDelete: function() {}
});

$(function() {
  delivery_same();
  product_prefs();
  changeRequered();
  Purpose_Check();
  select_all_soil();
});

$('.switch').click(function() {
  delivery_same();
  product_prefs();
  select_all_soil();
  //var id = $(this).children('input').attr('id');
  switchPremium($(this).children('input').attr('id'));
});

$("#dry_matter_content_to, #soil, #size_range_0_to").on('keydown keyup keypress blur', function(){
  Purpose_Check();
});

$('#country').on('select2:select', function (e) {
  changeRequered();
});
function changeRequered(){
  var val = $('#country').val();
  if(val === 'PL'){
    //$('#city').prop('required',true);
    $('#city').parent('.form-group').find('span').show();
    //$('#postalcode').prop('required',true);
    $('#postalcode').parent('.form-group').find('span').show();
    $('#phone').prop('required',true);
    $('#phone').parent('.form-group').find('span').show();
    $('#email').prop('required',false);
    $('#email').parent('.form-group').find('span').hide();
    $('#username').prop('required',true);
    $('#username').parent('.form-group').parent().parent('.form-group').find('span').show();
  }else{
    $('#city').prop('required',false);
    $('#city').parent('.form-group').find('span').hide();
    $('#postalcode').prop('required',false);
    $('#postalcode').parent('.form-group').find('span').hide();
    $('#phone').prop('required',false);
    $('#phone').parent('.form-group').find('span').hide();
    $('#email').prop('required',false);
    $('#email').parent('.form-group').find('span').show();
    $('#username').prop('required',false);
    $('#username').parent('.form-group').parent().parent('.form-group').find('span').hide();

    /*$( ".form-group" ).each(function() {
    $( this ).find( "input" ).prop('required',false);
    $( this ).find( "span" ).hide();
    $('#email').prop('required',true);
    $('#email').parent('.form-group').find('span').show();
  });*/
}
}

function delivery_same(){
  var checkbox = $('input#delivery_same');
  if ($(checkbox).prop('checked')) {
    $("#delivery_address").parent().hide();
  } else {
    $("#delivery_address").parent().show();
  }
}

function select_all_soil(){
  var checkbox = $('input#any_soil');
  if($(checkbox).prop('checked')) {
    @foreach(soil_list() as $key => $soil)
    $('#{{ str_slug($soil, '_')}}').prop('checked', true).change();
    @endforeach
  }
}

function product_prefs(){
  var checkbox = $('input#product_prefs');
  if ($(checkbox).prop('checked')) {
    $("#product_prefs_box").show();
  } else {
    $("#product_prefs_box").hide();
  }
}

function Purpose_Check(){
  var dmc = $('#dry_matter_content_to').val();
  var soil = $('#soil').val();
  var size1 = $('#size_range_0_to').val();
  var size2 = $('#size_range_1_to').val();
  var size3 = $('#size_range_2_to').val();
  var size4 = $('#size_range_3_to').val();
  var size = Math.max(size1, size2, size3, size4);

  if(soil == '28' && size1 > 65){
    $('#Export').prop('checked', true).change();
  }else{
    $('#Export').prop('checked', false).change();
  }

  if(dmc > 22){
    $('#Peeling').prop('checked', false).change();
  }else{
    $('#Peeling').prop('checked', true).change();
  }
  console.log('Val Changed! '+size1);
}


$( ".checkbox.switch-box .switch input" ).each(function( index,element ) {
  switchPremium(element.id);
  /*if ($('input#'+element.id).prop('checked')) {
  $('input#'+element.id).parent().parent().find('input[type=number]').prop('disabled', false);
  console.log( element.value + " Checked" );
} else {
$('input#'+element.id).parent().parent().find('input[type=number]').prop('disabled', true);
console.log( element.value + " Not Checked" );
}*/
});

function switchPremium(id){
  if ($('input#'+id).prop('checked')) {
    $('input#'+id).parent().parent().find('input[type=number]').prop('disabled', false);
  } else {
    $('input#'+id).parent().parent().find('input[type=number]').prop('disabled', true);
  }
}




</script>
@endpush
