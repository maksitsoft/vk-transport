@extends('frontend.layouts.app-fullwidth')
@section('title', Settings()->site_name . ' | ' . __('navs.general.home'))
@section('classes', 'home has-slider')
@section('slider')
<div class="page-header mb-0 pb-0 pt-0" style="background-image: url('{{ asset('img/vegking-banner-bg.png')}}');min-height: 100px;">
	<input type="hidden" name="ip" id="ip" value="">
	<div class="overlay" style="background-color: transparent;opacity:1"></div>
	<div class="page-header-bg text-center" style="position: static;opacity: 1;min-height: 100px;">
		<img src="{{ asset('img/Head-veking-map-background.jpg').getAutoVersion('img/Head-veking-map-background.jpg')}}" style="max-width:100%">
	</div>
	<div class="VegkingBannerBtns overlay-btns">
		<div class="wrap-inner">
			<div class="row">
				<div class="farmer-box" onmouseover="hoverfarmer(this);" onmouseout="unhoverfarmer(this);" data-toggle="modal" data-target="#sellercontact_modal">
						<a class="btn home-btn mb-4" data-toggle="modal" data-target="#sellercontact_modal" href="javascript:;">@lang('inner-content.frontend.homepage-content.sell')</a>
						<img id="farmer_img" src="{{ asset('img/farmer-yellow.png').getAutoVersion('img/farmer-yellow.png')}}" style="max-width:100%">
				</div>
				
					<div class="logo-box">
						<?php if(app()->getLocale() != 'en'){ ?>
							<?php $image_name_sitewise = 'img/banner-middle-logo-'.app()->getLocale().'.png'; ?>
							<a href="{{ url('/?offer=0') }}"><img src="<?php echo asset($image_name_sitewise).getAutoVersion('img/banner-middle-logo.png') ?>" style="max-width:100%"></a>
						<?php }else{ ?>
							<?php $image_name_sitewise = 'img/banner-middle-logo.png'; ?>
							<a href="{{ url('/?offer=0') }}"><img src="<?php echo asset($image_name_sitewise).getAutoVersion('img/banner-middle-logo.png') ?>" style="max-width:100%"></a>
						<?php } ?>
					</div>
				

				<div class="market-box" onmouseover="hovermarket(this);" onmouseout="unhovermarket(this);" data-toggle="modal" data-target="#buyercontact_modal">
					<a class="btn home-btn mb-4" data-toggle="modal" data-target="#buyercontact_modal" href="javascript:;">@lang('inner-content.frontend.homepage-content.buy')</a>
						<img id="market_img" src="{{ asset('img/market-yellow.png').getAutoVersion('img/market-yellow.png')}}" style="max-width:100%">
				</div>
			</div>
			<div class="overlay-header-title d-none">
				<h1 class="display-4 text-white mb-3 text-center"><mark>@lang('inner-content.frontend.homepage-content.slider-title')</mark></h1>
			</div>

		</div>
	</div>
	<div class="container pb-3 butonsgrp-mobile" style="min-height:100px">
		<div class="text-center" style="width:100%">
			<div class="w-100 text-white">
				<h1 class="display-4 text-white mb-3">@lang('inner-content.frontend.homepage-content.slider-title')</h1>
				<div class="VegkingBannerBtns">
					<a class="btn home-btn mr-3 text-uppercase" data-toggle="modal" data-target="#buyercontact_modal" href="javascript:;" >@lang('inner-content.frontend.homepage-content.buy')</a>
					<a class="btn home-btn text-uppercase" data-toggle="modal" data-target="#sellercontact_modal" href="javascript:;">@lang('inner-content.frontend.homepage-content.sell')</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('content')
<section id="aboutus" class="about pt-5 pb-5">
	<div class="container">
		<div class="row pb-3">
			<div class="col-md-6">
				<div class="author-image mb-4">
					<!--<img src="{{asset('img/british-flag-1907933.jpg')}}" width="100%;" height="100%;">-->
					<div class="embed-responsive embed-responsive-16by9">
						<!--<iframe id="video_player"  class="embed-responsive-item" src="{{asset('img/Option3.mp4')}}" controls autoplay loop allowfullscreen></iframe>-->
						<video width="400" controls autoplay muted loop allowfullscreen onloadstart="this.volume=0.2">
	            @if(app()->getLocale() == 'pl')
	              <source src="{{asset('img/Option3.mp4')}}" type="video/mp4">
							@else
	              <source src="{{asset('img/VEG-KING-eng.mp4')}}" type="video/mp4">
	            @endif
						  Your browser does not support HTML5 video.
						</video>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				@lang('inner-content.frontend.homepage-content.section-1')
				<button class="btn btn-outline-success btn-lg text-uppercase" id="LearnMoreBtnn">@lang('inner-content.frontend.homepage-content.read-more')</button>
			</div>
			<div class="col-md-12" id="LearnMoredescription" style="display: none;">
				@lang('inner-content.frontend.homepage-content.read-more-content')
			</div>
		</div>
	</div>
</section>

<section class="services-import section-first">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <h4 class="ImportExportText">@lang('inner-content.frontend.section-short-inner.import')</h4>
            </div>
        </div>
    </div>
	<div class="container" id="import-link">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 section-short">
				<!--<h2>@lang('inner-content.frontend.section-short-inner.import')</h2>-->
				<div class="section-short-inner">
					<figure> <img src="{{ asset('img/offer.png') }}" alt="" width="93" height="94"> </figure>
					<h5>@lang('inner-content.frontend.section-short-inner.heading-col-1')</h5>
					<span class="line"><em></em></span>
					<div>
						<p>@lang('inner-content.frontend.section-short-inner.content-col-1')</p>
					</div>
					<a href="" class="btn"></a> </div>
					<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 section-short">
					<h2></h2>
					<div class="section-short-inner">
						<figure> <img src="{{ asset('img/import-2-yellow.png') }}" alt="" width="80" height="95"> </figure>
						<h5>@lang('inner-content.frontend.section-short-inner.heading-col-2')</h5>
						<span class="line"><em></em></span>
						<div>
							<p>@lang('inner-content.frontend.section-short-inner.content-col-2')</p>
						</div>
						<a href="" class="btn"></a> </div>
						<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 section-short">
						<h2></h2>
						<div class="section-short-inner">
							<figure> <img src="{{ asset('img/vegetablesyellow.png') }}" alt="" width="95" height="101"> </figure>
							<h5>@lang('inner-content.frontend.section-short-inner.heading-col-3')</h5>
							<span class="line"><em></em></span>
							<div>
								<p>@lang('inner-content.frontend.section-short-inner.content-col-3')</p>
							</div>
							<a href="" class="btn"></a> </div>
							<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
						</div>
					</div>
				</div>
			</section>

			<section class="services-export">
                <div class="container-fluid">
                   <div class="row">
                      <div class="col-md-12">
                        <h4 class="ImportExportText">@lang('inner-content.frontend.section-short-inner.export')</h4>
                      </div>
                   </div>
                </div>
				<div class="container" id="export-link">
					<div class="row"><div class="col-xs-12 col-sm-12 col-md-4 section-short">
						<!--<h2>@lang('inner-content.frontend.section-short-inner.export')</h2>-->
						<div class="section-short-inner">
							<figure>
								<img src="{{ asset('img/export-1.png') }}" alt="" width="92" height="94">
							</figure>
							<h5>@lang('inner-content.frontend.section-short-inner.heading-row-1')</h5>
							<span class="line"><em></em></span>
							<div>
								<p>@lang('inner-content.frontend.section-short-inner.content-row-1')</p>
							</div>
							<a href="" class="btn"></a>
						</div>
						<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
					</div><div class="col-xs-12 col-sm-12 col-md-4 section-short">
						<h2></h2>
						<div class="section-short-inner">
							<figure>
								<img src="{{ asset('img/fast-delivery-yellow.png') }}" alt="" width="94" height="96">
							</figure>
							<h5>@lang('inner-content.frontend.section-short-inner.heading-row-2')</h5>
							<span class="line"><em></em></span>
							<div>
								<p>@lang('inner-content.frontend.section-short-inner.content-row-2')</p>
							</div>
							<a href="" class="btn"></a>
						</div>
						<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
					</div><div class="col-xs-12 col-sm-12 col-md-4 section-short">
						<h2></h2>
						<div class="section-short-inner">
							<figure>
								<img src="{{ asset('img/export-3.png') }}" alt="" width="92" height="94">
							</figure>
							<h5>@lang('inner-content.frontend.section-short-inner.heading-row-3')</h5>
							<span class="line"><em></em></span>
							<div>
								<p>@lang('inner-content.frontend.section-short-inner.content-row-3')</p>
							</div>
							<a href="" class="btn"></a>
						</div>
						<div class="bg" style="display: block; left: -100%; top: 0px; transition: all 333ms ease 0s;"></div>
					</div></div>
				</div>
			</section>

			<section class="container-fluid NewsLetterSec">
				<div class="row">
					<div class="col-md-4">
						<div class="SignUpSec">
							<h2>@lang('inner-content.frontend.newlettersec.newsletter')</h2>
							<p>@lang('inner-content.frontend.newlettersec.newsletter-content')</p>
							<div class="EqualLabelFiled">
								{{ html()->form('POST', route('frontend.subscribe'))->id('subscribe_form')->open() }}
								<input type="email" id="subscriber_email" name="email" placeholder="E-MAIL" required>
								<button class="signupbtn" type="submit"><img src="<?php echo URL::to('/') ?>/img/envelopegreen.png"></button>
								{{ html()->form()->close() }}
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="row NewproductSecmain">
							<div class="col-md-6">
								<div class="NewproductSec">
									<h2>@lang('inner-content.frontend.newlettersec.poffers')</h2>
									<h6 class="newsleeterTextForDesktop">@lang('inner-content.frontend.newlettersec.beets')</h6>
									<p class="d-none">@lang('inner-content.frontend.newlettersec.beets-content')</p>
									<button class="Seeofferbtn" data-toggle="modal" data-target="#buyercontact_modal">@lang('inner-content.frontend.newlettersec.see-offer')</button>
                                    <h6 class="newsleeterTextForMobile">@lang('inner-content.frontend.newlettersec.beets')</h6>
								</div>
							</div>
							<div class="col-md-6">
								<div class="productimageSec" style="background-image:url('<?php echo URL::to('/') ?>/img/offer-potatoes.jpg');">
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="salesHomesec">
            <div class="container-fluid">
            <div class="row">
						<div class="col-md-12">
							<h1 class="ContactPageHeading ContactPageHeadingFirstt">@lang('inner-content.frontend.salessec.title-1')</h1>
						</div>
					</div>
            </div>
				<div class="container pb-5 pt-5">
					<div class="row">
						<div class="col-md-6">
							<div class="SinglePersonDetails gregbg">
								<div class="row">
									<div class="col-md-4">
										<img src="<?php echo URL::to('/') ?>/img/c-user-profile-green.png" class="img-fluid UserProfileimg">
									</div>
									<div class="col-md-8">
										<ul class="ProfileDetails">
											<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-user-profile-name.png" class="userprofileicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.name-1') </span></li>
											<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><a class="righttext" href="https://wa.me/@lang('inner-content.frontend.salessec.phone-1')" target="_blank">@lang('inner-content.frontend.salessec.phone-1')</a></li>
											<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><a class="righttext" href="mailto:@lang('inner-content.frontend.salessec.email-1')">@lang('inner-content.frontend.salessec.email-1')</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="SinglePersonDetails gregbg">

								<div class="row">
									<div class="col-md-4">
										<img src="<?php echo URL::to('/') ?>/img/c-user-profile-green.png" class="img-fluid UserProfileimg">
									</div>
									<div class="col-md-8">
										<ul class="ProfileDetails">
											<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-user-profile-name.png" class="userprofileicon"></span><span class="righttext">@lang('inner-content.frontend.salessec.name-2')</span></li>
											<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><a class="righttext" href="https://wa.me/@lang('inner-content.frontend.salessec.phone-2')" target="_blank">@lang('inner-content.frontend.salessec.phone-2')</a></li>
											<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><a class="righttext" href="mailto:@lang('inner-content.frontend.salessec.email-2')">@lang('inner-content.frontend.salessec.email-2')</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<div class="LocationaddressSec mb-3 pl-4 pr-4 pb-3" style="background-color: rgb(244, 244, 244);padding: 15px 0px 5px;border-radius: 10px;text-align: center;">
								@lang('inner-content.frontend.salessec.title-2'): <br>
								<img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon2"> &nbsp;<a href="mailto:team@vegking.eu" class="" style="color: #000000;">team@vegking.eu</a>
							</div>
						</div>
						<div class="col-md-4"></div>
					</div>
				</div>
			</section>

			<section class="ContactHomeSec">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="ContactPageHeading">@lang('inner-content.frontend.contactsec.heading')</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="LocationaddressSec mb-2">
								<ul>
									<li class="polandAlignText"><span class="righttext"><img src="<?php echo URL::to('/') ?>/img/c-polland-flag.png" class="LocationFlag"></span><span class="lefttext">@lang('inner-content.frontend.contactsec.country-1')</span></li>
								</ul>
								<p class="AddressText" style="min-height: 90px;">@lang('inner-content.frontend.contactsec.address-1')</p>
								<div class="SinglePersonDetails">
									<ul class="ProfileDetails">
										<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><a class="righttext" href="https://wa.me/@lang('inner-content.frontend.contactsec.phone-1')" target="_blank">@lang('inner-content.frontend.contactsec.phone-1')</a></li>
										<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><a class="righttext" href="mailto:@lang('inner-content.frontend.contactsec.email-1')">@lang('inner-content.frontend.contactsec.email-1')</a></li>
									</ul>
								</div>
							</div>

							<?php /*?><div class="row">
								<div class="col-md-6">
									<div class="LocationaddressSec mb-3 pl-4 pr-4 pb-3 text-white">
										@if(app()->getLocale() == 'en')
										<i class="fas fa-envelope-open-text"></i> Accounting email: <br>
										<a href="mailto:accounts@vegking.eu" class="text-white">accounts@vegking.eu</a>
										@elseif(app()->getLocale() == 'pl')
										<i class="fas fa-envelope-open-text"></i> Księgowość email: <br>
										<a href="mailto:accounts@vegking.eu" class="text-white">accounts@vegking.eu</a>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="LocationaddressSec mb-3 pl-4 pr-4 pb-3 text-white">
										@if(app()->getLocale() == 'en')
										<i class="fas fa-envelope-open-text"></i> @lang('inner-content.frontend.salessec.title-2'): <br>
										<a href="mailto:team@vegking.eu" class="text-white">team@vegking.eu</a>
										@elseif(app()->getLocale() == 'pl')
										<i class="fas fa-envelope-open-text"></i> @lang('inner-content.frontend.salessec.title-2'): <br>
										<a href="mailto:team@vegking.eu" class="text-white">team@vegking.eu</a>
										@endif

									</div>
								</div>
							</div><?php */?>

						</div>
						<div class="col-md-6">
							<div class="LocationaddressSec mb-2">
								<ul>
									<li><span class="righttext"><img src="<?php echo URL::to('/') ?>/img/c-us-flag.png" class="LocationFlag"></span><span class="lefttext">@lang('inner-content.frontend.contactsec.country-2')</span></li>
								</ul>
								<p class="AddressText">@lang('inner-content.frontend.contactsec.address-2')</p>
								<div class="SinglePersonDetails">
									<ul class="ProfileDetails">
										<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-whatsapcalling.png" class="whatsapcallingicon"></span><a class="righttext" href="https://wa.me/@lang('inner-content.frontend.contactsec.phone-2')" target="_blank">@lang('inner-content.frontend.contactsec.phone-2')</a></li>
										<li><span class="lefticon"><img src="<?php echo URL::to('/') ?>/img/c-email.png" class="emailicon"></span><a class="righttext" href="mailto:@lang('inner-content.frontend.contactsec.email-2')">@lang('inner-content.frontend.contactsec.email-2')</a></li>
									</ul>
								</div>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="footerbottomText">@lang('inner-content.frontend.contactsec.content')</p>
						</div>
					</div>
				</div>
			</section>

			@endsection

			@push('after-scripts')

			<script src="https://www.jqueryscript.net/demo/Detect-Browser-Device-OS/dist/jquery.device.detector.js"></script>

<script type="text/javascript">
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
$(document).ready(function(){
    offer_param = findGetParameter('offer');
    if(offer_param != null){
        offer_param = offer_param * 1000;
        setTimeout(function(){
            $("#offer_menu").click();
        }, offer_param);
        
    }
});
jQuery(document).on("click",".HeaderMenusSec ul li a, .footer-link a",function(event){
            var thishref =$(this).attr("href");
            var url = thishref.substr(thishref.indexOf("#"));
            if(url.length>1){
                event.preventDefault();

                     if (window.matchMedia('(min-width: 992px)').matches) {
                        $('html, body').animate({
                                  scrollTop: $(url).offset().top
                        }, 1000);
                     } if (window.matchMedia('(max-width: 991px)').matches) {
                        $('html, body').animate({
                                  scrollTop: $(url).offset().top-210
                        }, 1000);
                     }

            }
    });

 /* jQuery(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            jQuery("#main-menu").addClass("Fixedheader");
        }else {
            jQuery("#main-menu").removeClass("Fixedheader");
        }
    }); */

           
			$(".section-short").hover(function(){
				$(this).addClass('hovered');
				$(this).find('.bg').css({"left": "0", "top": "0"});
			}, function(){
				$(this).removeClass('hovered');
				$(this).find('.bg').css({"left": "100%", "top": "0"});
			});

			@if($user_id)
			@php $user_id = $user_id; @endphp
			@else
			@php $user_id = 0; @endphp
			@endif
			$( document ).ready(function() {
				var user_id = {{ $user_id }};
				$.ajax({
					url: "https://jsonip.com",
					type: 'get',
					cache: false,
					success: function(res){
						console.log(res.ip)	;
						$("#ip").val(res.ip);
						var ip = $("#ip").val();
						var instance = $.fn.deviceDetector;

						$.ajax({
							url: "{{ route('frontend.referrer') }}",
							type: 'get',
							cache: false,
							data:{'_token':$('meta[name="csrf-token"]').attr('content'), 'browser_name':instance.getBrowserName(),'os_name':instance.getOsName(),'os_version': instance.getOsVersion(),'ip':ip ,'user_id':user_id },
							success: function(result){

							}
						});
					}
				});
			});

			$(function(){
				$('#LearnMoreBtnn').click(function(){
					$('#LearnMoredescription').toggle("slow");
				});
				$('#more_cookies_btn').click(function(){
					$('#more_cookies').toggle("fast");
					if($('#more_cookies').is(":visible")){
						$('#more_cookies_btn').hide();
						$('#cookie_pre_text').removeClass('pre');
					}
				});
			});

       $('#subscribe_form').on('submit', function(event) {
         event.preventDefault();
         var formData = new FormData(this);
         $.ajax({
            url: this.action,
            method: "post",
            processData: false,
            contentType: false,
            data: formData,
         }).done(function(response){
            if(response.status == 'success'){
               $('#subscribe_form').parent().append('<div class="text-white">You have successfully subscribed!</div>');
							 $('#subscribe_form #subscriber_email').val('');
            }else if(response.status == 'updated'){
							 $('#subscribe_form').parent().append('<div class="text-white">You are already subscribed!</div>');
							 $('#subscribe_form #subscriber_email').val('');
						}
         }).fail(function(jqXHR, textStatus){
            alert('Some error occurred. Please try again.');
         }).always(function(){
            $("#buyercontact_form .btn.btn-success").removeAttr('disabled');
         });
      });

			function hoverfarmer(element) {
			  $('#farmer_img').attr('src', '{{ asset("img/farmer-green.png").getAutoVersion("img/farmer-green.png")}}');
			}
			function unhoverfarmer(element) {
			  $('#farmer_img').attr('src', '{{ asset("img/farmer-yellow.png").getAutoVersion("img/farmer-yellow.png")}}');
			}
			function hovermarket(element) {
			  $('#market_img').attr('src', '{{ asset("img/market-green.png").getAutoVersion("img/market-green.png")}}');
			}
			function unhovermarket(element) {
			  $('#market_img').attr('src', '{{ asset("img/market-yellow.png").getAutoVersion("img/market-yellow.png")}}');
			}
    </script>

		@endpush
