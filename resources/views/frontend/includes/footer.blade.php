<footer>
  <section class="footer pt-5 pb-2" id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h6 class="text-white has-small-font-size">{{ Settings()->site_name }}</h6>
          <hr>
          <p class="about">{!! nl2br(Settings()->footer_about) !!}</p>
        </div>
        <div class="col-md-4">
          <h6 class="text-white has-small-font-size">@lang('inner-content.frontend.footer.about-property')</h6>
          <hr>
          <ul class="has-small-font-size pl-0 list-unstyled">
            <li class="footer-link"><a href="{{ url('/') }}#aboutus"><i class="fa fa-angle-right pr-1"></i> @lang('inner-content.frontend.footer.about-us')</a></li>
            <li><a href="<?php echo url('privacy-policy'); ?>"><i class="fa fa-angle-right pr-1"></i> @lang('inner-content.frontend.footer.privacy-policy')</a></li>
            <li><a href="<?php echo url('terms-conditions'); ?>"><i class="fa fa-angle-right pr-1"></i> @lang('inner-content.frontend.footer.terms')</a></li>
            <!--li><a href="#"><i class="fa fa-angle-right pr-1"></i> @lang('inner-content.frontend.footer.disclaimer')</a></li-->
            <li><a href="{{url('contact')}}"><i class="fa fa-angle-right pr-1"></i> @lang('inner-content.frontend.footer.contact')</a></li>
          </ul>
        </div>
        <div class="col-md-4">
          <h6 class="text-white has-small-font-size">@lang('inner-content.frontend.footer.contact-info')</h6>
          <hr>
          <ul class="has-small-font-size pl-0 list-unstyled">
            <!--li><span class="text-white">{!! nl2br(Settings()->address) !!}</span></li-->
            <li><span class="text-white">@lang('inner-content.frontend.footer.fulladdress')</span></li>
            @if(app()->getLocale() == 'en')
              <li><a href="tel:@lang('inner-content.frontend.contactsec.phone-2')"><i class="fas fa-phone-square-alt"></i> @lang('inner-content.frontend.contactsec.phone-2')</a></li>
            @elseif(app()->getLocale() == 'pl')
              <li><a href="tel:@lang('inner-content.frontend.contactsec.phone-1')"><i class="fas fa-phone-square-alt"></i> @lang('inner-content.frontend.contactsec.phone-1')</a></li>
            @endif
            <li><a href="mailto:{{ Settings()->email }}"><i class="fas fa-envelope-open-text"></i> {{ Settings()->email }}</a></li>
          </ul>
        </div>
      </div>
      <hr>
    </div>
    <div class="copyright text-center text-white pb-3">
      <p class="m-0 copyright">@lang('inner-content.frontend.footer.copyright') © {{date('Y')}} <!--<a class="script-font" href="{{ url('/') }}">{{ env('APP_NAME')}}</a>-->.@lang('inner-content.frontend.footer.copyright-content').</p>
    </div>
  </section>
</footer>
