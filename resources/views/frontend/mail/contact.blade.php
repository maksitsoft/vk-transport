@extends('frontend.layouts.email')
@section('title', @trans('strings.emails.contact.subject'))
@section('content')
<p><strong>@trans('validation.attributes.frontend.name'):</strong> {{ $request->name }}</p>
<p><strong>@trans('validation.attributes.frontend.email'):</strong> {{ $request->email }}</p>
<p><strong>@trans('validation.attributes.frontend.phone'):</strong> {{ $request->phone ?? 'N/A' }}</p>
<p><strong>@trans('validation.attributes.frontend.message'):</strong> {{ $request->message }}</p>
@endsection
