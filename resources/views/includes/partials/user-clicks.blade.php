<script>
    @if(Auth::user())
        $("body button, body .btn, a").click(function( event ) {
         var data ={};
		 
		 var eventname = $('.card-title').text() == undefined ? null : $.trim($('.card-title').text());
		 data['name']= $(event.target).attr("name") == undefined ? null : $(event.target).attr("name");
         data['button']= $(event.target).text() == undefined ? null : $(event.target).text()+ '('+eventname+')';
		 data['data']= $(this).val() == undefined ? null : $(this).val();
         data['page']= "{{url()->current()}}";
         data["_token"]="{{ csrf_token() }}";
         ga('send', 'event', data.button, data.page, 'Click');
		 mixpanel.track(data.page+' : '+data.button);
        $.ajax({

            url: "{{ route('frontend.user.tracker') }}",
            method: 'post',
            data: data,
            success: function(response){
             // console.log(response);
        }})

        });


        $("body input").blur(function( event ) {
         var data ={};
		// alert($(this).val());
            data['name']= $(event.target).attr("name") == undefined ? null : $(event.target).attr("name");
            data['data']= $(this).val() == undefined ? null : $(this).val();
            data['page']= "{{url()->current()}}";
            data["_token"]="{{ csrf_token() }}";
           // console.log(data);
           // ga('send', 'event', data.name, data.page, data.data);
		   mixpanel.track(data.page+' : '+data.name+' : '+data.data);
            $.ajax({
                url: "{{ route('frontend.user.tracker') }}",
                method: 'post',
                data: data,
                success: function(response){
                //console.log(response);
            }})

        });
    @endif
</script>
