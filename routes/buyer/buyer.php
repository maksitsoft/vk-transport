<?php
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\OfferController;
use App\Http\Controllers\Backend\BuyerController;
use App\Http\Controllers\Backend\Auth\User\UserController;
// All route names are prefixed with 'buyer.'.
Route::redirect('/', '/buyer/dashboard', 301);
Route::any('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::any('get_quote', [DashboardController::class, 'getQuotes'])->name('get_quote');
Route::any('updateBuyerPref','BuyerController@updateBuyerPref')->name('updateBuyerPref');
Route::POST('get-product-ajax-multiple', [ProductController::class, 'getProductAjaxMultiple'])->name('trading.getproductmultiple');
Route::GET('user/{id}/edit', [BuyerController::class, 'editbuyer'])->name('user.edit');
Route::POST('user/{id}/edit','BuyerController@updatebuyer')->name('user.edit');