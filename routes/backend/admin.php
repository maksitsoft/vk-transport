<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\OfferController;
use App\Http\Controllers\Backend\OfferRequestController;
use App\Http\Controllers\Backend\SaleController;
use App\Http\Controllers\Backend\VehicleController;
use App\Http\Controllers\Backend\LoadController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\BuyerController;
use App\Http\Controllers\Backend\SellerController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\AppHeadController;
use App\Http\Controllers\Backend\AccountController;
use App\Http\Controllers\Backend\MatchsController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\Backend\OffersentController;
use App\Http\Controllers\Backend\ListtransportController;
use App\Http\Controllers\Backend\BuyerprefController;
use App\Http\Controllers\Backend\ProductspecsController;
use App\Http\Controllers\Backend\ProductspecvaluesController;
use App\Http\Controllers\Backend\WarehouseController;
use App\Http\Controllers\Backend\ReferrerController;
use App\Http\Controllers\Backend\UserIpsController;
use App\Http\Controllers\Backend\TransportlistController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);

Route::get('clear-cache', [SettingController::class, 'clearCache'])->name('clear.cache');
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('translations', [LanguageController::class, 'index'])->name('translations');
Route::resource('currencyrates','CurrencyRateController');

// buyer import
Route::get('import-buyer', [BuyerController::class, 'import_buyer'])->name('import_buyer');
Route::get('buyer_import_scrape', [BuyerController::class, 'buyer_import_scrape'])->name('buyer_import_scrape');
Route::post('import_parse', [BuyerController::class, 'parseImport'])->name('import_parse');

// seller import
Route::get('import-seller', [SellerController::class, 'import_seller'])->name('import_seller');
Route::get('seller_import_scrape', [SellerController::class, 'seller_import_scrape'])->name('seller_import_scrape');
Route::post('import_seller_parse', [SellerController::class, 'parseImport'])->name('import_seller_parse');

Route::get('languages/loadLanguageFile', [LanguageController::class, 'loadLanguageFile'])->name('languages.loadLanguageFile');

Route::POST('languages/saveLanguageTrans', [LanguageController::class, 'saveLanguageTrans'])->name('languages.saveLanguageTrans');
Route::DELETE('languages/deleteLanguageLine', [LanguageController::class, 'deleteLanguageLine'])->name('languages.deleteLanguageLine');
Route::GET('languages/getLanguageLine', [LanguageController::class, 'getLanguageLine'])->name('languages.getLanguageLine');

Route::get('maching-requests-ajax/{offer}', [OfferController::class, 'matchingRequests'])->name('stock.matchings');
Route::POST('get_city_list/{country}', [OfferController::class, 'get_city_list'])->name('stock.get_city_list');
Route::POST('get_postal_code/{city}', [OfferController::class, 'get_postal_code'])->name('stock.get_postal_code');
Route::POST('maching-requests-apply', [OfferController::class, 'applyMatchingRequest'])->name('stock.matching.apply');
Route::POST('sellers/resend-invite/{seller_id}', [SellerController::class, 'resendInvite'])->name('seller.resend.invite');
Route::POST('matches-notify/{order}/{stock}', [MatchsController::class, 'sendNotification'])->name('matches.notify');
Route::POST('matches-makesale/{order}/{stock}', [MatchsController::class, 'makeSale'])->name('matches.makesale');
Route::any('matches-stock-api/{stock}', [MatchsController::class, 'matchStockAPI'])->name('matches.stock.api');
Route::any('matches-order-api/{order}', [MatchsController::class, 'matchOrderAPI'])->name('matches.order.api');
Route::any('check-matches-for-buyerprefs/{buyer}', [MatchsController::class, 'CheckMatchesForBuyerPrefId'])->name('matches.buyerprefs.api');
Route::any('check-matches-for-stock/{stock}', [MatchsController::class, 'CheckMatchesForStockId'])->name('matches.stockid.api');

Route::get('maching-offers-ajax/{offer_request}', [OfferRequestController::class, 'matchingOffers'])->name('requests.matchings');
Route::POST('maching-offers-apply', [OfferRequestController::class, 'applyMatchingOffer'])->name('requests.matching.apply');
Route::POST('get-product-ajax', [ProductController::class, 'getProductAjax'])->name('trading.getproduct');
Route::POST('get-product-stock-ajax', [ProductController::class, 'getProductStockAjax'])->name('trading.getproductforstock');
Route::POST('get-product-ajax-multiple', [ProductController::class, 'getProductAjaxMultiple'])->name('trading.getproductmulitple');
Route::POST('get-product-buyer-ajax', [ProductController::class, 'getProductForBuyerAjax'])->name('trading.getproductforbuyer');
Route::POST('get-vcolor-ajax', [OfferController::class, 'getvcolorAjax'])->name('trading.getcolor');
Route::POST('get-psample-ajax', [OfferController::class, 'getproductsampleAjax'])->name('trading.getproductsample');
Route::POST('get-buyerpaymentpref-ajax', [SaleController::class, 'getbuyerpaymentprefAjax'])->name('trading.getbuyerpaymentpref');
Route::POST('get-match-ajax', [SaleController::class, 'getmatchAjax'])->name('trading.getmatch');
Route::POST('get-buyer-ajax', [BuyerController::class, 'getBuyerAjax'])->name('trading.getbuyer');
Route::POST('get-stock-ajax', [SaleController::class, 'getStockAjax'])->name('trading.getstock');
Route::POST('get-apphead-values-ajax', [ProductspecsController::class, 'getAppHeadValues'])->name('trading.getappheadvalues');
Route::post('upload', 'UploadController@uploadSubmit');
Route::get('download', 'UploadController@downloadFile');
Route::POST('table-reorder-ajax', [AppHeadController::class, 'reorder'])->name('table.reorder');
Route::POST('transport/list', [ListtransportController::class, 'index'])->name('transport.update');
Route::resource('trading/stock','OfferController');
Route::resource('trading/stockv2','StockController');
Route::resource('trading/products','ProductController');

Route::resource('trading/offersent','OffersentController');
Route::resource('trading/buyerpref','BuyerprefController');
Route::resource('trading/productspecs','ProductspecsController');
Route::resource('trading/productspecvalues','ProductspecvaluesController');
Route::resource('trading/warehouse','WarehouseController');
Route::any('send_Invoice/{matchestemp}', [OffersentController::class, 'send_Invoice'])->name('matchestemp.send_Invoice');
Route::any('view_Invoice/{matchestemp}', [OffersentController::class, 'view_Invoice'])->name('matchestemp.view_Invoice');
Route::any('InvoiceSend/{match}', [MatchsController::class, 'InvoiceSend'])->name('matches.InvoiceSend');
Route::any('InvoiceView/{match}', [MatchsController::class, 'InvoiceView'])->name('matches.InvoiceView');
Route::any('InvoiceSendtoAll', [MatchsController::class, 'InvoiceSendtoAll'])->name('matches.InvoiceSendtoAll');
/*Route::resource('trading/requests','OfferRequestController', ['parameters' => ['requests' => 'offer_request']]);*/
Route::resource('trading/sales','SaleController');
Route::any('SaleInvoiceView/{sale}', [SaleController::class, 'InvoiceView'])->name('sales.SaleInvoiceView');
Route::resource('sellers','SellerController');
Route::resource('buyers','BuyerController');
Route::resource('referrer','ReferrerController');
Route::get('usertracking', [UserIpsController::class, 'usertracking'])->name('user-ips.usertracking');
Route::resource('user-ips','UserIpsController');
Route::any('exportExcel', [BuyerController::class, 'exportExcel'])->name('buyers.exportExcel');
Route::resource('appheads','AppHeadController');
Route::any('trading/matches/update-all','MatchsController@updateAll')->name('matches.updateAll');
Route::resource('trading/matches','MatchsController');
Route::resource('trading/buyerleads','BuyerleadController');

Route::resource('transport/vehicles','VehicleController');
Route::resource('transport/loads','LoadController');
Route::resource('transport/postalcodes','PostalCodeController');
Route::resource('transport/carrier','CarrierController');
Route::resource('transport/list','ListtransportController');

//Route::POST('getsales','ListtransportController@getsales')->name('list.getsales');
//Route::POST('getsales', [ListtransportController::class, 'getsales'])->name('list.getsales');

  
Route::resource('transport/transportlist','TransportlistController');
Route::get('transport/addtransport','TransportlistController@addtransport')->name('transportlist.addtransport');
Route::POST('transport/savetransload','TransportlistController@savetransportload')->name('transportlist.savetransportload');


Route::POST('gettransportloads', [TransportlistController::class, 'gettransportloads'])->name('transportlist.gettransportloads');
Route::POST('downloadzip', [TransportlistController::class, 'downloadzip'])->name('transportlist.downloadzip');
Route::GET('transport/previewload/{id}', [TransportlistController::class, 'previewpdf'])->name('transportlist.previewpdf');
Route::POST('transport/sendloadpdf', [TransportlistController::class, 'sendloadpdf'])->name('transportlist.sendloadpdf');
Route::POST('updatetransportloadsajax', [TransportlistController::class, 'updatetransportloadsajax'])->name('transportlist.updatetransportloadsajax');
//Route::get('transport/transportlist/edittransport','TransportlistController@edittransport')->name('transportlist.edittransport');

Route::resource('setting','SettingController');
Route::resource('messages','MessageController');
Route::resource('pages','PageController');
Route::get('accounts/buyer_templates',[AccountController::class, 'buyer_templates'])->name('accounts.buyer_templates');
Route::get('accounts/buyer_invoice_history',[AccountController::class, 'buyer_invoice_history'])->name('accounts.buyer_invoice_history');
Route::resource('accounts/purchaseorder','PurchaseOrderController');
Route::any('OrderInvoiceView/{order}', [AccountController::class, 'OrderInvoiceView'])->name('accounts.OrderInvoiceView');
Route::any('OrderInvoiceSend/{order}', [AccountController::class, 'OrderInvoiceSend'])->name('accounts.OrderInvoiceSend');
Route::resource('accounts/invoices','InvoiceController');
Route::GET('accounts/invoices/{invoice_id}/payment','InvoiceController@payment')->name('invoices.payment');
//Export Excel
Route::any('exports', [BuyerController::class,'exports'])->name('buyers.exports');
Route::any('exports', [SellerController::class,'exports'])->name('buyers.exports');
Route::any('offersentexports', [OffersentController::class,'offersentexports'])->name('offersent.offersentexports');
Route::any('stocksexports', [OfferController::class, 'stocksexports'])->name('stock.stocksexports');
Route::any('saleexports', [SaleController::class, 'saleexports'])->name('sales.saleexports');
Route::any('sales/saletotran', [SaleController::class, 'saletotran'])->name('sales.saletotran');
Route::any('matchesexports', [MatchsController::class, 'matchesexports'])->name('matches.matchesexports');
Route::any('buyerprefexports', [BuyerprefController::class, 'buyerprefexports'])->name('buyerpref.buyerprefexports');
Route::any('sellerexports', [SellerController::class, 'sellerexports'])->name('sellers.sellerexports');
Route::any('transportexports', [TransportlistController::class, 'transportexports'])->name('sellers.transportexports');
Route::any('transportpdf', [TransportlistController::class, 'downloadpdf'])->name('sellers.downloadpdf');
Route::any('generate-doc', [TransportlistController::class, 'generateDoc'])->name('sellers.downloaddoc');
Route::any('warehouseexports', [WarehouseController::class, 'warehouseexports'])->name('warehouse.warehouseexports');
Route::any('productsexports', [ProductController::class, 'productsexports'])->name('products.productsexports');
Route::any('productspecexports', [ProductspecsController::class, 'productspecexports'])->name('productspecs.productspecexports');
Route::any('productspecvaluesexports', [ProductspecvaluesController::class, 'productspecvaluesexports'])->name('productspecvalues.productspecvaluesexports');

//Import Excel
Route::post('stockimport', [OfferController::class, 'stockimport'])->name('stock.stockimport');

//Re-Order
Route::POST('product-spec-reorder-ajax', [ProductspecsController::class, 'reorder'])->name('productspec.reorder');

// Email Message email_templates
Route::resource('email-templates','EmailTemplateController');
