<?php
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\OfferController;
use App\Http\Controllers\Backend\SellerController;
use App\Http\Controllers\Backend\Auth\User\UserController;
// All route names are prefixed with 'seller.'.
Route::redirect('/', '/seller/dashboard', 301);
Route::any('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::POST('get-product-ajax', [ProductController::class, 'getProductAjax'])->name('trading.getproduct');
Route::resource('trading/stock','OfferController');
Route::resource('transport/transportlist','TransportlistController');
Route::POST('get_city_list/{country}', [OfferController::class, 'get_city_list'])->name('stock.get_city_list');
Route::POST('get_postal_code/{city}', [OfferController::class, 'get_postal_code'])->name('stock.get_postal_code');
Route::POST('get-vcolor-ajax', [OfferController::class, 'getvcolorAjax'])->name('trading.getcolor');
Route::get('maching-requests-ajax/{offer}', [OfferController::class, 'matchingRequests'])->name('stock.matchings');
Route::POST('maching-requests-apply', [OfferController::class, 'applyMatchingRequest'])->name('stock.matching.apply');
Route::resource('accounts/purchaseorder','PurchaseOrderController');
// Route::any('OrderInvoiceView/{order}', [AccountController::class, 'OrderInvoiceView'])->name('accounts.OrderInvoiceView');
// Route::any('OrderInvoiceSend/{order}', [AccountController::class, 'OrderInvoiceSend'])->name('accounts.OrderInvoiceSend');
Route::resource('accounts/invoices','InvoiceController');
Route::GET('accounts/invoices/{invoice_id}/payment','InvoiceController@payment')->name('invoices.payment');
Route::POST('get-psample-ajax', [OfferController::class, 'getproductsampleAjax'])->name('trading.getproductsample');
Route::GET('user/{id}/edit', [SellerController::class, 'editseller'])->name('user.edit');
//Route::POST('user/{id}/edit', [SellerController::class, 'updateseller'])->name('user.edit');
Route::POST('user/{id}/edit','SellerController@updateseller')->name('user.edit');
Route::resource('trading/stockv2','StockController');
Route::POST('get-product-stock-ajax', [ProductController::class, 'getProductStockAjax'])->name('trading.getproductforstock');