FROM php:7.3-fpm-alpine

RUN apk update

RUN apk add  \
wget curl \
php php-cli php-xml php-common php-mbstring \
php-curl php-openssl php-json php-phar php-dom 

RUN apk add --virtual .build-deps $PHPIZE_DEPS \ 
&& apk add libpng-dev imagemagick imagemagick-libs imagemagick-dev

RUN docker-php-ext-install pdo pdo_mysql bcmath 
RUN docker-php-ext-install gd

RUN pecl install xdebug \
&& docker-php-ext-enable xdebug

RUN pecl install imagick-3.4.4 \
&& docker-php-ext-enable imagick

RUN apk add --no-cache libzip-dev \
&& docker-php-ext-configure zip \
&& docker-php-ext-install zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

WORKDIR /var/www
