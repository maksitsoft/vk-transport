<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     */
    public function run()
    {
        Model::unguard();

        $this->truncateMultiple([
            'cache',
            'jobs',
            'sessions',
        ]);

        $this->call(AuthTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
       // $this->call(AppHeadTableSeeder::class);
        $this->call(PostalCodeTableSeeder::class);
        //$this->call(TrustlevelAppHeadTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);
        $this->call(LanguageLineTableSeeder::class);

        Model::reguard();
    }
}
