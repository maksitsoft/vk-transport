<?php

use Illuminate\Database\Seeder;

class EmailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\EmailTemplate::firstOrCreate(['title'=>'SELLER REGISTER'],[
          'title' => 'SELLER REGISTER',
          'subject' => 'Confirm email to sell your stock in 1 click',
          'email_content' => '<p><span style="font-weight: 400;">Hi [name],</span></p>
<p><span style="font-weight: 400;">Click here to confirm and sell your stocks [verification_link]</span></p>
<p><span style="font-weight: 400;">Sell your stock in 1 click</span></p>
<p><span style="font-weight: 400;">Call: [english_phone_number]</span></p>',
          'email_content_de' => '<p><span style="font-weight: 400;">Hallo [name],</span></p>
          <p><span style="font-weight: 400;">Klicken Sie hier, um Ihren Lager zu best&auml;tigen und zu verkaufen [verification_link]</span></p>
          <p><span style="font-weight: 400;">Verkaufen Sie Ihre Lagerbest&auml;nde mit 1 Klick</span></p>
          <p><span style="font-weight: 400;">Call: [english_phone_number]</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">Cześć [name],</span></p>
<p><span style="font-weight: 400;">Kliknij tu aby potwierdzić i przesłać sw&oacute;j produkt [verification_link]</span></p>
<p><span style="font-weight: 400;">Sprzedaj sw&oacute;j produkt 1 kliknięciem</span></p>
<p><span style="font-weight: 400;">Zadzwoń: 48 22 122 87 69</span></p>',
          'sms_content' => 'Hi [name],
Please check for a Vegking email to click the confirm link  and sell your stocks now.
The VegKing Team',
          'sms_content_de' => 'Hallo [name],
          Bitte sehen sie auf die E-Mail von Firma Vegking, dann klicken sie auf den Bestätigungslink um ihren Lager zu verkaufen
          Das VegKing Team',
          'sms_content_pl' => 'Cześć [name],
Proszę sprawdzić wiadomość e-mail dla wegetarian, aby kliknąć link potwierdzenia i sprzedać swoje akcje teraz.
Zespół VegKing',
          'shortcodes' => '[name], [first_name], [verification_link], [english_phone_number]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'VERIFY EMAIL'],[
          'title' => 'VERIFY EMAIL',
          'subject' => 'Please verify your email',
          'email_content' => '<p><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Hi [name],</span></p><p><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Click here to verify your email [verification_link]</span></p>',
          'email_content_de' => '<p>Hallo [name],</p>
          <p>Klicken Sie hier um Ihre E-Mail-Adresse zu &uuml;berpr&uuml;fen [verification_link]</p>',
          'email_content_pl' => '<p><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Hi [name],</span></p><p><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Click here to verify your email [verification_link]</span></p>',
          'sms_content' => 'Hi [name],
Click here to verify your whatsapp [verification_link]',
          'sms_content_de' => 'Hallo [name],
          Klicken Sie hier um Ihre WhatsApp zu überprüfen [verification_link]',
          'sms_content_pl' => 'Cześć [name],
Kliknij tutaj, aby zweryfikować WhatsApp [verification_link]',
            'shortcodes' => '[name], [first_name], [verification_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'SELLER REGISTER TO TEAM'],[
          'title' => 'SELLER REGISTER TO TEAM',
          'subject' => 'New Seller Registered!',
          'email_content' => '<p><span style="font-weight: 400;">TRADERS member [team_member_name],</span></p>
            <p><span style="font-weight: 400;">[name] just registered.</span></p>
            <p><span style="font-weight: 400;">Seller info:</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">View Seller: [view_seller_link]</span></p>',
          'email_content_de' => '<p><span style="font-weight: 400;">Hallo TRADERS Mitglied,</span></p>
            <p><span style="font-weight: 400;">[name] Gerade registriert.</span></p>
            <p><span style="font-weight: 400;">Verk&auml;uferinformationen:</span></p>
            <p><span style="font-weight: 400;">E-Mail-Adresse: [email]</span></p>
            <p><span style="font-weight: 400;">Telefonnummer: [phone]</span></p>
            <p><span style="font-weight: 400;">Verk&auml;ufer anzeigen: [view_seller_link]</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">TRADERS member [team_member_name],</span></p>
            <p><span style="font-weight: 400;">[name] just registered.</span></p>
            <p><span style="font-weight: 400;">Seller info:</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">View Seller: [view_seller_link]</span></p>',
          'sms_content' => 'TRADERS member [team_member_name],
[name] just registered.
Seller info:
Email: [email]
Phone: [phone]
View Seller: [view_seller_link]',
          'sms_content_de' => 'TRADERS Mitglied [team_member_name],
[name] Gerade registriert.
Verkäuferinformationen:
E-Mail-Adresse: [email]
Telefonnummer: [phone]
Verkäufer anzeigen: [view_seller_link]',
          'sms_content_pl' => 'TRADERS member [team_member_name],
[name] just registered.
Seller info:
Email: [email]
Phone: [phone]
View Seller: [view_seller_link]',
         'shortcodes' => '[team_member_name], [name], [email], [phone], [view_seller_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER REGISTER'],[
          'title' => 'BUYER REGISTER',
          'subject' => 'Confirm email to get the best quotes',
          'email_content' => '<p><span style="font-weight: 400;">Please click here to confirm your email and get the best price quotes [verification_link]</span></p>
            <p><span style="font-weight: 400;">or Call: [english_phone_number]</span></p>
            <p><span style="font-weight: 400;">Product: [product_name]</span></p>
            <p><span style="font-weight: 400;">Product Sub-type: [product_sub_type]</span></p>
            <p><span style="font-weight: 400;">Name: [name]</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">Preferred method of contact (you’ll get all you choose): [contact_preferred_method]</span></p>
            <p><span style="font-weight: 400;">Notes: [notes]</span></p>
            <p><span style="font-weight: 400;">The Transport Team</span></p>',
          'email_content_de' => '<p>Bitte Klicken Sie hier, um Ihre E-Mail-Adresse zu best&auml;tigen und die besten Preisangebote zu erhalten <span style="font-weight: 400;">[verification_link]</span></p>
            <p><span style="font-weight: 400;">oder anrufen: [english_phone_number]</span></p>
            <p><span style="font-weight: 400;">Produkt: [product_name]<br /></span></p>
            <p><span style="font-weight: 400;">Produkt-Untertyp: [product_sub_type]<br /></span></p>
            <p><span style="font-weight: 400;">Name: [name]<br /></span></p>
            <p><span style="font-weight: 400;">E-Mail-Adresse: [email]<br /></span></p>
            <p><span style="font-weight: 400;">Telefonnummer: [phone]</span></p>
            <p><span style="font-weight: 400;">Bevorzugte Kontaktmethode (Sie erhalten alles, was Sie w&uuml;nschen): [contact_preferred_method]</span></p>
            <p><span style="font-weight: 400;">Notizen: [notes]</span></p>
            <p><span style="font-weight: 400;">Das Transport-Team:</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">Proszę kliknąć tutaj, aby potwierdzić swój adres e-mail i uzyskać najlepsze oferty cen [verification_link]</span></p>
            <p><span style="font-weight: 400;">albo zadzwoń do nas: [english_phone_number]</span></p>
            <p><span style="font-weight: 400;">Produkt: [product_name]</span></p>
            <p><span style="font-weight: 400;">Podtyp produktu: [product_sub_type]</span></p>
            <p><span style="font-weight: 400;">Imię: [name]</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Preferowana metoda kontaktu (dostaniesz wszystko, co wybierzesz): [contact_preferred_method]</span></p>
            <p><span style="font-weight: 400;">Notatki: [notes]</span></p>
            <p><span style="font-weight: 400;">Zespół Transportu</span></p>',
          'sms_content' => 'Hi [name],
Please check for a Vegking email and get the best price quotes.
The VegKing Team',
          'sms_content_de' => 'Hallo [name],
Please check for a Vegking email and get the best price quotes.
Das VegKing Team',
          'sms_content_pl' => 'Cześć [name],
Sprawdź wiadomość e-mail dla wegetarian i uzyskaj najlepsze oferty cenowe.
Zespół VegKing',
          'shortcodes' => '[name], [email], [phone], [verification_link], [english_phone_number], [contact_preferred_method], [product_name], [product_sub_type], [notes]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER REGISTER TO TEAM'],[
          'title' => 'BUYER REGISTER TO TEAM',
          'subject' => 'New Buyer registered',
          'email_content' => '<p><span style="font-weight: 400;">Hi TRADERS member [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Buyer info:</span></p>
            <p><span style="font-weight: 400;">Product: [product_name]</span></p>
            <p><span style="font-weight: 400;">Product Sub-type: [product_sub_type]</span></p>
            <p><span style="font-weight: 400;">Name: [name]</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">View Buyer: [view_buyer_link]</span></p>',
          'email_content_de' => '<p>Hallo TRADERS Mitglied<span style="font-weight: 400;"> [team_member_name]</span>,</p>
            <p>K&auml;uferinformationen:</p>
            <p>Produkt: <span style="font-weight: 400;">[product_name]</span></p>
            <p>Produkt-Untertyp: <span style="font-weight: 400;">[product_sub_type]</span></p>
            <p>Name: <span style="font-weight: 400;">[name]</span></p>
            <p>E-Mail-Adresse: [email]</p>
            <p>Telefonnummer: [phone]</p>
            <p>K&auml;ufer anzeigen: <span style="font-weight: 400;">[view_buyer_link]</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">Cześć, członkiem TRADERS [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Informacje o kupującym:</span></p>
            <p><span style="font-weight: 400;">Produkt: [product_name]</span></p>            
            <p><span style="font-weight: 400;">Podtyp produktu: [product_sub_type]</span></p>
            <p><span style="font-weight: 400;">Imię: [name]</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Wyświetl kupującego: [view_buyer_link]</span></p>',
          'sms_content' => 'Hi TRADERS member [team_member_name],
Buyer info:
Product: [product_name]
Product Sub-type: [product_sub_type]
Name: [name]
Email: [email]
Phone: [phone]
View Buyer: [view_buyer_link]',
          'sms_content_de' => 'Hallo TRADERS Mitglied [team_member_name],
Käuferinformationen:
Produkt: [product_name]
Produkt-Untertyp: [product_sub_type]
Name: [name]
E-Mail-Adresse: [email]
Telefonnummer: [phone]
Käufer anzeigen: [view_buyer_link]',
          'sms_content_pl' => 'Cześć, członkiem TRADERS [team_member_name],
Informacje o potencjalnym nabywcy:
Produkt: [product_name]
Podtyp produktu: [product_sub_type]
Imię: [name]
E-mail: [email]
Telefon: [phone]
Wyświetl kupującego: [view_buyer_link]',
'shortcodes' => '[team_member_name], [name], [email], [phone], [product_name], [product_sub_type], [view_buyer_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER CREATED'],[
          'title' => 'BUYER CREATED',
          'subject' => 'You became a buyer now. Get the best quotes',
          'email_content' => '
            <p><span style="font-weight: 400;">Name: [name]</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Password: [password]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">The Transport Team</span></p>',
          'email_content_de' => '<p><span style="font-weight: 400;">Name: [name]</span></p>
            <p><span style="font-weight: 400;">E-Mail-Adresse: [email]</span></p>
            <p><span style="font-weight: 400;">Passwort: [password]</span></p>
            <p><span style="font-weight: 400;">Telefonnummer: [phone]</span></p>
            <p><span style="font-weight: 400;">Das Transport-Team</span></p>',
          'email_content_pl' => '
            <p><span style="font-weight: 400;">Imię: [name]</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">hasło: [password]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Zespół Transportu</span></p>',
          'sms_content' => 'Hi [name],
Email: [email]
Phone: [phone]
The VegKing Team',
          'sms_content_de' => 'Hallo [name],
E-Mail-Adresse: [email]
Telefonnummer: [phone]
Das VegKing Team',
          'sms_content_pl' => 'Cześć [name],
E-mail: [email]
Telefon: [phone]
Zespół VegKing',
          'shortcodes' => '[name], [email], [phone]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'SELLER CREATED'],[
          'title' => 'SELLER CREATED',
          'subject' => 'You became a seller now. Sell your stock in 1 click',
          'email_content' => '
            <p><span style="font-weight: 400;">Name: [name]</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Password: [password]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">Click here to upload stock: [upload_stock_link]</span></p>
            <p><span style="font-weight: 400;">The Transport Team</span></p>',
          'email_content_de' => '<p>Name: [name]</p>
            <p>E-Mail-Adresse: <span style="font-weight: 400;">[email]</span></p>
            <p><span style="font-weight: 400;">Passwort: [password]</span></p>
            <p><span style="font-weight: 400;">Telefonnummer: [phone]</span></p>
            <p><span style="font-weight: 400;">Das Transport-Team:</span></p>',
          'email_content_pl' => '
            <p><span style="font-weight: 400;">Imię: [name]</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">hasło: [password]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Kliknij tutaj, aby przesłać akcje: [upload_stock_link]</span></p>
            <p><span style="font-weight: 400;">Zespół Transportu</span></p>',
          'sms_content' => 'Hi [name],
Email: [email]
Password: [password]
Phone: [phone]
Click here to upload stock: [upload_stock_link]
The VegKing Team',
          'sms_content_de' => 'Hallo [name],
E-Mail-Adresse: [email]
Telefonnummer: [phone]
The VegKing Team',
          'sms_content_pl' => 'Cześć [name],
E-mail: [email]
hasło: [password]
Telefon: [phone]
Kliknij tutaj, aby przesłać akcje: [upload_stock_link]
Zespół VegKing',
          'shortcodes' => '[name], [email], [phone], [password], [upload_stock_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'IMPORT SELLER NOTIFICATION EMAIL'],[
        'title' => 'IMPORT SELLER NOTIFICATION EMAIL',
        'subject' => 'Import Seller Notification Email',
        'email_content' => '<p><span style="font-weight: 400;">User Name [username]</span></p>
          <p><span style="font-weight: 400;">First Name [first_name]</span></p>
          <p><span style="font-weight: 400;">Email [email]</span></p>
          <p><span style="font-weight: 400;">Phone [english_phone_number]</span></p>
          <p><span style="font-weight: 400;">Upload Link [upload_stock_link]</span></p>
          <p><span style="font-weight: 400;">Password [password]</span></p>
          <p><span style="font-weight: 400;">TRADERS member [team_member_name]</span></p>',
        'email_content_de' => '<p><span style="font-weight: 400;">Nutzername [username]</span></p>
          <p><span style="font-weight: 400;">Vorname [first_name]</span></p>
          <p><span style="font-weight: 400;">Telefonnummer [phone]</span></p>
          <p><span style="font-weight: 400;">Link hochladen [upload_stock_link]</span></p>
          <p><span style="font-weight: 400;">Passwort [password]</span></p>
          <p><span style="font-weight: 400;">TRADERS-Mitglied <span style="font-weight: 400;">[team_member_name]</span></p>',
        'email_content_pl' => '<p><span style="font-weight: 400;">User Name [username]</span></p>
          <p><span style="font-weight: 400;">First Name [first_name]</span></p>
          <p><span style="font-weight: 400;">Email [email]</span></p>
          <p><span style="font-weight: 400;">Phone [english_phone_number]</span></p>
          <p><span style="font-weight: 400;">Upload Link [upload_stock_link]</span></p>
          <p><span style="font-weight: 400;">Password [password]</span></p>
          <p><span style="font-weight: 400;">TRADERS member [team_member_name]</span></p>',
        'shortcodes' => '[username], [first_name], [email], [english_phone_number], [upload_stock_link], [password], [team_member_name]'
    ]);
    App\EmailTemplate::firstOrCreate(['title'=>'IMPORT BUYER NOTIFICATION EMAIL'],[
        'title' => 'IMPORT BUYER NOTIFICATION EMAIL',
        'subject' => 'Import Buyer Notification Email',
        'email_content' => '<p><span style="font-weight: 400;">User Name [username]</span></p>
          <p><span style="font-weight: 400;">First Name [first_name]</span></p>
          <p><span style="font-weight: 400;">Email [email]</span></p>
          <p><span style="font-weight: 400;">Phone [english_phone_number]</span></p>
          <p><span style="font-weight: 400;">Upload Link [upload_stock_link]</span></p>
          <p><span style="font-weight: 400;">Password [password]</span></p>
          <p><span style="font-weight: 400;">TRADERS member [team_member_name]</span></p>',
        'email_content_de' => '<p><span style="font-weight: 400;">Nutzername [username]</span></p>
          <p><span style="font-weight: 400;">Vorname [first_name]</span></p>
          <p><span style="font-weight: 400;">Telefonnummer [phone]</span></p>
          <p><span style="font-weight: 400;">Link hochladen [upload_stock_link]</span></p>
          <p><span style="font-weight: 400;">Passwort [password]</span></p>
          <p><span style="font-weight: 400;">TRADERS-Mitglied <span style="font-weight: 400;">[team_member_name]</span></p>',
        'email_content_pl' => '<p><span style="font-weight: 400;">User Name [username]</span></p>
          <p><span style="font-weight: 400;">First Name [first_name]</span></p>
          <p><span style="font-weight: 400;">Email [email]</span></p>
          <p><span style="font-weight: 400;">Phone [english_phone_number]</span></p>
          <p><span style="font-weight: 400;">Upload Link [upload_stock_link]</span></p>
          <p><span style="font-weight: 400;">Password [password]</span></p>
          <p><span style="font-weight: 400;">TRADERS member [team_member_name]</span></p>',
        'shortcodes' => '[username], [first_name], [email], [english_phone_number], [upload_stock_link], [password], [team_member_name]'
    ]);
      App\EmailTemplate::firstOrCreate(['title'=>'GET QUOTES'],[
          'title' => 'GET QUOTES',
          'subject' => 'A Buyer asked for a quote',
          'email_content' => '
            <p><span style="font-weight: 400;">Hi [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Buyer [name] asked for a quote.</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">Whatsapp : [phone]</span></p>',
          'email_content_de' => '<p>Hallo <span style="font-weight: 400;">[team_member_name],</span></p>
            <p>K&auml;ufer <span style="font-weight: 400;">[name]</span></p>
            <p>Telefonnummer<span style="font-weight: 400;">: [phone]</span></p>
            <p>WhatsApp<span style="font-weight: 400;">: [phone]</span></p>',
          'email_content_pl' => '
            <p><span style="font-weight: 400;">cześć [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Kupujący [name] poprosił o wycenę.</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Whatsapp: [phone]</span></p>',
          'sms_content' => 'Hi [team_member_name],
Buyer [name] asked for a quote.
Phone: [phone]
Whatsapp : [phone]
View Buyer: [view_buyer_link]
The VegKing Team',
          'sms_content_de' => 'Hi [team_member_name],
Buyer [name] asked for a quote.
Phone: [phone]
Whatsapp : [phone]
View Buyer: [view_buyer_link]
The VegKing Team',
          'sms_content_pl' => 'Cześć [name],
Kupujący [name] poprosił o wycenę.
Telefon: [phone]
Whatsapp: [phone]
Wyświetl kupującego: [view_buyer_link]
Zespół VegKing',
          'shortcodes' => '[team_member_name], [name], [phone], [view_buyer_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER SHORT REGISTER'],[
          'title' => 'BUYER SHORT REGISTER',
          'subject' => 'Confirm email to get the best quotes',
          'email_content' => '<p><span style="font-weight: 400;">Please click here to confirm your email and get the best price quotes [verification_link]</span></p>
            <p><span style="font-weight: 400;">or Call: [english_phone_number]</span></p>
            <p><span style="font-weight: 400;">Name: [name]</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">The Transport Team</span></p>',
          'email_content_de' => '<p>Bitte Klicken Sie hier, um Ihre E-Mail-Adresse zu best&auml;tigen und die besten Preisangebote zu erhalten <span style="font-weight: 400;">[verification_link]</span></p>
            <p><span style="font-weight: 400;">oder anrufen: [english_phone_number]</span></p>
            <p><span style="font-weight: 400;">Name: [name]<br /></span></p>
            <p><span style="font-weight: 400;">E-Mail-Adresse: [email]<br /></span></p>
            <p><span style="font-weight: 400;">Telefonnummer: [phone]</span></p>
            <p><span style="font-weight: 400;">Das Transport-Team:</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">Proszę kliknąć tutaj, aby potwierdzić swój adres e-mail i uzyskać najlepsze oferty cen [verification_link]</span></p>
            <p><span style="font-weight: 400;">albo zadzwoń do nas: [english_phone_number]</span></p>
            <p><span style="font-weight: 400;">Imię: [name]</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Zespół Transportu</span></p>',
          'sms_content' => 'Hi [name],
Please check for a Vegking email and get the best price quotes.
The VegKing Team',
          'sms_content_de' => 'Hallo [name],
Please check for a Vegking email and get the best price quotes.
Das VegKing Team',
          'sms_content_pl' => 'Cześć [name],
Sprawdź wiadomość e-mail dla wegetarian i uzyskaj najlepsze oferty cenowe.
Zespół VegKing',
          'shortcodes' => '[name], [email], [phone], [verification_link], [english_phone_number]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER SHORT REGISTER TO TEAM'],[
          'title' => 'BUYER SHORT REGISTER TO TEAM',
          'subject' => 'New Buyer short registered',
          'email_content' => '<p><span style="font-weight: 400;">Hi TRADERS member [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Buyer info:</span></p>
            <p><span style="font-weight: 400;">Name: [name]</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">View Buyer: [view_buyer_link]</span></p>',
          'email_content_de' => '<p>Hallo TRADERS Mitglied<span style="font-weight: 400;"> [team_member_name]</span>,</p>
            <p>K&auml;uferinformationen:</p>
            <p>Name: <span style="font-weight: 400;">[name]</span></p>
            <p>E-Mail-Adresse: [email]</p>
            <p>Telefonnummer: [phone]</p>
            <p>K&auml;ufer anzeigen: <span style="font-weight: 400;">[view_buyer_link]</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">Cześć, członkiem TRADERS [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Informacje o kupującym:</span></p>
            <p><span style="font-weight: 400;">Imię: [name]</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Wyświetl kupującego: [view_buyer_link]</span></p>',
          'sms_content' => 'Hi TRADERS member [team_member_name],
Buyer info:
Name: [name]
Email: [email]
Phone: [phone]
View Buyer: [view_buyer_link]',
          'sms_content_de' => 'Hallo TRADERS Mitglied [team_member_name],
Käuferinformationen:
Name: [name]
E-Mail-Adresse: [email]
Telefonnummer: [phone]
Käufer anzeigen: [view_buyer_link]',
          'sms_content_pl' => 'Cześć, członkiem TRADERS [team_member_name],
Informacje o potencjalnym nabywcy:
Imię: [name]
E-mail: [email]
Telefon: [phone]
Wyświetl kupującego: [view_buyer_link]',
'shortcodes' => '[team_member_name], [name], [email], [phone], [view_buyer_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER SHORT REGISTER STEP1'],[
          'title' => 'BUYER SHORT REGISTER STEP1',
          'subject' => 'Thanks for contacting us',
          'email_content' => '
            <p><span style="font-weight: 400;">Thank you for join to us. If you want to complete your registration please go to your profile and complete registration.</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">View Buyerlead: [view_buyerlead_link]</span></p>',
          'email_content_de' => '<p><span style="font-weight: 400;">Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Sehen Sie sich Buyerlead: [view_buyerlead_link]</span></p>',
          'email_content_pl' => '
            <p><span style="font-weight: 400;">Dziękujemy za dołącz do nas. Jeśli chcesz dokończyć rejestrację, przejdź do swojego profilu i dokończ rejestrację.</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Zobacz Buyerlead: [view_buyerlead_link]</span></p>',
          'sms_content' => 'Thank you for join to us. If you want to complete your registration please go to your profile and complete registration.
Email: [email]
View Buyerlead: [view_buyerlead_link]
The VegKing Team',
          'sms_content_de' => 'Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.
E-mail: [email]
Sehen Sie sich Buyerlead: [view_buyerlead_link]
The VegKing Team',
          'sms_content_pl' => 'Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.
E-mail: [email]
Zobacz Buyerlead: [view_buyerlead_link]
Zespół VegKing',
          'shortcodes' => '[email], [view_buyerlead_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER SHORT REGISTER STEP2'],[
          'title' => 'BUYER SHORT REGISTER STEP2',
          'subject' => 'Thanks for contacting us',
          'email_content' => '
            <p><span style="font-weight: 400;">Thank you for join to us. If you want to complete your registration please go to your profile and complete registration.</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>
            <p><span style="font-weight: 400;">View Buyerlead: [view_buyerlead_link]</span></p>',
          'email_content_de' => '<p><span style="font-weight: 400;">Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Sehen Sie sich Buyerlead: [view_buyerlead_link]</span></p>',
          'email_content_pl' => '
            <p><span style="font-weight: 400;">Dziękujemy za dołącz do nas. Jeśli chcesz dokończyć rejestrację, przejdź do swojego profilu i dokończ rejestrację.</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>
            <p><span style="font-weight: 400;">Zobacz Buyerlead: [view_buyerlead_link]</span></p>',
          'sms_content' => 'Thank you for join to us. If you want to complete your registration please go to your profile and complete registration.
Email: [email]
Phone: [phone]
View Buyerlead: [view_buyerlead_link]
The VegKing Team',
          'sms_content_de' => 'Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.
E-mail: [email]
Telefon: [phone]
Sehen Sie sich Buyerlead: [view_buyerlead_link]
The VegKing Team',
          'sms_content_pl' => 'Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.
E-mail: [email]
Telefon: [phone]
Zobacz Buyerlead: [view_buyerlead_link]
Zespół VegKing',
          'shortcodes' => '[email], [phone], [view_buyerlead_link]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER SHORT REGISTER STEP1 TO TEAM'],[
          'title' => 'BUYER SHORT REGISTER STEP1 TO TEAM',
          'subject' => 'New Buyer completed step1',
          'email_content' => '
            <p><span style="font-weight: 400;">Hi TRADERS member [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Buyer info:</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>',
          'email_content_de' => '
            <p><span style="font-weight: 400;">Hallo TRADERS-Mitglied [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Käufer info:</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">Cześć, członkiem TRADERS [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Informacje o kupującym:</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>',
          'sms_content' => 'Hi TRADERS member [team_member_name],
Buyer info:
Email: [email]
The VegKing Team',
          'sms_content_de' => 'Cześć, członkiem TRADERS [team_member_name],
Käufer info:
E-mail: [email]
The VegKing Team',
          'sms_content_pl' => 'Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.
Informacje o kupującym:
E-mail: [email]
Zespół VegKing',
          'shortcodes' => '[email], [team_member_name]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'BUYER SHORT REGISTER STEP2 TO TEAM'],[
          'title' => 'BUYER SHORT REGISTER STEP2 TO TEAM',
          'subject' => 'New Buyer completed step2',
          'email_content' => '
            <p><span style="font-weight: 400;">Hi TRADERS member [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Buyer info:</span></p>
            <p><span style="font-weight: 400;">Email: [email]</span></p>
            <p><span style="font-weight: 400;">Phone: [phone]</span></p>',
          'email_content_de' => '
            <p><span style="font-weight: 400;">Hallo TRADERS-Mitglied [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Käufer info:</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>',
          'email_content_pl' => '<p><span style="font-weight: 400;">Cześć, członkiem TRADERS [team_member_name],</span></p>
            <p><span style="font-weight: 400;">Informacje o kupującym:</span></p>
            <p><span style="font-weight: 400;">E-mail: [email]</span></p>
            <p><span style="font-weight: 400;">Telefon: [phone]</span></p>',
          'sms_content' => 'Hi TRADERS member [team_member_name],
Buyer info:
Email: [email]
Phone: [phone]
The VegKing Team',
          'sms_content_de' => 'Cześć, członkiem TRADERS [team_member_name],
Käufer info:
E-mail: [email]
Telefon: [phone]
The VegKing Team',
          'sms_content_pl' => 'Vielen Dank, dass Sie sich uns anschließen. Wenn Sie Ihre Registrierung abschließen möchten, gehen Sie zu Ihrem Profil und schließen Sie die Registrierung ab.
Informacje o kupującym:
E-mail: [email]
Telefon: [phone]
Zespół VegKing',
          'shortcodes' => '[email], [phone], [team_member_name]'
      ]);
      App\EmailTemplate::firstOrCreate(['title'=>'STOCK MATCHED'],[
          'title' => 'STOCK MATCHED',
          'subject' => 'Your preference product is waiting',
          'email_content' => '<p><span style="font-weight: 400;">Hi TRADERS member[team_member_name],</span></p>
            <p><span style="font-weight: 400;">Stock: #[stock_id] ([seller_username]) matched to Buyer: #[buyer_id] ([buyer_username]) of [product_name]</span></p>
            <p><span style="font-weight: 400;">Stock Price: [stock_price]</span></p>
            <p><span style="font-weight: 400;">Buyer Premiums: [buyer_total_prefs]</span></p>
            <p><span style="font-weight: 400;">P/Ton: [profit_per_ton]</span></p>
            <p><span style="font-weight: 400;">View All Matches: [view_matches_link]</span></p>',

          'email_content_de' => '<p><span style="font-weight: 400;">Hallo TRADERS Mitglied[team_member_name],</span></p>
            <p><span style="font-weight: 400;">Lager: #[stock_id] ([seller_username]) abgestimmt auf Käuferin: #[buyer_id] ([buyer_username]) von [product_name]</span></p>
            <p><span style="font-weight: 400;">Standard Preis: [stock_price]</span></p>
            <p><span style="font-weight: 400;">Käuferprämien: [buyer_total_prefs]</span></p>
            <p><span style="font-weight: 400;">P / Ton: [profit_per_ton]</span></p>
            <p><span style="font-weight: 400;">Alle Übereinstimmungen anzeigen: [view_matches_link]</span></p>',

          'email_content_pl' => '<p><span style="font-weight: 400;">cześć TRADERS członek[team_member_name],</span></p>
            <p><span style="font-weight: 400;">Zbiory: #[stock_id] ([seller_username]) dopasowane do Buyer: #[buyer_id] ([buyer_username]) z [product_name]</span></p>
            <p><span style="font-weight: 400;">Cena akcji: [stock_price]</span></p>
            <p><span style="font-weight: 400;">Premie kupującego: [buyer_total_prefs]</span></p>
            <p><span style="font-weight: 400;">P / tona: [profit_per_ton]</span></p>
            <p><span style="font-weight: 400;">Zobacz wszystkie mecze: [view_matches_link]</span></p>',
          'sms_content' => 'Hi TRADERS member [team_member_name],
*Stock: #[stock_id] * ([seller_username]) matched to *Buyer: #[buyer_id] * ([buyer_username]) of [product_name]
*Stock Price*: [stock_price]
*Buyer Premiums: [buyer_total_prefs]
*PTon: [profit_per_ton]
*View All Matches*: [view_matches_link]',
          'sms_content_de' => 'Hallo TRADERS Mitglied [team_member_name],
*Lager: #[stock_id] * ([seller_username]) abgestimmt auf *Käuferin: #[buyer_id] * ([buyer_username]) von [product_name]
*Standard Preis: [stock_price]
*Käuferprämien: [buyer_total_prefs]
*PTon: [profit_per_ton]
*Alle Übereinstimmungen anzeigen: [view_matches_link]',
          'sms_content_pl' => 'cześć TRADERS członek [team_member_name],
*Zbiory: #[stock_id] * ([seller_username]) dopasowane do *Buyer: #[buyer_id] * ([buyer_username]) z [product_name]
Cena akcji: [stock_price]
Premie kupującego: [buyer_total_prefs]
PTon: [profit_per_ton]
Zobacz wszystkie mecze: [view_matches_link]',
'shortcodes' => '[team_member_name], [stock_id], [seller_username], [buyer_id], [buyer_username], [product_name], [stock_price], [buyer_total_prefs], [profit_per_ton], [view_matches_link]'
      ]);
    }
}
