<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyerProductPrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyer_product_prefs', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->timestamps();
            $table->bigInteger('buyer_pref_id');
            $table->bigInteger('key');
            $table->string('value')->nullable();
            $table->bigInteger('premium')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyer_product_prefs');
    }
}
