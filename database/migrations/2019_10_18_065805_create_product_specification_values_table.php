<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSpecificationValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_specification_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id');
            $table->integer('product_specification_id');
            $table->integer('parent_id')->nullable();
            $table->string('value');
            $table->timestamps();
			$table->integer('premium')->nullable();
			$table->integer('volume')->nullable();
			$table->enum('default', array('0', '1'))->default('0');
			$table->enum('status', array('0', '1'))->default('1');
			$table->float('extra_supply_cost')->nullable();
			$table->float('extra_cost_to_buyer_factor')->nullable();
			$table->text('description')->nullable();
			$table->string('ec')->nullable();
			$table->string('ecbf')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_specification_values');
    }
}
