<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned()->index();
            $table->bigInteger('product_id')->unsigned()->index();
            $table->string('size_from')->nullable();
            $table->string('size_to')->nullable();
            $table->string('quantity')->nullable();
            $table->text('postalcode')->nullable();
            $table->text('street')->nullable();
			$table->text('country')->nullable();
            $table->float('price')->nullable();
            $table->timestamps();
			$table->integer('available_per_day')->nullable(); 
			$table->date('available_from_date')->nullable();
			$table->integer('pallets_available')->nullable();
			$table->text('image')->nullable();
			$table->enum('status', array('listed', 'instore', 'outstore'))->default('listed');
			$table->string('note')->nullable();
			$table->text('city')->nullable();
			$table->enum('stock_status', array('unavailable', 'available', 'upcoming_stock'))->nullable();
			$table->enum('load_status', array('ready_for_collection', 'unplanned', 'planned'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
