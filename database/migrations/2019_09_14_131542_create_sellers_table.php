<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('sellers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->default(0);
            $table->string('username')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
           	$table->text('seller2_contact')->nullable();
			$table->text('transport_contact')->nullable();
			$table->text('accounts_contact')->nullable();
			$table->text('company')->nullable();
			$table->string('vat')->nullable();
			$table->string('city')->nullable();
			$table->string('postalcode')->nullable();
			$table->string('address')->nullable();
			$table->string('country')->nullable();
			$table->string('note')->nullable();
			$table->integer('truck_loads_day')->nullable();
			$table->integer('truck_loads_week')->nullable();
			$table->integer('	truck_loads_total')->nullable();
			$table->integer('available_stocks')->nullable();
			$table->enum('invite_sent', array('0', '1'))->default('0');
			$table->enum('verified', array('0', '1'))->default('0');
			$table->text('location')->nullable();
            $table->enum('status', array('0', '1'))->default('1');
			$table->timestamps();
			$table->integer('trust_level')->nullable();
			$table->integer('contact_email')->nullable();
			$table->integer('contact_sms')->nullable();
			$table->integer('contact_whatsapp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
