<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('buyer_id')->unsigned()->index();
            $table->bigInteger('seller_id')->unsigned()->index();
            $table->bigInteger('stock_id')->unsigned()->index();
            $table->float('price')->nullable();
            $table->date('delivery_date')->nullable();
            $table->timestamps();
			$table->integer('sale_id')->nullable();
			$table->enum('status', array('ordered', 'confirmed'))->default('ordered');
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
