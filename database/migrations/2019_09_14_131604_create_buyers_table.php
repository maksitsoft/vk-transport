<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('company')->nullable();
			$table->string('vat')->nullable();
            $table->text('delivery_address')->nullable();
			$table->string('city')->nullable();
			$table->string('postalcode')->nullable();
			$table->string('address')->nullable();
			$table->string('country')->nullable();
			$table->enum('delivery_same', array('0', '1'))->default('1');
			$table->enum('product_prefs', array('0', '1'))->default('0');
            $table->float('credit_limit')->nullable();
            $table->string('dry_matter_content')->nullable();
            $table->string('transportation')->nullable();
            $table->string('price_prefs')->nullable();
            $table->text('size_range')->nullable();
            $table->string('soil')->nullable();
            $table->string('total_prefs')->nullable();
            $table->text('note')->nullable();
            $table->text('buyer2_contact')->nullable();
            $table->text('transport_contact')->nullable();
            $table->text('accounts_contact')->nullable();
			 $table->enum('status', array('0', '1'))->default('1');
            $table->text('defects')->nullable();
            $table->timestamps();
			 $table->string('truck_quantity')->nullable();
			 $table->integer('trust_level')->nullable();
			 $table->integer('user_id')->nullable();
			 $table->integer('all_varieties')->default(0);
			 $table->string('extra_transport_cost_per_ton')->nullable();
			 $table->integer('contact_email')->default(0);
			 $table->integer('contact_sms')->default(0);
			 $table->integer('contact_whatsapp')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyers');
    }
}
