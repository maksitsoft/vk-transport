<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_heads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->text('desc', 191)->nullable();
            $table->string('type', 191)->nullable();
            $table->enum('is_active', ['0', '1'])->default('1');
            $table->timestamps();
			$table->integer('parent_ref')->nullable();
			$table->string('unique_hash')->unique()->default("-");
			 $table->float('extra_supply_cost', 8, 2)->nullable()->default(null);
			 $table->float('extra_cost_to_buyer_factor', 8, 2)->nullable()->default(null);
			 $table->integer('volume')->nullable();
			 $table->string('product', 191)->default('Potato');
			 $table->integer('product_id')->nullable()->default(364);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('app_heads');
    }
}
