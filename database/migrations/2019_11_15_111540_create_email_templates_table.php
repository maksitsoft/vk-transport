<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('subject')->nullable();
            $table->text('email_content')->nullable();
            $table->text('email_content_de')->nullable();
            $table->text('email_content_pl')->nullable();
            $table->text('sms_content')->nullable();
            $table->text('sms_content_de')->nullable();
            $table->text('sms_content_pl')->nullable();
            $table->text('whatsapp_content')->nullable();
            $table->integer('sent')->default(0);
            $table->enum('status', array('1', '0'))->default('1');
            $table->timestamps();
			$table->string('shortcodes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_templates');
    }
}
