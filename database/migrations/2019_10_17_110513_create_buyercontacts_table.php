<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyercontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyercontacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->integer('product_id')->nullable();
            $table->timestamps();
			 $table->string('prefered_method')->nullable();
			 $table->text('notes')->nullable();
			 $table->timestamp('email_verified_at')->nullable();
			 $table->string('email_verification_code')->nullable();
			 $table->string('company')->nullable();
			 $table->string('referral')->nullable();
			 $table->string('product_sub_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyercontacts');
    }
}
