<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_specifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id');
            $table->string('display_name');
            $table->integer('importance')->nullable();
            $table->integer('order')->nullable();
			$table->enum('required', array('Yes', 'No'))->default('Yes');
			$table->timestamps();
			$table->integer('parent_id')->nullable();
            $table->enum('buyer_hasmany', array('Yes', 'No'))->default('No');
            $table->enum('stock_hasmany', array('Yes', 'No'))->default('No');
            $table->bigInteger('reference_id')->nullable();
            $table->string('field_type')->nullable();
			$table->enum('buyer_pref_anylogic', array('Yes', 'No'))->default('No');
            $table->enum('display_in_transport', array('Yes', 'No'))->default('No');
            $table->string('can_edit')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_specifications');
    }
}
