<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
			 $table->float('price')->nullable();
			  $table->timestamps();
             $table->integer('buyer_id')->default(0);
			$table->integer('match_id')->default(0);
			$table->integer('stock_id')->default(0);
			$table->integer('quantity')->default(0);
			$table->integer('payment_term')->nullable();
			$table->integer('payment_type')->nullable();
			$table->integer('payment_currency')->nullable();
			$table->enum('payment_status', ['Paid', 'Unpaid'])->default('Unpaid');
           $table->string('defect_percentage')->nullable();
            $table->enum('status', array('ordered', 'confirmed',))->default('ordered');
			$table->integer('acceptedPct')->nullable();
			$table->integer('rejectedPct')->nullable();
			$table->integer('paidPct')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
