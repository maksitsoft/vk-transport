<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('variety')->nullable();
            $table->string('size_from')->nullable();
            $table->string('size_to')->nullable();
            $table->string('packing')->nullable();
            $table->string('quantity')->nullable();
            $table->string('flesh_color')->nullable();
            $table->string('location')->nullable();
            $table->float('price')->nullable();
            $table->enum('status', array('0', '1'))->default('1');
            $table->timestamps();
			$table->string('image')->nullable();
			$table->string('homepage_image')->nullable();
			$table->enum('type', array('Product', 'Service'))->default('Product');
			$table->string('name_pl')->nullable();
			$table->string('name_de')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
